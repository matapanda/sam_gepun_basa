<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Activate extends CI_Controller {

	public function __construct(){
		parent::__construct();

	}

    public function index()
    {
        $code   = $this->input->get('code');
        $email  = $this->input->get('email_user');

        if (strlen($code)==50 && !empty($email))
        {
            
            $this->db->select('kode_aktifasi');
            $this->db->from('user');
            $this->db->where('email_user', $email);

            $kode_aktifasi = $this->db->query('select kode_aktifasi from user where kode_aktifasi = "'.$code.'"')->row();

            // echo "<pre>";
            // print_r($kode_aktifasi);
            // echo "</pre>";
            // die();

            if ($kode_aktifasi) {
                if ($kode_aktifasi->kode_aktifasi === $code)
                {
                    $data = array(
                        'kode_aktifasi'         => NULL,
                        'aktif'                 => "1",
                  
                    );
                    $this->db->where('email_user', $email);
                    $this->db->update('user', $data);
    
                    // $this->session->set_flashdata('info', '<script>swal({title: "Success", text: "Your account has been activated", timer: 10000, icon: "success", button: false});</script>');
    
                    $this->session->set_userdata('flash_welcome','Sukses');
    
                    // echo "<script>alert('Sukses, Silahkan Login')</script>";
                    redirect(base_url('user/login'), 'refresh');
    
                    return TRUE;
                } else{
                    return FALSE;
                }       
            }else{
                $this->session->set_userdata('flash_welcome','Sudah');
                redirect(base_url('user/login'), 'refresh');
            }
            

            
        
           
        }


        else if(empty($code) && empty($email))
        {
            show_404();
        }
        else
        {
            $this->session->set_flashdata('info', '<script>swal({title: "Error", text: "Your activation code link is incorrect", timer: 10000, icon: "error", button: false});</script>');
            redirect(base_url(), 'refresh');
        }
    }




}
