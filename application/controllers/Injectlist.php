<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Injectlist extends CI_Controller {
   
	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");


        // $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function check_list_data(){
		$data = [
            "data_diri"=>count($this->mm->get_data_all_where("user_data", [])),
            "ketenagakerjaan"=>count($this->mm->get_data_all_where("user_kerja", [])),
            "kesehatan"=>count($this->mm->get_data_all_where("user_kesehatan", [])),
            "keuangan"=>count($this->mm->get_data_all_where("user_keuangan", [])),
            "pendidikan"=>count($this->mm->get_data_all_where("user_pendidikan", [])),
            "kegiatan_organisasi"=>count($this->mm->get_data_all_where("user_or", [])),
            "sosial_ekonomi"=>count($this->mm->get_data_all_where("user_sosial", [])),
            "transportasi"=>count($this->mm->get_data_all_where("user_transport", [])),
            "lingkungan"=>count($this->mm->get_data_all_where("user_tmpt", []))
        ];

        return $data;
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
}
