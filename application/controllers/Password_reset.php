<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Password_reset extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('model_password');
		$this->load->helper('string');
		$this->load->library("response_message");
		$this->load->library("magic_pattern");
	}

	public function index()
	{

		$this->form_validation->set_rules('email_user', 'email', 'trim|required|valid_email');

		if ($this->form_validation->run() == TRUE)
		{
			$email 			= $this->input->post('email_user');
			$kode_aktifasi 	= random_string('alnum', 50);

			if ($this->model_password->cek($email, $kode_aktifasi))
			{
				$this->_email_reset($email, $kode_aktifasi);

				$this->session->set_flashdata('item','Berhasil, Silhakan cek email anda'); 
				redirect(base_url('user/login'),'refresh');

			}
			else
			{
				$this->session->set_flashdata('info', '<script>swal({title: "Error", text: "No account with email you entered", timer: 10000, icon: "error", button: false});</script>');
				redirect(base_url('password_reset'),'refresh');
			}
		}
		else
		{
			$data = array('title' => 'Password Reset');
	
			$this->load->view('user/reset_password');
		}
	}

	private function val_form_insert_register(){
        $config_val_input = array(
                array(
                    'field'=>'password_user',
                    'label'=>'password_user',
                    'rules'=>'required|min_length[5]|max_length[20]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length'=>"%s Minimal 5 karakter",
                        'max_length'=>"%s Maximal 20 karakter"
                    )      
                ),
                array(
                    'field'=>'repassword',
                    'label'=>'repassword',
                    'rules'=>'required|matches[password_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'matches'=>"%s ".$this->response_message->get_error_msg("RE_PASSWORD_FAIL")
                    )      
                ),
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }


	public function reset()
	{
		$code 	= $this->input->get('code');
		$email 	= $this->input->get('email_user');

		$this->form_validation->set_rules('password_user', 'password', 'trim|required|min_length[5]');
		$this->form_validation->set_rules('password_confirm', 'confirm password', 'trim|required|matches[password_user]');
		
		if ($this->form_validation->run() == TRUE)
		{
			$password = $this->input->post('password_user');
			// $hash =  hash("sha256", $password);

			// print_r($hash);
			// die();
			// $this->db->query('UPDATE user SET kode_aktifasi=null, password_user="'.$hash.'" WHERE email_user="'.$email.'" ');		

			$this->model_password->renew($email, $password);
			$this->_email_sukses($email);

			$this->session->set_flashdata('renew','Password baru anda sudah berhasil dirubah <br> Silahkan Login..'); 
			redirect(base_url('user/login'), 'refresh');
			
	       	
		}
		else
		{
			
			if (strlen($code)==50 && !empty($email))
			{
				if ($this->model_password->reset($code, $email))
				{
					$data = array('title' => 'Password Renew');

					$this->load->view('user/renew_password');
			
				} else {
					$this->session->set_flashdata('info', '<script>swal({title: "Success", text: "Your password reset link is incorrect", timer: 10000, icon: "success", button: false});</script>');
					redirect(base_url('password_reset'), 'refresh');
				}
			}
			else if(empty($code) && empty($email))
			{
				show_404();
			}
			else
			{
				$this->session->set_flashdata('info', '<script>swal({title: "Success", text: "Your password reset link is incorrect", timer: 10000, icon: "success", button: false});</script>');
				redirect(base_url('password_reset'), 'refresh');
			}
		}
	}

	function _email_reset($email, $kode_aktifasi)
	{
		 $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'meilinda41@gmail.com', // change it to yours
          'smtp_pass' => 'Leona311', // change it to yours
          'mailtype' => 'html',
          'charset' => 'iso-8859-1',
          'wordwrap' => TRUE
        );

		 	$html = '
		<div style="margin-bottom: 20px; font-weight: bold;">Hello '.$email.',</div>

		<div>Someone has requested a password reset to our sistem.</div>
		<div style="margin-bottom: 10px;">To reset your password, please click this link and we will redirect you to reset password form: <a href="'.base_url('password_reset/reset?code='.$kode_aktifasi.'&email_user='.$email).'">Reset my password</a>.
		</div>
		<div>If that link cannot working you can access this URL from your browser</div>
		<div><a href="'.base_url('password_reset/reset?code='.$kode_aktifasi.'&email_user='.$email).'">'.base_url('password_reset/reset?code='.$kode_aktifasi.'&email_user='.$email).'</a></div>

		<div style="margin-bottom: 20px; margin-top: 10px;">If you have a question reply this email and we will contact you as soon as possible, thank you!</div>
		';

		 	
              $this->load->library('email', $config);
              $this->email->set_newline("\r\n");
              $this->email->from('meilinda41@gmail.com'); // change it to yours
              $this->email->to($email);// change it to yours
              $this->email->subject('Reset Password');
              $this->email->message($html);
              if($this->email->send())
             {
              
             }
             else
            {
             show_error($this->email->print_debugger());
            }

	}

	function _email_sukses($email)
	{
		$config = [
			'mailtype'  	=> 'html',
			'charset'   	=> 'utf-8',
			'protocol'  	=> 'smtp',
			'smtp_host' 	=> 'smtp.gmail.com',
			'smtp_user' 	=> '', // gmail address
			'smtp_pass'   	=> '', // gmail password
			'smtp_crypto' 	=> 'ssl',
			'smtp_port'   	=> 465,
			'crlf'    		=> "\r\n",
			'newline' 		=> "\r\n"
		];

		$this->load->library('email', $config);
		$this->email->from('gmail@gmail.com', 'Your Name');

		$this->email->to($email);
		$this->email->subject('[Password Reset Success]');

		$html = '
		<div style="margin-bottom: 20px; font-weight: bold;">Hello '.$email.',</div>

		<div>Your password has been reset at '.date('Y/m/d h:i:s a').', please keep your account secure</div>

		<div style="margin-bottom: 20px; margin-top: 10px;">If you have a question reply this email and we will contact you as soon as possible, thank you!</div>
		';

		$this->email->message($html);
		return $this->email->send();
	}
}

/* End of file password_reset.php */
/* Location: ./application/controllers/password_reset.php */