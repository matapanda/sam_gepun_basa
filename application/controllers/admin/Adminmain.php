<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminmain extends CI_Controller {
    public $rules = "ADMIN_MANAGEMENT";
    public $title = "Admin Management";

    public $tbl_admin = "admin";
    public $tbl_admin_tipe = "admin_tipe";

    public $id_ses;

    public $date_now;


	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();

        $this->id_ses = $this->session->userdata("aduhduh_ketauan")["id_admin"];

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "admin_main";

        $data["list_tipe_admin"] = $this->mm->get_data_all_where($this->tbl_admin_tipe, array("is_del"=>"0"));
		// $data["list_data"] = $this->mm->get_data_all_where("admin", array("is_del"=>"0"));
		$this->load->view('index', $data);
	}

    public function list_data(){

        $draw = @$_POST['draw'];
        $start = @$_POST['start'];
        $length = @$_POST['length'];
        $search = @$_POST['search'];
            $search_val = $search['value'];

        $start_page = $start;
        
        if($search_val){
            $data["data"] = $this->ot->get_admin_like(["nama_admin", "username", "email", "id_admin"], $search_val, $start_page, $length);
        }else{
            $data["data"] = $this->ot->get_admin_like_all($start_page, $length);
        }

        $data["draw"] = $draw;
        $data["recordsTotal"] = $this->ot->admin_count()["count_data"];
        $data["recordsFiltered"] = $this->ot->admin_count()["count_data"];


        echo json_encode($data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================
	public function val_form_insert_admin(){
        $config_val_input = array(
                array(
                    'field'=>'id_tipe_admin',
                    'label'=>'Tipe Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails|is_unique[admin.email]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'Username',
                    'rules'=>'required|is_unique[admin.username]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'tlp',
                    'label'=>'No. Tlp Admin',
                    'rules'=>'required|is_unique[admin.tlp]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'nama_admin',
                    'label'=>'Nama Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                    "email"=>"",
                    "username"=>"",
                    "tlp"=>"",
                    "password"=>"",
                    "repassword"=>"",
                    "nama_admin"=>""
                );

        // print_r($_POST);
        // die();
        if($this->val_form_insert_admin()){
            $id_admin_tipe = $this->input->post("id_admin_tipe");
            $email = $this->input->post("email");
            $username = $this->input->post("username");
            $tlp = $this->input->post("tlp");
            $password = $this->input->post("password");
            $repassword = $this->input->post("repassword");
            $nama_admin = $this->input->post("nama_admin");

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $username]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $data = [
                    "id_admin"=>"",
                    "id_admin_tipe"=>$id_admin_tipe,
                    "email"=>$email,
                    "username"=>$username,
                    "tlp"=>$tlp,
                    "password"=>$password,
                    "nama_admin"=>$nama_admin,
                    "status_active"=>"0",
                    "is_del"=>"1",
                    "date_crt"=>$this->date_now,
                    "adm_crt"=>$this->id_ses,
                    "date_up"=>$this->date_now,
                    "adm_up"=>$this->id_ses
                ];

                if ($password == $repassword) {
                    $insert = $this->mm->insert_data($this->tbl_admin, $data);

                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_PASSWORD_FAIL"));
                    
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));
            $msg_detail["email"]= strip_tags(form_error('email'));
            $msg_detail["username"]= strip_tags(form_error('username'));
            $msg_detail["tlp"]= strip_tags(form_error('tlp'));
            $msg_detail["password"]= strip_tags(form_error('password'));
            $msg_detail["nama_admin"]= strip_tags(form_error('nama_admin'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_admin"])){
        	$id_admin = $this->input->post('id_admin');
        	$data = $this->mm->get_data_each("admin", array("id_admin"=>$id_admin, "is_del"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update_admin(){
        $config_val_input = array(
                array(
                    'field'=>'id_admin',
                    'label'=>'ID Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'id_tipe_admin',
                    'label'=>'Tipe Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'email',
                    'label'=>'Email',
                    'rules'=>'required|valid_emails',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_emails'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )  
                ),array(
                    'field'=>'username',
                    'label'=>'Username',
                    'rules'=>'required|',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'tlp',
                    'label'=>'No. Tlp Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_admin',
                    'label'=>'Nama Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                    "id_admin_tipe"=>"",
                    "email"=>"",
                    "username"=>"",
                    "tlp"=>"",
                    "nama_admin"=>""
                );

        if($this->val_form_update_admin()){
        	$id_admin 		= $this->input->post("id_admin", true);

            $id_admin_tipe = $this->input->post("id_admin_tipe");
            $email = $this->input->post("email");
            $username = $this->input->post("username");
            $tlp = $this->input->post("tlp");
            $nama_admin = $this->input->post("nama_admin");


          	// check username
          	if(!$this->mm->get_data_each("admin", array("username"=>$username, "id_admin!="=>$id_admin))){

                $type_pattern   = "allowed_general_char";

                $arr_pattern  = [[$type_pattern, $username]];


                if($this->magic_pattern->set_list_pattern($arr_pattern )){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                } else{
                    $set = array(
                        "id_admin_tipe"=>$id_admin_tipe,
                        "email"=>$email,
                        "username"=>$username,
                        "tlp"=>$tlp,
                        "nama_admin"=>$nama_admin,
                        "date_up"=>$this->date_now,
                        "adm_up"=>$this->id_ses
                    );

                    $where = array("id_admin"=>$id_admin);

                    $update = $this->mm->update_data($this->tbl_admin, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }   
                }
          	}else{
          		$msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));
            
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));
            $msg_detail["email"]= strip_tags(form_error('email'));
            $msg_detail["username"]= strip_tags(form_error('username'));
            $msg_detail["tlp"]= strip_tags(form_error('tlp'));     
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where = array("id_admin"=>$id_admin);

            $set = array("is_del"=>"1");
            $where = array("id_admin"=>$id_admin);

        	// $delete_admin = $this->mm->delete_data("admin", array("id_admin"=>$id_admin));
        	$delete_admin = $this->mm->update_data("admin", $set, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    public function val_form_ch_pass_admin(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi Password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass_admin()){
        	$id_admin 		= $this->input->post("id_admin");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");

          	// check username
          	if($password == $repassword){
          		$set = array(
          				"password"=>hash("sha256", $password)
          			);

          		$where = array("id_admin"=>$id_admin);

          		$update = $this->mm->update_data("admin", $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["username"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));         
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================


#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

    public function disabled_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where 	= array("id_admin"=>$id_admin);
        	$set 	= array("status_active"=>"0");
        	
        	$update_admin = $this->mm->update_data("admin", $set, $where);
        	if($update_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================
    public function activate_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin"=>"",
                );

        if($_POST["id_admin"]){
        	$id_admin = $this->input->post("id_admin");
        	$where 	= array("id_admin"=>$id_admin);
        	$set 	= array("status_active"=>"1");
        	
        	$update_admin = $this->mm->update_data("admin", $set, $where);
        	if($update_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin"]= strip_tags(form_error('id_admin'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================


}
