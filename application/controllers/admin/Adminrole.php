<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Adminrole extends CI_Controller {
    public $rules = "ADMIN_ROLE";
    public $title = "Admin Management Role";

    public $tbl_main = "admin_role";
    // public $tbl_main_kd_role = "admin_kd_role";

    public $id_ses;

    public $date_now;


	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();

        // $this->id_ses = $this->session->userdata("aduhduh_ketauan")["id_role"];
        $this->id_ses = "1";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["title"] = $this->title;
        $data["page"] = "admin/admin_role";

		$this->load->view('index', $data);
	}

    public function list_data(){

        // print_r($_POST);

        $draw = @$_POST['draw'];
        $start = @$_POST['start'];
        $length = @$_POST['length'];
        $search = @$_POST['search'];
            $search_val = $search['value'];

        $start_page = $start;
        
        $count = 0;
        if($search_val){
            $data["data"] = $this->ot->get_like_all_admin_role(["id_role", "kd_role", "ket", "role"], $search_val, $start_page, $length);

            $count = $this->ot->get_like_all_admin_role_count(["id_role", "kd_role", "ket", "role"], $search_val);
        }else{
            $data["data"] = $this->ot->get_all_admin_role($start_page, $length);
            $count = $this->ot->get_all_admin_role_count();
        }

        $data["draw"] = $draw;
        $data["recordsTotal"] = $count["count_data"];
        $data["recordsFiltered"] = $count["count_data"];


        // print_r($data);

        echo json_encode($data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================
	public function val_form_insert_data(){
        $config_val_input = array(
                array(
                    'field'=>'kd_role',
                    'label'=>'Kode Role',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'ket',
                    'label'=>'Keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'role',
                    'label'=>'Role',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "kd_role"=>"",
                    "ket"=>"",
                    "role"=>""
                );

        // print_r($_POST);
        // die();
        if($this->val_form_insert_data()){
            $kd_role = $this->input->post("kd_role", true);
            $ket = $this->input->post("ket", true);
            $role = $this->input->post("role", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $kd_role]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $data = [
                    "id_role"=>"",
                    "kd_role"=>$kd_role,
                    "ket"=>$ket,
                    "role"=>$role,
                    "sts_act"=>"1",
                    "is_delete"=>"0",
                    "date_crt"=>$this->date_now,
                    "adm_crt"=>$this->id_ses,
                    "date_up"=>$this->date_now,
                    "adm_up"=>$this->id_ses
                ];

                $insert = $this->mm->insert_data($this->tbl_main, $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }

            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["kd_role"]= strip_tags(form_error('kd_role'));
            $msg_detail["ket"]= strip_tags(form_error('ket'));
            $msg_detail["role"]= strip_tags(form_error('role'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_role"])){
        	$id_role = $this->input->post('id_role');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_role"=>$id_role, "is_delete"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================

    public function val_form_update_data(){
        $config_val_input = array(
            array(
                'field'=>'kd_role',
                'label'=>'kd_role Admin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'ket',
                'label'=>'Keterangan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'role',
                'label'=>'role Page',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                ) 
            )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_role"=>"",
                    "kd_role"=>"",
                    "ket"=>"",
                    "role"=>""
                );

        if($this->val_form_update_data()){
        	$id_role 		= $this->input->post("id_role", true);

            $kd_role = $this->input->post("kd_role", true);
            $ket = $this->input->post("ket", true);
            $role = $this->input->post("role", true);

          
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $kd_role]];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $set = array(
                    "kd_role"=>$kd_role,
                    "ket"=>$ket,
                    "role"=>$role
                );

                $where = array("id_role"=>$id_role);

                $update = $this->mm->update_data($this->tbl_main, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }   
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["id_role"]= strip_tags(form_error('id_role'));
            $msg_detail["kd_role"]= strip_tags(form_error('kd_role'));
            $msg_detail["ket"]= strip_tags(form_error('ket'));
            $msg_detail["role"]= strip_tags(form_error('role')); 
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================

    public function delete_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_role"=>"",
                );

        if($_POST["id_role"]){
        	$id_role = $this->input->post("id_role");

            $set = array("is_delete"=>"1");
            $where = array("id_role"=>$id_role);

        	$delete_data = $this->mm->update_data($this->tbl_main, $set, $where);
            
            if($delete_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_role"]= strip_tags(form_error('id_role'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

    public function disable_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_role"=>"",
                );

        if($_POST["id_role"]){
        	$id_role = $this->input->post("id_role");
        	$where 	= array("id_role"=>$id_role);
        	$set 	= array("sts_act"=>"0");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_role"]= strip_tags(form_error('id_role'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================
    public function activate_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_role"=>"",
                );

        if($_POST["id_role"]){
        	$id_role = $this->input->post("id_role");
        	$where 	= array("id_role"=>$id_role);
        	$set 	= array("sts_act"=>"1");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_role"]= strip_tags(form_error('id_role'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================


}
