<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admintipe extends CI_Controller {
    public $rules = "ADMIN_TIPE";
    public $title = "Admin Management";

    public $tbl_main = "admin_tipe";
    public $tbl_admin_role = "admin_role";
    public $tbl_admin_role_tipe = "admin_role_tipe";
    // public $tbl_main_tipe = "admin_tipe";

    public $id_ses;

    public $date_now;


	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");
        
        // $this->auth_v0->check_session_active_ad();

        // $this->id_ses = $this->session->userdata("aduhduh_ketauan")["id_admin_tipe"];
        $this->id_ses = "1";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["title"] = $this->title;
        $data["page"] = "admin/admin_tipe";

        $data["list_admin_role"] = $this->mm->get_data_all_where($this->tbl_admin_role, ["sts_act"=>"1","is_del"=>"0"]);

		$this->load->view('index', $data);
	}

    public function list_data(){

        // print_r($_POST);

        $draw = @$_POST['draw'];
        $start = @$_POST['start'];
        $length = @$_POST['length'];
        $search = @$_POST['search'];
            $search_val = $search['value'];

        $start_page = $start;
        
        $count = 0;
        if($search_val){
            $data["data"] = $this->ot->get_like_all_admin_tipe(["tipe", "ket", "redirect"], $search_val, $start_page, $length);

            $count = $this->ot->get_like_all_admin_count(["tipe", "ket", "redirect"], $search_val);
        }else{
            $data["data"] = $this->ot->get_all_admin_tipe($start_page, $length);
            $count = $this->ot->get_all_admin_tipe_count();
        }

        $data["draw"] = $draw;
        $data["recordsTotal"] = $count["count_data"];
        $data["recordsFiltered"] = $count["count_data"];


        // print_r($data);

        echo json_encode($data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================
	public function val_form_insert_data(){
        $config_val_input = array(
                array(
                    'field'=>'tipe',
                    'label'=>'Tipe Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'ket',
                    'label'=>'Keterangan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'redirect',
                    'label'=>'Redirect Page',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "tipe"=>"",
                    "ket"=>"",
                    "redirect"=>""
                );

        // print_r($_POST);
        // die();
        if($this->val_form_insert_data()){
            $tipe = $this->input->post("tipe");
            $ket = $this->input->post("ket");
            $redirect = $this->input->post("redirect");
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $tipe]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $data = [
                    "id_admin_tipe"=>"",
                    "tipe"=>$tipe,
                    "ket"=>$ket,
                    "redirect"=>$redirect,
                    "sts_act"=>"1",
                    "is_del"=>"0",
                    "date_crt"=>$this->date_now,
                    "adm_crt"=>$this->id_ses,
                    "date_up"=>$this->date_now,
                    "adm_up"=>$this->id_ses
                ];

                $insert = $this->mm->insert_data($this->tbl_main, $data);

                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }

            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["tipe"]= strip_tags(form_error('tipe'));
            $msg_detail["ket"]= strip_tags(form_error('ket'));
            $msg_detail["redirect"]= strip_tags(form_error('redirect'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_admin_tipe"])){
        	$id_admin_tipe = $this->input->post('id_admin_tipe');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_admin_tipe"=>$id_admin_tipe, "is_del"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================

    public function val_form_update_data(){
        $config_val_input = array(
            array(
                'field'=>'tipe',
                'label'=>'Tipe Admin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'ket',
                'label'=>'Keterangan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'redirect',
                'label'=>'Redirect Page',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                ) 
            )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                    "tipe"=>"",
                    "ket"=>"",
                    "redirect"=>""
                );

        if($this->val_form_update_data()){
        	$id_admin_tipe 		= $this->input->post("id_admin_tipe", true);

            $tipe = $this->input->post("tipe");
            $ket = $this->input->post("ket");
            $redirect = $this->input->post("redirect");

          
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $tipe]];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $set = array(
                    "tipe"=>$tipe,
                    "ket"=>$ket,
                    "redirect"=>$redirect
                );

                $where = array("id_admin_tipe"=>$id_admin_tipe);

                $update = $this->mm->update_data($this->tbl_main, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }   
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));
            $msg_detail["tipe"]= strip_tags(form_error('tipe'));
            $msg_detail["ket"]= strip_tags(form_error('ket'));
            $msg_detail["redirect"]= strip_tags(form_error('redirect')); 
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================

    public function delete_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                );

        if($_POST["id_admin_tipe"]){
        	$id_admin_tipe = $this->input->post("id_admin_tipe");

            $set = array("is_del"=>"1");
            $where = array("id_admin_tipe"=>$id_admin_tipe);

        	$delete_data = $this->mm->update_data($this->tbl_main, $set, $where);
            
            if($delete_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

    public function disable_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                );

        if($_POST["id_admin_tipe"]){
        	$id_admin_tipe = $this->input->post("id_admin_tipe");
        	$where 	= array("id_admin_tipe"=>$id_admin_tipe);
        	$set 	= array("sts_act"=>"0");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================
    public function activate_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                );

        if($_POST["id_admin_tipe"]){
        	$id_admin_tipe = $this->input->post("id_admin_tipe");
        	$where 	= array("id_admin_tipe"=>$id_admin_tipe);
        	$set 	= array("sts_act"=>"1");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------add_role------------------------------------
#===============================================================================
   
    public function val_form_add_role(){
        $config_val_input = array(
            array(
                'field'=>'id_admin_tipe',
                'label'=>'Id Tipe',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'list_role',
                'label'=>'Role',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            )
        );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function add_role(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_admin_tipe"=>"",
                    "list_role"=>""
                );

        if($this->val_form_add_role()){
        	$id_admin_tipe  = $this->input->post("id_admin_tipe", true);
            $list_role      = $this->input->post("list_role", true);
          
            $type_pattern   = "allowed_general_char";
            $arr_pattern  = [[$type_pattern, $id_admin_tipe]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                try {
                    $delete_role = $this->mm->delete_data($this->tbl_admin_role_tipe, ["id_admin_tipe"=>$id_admin_tipe]);
                    if($delete_role){

                        $list_role = json_decode($list_role);
                        if($list_role){
                            $data = [];
                            foreach ($list_role as $value) {
                                // print_r($value);
                                $tmp = [
                                    "id_admin_role_tipe" => "",
                                    "id_role" => $value,
                                    "id_admin_tipe" => $id_admin_tipe,
                                    "date_up" => $this->date_now,
                                    "adm_up" => $this->id_ses
                                ];

                                array_push($data, $tmp);
                            }

                            $insert = $this->mm->insert_batch_data($this->tbl_admin_role_tipe, $data);
                            if($insert){
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                            }
                        }
                    }
                    
                } catch (\Throwable $th) {
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["id_admin_tipe"]= strip_tags(form_error('id_admin_tipe'));
            $msg_detail["list_role"]= strip_tags(form_error('list_role'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------add_role------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------add_role------------------------------------
#===============================================================================
   
    function get_role_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_admin_tipe"])){
        	$id_admin_tipe = $this->input->post('id_admin_tipe');
        	$data = $this->mm->get_data_all_where($this->tbl_admin_role_tipe, array("id_admin_tipe"=>$id_admin_tipe));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

#===============================================================================
#-----------------------------------add_role------------------------------------
#===============================================================================


}
