<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Listjenis extends CI_Controller {
    public $title = "Data Master Semua Jenis";
    public $tbl_main = "ms_jenis_all";

    public $id_admin_ses;

    public $date_now;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("magic_pattern");
        $this->load->library("Uploadfilev0");
        
        
        // $this->auth_v0->check_session_active_ad();
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index($id_parent = 0){
		$data["page"] = "ms_jenis_all";
        $data["title"] = $this->title;
		$data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, ["ms_jenis_all_id"=>$id_parent, "tipe!="=>"0"]);

        $data["patern"] = [];

        $data_perent = $this->mm->get_data_each($this->tbl_main, ["id_ms_jenis_all"=>$id_parent]);
        $data["parent"] = $data_perent;

        if($id_parent != 0){
            $pattern = $data_perent["pattern"];
                                   
            $arr_parent = json_decode($pattern);
            // print_r($arr_parent);
            // die();

            foreach ($arr_parent as $key => $value) {
                $tmp_val = $this->mm->get_data_each($this->tbl_main, ["id_ms_jenis_all"=>$value]);
                $data["patern"][$key] = $tmp_val;   
            }
        }
        
        // print_r("<pre>");
        // print_r($data);
        // die();
		$this->load->view('index', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================

    public function val_form_insert(){
        $config_val_input = [
                [
                    'field'=>'ms_jenis_all_id',
                    'label'=>'ms_jenis_all_id',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]  
                ],[
                    'field'=>'nm_jenis',
                    'label'=>'nm_jenis',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ]  
                ],[
                    'field'=>'kode_jenis',
                    'label'=>'kode_jenis',
                    'rules'=>'required',
                    'errors'=>[
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ] 
                ]
            ];
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "ms_jenis_all_id"=>"",
                    "nm_jenis"=>"",
                    "kode_jenis"=>"",
                    "desc"=>""
                );

        // print_r("<pre>");
        // print_r($_POST);
        // die();

        if($this->val_form_insert()){
            $ms_jenis_all_id 	= $this->input->post("ms_jenis_all_id", true);
            $nm_jenis 		    = $this->input->post("nm_jenis", true);
            $kode_jenis 		= $this->input->post("kode_jenis", true);
            $desc 	            = $this->input->post("desc", true);

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $nm_jenis]];

            if($this->magic_pattern->set_list_pattern($arr_pattern)){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                
                $data_parent = $this->mm->get_data_each($this->tbl_main, array("id_ms_jenis_all"=>$ms_jenis_all_id));
                
                $dt_pattern_parent = "[]";
                if(isset($data_parent["pattern"])){
                    $dt_pattern_parent = $data_parent["pattern"];
                }
                $pattern_parent = json_decode($dt_pattern_parent);

                // if($ms_jenis_all_id){    
                    array_push($pattern_parent, $ms_jenis_all_id);
                // }

                // print_r("<pre>");
                // print_r($pattern_parent);
                // die();

                $data = [
                    "id_ms_jenis_all"=>"",
                    "ms_jenis_all_id"=>$ms_jenis_all_id,
                    "pattern"=>json_encode($pattern_parent),
                    "tipe"=>count($pattern_parent),
                    "kode_jenis"=>$kode_jenis,
                    "nm_jenis"=>$nm_jenis,
                    "desc"=>$desc,
                    "sts_act"=>"1"
                ];
                
                $insert = $this->mm->insert_data($this->tbl_main, $data);
                if($insert){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["ms_jenis_all_id"] 		= strip_tags(form_error('ms_jenis_all_id'));
            $msg_detail["nm_jenis"] 	= strip_tags(form_error('nm_jenis'));
            $msg_detail["kode_jenis"] 	= strip_tags(form_error('kode_jenis'));
            $msg_detail["desc"] 	= strip_tags(form_error('desc'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
    
#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_ms_jenis_all"])){
        	$id_ms_jenis_all = $this->input->post('id_ms_jenis_all');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_ms_jenis_all"=>$id_ms_jenis_all));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================

    public function update_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
            "nm_jenis"=>"",
            "kode_jenis"=>"",
            "desc"=>"",
        );

        if(isset($_POST["id_ms_jenis_all"])){
            if($this->val_form_insert()){
                $id_ms_jenis_all 			= $this->input->post("id_ms_jenis_all", true);
                
                $nm_jenis 		= $this->input->post("nm_jenis", true);
                $kode_jenis 		= $this->input->post("kode_jenis", true);
                $desc 	    = $this->input->post("desc", true);
                
                $type_pattern   = "allowed_general_char";

                $arr_pattern  = [[$type_pattern, $nm_jenis]];
                // check point_mcctv_main

                if(isset($_POST["id_ms_jenis_all"])){
                    $id_ms_jenis_all = $this->input->post("id_ms_jenis_all");
                    if($this->magic_pattern->set_list_pattern($arr_pattern )){
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                    } else{
                        $set = ["nm_jenis"=>$nm_jenis,
                                "kode_jenis"=>$kode_jenis,
                                "desc"=>$desc];

                        $where = array("id_ms_jenis_all"=>$id_ms_jenis_all);

                        $update = $this->mm->update_data($this->tbl_main, $set, $where);
                        if($update){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                        } 
                        
                    }
                }
                    
            }else{
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                
                $msg_detail["ms_jenis_all_id"] 		= strip_tags(form_error('ms_jenis_all_id'));
                $msg_detail["nm_jenis"] 	= strip_tags(form_error('nm_jenis'));
                $msg_detail["kode_jenis"] 	= strip_tags(form_error('kode_jenis'));
                $msg_detail["desc"] 	= strip_tags(form_error('desc'));
            }
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================

    public function delete_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_ms_jenis_all"=>"",
                );

        if($_POST["id_ms_jenis_all"]){
        	$id_ms_jenis_all = $this->input->post("id_ms_jenis_all");
        	$where = array("id_ms_jenis_all"=>$id_ms_jenis_all);

        	$delete_data = $this->mm->delete_data($this->tbl_main, $where);
            
            if($delete_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_ms_jenis_all"]= strip_tags(form_error('id_ms_jenis_all'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_data--------------------------------
#===============================================================================
}
