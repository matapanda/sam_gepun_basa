<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportusermain extends CI_Controller {
    public $title = "Laporan Data User";
    public $tbl_main = "user";


    public $id_user_sess;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->auth_v0->check_session_active_ad();
        
        $this->id_admin_sess = $this->session->userdata("ih_mau_ngapain")["id_admin"];
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "report_user";
        $data["title"] = $this->title;
        
		$this->load->view('index', $data);
	}

    public function index_list_data(){
        $draw = @$_POST['draw'];
        $start = @$_POST['start'];
        $length = @$_POST['length'];
        $search = @$_POST['search'];
            $search_val = $search['value'];

        $start_page = $start;
        // if($start != 0){
        //     $start_page = $start*$length;
        // }
        
        if($search_val){
            
            $data["data"] = $this->ot->get_user_report_like(["nama_user", "username_user", "email_user", "us.id_user", "nik_user", "tlp_user"], $search_val, $start_page, $length);
        }else{
            $data["data"] = $this->ot->get_user_report($start_page, $length);
        }

        // print_r($this->db->last_query());
        $count_list = $this->ot->user_count()["count_data"];
        $data["draw"] = $draw;
        $data["recordsTotal"] = $count_list;
        $data["recordsFiltered"] = $count_list;


        echo json_encode($data);
        // print_r("<pre>");
        // print_r($_POST);
        // print_r($this->db->last_query());
    }
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------list_data-----------------------------------
#===============================================================================

    function out_profil($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_data"] = $this->mm->get_data_each("user_data", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_profile" ,$data);
    }

    function out_ketenagakerjaan($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_kerja"] = $this->mm->get_data_each("user_kerja", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_kerja" ,$data);
    }

    function out_kesehatan($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_kesehatan"] = $this->mm->get_data_each("user_kesehatan", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_kesehatan" ,$data);
    }

    function out_keuangan($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_keuangan"] = $this->mm->get_data_each("user_keuangan", ["id_user"=> $id_user]);

        // print_r("<pre>");
        // print_r($data);
        // die();
        $this->load->view("admin/d_user/resume_user_keuangan" ,$data);
    }

    function out_or($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_or"] = $this->mm->get_data_each("user_or", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_or" ,$data);
    }

    function out_pendidikan($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_pendidikan"] = $this->mm->get_data_each("user_pendidikan", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_pendidikan" ,$data);
    }


    function out_sosial($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_sosial"] = $this->mm->get_data_each("user_sosial", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_sosial" ,$data);
    }

    function out_tmpt($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_tmpt"] = $this->mm->get_data_each("user_tmpt", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_tmpt" ,$data);
    }


    function out_transport($id_user){
        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $id_user]);
        $data["user_transport"] = $this->mm->get_data_each("user_transport", ["id_user"=> $id_user]);


        $this->load->view("admin/d_user/resume_user_transport" ,$data);
    }

#===============================================================================
#-----------------------------------list_data-----------------------------------
#===============================================================================


}
