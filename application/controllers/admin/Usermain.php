<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Usermain extends CI_Controller {

    public $tbl_main = "user";

    public $id_user_sess;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('other/other', 'ot');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0");
        $this->load->library("magic_pattern");

        $this->auth_v0->check_session_active_ad();
        
        $this->id_admin_sess = $this->session->userdata("ih_mau_ngapain")["id_admin"];
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "user_main";
		$data["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
		$this->load->view('index', $data);
	}

    public function index_list_data(){
        $draw = @$_POST['draw'];
        $start = @$_POST['start'];
        $length = @$_POST['length'];
        $search = @$_POST['search'];
            $search_val = $search['value'];

        $start_page = $start;
        // if($start != 0){
        //     $start_page = $start*$length;
        // }
        
        if($search_val){
            
            $data["data"] = $this->ot->get_user_like(["nama_user", "username_user", "email_user", "us.id_user", "nik_user", "tlp_user"], $search_val, $start_page, $length);
        }else{
            $data["data"] = $this->ot->get_user_all($start_page, $length);
        }

        // print_r($this->db->last_query());
        $count_list = $this->ot->user_count()["count_data"];
        $data["draw"] = $draw;
        $data["recordsTotal"] = $count_list;
        $data["recordsFiltered"] = $count_list;


        echo json_encode($data);
        // print_r("<pre>");
        // print_r($_POST);
        // print_r($this->db->last_query());
    }
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


	public function val_form_insert_data(){
        $config_val_input = array(
                array(
                    'field'=>'email_user',
                    'label'=>'email_user',
                    'rules'=>'required|valid_emails|is_unique[user.email_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email_users'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    )  
                ),array(
                    'field'=>'username_user',
                    'label'=>'username_user',
                    'rules'=>'required|is_unique[user.username_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'is_unique'=>"%s ".$this->response_message->get_error_msg("EMAIL_AVAIL")
                    ) 
                ),array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tlp_user',
                    'label'=>'tlp_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_user',
                    'label'=>'nik_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = [
            "id_user"=>"",
            "email_user"=>"",
            "username_user"=>"",
            "password"=>"",
            "repassword"=>"",
            "status_active_user"=>"",
            "nama_user"=>"",
            "nik_user"=>"",
            "tlp_user"=>"",
            "alamat_user"=>"",
            "is_del_user"=>""
        ];
               

        // print_r($_POST);
        // die();
        if($this->val_form_insert_data()){
            $email_user = $this->input->post("email_user", true);
            $username_user = $this->input->post("username_user", true);
            $password = $this->input->post("password", true);
            $repassword = $this->input->post("repassword", true);
            
            $nama_user = $this->input->post("nama_user", true);
            $nik_user = $this->input->post("nik_user", true);
            $tlp_user = $this->input->post("tlp_user", true);
            $alamat_user = $this->input->post("alamat_user", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $email_user],
                                [$type_pattern, $username_user],
                                [$type_pattern, $password]];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                
                if ($password == $repassword) {
                    $data = [
                        "id_user" => "",
                        "email_user" => $email_user,
                        "username_user" => $username_user,
                        "password_user" => hash("sha256", $password),
                        "status_active_user" => "0",
                        "nama_user" => $nama_user,
                        "nik_user" => $nik_user,
                        "tlp_user" => $tlp_user,
                        "alamat_user" => "",
                        "is_del_user" => "0"
                    ];

                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("RE_password_FAIL"));
                    
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["email_user"]= strip_tags(form_error('email_user'));
            $msg_detail["username_user"]= strip_tags(form_error('username_user'));
            $msg_detail["password"]= strip_tags(form_error('password'));
            $msg_detail["repassword"]= strip_tags(form_error('repassword'));
            
            $msg_detail["status_active_user"]= strip_tags(form_error('status_active_user'));
            $msg_detail["nama_user"]= strip_tags(form_error('nama_user'));
            $msg_detail["nik_user"]= strip_tags(form_error('nik_user'));
            $msg_detail["tlp_user"]= strip_tags(form_error('tlp_user'));
            $msg_detail["alamat_user"]= strip_tags(form_error('alamat_user'));
            $msg_detail["is_del_user"]= strip_tags(form_error('is_del_user'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_user"])){
        	$id_user = $this->input->post('id_user');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$id_user, "is_del_user"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================

    public function val_form_update_data(){
        $config_val_input = array(
                array(
                    'field'=>'email_user',
                    'label'=>'email_user',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email_users'=>"%s ".$this->response_message->get_error_msg("EMAIL")
                    )  
                ),array(
                    'field'=>'username_user',
                    'label'=>'username_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    ) 
                ),array(
                    'field'=>'nama_user',
                    'label'=>'Nama Admin',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'tlp_user',
                    'label'=>'tlp_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'nik_user',
                    'label'=>'Nomor Induk Pegawai',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_data(){
        
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = [
            "id_user"=>"",
            "email_user"=>"",
            "username_user"=>"",
            "password"=>"",
            "repassword"=>"",
            "status_active_user"=>"",
            "nama_user"=>"",
            "nik_user"=>"",
            "tlp_user"=>"",
            "alamat_user"=>"",
            "is_del_user"=>""
        ];

        if($this->val_form_update_data()){
        	$id_user 		= $this->input->post("id_user", true);

            $email_user 		= $this->input->post("email_user", true);
            $username_user 		= $this->input->post("username_user", true);
            $password 		= $this->input->post("password", true);
            $repassword 	= $this->input->post("repassword", true);
            $nama_user 	        = $this->input->post("nama_user", true);
            $nik_user 		    = $this->input->post("nik_user", true);
            $tlp_user 	        = $this->input->post("tlp_user", true);
            $alamat_user 		= $this->input->post("alamat_user", true);

          	// check username_user
          	if(!$this->mm->get_data_each($this->tbl_main, array("username_user"=>$username_user, "id_user!="=>$id_user))){

                $type_pattern   = "allowed_general_char";

                $arr_pattern  = [
                                    [$type_pattern, $email_user],
                                    [$type_pattern, $username_user],
                                    [$type_pattern, $password],
                                    [$type_pattern, $nama_user]];


                if($this->magic_pattern->set_list_pattern($arr_pattern )){
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                } else{
                    // check username_user
                    if(!$this->mm->get_data_each($this->tbl_main, array("username_user"=>$username_user, "id_user!="=>$id_user))){
                        // check email_user 
                        // print_r("<per>");
                        //     print_r($this->mm->get_data_each($this->tbl_main, ["id_user!="=>$id_user, "email_user"=>$email_user]));
                        //     die();
                        if(!$this->mm->get_data_each($this->tbl_main, ["id_user!="=>$id_user, "email_user"=>$email_user])){
                            
                            $set = array(
                                "email_user"=>$email_user,
                                "username_user"=>$username_user,
                                "nama_user"=>$nama_user,
                                "nik_user"=>$nik_user,
                                "tlp_user"=>$tlp_user,
                                "alamat_user"=>$alamat_user
                            );
                            $where = array("id_user"=>$id_user);
                            $update = $this->mm->update_data($this->tbl_main, $set, $where);
                            if($update){
                                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                            }
                        } else{
                            $msg_detail["email_user"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
                        }
                    }else{
                        $msg_detail["username_user"] = $this->response_message->get_error_msg("USER_AVAIL");
                    }
                }
          	}else{
          		$msg_detail["username_user"] = $this->response_message->get_error_msg("EMAIL_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["email_user"]= strip_tags(form_error('email_user'));
            $msg_detail["username_user"]= strip_tags(form_error('username_user'));
            $msg_detail["password"]= strip_tags(form_error('password'));
            $msg_detail["repassword"]= strip_tags(form_error('repassword'));
            
            $msg_detail["status_active_user"]= strip_tags(form_error('status_active_user'));
            $msg_detail["nama_user"]= strip_tags(form_error('nama_user'));
            $msg_detail["nik_user"]= strip_tags(form_error('nik_user'));
            $msg_detail["tlp_user"]= strip_tags(form_error('tlp_user'));
            $msg_detail["alamat_user"]= strip_tags(form_error('alamat_user'));
            $msg_detail["is_del_user"]= strip_tags(form_error('is_del_user'));           
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id_user"]){
        	$id_user = $this->input->post("id_user");
        	$where = array("id_user"=>$id_user);

            $set = array("is_del_user"=>"1");
            $where = array("id_user"=>$id_user);

        	// $delete_admin = $this->mm->delete_data($this->tbl_main, array("id_user"=>$id_user));
        	$delete_admin = $this->mm->update_data($this->tbl_main, $set, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================
    public function val_form_ch_pass_admin(){
        $config_val_input = array(
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )     
                ),array(
                    'field'=>'repassword',
                    'label'=>'Ulangi password',
                    'rules'=>'required|alpha_numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'required'=>"%s ".$this->response_message->get_error_msg("NUMBER_CHAR")
                    )
                       
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function change_pass_admin(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "password"=>"",
                    "repassword"=>""
                );

        if($this->val_form_ch_pass_admin()){
        	$id_user 		= $this->input->post("id_user");
            $password 		= $this->input->post("password");
            $repassword 	= $this->input->post("repassword");

          	// check username_user
          	if($password == $repassword){
          		$set = array(
          				"password_user"=>hash("sha256", $password)
          			);

          		$where = array("id_user"=>$id_user);

          		$update = $this->mm->update_data($this->tbl_main, $set, $where);
	            if($update){
	                $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
	            }
          	}else{
          		$msg_detail["username_user"] = $this->response_message->get_error_msg("email_user_AVAIL");
          	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["password"] 	= strip_tags(form_error('password'));
            $msg_detail["repassword"] 	= strip_tags(form_error('repassword'));         
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------change_password_admin-----------------------
#===============================================================================


#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================

    public function disabled_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id_user"]){
        	$id_user = $this->input->post("id_user");
        	$where 	= array("id_user"=>$id_user);
        	$set 	= array("status_active_user"=>"0");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------dasabled_admin------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================
    public function activate_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user"=>"",
                );

        if($_POST["id_user"]){
        	$id_user = $this->input->post("id_user");
        	$where 	= array("id_user"=>$id_user);
        	$set 	= array("status_active_user"=>"1");
        	
        	$update_data = $this->mm->update_data($this->tbl_main, $set, $where);
        	if($update_data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user"]= strip_tags(form_error('id_user'));        
        }

        $msg_detail["list_data"] = $this->mm->get_data_all_where($this->tbl_main, array("is_del_user"=>"0"));
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------activate_admin------------------------------
#===============================================================================


}
