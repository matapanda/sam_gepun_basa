<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Loginuser extends CI_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model('login/user_main', 'am');

		$this->load->library("response_message");
		$this->load->library("Auth_v0_user");

        $this->auth_v0_user->auth_login();
	}

	public function index(){
        // print_r($_SESSION);
        $cek_userdata = $this->session->userdata('flash_welcome');
        $data["page"] = "login_user_new";
        $data["session_cek"] = $cek_userdata;
		$this->load->view('login/login_user_new', $data);
	}

	public function logout(){
		$this->auth_v0_user->destroy_session();
	}

    public function check_session(){
        print_r($_SESSION);
    }

	private function val_form_log(){
        $config_val_input = array(
                array(
                    'field'=>'username',
                    'label'=>'username',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'password',
                    'label'=>'password',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                )
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

	public function get_auth(){
		$msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("LOG_FAIL"));
        $msg_detail = array("username" => "",
                            "pass" => "");
                            
        if($this->val_form_log()){
        	$username = $this->db->escape($this->input->post('username', true));
    		$pass = $this->input->post('password', true);
    		
            $cek = $this->am->select_user($username, hash("sha256", $pass));
            if($cek){
                $cek["status_log"] = true;
                if($this->auth_v0_user->set_session($cek)){
                    $msg_main = array("status" => true, "msg"=>$this->response_message->get_success_msg("LOG_SUC_USER"));
                }
    		}

        }else {
        	$msg_detail["username"] = form_error("username");
            $msg_detail["pass"] = form_error("pass");

            $msg_main = array("status" => false, "msg"=>$this->response_message->get_error_msg("INSERT_FAIL"));
        }
        // print_r($cek);

        $msg_array = $this->response_message->default_mgs($msg_main,$msg_detail);
        print_r(json_encode($msg_array));
	}
}
