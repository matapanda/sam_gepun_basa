<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kesehatanuser extends CI_Controller {
    public $title = "Kesehatan Masyarakat";
    public $tbl_main = "user_kesehatan";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "JENIS_KB"=>"1",
        "JN_VAKSIN"=>"9",
        "JN_DOSIS"=>"13",
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");
        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        $id_user 	        = $this->id_user_ses;
		$data["page"]       = "kesehatan";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);
        
		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$id_user));

        $data["jenis_kb"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JENIS_KB"]]);
        $data["jenis_vaksin"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_VAKSIN"]]);
        $data["jenis_dosis"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_DOSIS"]]);

        // print_r("<pre>");
        // print_r($data);
        // die();

		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();

        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'bb',
                'label'=>'bb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'tb',
                'label'=>'tb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_vaksin',
                'label'=>'q_vaksin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'d_vaksin',
                'label'=>'d_vaksin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_kebutuhan_khusus',
                'label'=>'q_kebutuhan_khusus',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_butawarna',
                'label'=>'q_butawarna',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_expektor_kb',
                'label'=>'q_expektor_kb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'kb_jenis',
                'label'=>'kb_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_stunting',
                'label'=>'q_stunting',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'stnt_lingkar_kpl',
                'label'=>'stnt_lingkar_kpl',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ibu_hamil',
                'label'=>'q_ibu_hamil',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_rutin_pemeriksaan',
                'label'=>'hml_rutin_pemeriksaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_usia_kehamilan',
                'label'=>'hml_usia_kehamilan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_tmp_periksa',
                'label'=>'hml_tmp_periksa',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();

        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "bb" => "",
                    "tb" => "",
                    "q_vaksin" => "",
                    "d_vaksin" => "",
                    "q_kebutuhan_khusus" => "",
                    "q_butawarna" => "",
                    "q_expektor_kb" => "",
                    "kb_jenis" => "",
                    "q_stunting" => "",
                    "stnt_lingkar_kpl" => "",
                    "q_ibu_hamil" => "",
                    "hml_rutin_pemeriksaan" => "",
                    "hml_usia_kehamilan" => "",
                    "hml_tmp_periksa" => ""
                );

        if($this->val_form_insert()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $bb = $this->input->post("bb", true);
            $tb = $this->input->post("tb", true);
            $q_vaksin = $this->input->post("q_vaksin", true);
            $d_vaksin = $this->input->post("d_vaksin", true);
            $q_kebutuhan_khusus = $this->input->post("q_kebutuhan_khusus", true);
            $q_butawarna = $this->input->post("q_butawarna", true);
            $q_expektor_kb = $this->input->post("q_expektor_kb", true);
            $kb_jenis = $this->input->post("kb_jenis", true);
            $q_stunting = $this->input->post("q_stunting", true);
            $stnt_lingkar_kpl = $this->input->post("stnt_lingkar_kpl", true);
            $q_ibu_hamil = $this->input->post("q_ibu_hamil", true);
            $hml_rutin_pemeriksaan = $this->input->post("hml_rutin_pemeriksaan", true);
            $hml_usia_kehamilan = $this->input->post("hml_usia_kehamilan", true);
            $hml_tmp_periksa = $this->input->post("hml_tmp_periksa", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_kesehatan" => "",
                    "id_user" => $this->id_user_ses,
                    "bb" => $bb,
                    "tb" => $tb,
                    "q_vaksin" => $q_vaksin,
                    "d_vaksin" => $d_vaksin,
                    "q_kebutuhan_khusus" => $q_kebutuhan_khusus,
                    "q_butawarna" => $q_butawarna,
                    "q_expektor_kb" => $q_expektor_kb,
                    "kb_jenis" => $kb_jenis,
                    "q_stunting" => $q_stunting,
                    "stnt_lingkar_kpl" => $stnt_lingkar_kpl,
                    "q_ibu_hamil" => $q_ibu_hamil,
                    "hml_rutin_pemeriksaan" => $hml_rutin_pemeriksaan,
                    "hml_usia_kehamilan" => $hml_usia_kehamilan,
                    "hml_tmp_periksa" => $hml_tmp_periksa,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["bb"]  = strip_tags(form_error('bb'));
            $msg_detail["tb"]  = strip_tags(form_error('tb'));
            $msg_detail["q_vaksin"]  = strip_tags(form_error('q_vaksin'));
            $msg_detail["d_vaksin"]  = strip_tags(form_error('d_vaksin'));
            $msg_detail["q_kebutuhan_khusus"]  = strip_tags(form_error('q_kebutuhan_khusus'));
            $msg_detail["q_butawarna"]  = strip_tags(form_error('q_butawarna'));
            $msg_detail["q_expektor_kb"]  = strip_tags(form_error('q_expektor_kb'));
            $msg_detail["kb_jenis"]  = strip_tags(form_error('kb_jenis'));
            $msg_detail["q_stunting"]  = strip_tags(form_error('q_stunting'));
            $msg_detail["stnt_lingkar_kpl"]  = strip_tags(form_error('stnt_lingkar_kpl'));
            $msg_detail["q_ibu_hamil"]  = strip_tags(form_error('q_ibu_hamil'));
            $msg_detail["hml_rutin_pemeriksaan"]  = strip_tags(form_error('hml_rutin_pemeriksaan'));
            $msg_detail["hml_usia_kehamilan"]  = strip_tags(form_error('hml_usia_kehamilan'));
            $msg_detail["hml_tmp_periksa"]  = strip_tags(form_error('hml_tmp_periksa'));   
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            array(
                'field'=>'bb',
                'label'=>'bb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'tb',
                'label'=>'tb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_vaksin',
                'label'=>'q_vaksin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'d_vaksin',
                'label'=>'d_vaksin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_kebutuhan_khusus',
                'label'=>'q_kebutuhan_khusus',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_butawarna',
                'label'=>'q_butawarna',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_expektor_kb',
                'label'=>'q_expektor_kb',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'kb_jenis',
                'label'=>'kb_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_stunting',
                'label'=>'q_stunting',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'stnt_lingkar_kpl',
                'label'=>'stnt_lingkar_kpl',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ibu_hamil',
                'label'=>'q_ibu_hamil',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_rutin_pemeriksaan',
                'label'=>'hml_rutin_pemeriksaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_usia_kehamilan',
                'label'=>'hml_usia_kehamilan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hml_tmp_periksa',
                'label'=>'hml_tmp_periksa',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "bb" => "",
                    "tb" => "",
                    "q_vaksin" => "",
                    "d_vaksin" => "",
                    "q_kebutuhan_khusus" => "",
                    "q_butawarna" => "",
                    "q_expektor_kb" => "",
                    "kb_jenis" => "",
                    "q_stunting" => "",
                    "stnt_lingkar_kpl" => "",
                    "q_ibu_hamil" => "",
                    "hml_rutin_pemeriksaan" => "",
                    "hml_usia_kehamilan" => "",
                    "hml_tmp_periksa" => ""
                );

        if($this->val_form_update()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $bb = $this->input->post("bb", true);
            $tb = $this->input->post("tb", true);
            $q_vaksin = $this->input->post("q_vaksin", true);
            $d_vaksin = $this->input->post("d_vaksin", true);
            $q_kebutuhan_khusus = $this->input->post("q_kebutuhan_khusus", true);
            $q_butawarna = $this->input->post("q_butawarna", true);
            $q_expektor_kb = $this->input->post("q_expektor_kb", true);
            $kb_jenis = $this->input->post("kb_jenis", true);
            $q_stunting = $this->input->post("q_stunting", true);
            $stnt_lingkar_kpl = $this->input->post("stnt_lingkar_kpl", true);
            $q_ibu_hamil = $this->input->post("q_ibu_hamil", true);
            $hml_rutin_pemeriksaan = $this->input->post("hml_rutin_pemeriksaan", true);
            $hml_usia_kehamilan = $this->input->post("hml_usia_kehamilan", true);
            $hml_tmp_periksa = $this->input->post("hml_tmp_periksa", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "bb" => $bb,
                    "tb" => $tb,
                    "q_vaksin" => $q_vaksin,
                    "d_vaksin" => $d_vaksin,
                    "q_kebutuhan_khusus" => $q_kebutuhan_khusus,
                    "q_butawarna" => $q_butawarna,
                    "q_expektor_kb" => $q_expektor_kb,
                    "kb_jenis" => $kb_jenis,
                    "q_stunting" => $q_stunting,
                    "stnt_lingkar_kpl" => $stnt_lingkar_kpl,
                    "q_ibu_hamil" => $q_ibu_hamil,
                    "hml_rutin_pemeriksaan" => $hml_rutin_pemeriksaan,
                    "hml_usia_kehamilan" => $hml_usia_kehamilan,
                    "hml_tmp_periksa" => $hml_tmp_periksa,
                    
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["bb"]  = strip_tags(form_error('bb'));
            $msg_detail["tb"]  = strip_tags(form_error('tb'));
            $msg_detail["q_vaksin"]  = strip_tags(form_error('q_vaksin'));
            $msg_detail["d_vaksin"]  = strip_tags(form_error('d_vaksin'));
            $msg_detail["q_kebutuhan_khusus"]  = strip_tags(form_error('q_kebutuhan_khusus'));
            $msg_detail["q_butawarna"]  = strip_tags(form_error('q_butawarna'));
            $msg_detail["q_expektor_kb"]  = strip_tags(form_error('q_expektor_kb'));
            $msg_detail["kb_jenis"]  = strip_tags(form_error('kb_jenis'));
            $msg_detail["q_stunting"]  = strip_tags(form_error('q_stunting'));
            $msg_detail["stnt_lingkar_kpl"]  = strip_tags(form_error('stnt_lingkar_kpl'));
            $msg_detail["q_ibu_hamil"]  = strip_tags(form_error('q_ibu_hamil'));
            $msg_detail["hml_rutin_pemeriksaan"]  = strip_tags(form_error('hml_rutin_pemeriksaan'));
            $msg_detail["hml_usia_kehamilan"]  = strip_tags(form_error('hml_usia_kehamilan'));
            $msg_detail["hml_tmp_periksa"]  = strip_tags(form_error('hml_tmp_periksa'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
