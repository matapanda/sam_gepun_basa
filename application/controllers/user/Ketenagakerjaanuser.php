<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ketenagakerjaanuser extends CI_Controller {
    public $title = "Ketenangakerjaan Masyarakat";
    public $sts_kerja_ketl_main = "user_kerja";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "JN_PEKERJAAN"=>"17",
        "STS_PEKERJAAN"=>"18",
        "BIDANG_KERJA"=>"121",
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");
        
        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        $id_user 	        = $this->id_user_ses;
		$data["page"]       = "ketenagakerjaan";
        $data["title"]      = $this->title;

        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);
		
        $data["list_data"]  = $this->mm->get_data_each($this->sts_kerja_ketl_main, array("id_user"=>$id_user));

        $data["sts_pekerjaan"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["STS_PEKERJAAN"]]);

        $data["jn_pekerjaan"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_PEKERJAAN"]]);

        $data["bidang_pekerjaan"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["BIDANG_KERJA"]]);

        // print_r("<pre>");
        // print_r($data);
        // die();

		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();

        $cek_data = $this->mm->get_data_each($this->sts_kerja_ketl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'sts_kerja',
                'label'=>'sts_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'jenis_kerja',
                'label'=>'jenis_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'tempat_kerja',
                'label'=>'tempat_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'bidang_pekerjaan',
                'label'=>'bidang_pekerjaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'penghasilan_perbulan',
                'label'=>'penghasilan_perbulan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_terima_pensiun',
                'label'=>'q_terima_pensiun',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();

        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "sts_kerja" => "",
                    "sts_kerja_ket" => "",
                    "jenis_kerja" => "",
                    "tempat_kerja" => "",
                    "bidang_pekerjaan" => "",
                    "penghasilan_perbulan" => "",
                    "q_terima_pensiun" => ""
                );

        if($this->val_form_insert()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $sts_kerja = $this->input->post("sts_kerja", true);
            $sts_kerja_ket = $this->input->post("sts_kerja_ket", true);
            $jenis_kerja = $this->input->post("jenis_kerja", true);
            $tempat_kerja = $this->input->post("tempat_kerja", true);
            $bidang_pekerjaan = $this->input->post("bidang_pekerjaan", true);
            $penghasilan_perbulan = $this->input->post("penghasilan_perbulan", true);
            $q_terima_pensiun = $this->input->post("q_terima_pensiun", true);
            
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_kerja" => "",
                    "id_user" => $this->id_user_ses,
                    "sts_kerja" => $sts_kerja,
                    "sts_kerja_ket" => $sts_kerja_ket,
                    "jenis_kerja" => $jenis_kerja,
                    "tempat_kerja" => $tempat_kerja,
                    "bidang_pekerjaan" => $bidang_pekerjaan,
                    "penghasilan_perbulan" => $penghasilan_perbulan,
                    "q_terima_pensiun" => $q_terima_pensiun,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->sts_kerja_ketl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["sts_kerja"]  = strip_tags(form_error('sts_kerja'));
            $msg_detail["sts_kerja_ket"]  = strip_tags(form_error('sts_kerja_ket'));
            $msg_detail["jenis_kerja"]  = strip_tags(form_error('jenis_kerja'));
            $msg_detail["tempat_kerja"]  = strip_tags(form_error('tempat_kerja'));
            $msg_detail["bidang_pekerjaan"]  = strip_tags(form_error('bidang_pekerjaan'));
            $msg_detail["penghasilan_perbulan"]  = strip_tags(form_error('penghasilan_perbulan'));
            $msg_detail["q_terima_pensiun"]  = strip_tags(form_error('q_terima_pensiun'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            array(
                'field'=>'sts_kerja',
                'label'=>'sts_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'jenis_kerja',
                'label'=>'jenis_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'tempat_kerja',
                'label'=>'tempat_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'bidang_pekerjaan',
                'label'=>'bidang_pekerjaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'penghasilan_perbulan',
                'label'=>'penghasilan_perbulan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_terima_pensiun',
                'label'=>'q_terima_pensiun',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "sts_kerja" => "",
                    "sts_kerja_ket" => "",
                    "jenis_kerja" => "",
                    "tempat_kerja" => "",
                    "bidang_pekerjaan" => "",
                    "penghasilan_perbulan" => "",
                    "q_terima_pensiun" => "",
                );

        if($this->val_form_update()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $sts_kerja = $this->input->post("sts_kerja", true);
            $sts_kerja_ket = $this->input->post("sts_kerja_ket", true);
            $jenis_kerja = $this->input->post("jenis_kerja", true);
            $tempat_kerja = $this->input->post("tempat_kerja", true);
            $bidang_pekerjaan = $this->input->post("bidang_pekerjaan", true);
            $penghasilan_perbulan = $this->input->post("penghasilan_perbulan", true);
            $q_terima_pensiun = $this->input->post("q_terima_pensiun", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "sts_kerja" => $sts_kerja,
                    "sts_kerja_ket" => $sts_kerja_ket,
                    "jenis_kerja" => $jenis_kerja,
                    "tempat_kerja" => $tempat_kerja,
                    "bidang_pekerjaan" => $bidang_pekerjaan,
                    "penghasilan_perbulan" => $penghasilan_perbulan,
                    "q_terima_pensiun" => $q_terima_pensiun,
                    
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->sts_kerja_ketl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["sts_kerja"]  = strip_tags(form_error('sts_kerja'));
            $msg_detail["sts_kerja_ket"]  = strip_tags(form_error('sts_kerja_ket'));
            $msg_detail["jenis_kerja"]  = strip_tags(form_error('jenis_kerja'));
            $msg_detail["tempat_kerja"]  = strip_tags(form_error('tempat_kerja'));
            $msg_detail["bidang_pekerjaan"]  = strip_tags(form_error('bidang_pekerjaan'));
            $msg_detail["penghasilan_perbulan"]  = strip_tags(form_error('penghasilan_perbulan'));
            $msg_detail["q_terima_pensiun"]  = strip_tags(form_error('q_terima_pensiun'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
