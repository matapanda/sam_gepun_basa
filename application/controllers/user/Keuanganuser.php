<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Keuanganuser extends CI_Controller {
    public $title = "Keuangan Masyarakat";
    public $tbl_main = "user_keuangan";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"]       = "user_keuangan";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$this->id_user_ses));
		
        $this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'q_harta_tidak_bergerak',
                'label'=>'q_harta_tidak_bergerak',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'htb_detail',
                'label'=>'htb_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_harta_bergerak',
                'label'=>'q_harta_bergerak',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'hb_detail',
                'label'=>'hb_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_deposito',
                'label'=>'q_deposito',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_piutang',
                'label'=>'q_piutang',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_hutang',
                'label'=>'q_hutang',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ht_nominal',
                'label'=>'ht_nominal',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'t_kekayaan',
                'label'=>'t_kekayaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_penghasilan_per_th',
                'label'=>'q_penghasilan_per_th',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_penghasilan_profesi',
                'label'=>'q_penghasilan_profesi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_penghasilan_usaha',
                'label'=>'q_penghasilan_usaha',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_penghasilan_hibah',
                'label'=>'q_penghasilan_hibah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'t_penghasilan',
                'label'=>'t_penghasilan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'t_pengeluaran_rutin',
                'label'=>'t_pengeluaran_rutin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'t_pengeluaran_lainnya',
                'label'=>'t_pengeluaran_lainnya',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_harta_tidak_bergerak" => "",
                    "htb_detail" => "",
                    "q_harta_bergerak" => "",
                    "hb_detail" => "",
                    "q_deposito" => "",
                    "q_piutang" => "",
                    "q_hutang" => "",
                    "ht_nominal" => "",
                    "t_kekayaan" => "",
                    "q_penghasilan_per_th" => "",
                    "q_penghasilan_profesi" => "",
                    "q_penghasilan_usaha" => "",
                    "q_penghasilan_hibah" => "",
                    "t_penghasilan" => "",
                    "t_pengeluaran_rutin" => "",
                    "t_pengeluaran_lainnya" => ""
                );

        if($this->val_form_insert()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $q_harta_tidak_bergerak = $this->input->post("q_harta_tidak_bergerak", true);
            $htb_detail = $this->input->post("htb_detail", true);
            $q_harta_bergerak = $this->input->post("q_harta_bergerak", true);
            $hb_detail = $this->input->post("hb_detail", true);
            $q_deposito = $this->input->post("q_deposito", true);
            $q_piutang = $this->input->post("q_piutang", true);
            $q_hutang = $this->input->post("q_hutang", true);
            $ht_nominal = $this->input->post("ht_nominal", true);
            $t_kekayaan = $this->input->post("t_kekayaan", true);
            $q_penghasilan_per_th = $this->input->post("q_penghasilan_per_th", true);
            $q_penghasilan_profesi = $this->input->post("q_penghasilan_profesi", true);
            $q_penghasilan_usaha = $this->input->post("q_penghasilan_usaha", true);
            $q_penghasilan_hibah = $this->input->post("q_penghasilan_hibah", true);
            $t_penghasilan = $this->input->post("t_penghasilan", true);
            $t_pengeluaran_rutin = $this->input->post("t_pengeluaran_rutin", true);
            $t_pengeluaran_lainnya = $this->input->post("t_pengeluaran_lainnya", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_keuangan" => "",
                    "id_user" => $this->id_user_ses,
                    "q_harta_tidak_bergerak" => $q_harta_tidak_bergerak,
                    "htb_detail" => $htb_detail,
                    "q_harta_bergerak" => $q_harta_bergerak,
                    "hb_detail" => $hb_detail,
                    "q_deposito" => $q_deposito,
                    "q_piutang" => $q_piutang,
                    "q_hutang" => $q_hutang,
                    "ht_nominal" => $ht_nominal,
                    "t_kekayaan" => $t_kekayaan,
                    "q_penghasilan_per_th" => $q_penghasilan_per_th,
                    "q_penghasilan_profesi" => $q_penghasilan_profesi,
                    "q_penghasilan_usaha" => $q_penghasilan_usaha,
                    "q_penghasilan_hibah" => $q_penghasilan_hibah,
                    "t_penghasilan" => $t_penghasilan,
                    "t_pengeluaran_rutin" => $t_pengeluaran_rutin,
                    "t_pengeluaran_lainnya" => $t_pengeluaran_lainnya,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["q_harta_tidak_bergerak"]  = strip_tags(form_error('q_harta_tidak_bergerak'));
            $msg_detail["htb_detail"]  = strip_tags(form_error('htb_detail'));
            $msg_detail["q_harta_bergerak"]  = strip_tags(form_error('q_harta_bergerak'));
            $msg_detail["hb_detail"]  = strip_tags(form_error('hb_detail'));
            $msg_detail["q_deposito"]  = strip_tags(form_error('q_deposito'));
            $msg_detail["q_piutang"]  = strip_tags(form_error('q_piutang'));
            $msg_detail["q_hutang"]  = strip_tags(form_error('q_hutang'));
            $msg_detail["ht_nominal"]  = strip_tags(form_error('ht_nominal'));
            $msg_detail["t_kekayaan"]  = strip_tags(form_error('t_kekayaan'));
            $msg_detail["q_penghasilan_per_th"]  = strip_tags(form_error('q_penghasilan_per_th'));
            $msg_detail["q_penghasilan_profesi"]  = strip_tags(form_error('q_penghasilan_profesi'));
            $msg_detail["q_penghasilan_usaha"]  = strip_tags(form_error('q_penghasilan_usaha'));
            $msg_detail["q_penghasilan_hibah"]  = strip_tags(form_error('q_penghasilan_hibah'));
            $msg_detail["t_penghasilan"]  = strip_tags(form_error('t_penghasilan'));
            $msg_detail["t_pengeluaran_rutin"]  = strip_tags(form_error('t_pengeluaran_rutin'));
            $msg_detail["t_pengeluaran_lainnya"]  = strip_tags(form_error('t_pengeluaran_lainnya'));    
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            
            array(
                'field'=>'q_harta_tidak_bergerak',
                'label'=>'q_harta_tidak_bergerak',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'htb_detail',
                'label'=>'htb_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_harta_bergerak',
                'label'=>'q_harta_bergerak',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'hb_detail',
                'label'=>'hb_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_deposito',
                'label'=>'q_deposito',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_piutang',
                'label'=>'q_piutang',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_hutang',
                'label'=>'q_hutang',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'ht_nominal',
                'label'=>'ht_nominal',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'t_kekayaan',
                'label'=>'t_kekayaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_penghasilan_per_th',
                'label'=>'q_penghasilan_per_th',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_penghasilan_profesi',
                'label'=>'q_penghasilan_profesi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_penghasilan_usaha',
                'label'=>'q_penghasilan_usaha',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_penghasilan_hibah',
                'label'=>'q_penghasilan_hibah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'t_penghasilan',
                'label'=>'t_penghasilan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'t_pengeluaran_rutin',
                'label'=>'t_pengeluaran_rutin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'t_pengeluaran_lainnya',
                'label'=>'t_pengeluaran_lainnya',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_harta_tidak_bergerak" => "",
                    "htb_detail" => "",
                    "q_harta_bergerak" => "",
                    "hb_detail" => "",
                    "q_deposito" => "",
                    "q_piutang" => "",
                    "q_hutang" => "",
                    "ht_nominal" => "",
                    "t_kekayaan" => "",
                    "q_penghasilan_per_th" => "",
                    "q_penghasilan_profesi" => "",
                    "q_penghasilan_usaha" => "",
                    "q_penghasilan_hibah" => "",
                    "t_penghasilan" => "",
                    "t_pengeluaran_rutin" => "",
                    "t_pengeluaran_lainnya" => ""
                );

        if($this->val_form_update()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $q_harta_tidak_bergerak = $this->input->post("q_harta_tidak_bergerak", true);
            $htb_detail = $this->input->post("htb_detail", true);
            $q_harta_bergerak = $this->input->post("q_harta_bergerak", true);
            $hb_detail = $this->input->post("hb_detail", true);
            $q_deposito = $this->input->post("q_deposito", true);
            $q_piutang = $this->input->post("q_piutang", true);
            $q_hutang = $this->input->post("q_hutang", true);
            $ht_nominal = $this->input->post("ht_nominal", true);
            $t_kekayaan = $this->input->post("t_kekayaan", true);
            $q_penghasilan_per_th = $this->input->post("q_penghasilan_per_th", true);
            $q_penghasilan_profesi = $this->input->post("q_penghasilan_profesi", true);
            $q_penghasilan_usaha = $this->input->post("q_penghasilan_usaha", true);
            $q_penghasilan_hibah = $this->input->post("q_penghasilan_hibah", true);
            $t_penghasilan = $this->input->post("t_penghasilan", true);
            $t_pengeluaran_rutin = $this->input->post("t_pengeluaran_rutin", true);
            $t_pengeluaran_lainnya = $this->input->post("t_pengeluaran_lainnya", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "q_harta_tidak_bergerak" => $q_harta_tidak_bergerak,
                    "htb_detail" => $htb_detail,
                    "q_harta_bergerak" => $q_harta_bergerak,
                    "hb_detail" => $hb_detail,
                    "q_deposito" => $q_deposito,
                    "q_piutang" => $q_piutang,
                    "q_hutang" => $q_hutang,
                    "ht_nominal" => $ht_nominal,
                    "t_kekayaan" => $t_kekayaan,
                    "q_penghasilan_per_th" => $q_penghasilan_per_th,
                    "q_penghasilan_profesi" => $q_penghasilan_profesi,
                    "q_penghasilan_usaha" => $q_penghasilan_usaha,
                    "q_penghasilan_hibah" => $q_penghasilan_hibah,
                    "t_penghasilan" => $t_penghasilan,
                    "t_pengeluaran_rutin" => $t_pengeluaran_rutin,
                    "t_pengeluaran_lainnya" => $t_pengeluaran_lainnya,
                    
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["q_harta_tidak_bergerak"]  = strip_tags(form_error('q_harta_tidak_bergerak'));
            $msg_detail["htb_detail"]  = strip_tags(form_error('htb_detail'));
            $msg_detail["q_harta_bergerak"]  = strip_tags(form_error('q_harta_bergerak'));
            $msg_detail["hb_detail"]  = strip_tags(form_error('hb_detail'));
            $msg_detail["q_deposito"]  = strip_tags(form_error('q_deposito'));
            $msg_detail["q_piutang"]  = strip_tags(form_error('q_piutang'));
            $msg_detail["q_hutang"]  = strip_tags(form_error('q_hutang'));
            $msg_detail["ht_nominal"]  = strip_tags(form_error('ht_nominal'));
            $msg_detail["t_kekayaan"]  = strip_tags(form_error('t_kekayaan'));
            $msg_detail["q_penghasilan_per_th"]  = strip_tags(form_error('q_penghasilan_per_th'));
            $msg_detail["q_penghasilan_profesi"]  = strip_tags(form_error('q_penghasilan_profesi'));
            $msg_detail["q_penghasilan_usaha"]  = strip_tags(form_error('q_penghasilan_usaha'));
            $msg_detail["q_penghasilan_hibah"]  = strip_tags(form_error('q_penghasilan_hibah'));
            $msg_detail["t_penghasilan"]  = strip_tags(form_error('t_penghasilan'));
            $msg_detail["t_pengeluaran_rutin"]  = strip_tags(form_error('t_pengeluaran_rutin'));
            $msg_detail["t_pengeluaran_lainnya"]  = strip_tags(form_error('t_pengeluaran_lainnya'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
