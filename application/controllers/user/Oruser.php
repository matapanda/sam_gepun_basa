<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Oruser extends CI_Controller {
    public $title = "Kegiatan dan Organisasi Masyarakat";
    public $tbl_main = "user_or";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "JN_SERTIFIKAT"=>"43"
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        $id_user 	        = $this->id_user_ses;
		$data["page"]       = "organisasi_sosial";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$id_user));
        $data["jenis_sertifikat"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_SERTIFIKAT"]]);

		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'or_hobby',
                'label'=>'or_hobby',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_keg_pengamalan_pancasila',
                'label'=>'q_keg_pengamalan_pancasila',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'pc_detail',
                'label'=>'pc_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_kerjabakti',
                'label'=>'q_kerjabakti',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_rukun_kematian',
                'label'=>'q_rukun_kematian',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_keg_keagamaan',
                'label'=>'q_keg_keagamaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_jimpitan',
                'label'=>'q_jimpitan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_arisan',
                'label'=>'q_arisan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_gotong_royong',
                'label'=>'q_gotong_royong',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_organisasi',
                'label'=>'q_ikut_organisasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'or_detail',
                'label'=>'or_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_pelatihan',
                'label'=>'q_ikut_pelatihan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'pel_detail',
                'label'=>'pel_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(   
                    "or_hoor_hobbyy"=>"",
                    "q_keg_pengamalan_pancasila"=>"",
                    "pc_detail"=>"",
                    "q_kerjabakti"=>"",
                    "q_rukun_kematian"=>"",
                    "q_keg_keagamaan"=>"",
                    "q_jimpitan"=>"",
                    "q_arisan"=>"",
                    "q_gotong_royong"=>"",
                    "q_ikut_organisasi"=>"",
                    "or_detail"=>"",
                    "q_ikut_pelatihan"=>"",
                    "pel_detail"=>""
                );

        if($this->val_form_insert()){
            
            $or_hobby = $this->input->post("or_hobby", true);
            $q_keg_pengamalan_pancasila = $this->input->post("q_keg_pengamalan_pancasila", true);
            $pc_detail = $this->input->post("pc_detail", true);
            $q_kerjabakti = $this->input->post("q_kerjabakti", true);
            $q_rukun_kematian = $this->input->post("q_rukun_kematian", true);
            $q_keg_keagamaan = $this->input->post("q_keg_keagamaan", true);
            $q_jimpitan = $this->input->post("q_jimpitan", true);
            $q_arisan = $this->input->post("q_arisan", true);
            $q_gotong_royong = $this->input->post("q_gotong_royong", true);
            $q_ikut_organisasi = $this->input->post("q_ikut_organisasi", true);
            $or_detail = $this->input->post("or_detail", true);
            $q_ikut_pelatihan = $this->input->post("q_ikut_pelatihan", true);
            $pel_detail = $this->input->post("pel_detail", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_or" => "",
                    "id_user" => $this->id_user_ses,

                    "or_hobby" => $or_hobby,
                    "q_keg_pengamalan_pancasila" => $q_keg_pengamalan_pancasila,
                    "pc_detail" => $pc_detail,
                    "q_kerjabakti" => $q_kerjabakti,
                    "q_rukun_kematian" => $q_rukun_kematian,
                    "q_keg_keagamaan" => $q_keg_keagamaan,
                    "q_jimpitan" => $q_jimpitan,
                    "q_arisan" => $q_arisan,
                    "q_gotong_royong" => $q_gotong_royong,
                    "q_ikut_organisasi" => $q_ikut_organisasi,
                    "or_detail" => $or_detail,
                    "q_ikut_pelatihan" => $q_ikut_pelatihan,
                    "pel_detail" => $pel_detail,

                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["or_hobby"]  = strip_tags(form_error('or_hobby'));
            $msg_detail["q_keg_pengamalan_pancasila"]  = strip_tags(form_error('q_keg_pengamalan_pancasila'));
            $msg_detail["pc_detail"]  = strip_tags(form_error('pc_detail'));
            $msg_detail["q_kerjabakti"]  = strip_tags(form_error('q_kerjabakti'));
            $msg_detail["q_rukun_kematian"]  = strip_tags(form_error('q_rukun_kematian'));
            $msg_detail["q_keg_keagamaan"]  = strip_tags(form_error('q_keg_keagamaan'));
            $msg_detail["q_jimpitan"]  = strip_tags(form_error('q_jimpitan'));
            $msg_detail["q_arisan"]  = strip_tags(form_error('q_arisan'));
            $msg_detail["q_gotong_royong"]  = strip_tags(form_error('q_gotong_royong'));
            $msg_detail["q_ikut_organisasi"]  = strip_tags(form_error('q_ikut_organisasi'));
            $msg_detail["or_detail"]  = strip_tags(form_error('or_detail'));
            $msg_detail["q_ikut_pelatihan"]  = strip_tags(form_error('q_ikut_pelatihan')); 

            $msg_detail["pel_detail"]  = strip_tags(form_error('pel_detail')); 
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            array(
                'field'=>'or_hobby',
                'label'=>'or_hobby',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_keg_pengamalan_pancasila',
                'label'=>'q_keg_pengamalan_pancasila',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'pc_detail',
                'label'=>'pc_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_kerjabakti',
                'label'=>'q_kerjabakti',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_rukun_kematian',
                'label'=>'q_rukun_kematian',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_keg_keagamaan',
                'label'=>'q_keg_keagamaan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_jimpitan',
                'label'=>'q_jimpitan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_arisan',
                'label'=>'q_arisan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_gotong_royong',
                'label'=>'q_gotong_royong',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_organisasi',
                'label'=>'q_ikut_organisasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'or_detail',
                'label'=>'or_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_pelatihan',
                'label'=>'q_ikut_pelatihan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'pel_detail',
                'label'=>'pel_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "or_hoor_hobbyy"=>"",
                    "q_keg_pengamalan_pancasila"=>"",
                    "pc_detail"=>"",
                    "q_kerjabakti"=>"",
                    "q_rukun_kematian"=>"",
                    "q_keg_keagamaan"=>"",
                    "q_jimpitan"=>"",
                    "q_arisan"=>"",
                    "q_gotong_royong"=>"",
                    "q_ikut_organisasi"=>"",
                    "or_detail"=>"",
                    "q_ikut_pelatihan"=>"",
                    "pel_detail"=>""
                );

        if($this->val_form_update()){
            // $id_user_keuangan = $this->input->post("id_user_keuangan", true);

            $or_hobby = $this->input->post("or_hobby", true);
            $q_keg_pengamalan_pancasila = $this->input->post("q_keg_pengamalan_pancasila", true);
            $pc_detail = $this->input->post("pc_detail", true);
            $q_kerjabakti = $this->input->post("q_kerjabakti", true);
            $q_rukun_kematian = $this->input->post("q_rukun_kematian", true);
            $q_keg_keagamaan = $this->input->post("q_keg_keagamaan", true);
            $q_jimpitan = $this->input->post("q_jimpitan", true);
            $q_arisan = $this->input->post("q_arisan", true);
            $q_gotong_royong = $this->input->post("q_gotong_royong", true);
            $q_ikut_organisasi = $this->input->post("q_ikut_organisasi", true);
            $or_detail = $this->input->post("or_detail", true);
            $q_ikut_pelatihan = $this->input->post("q_ikut_pelatihan", true);
            $pel_detail = $this->input->post("pel_detail", true);
            
            $pel_detail = $this->input->post("pel_detail", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "or_hobby" => $or_hobby,
                    "q_keg_pengamalan_pancasila" => $q_keg_pengamalan_pancasila,
                    "pc_detail" => $pc_detail,
                    "q_kerjabakti" => $q_kerjabakti,
                    "q_rukun_kematian" => $q_rukun_kematian,
                    "q_keg_keagamaan" => $q_keg_keagamaan,
                    "q_jimpitan" => $q_jimpitan,
                    "q_arisan" => $q_arisan,
                    "q_gotong_royong" => $q_gotong_royong,
                    "q_ikut_organisasi" => $q_ikut_organisasi,
                    "or_detail" => $or_detail,
                    "q_ikut_pelatihan" => $q_ikut_pelatihan,
                    "pel_detail" => $pel_detail,
                    
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["or_hobby"]  = strip_tags(form_error('or_hobby'));
            $msg_detail["q_keg_pengamalan_pancasila"]  = strip_tags(form_error('q_keg_pengamalan_pancasila'));
            $msg_detail["pc_detail"]  = strip_tags(form_error('pc_detail'));
            $msg_detail["q_kerjabakti"]  = strip_tags(form_error('q_kerjabakti'));
            $msg_detail["q_rukun_kematian"]  = strip_tags(form_error('q_rukun_kematian'));
            $msg_detail["q_keg_keagamaan"]  = strip_tags(form_error('q_keg_keagamaan'));
            $msg_detail["q_jimpitan"]  = strip_tags(form_error('q_jimpitan'));
            $msg_detail["q_arisan"]  = strip_tags(form_error('q_arisan'));
            $msg_detail["q_gotong_royong"]  = strip_tags(form_error('q_gotong_royong'));
            $msg_detail["q_ikut_organisasi"]  = strip_tags(form_error('q_ikut_organisasi'));
            $msg_detail["or_detail"]  = strip_tags(form_error('or_detail'));
            $msg_detail["q_ikut_pelatihan"]  = strip_tags(form_error('q_ikut_pelatihan')); 

            $msg_detail["pel_detail"]  = strip_tags(form_error('pel_detail')); 
            
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
