<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pendidikanuser extends CI_Controller {
    public $title = "Pendidikan Masyarakat";

    public $tbl_main = "user_pendidikan";

    public $id_user_sess;

    

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        // $this->auth_v0_user->check_session_active();
        
        $this->id_user_sess = $this->session->userdata("ngadi_ngadi_aja")["id_user"];

        // $this->id_user_sess = 4;
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
		$data["page"] = "user_pendidikan";
		$data["title"] = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_sess);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_sess);

		$data["list_data"] = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_sess]);
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


	public function val_form_insert_data(){
        $config_val_input = array(
                array(
                    'field'=>'d_pendidikan',
                    'label'=>'d_pendidikan',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                ),array(
                    'field'=>'q_buta_huruf',
                    'label'=>'q_buta_huruf',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )  
                )
            );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_data(){
        if(isset($_POST["id_user_pendidikan"])){
            if(!$_POST["id_user_pendidikan"]){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                $msg_detail = [
                    "d_pendidikan"=>"",
                    "q_buta_huruf"=>""
                ];
                
                if($this->val_form_insert_data()){
                    $d_pendidikan = $this->input->post("d_pendidikan", true);
                    $q_buta_huruf = $this->input->post("q_buta_huruf", true);
                    
                    $type_pattern   = "allowed_general_char";
        
                    $arr_pattern  = [[$type_pattern, $q_buta_huruf]];
        
                    if($this->magic_pattern->set_list_pattern($arr_pattern )){
                        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
                    } else{
                        
                        $data = [
                            "id_user_pendidikan"=> "",
                            "id_user"=> $this->id_user_sess,
                            "d_pendidikan"=> $d_pendidikan,
                            "q_buta_huruf"=> $q_buta_huruf,
                        ];

                        // print_r("<per>");
                        // print_r($data);
        
                        $insert = $this->mm->insert_data($this->tbl_main, $data);
                        if($insert){
                            $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                        }
                       
                    }
                }else{
                    $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
                    
                    $msg_detail["d_pendidikan"]= strip_tags(form_error('d_pendidikan'));
                    $msg_detail["q_buta_huruf"]= strip_tags(form_error('q_buta_huruf'));
                    
                }
        
                $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
                print_r(json_encode($res_msg));
            }else{
                $this->update_data();
            }
        }else{
            $this->update_data();
        }
        
    }
#===============================================================================
#-----------------------------------insert_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================
    public function get_data(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("GET_FAIL"));
        $msg_detail = array();

        if(isset($_POST["id_user_pendidikan"])){
        	$id_user_pendidikan = $this->input->post('id_user_pendidikan');
        	$data = $this->mm->get_data_each($this->tbl_main, array("id_user_pendidikan"=>$id_user_pendidikan, "is_del_user"=>"0"));
        	if($data){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("GET_SUC"));
	        }
        }
        $msg_detail["list_data"] = $data;
        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------get_data------------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================

    public function val_form_update_data(){
        $config_val_input = array(
            array(
                'field'=>'d_pendidikan',
                'label'=>'d_pendidikan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            ),array(
                'field'=>'q_buta_huruf',
                'label'=>'q_buta_huruf',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )  
            )
            
        );
            
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update_data(){
        
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = [
            "id_user_pendidikan"=>"",
            "d_pendidikan"=>"",
            "q_buta_huruf"=>""
        ];

        if($this->val_form_update_data()){
        	$id_user_pendidikan 	= $this->input->post("id_user_pendidikan", true);
            $d_pendidikan           = json_decode($this->input->post("d_pendidikan", true));
            $q_buta_huruf           = $this->input->post("q_buta_huruf", true);
            

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $id_user_pendidikan]];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
               
                $set = array(
                    "d_pendidikan"=>json_encode($d_pendidikan),
                    "q_buta_huruf"=>$q_buta_huruf
                );
                $where = array("id_user_pendidikan"=>$id_user_pendidikan);
                
                // print_r("<per>");
                // print_r($set);

                $update = $this->mm->update_data($this->tbl_main, $set, $where);
                if($update){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
          	
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user_pendidikan"]= strip_tags(form_error('id_user_pendidikan'));
            $msg_detail["d_pendidikan"]= strip_tags(form_error('d_pendidikan'));
            $msg_detail["q_buta_huruf"]= strip_tags(form_error('q_buta_huruf'));                    
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_data--------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

    public function delete_admin(){
    	$msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "id_user_pendidikan"=>"",
                );

        if($_POST["id_user_pendidikan"]){
        	$id_user_pendidikan = $this->input->post("id_user_pendidikan");
        	
            $where = array("id_user_pendidikan"=>$id_user_pendidikan);

        	// $delete_admin = $this->mm->delete_data($this->tbl_main, array("id_user_pendidikan"=>$id_user_pendidikan));
        	$delete_admin = $this->mm->delete_data($this->tbl_main, $where);
            
            if($delete_admin){
        		$msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("DELETE_SUC"));
        	}
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["id_user_pendidikan"]= strip_tags(form_error('id_user_pendidikan'));        
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------delete_admin--------------------------------
#===============================================================================

}
