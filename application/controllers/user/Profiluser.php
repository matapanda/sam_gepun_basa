<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profiluser extends CI_Controller {
    public $title = "Data Kependudukan";
    public $tbl_main = "user_data";
    public $tbl_user = "user";

    public $id_user_sess;
	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        $id_user 	       = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
		$data["page"]      = "profil";
        $data["title"]     = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

		$data["list_data"] = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$id_user));
		$data["nik_kk"]    = $this->mm->get_data_each($this->tbl_user, array("id_user"=>$id_user));
        
        $data_kel_kec = [];
        $kec = $this->mm->get_data_all_where("ms_distrik_pem", ["distrik_pem_id"=>"0"]);
        foreach ($kec as $key => $value) {
            $data_kel_kec[$value->id_distrik_pem]["main"] = $value;
            $data_kel_kec[$value->id_distrik_pem]["item"] = [];
        }

        $kel = $this->mm->get_data_all_where("ms_distrik_pem", ["distrik_pem_id !="=>"0"]);
        foreach ($kel as $key => $value) {
            $data_kel_kec[$value->distrik_pem_id]["item"][$value->id_distrik_pem] = $value;
        }
        // print_r("<pre>");
        // print_r($data_kel_kec);
        // die();
        $data["data_kel_kec"] = $data_kel_kec;

        // $data["select_ktp_kec_user_data"] = $this->mm->get_data_all_where("ms_distrik_pem", array("distrik_pem_id"=>"0"));
        // $data["select_ktp_kel_user_data"] = $this->mm->get_data_all_where("ms_distrik_pem", array("distrik_pem_id !="=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
    public function get_ktp_kec_user_data(){
        // print_r($_GET);
        $param_ktp_kec_user_data = "";
        $opsi_ktp_kel_user_data = "<option value='' selected>Pilih ktp_kel_user_dataurahan</option>";
        if(isset($_GET["param_ktp_kec_user_data"])){
                $param_ktp_kec_user_data = $_GET["param_ktp_kec_user_data"];
        }

        //ktp_kel_user_dataurahan
        $data_ktp_kec_user_data = $this->mm->get_data_all_where("ms_distrik_pem", ["distrik_pem_id"=>$param_ktp_kec_user_data]);
        foreach ($data_ktp_kec_user_data as $key => $value) {
            $id_distrik_pem = $value->id_distrik_pem;
            $nama_distrik_pem = $value->nama_distrik_pem;
            $opsi_ktp_kel_user_data .= "<option value='".$id_distrik_pem."'>".$nama_distrik_pem."</option>";
        }
        print_r($opsi_ktp_kel_user_data);
    }

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

    public function val_form_update(){
        $config_val_input = array(
            array(
                'field'=>'ds_nama_dasawisma',
                'label'=>'nama lengkap',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'nik_user',
                'label'=>'NIK',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'kk_user',
                'label'=>'KK',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ms_jk',
                'label'=>'jenis kelamin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ms_tgl_lhr',
                'label'=>'tanggal lahir',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ms_tmp_lhr',
                'label'=>'tempat lahir',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),
            array(
                'field'=>'ms_sts_pernikahan',
                'label'=>'status kawin',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'srt_nikah_no',
                'label'=>'nomor akta nikah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'srt_akta_cerai',
                'label'=>'nomor akta cerai',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'srt_kematian_pasangan',
                'label'=>'nomor akta cerai (mati)',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'q_akte_user',
                'label'=>'akta',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'akte_no_user',
                'label'=>'nomor akta kelahiran',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'akte_alasan_user',
                'label'=>'keterangan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'wn_no_paspor',
                'label'=>'nomor paspor',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ms_wn',
                'label'=>'kewarganegaraan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ms_suku',
                'label'=>'suku bangsa',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ms_agama',
                'label'=>'agama',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ms_gol_darah',
                'label'=>'golongan darah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),

            // ALAMAT KTP
            array(
                'field'=>'ktp_alamat_user_data',
                'label'=>'Alamat KTP',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ktp_provinsi',
                'label'=>'provinsi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ktp_kota_user_data',
                'label'=>'kota',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ktp_kec_user_data',
                'label'=>'kecamatan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ktp_kel_user_data',
                'label'=>'kelurahan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),
            array(
                'field'=>'ktp_rt_user_data',
                'label'=>'RT',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'ktp_rw_user_data',
                'label'=>'RW',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),

            // ALAMAT DOMISILI
            array(
                'field'=>'d_alamat_user_data',
                'label'=>'alamat',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_provinsi',
                'label'=>'provinsi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_kota_user_data',
                'label'=>'kota',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_kec_user_data',
                'label'=>'kecamatan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_kel_user_data',
                'label'=>'kelurahan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_rt_user_data',
                'label'=>'RT',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),
            array(
                'field'=>'d_rw_user_data',
                'label'=>'RW',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                )       
            ),

            // array(
            //     'field'=>'ms_pd_terakhir',
            //     'label'=>'ms_pd_terakhir',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            // array(
            //     'field'=>'akte_alasan_user',
            //     'label'=>'akte_alasan_user',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            // array(
            //     'field'=>'q_sts_tinggal',
            //     'label'=>'q_sts_tinggal',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            // array(
            //     'field'=>'q_lama_tinggal',
            //     'label'=>'q_lama_tinggal',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            // array(
            //     'field'=>'q_ingin_tinggal',
            //     'label'=>'q_ingin_tinggal',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            // array(
            //     'field'=>'wn_almt_user_data',
            //     'label'=>'wn_almt_user_data',
            //     'rules'=>'required',
            //     'errors'=>array(
            //         'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
            //     )       
            // ),
            
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    // DATA PERSONAL
                    "ds_nama_dasawisma"=>"",
                    "nik_user"=>"",
                    "kk_user"=>"",
                    "ms_jk"=>"",
                    "ms_tgl_lhr"=>"",
                    "ms_tmp_lhr"=>"",
                    "ms_sts_pernikahan"=>"",
                    "srt_nikah_no"=>"",
                    "srt_akta_cerai"=>"",
                    "srt_kematian_pasangan"=>"",
                    "q_akte_user"=>"",
                    "akte_no_user"=>"",
                    "akte_alasan_user"=>"",
                    "wn_no_paspor"=>"",
                    "ms_wn"=>"",
                    "ms_suku"=>"",
                    "ms_agama"=>"",
                    "ms_gol_darah"=>"",

                    // ALAMAT KTP
                    "ktp_alamat_user_data"=>"",
                    "ktp_provinsi"=>"",
                    "ktp_kota_user_data"=>"",
                    "ktp_kec_user_data"=>"",
                    "ktp_kel_user_data"=>"",
                    "ktp_rt_user_data"=>"",
                    "ktp_rw_user_data"=>"",

                    // ALAMAT DOMISILI
                    "d_alamat_user_data"=>"",
                    "d_provinsi"=>"",
                    "d_kota_user_data"=>"",
                    "d_kec_user_data"=>"",
                    "d_kel_user_data"=>"",
                    "d_rt_user_data"=>"",
                    "d_rw_user_data"=>"",
                    
                    // "ms_pd_terakhir"=>"",
                    // "akte_alasan_user"=>"",
                    // "q_sts_tinggal"=>"",
                    // "q_lama_tinggal"=>"",
                    // "q_ingin_tinggal"=>"",
                    // "wn_almt_user_data"=>"",
                );

        if($this->val_form_update()){
        	$id_user 	  = $this->session->userdata("ngadi_ngadi_aja")["id_user"];

            $ds_nama_dasawisma 	    = $this->input->post("ds_nama_dasawisma", true);
            $nik_user 	            = $this->input->post("nik_user", true);
            $kk_user 	            = $this->input->post("kk_user", true);
            $ms_jk 	                = $this->input->post("ms_jk", true);

            $tgl_lhr_raw 		    = $this->input->post("ms_tgl_lhr", true);
            $ms_tgl_lhr 		    = explode("-",$tgl_lhr_raw)[2]."-".explode("-",$tgl_lhr_raw)[1]."-".explode("-",$tgl_lhr_raw)[0];
            
            $ms_tmp_lhr 		    = $this->input->post("ms_tmp_lhr", true);
            $ms_sts_pernikahan 	    = $this->input->post("ms_sts_pernikahan", true);
            $srt_nikah_no 	        = $this->input->post("srt_nikah_no", true);
            $srt_akta_cerai 	    = $this->input->post("srt_akta_cerai", true);
            $srt_kematian_pasangan 	= $this->input->post("srt_kematian_pasangan", true);
            $q_akte_user 	        = $this->input->post("q_akte_user", true);
            $akte_no_user 	        = $this->input->post("akte_no_user", true);
            $akte_alasan_user 	    = $this->input->post("akte_alasan_user", true);
            $wn_no_paspor 	        = $this->input->post("wn_no_paspor", true);
            $ms_wn 	                = $this->input->post("ms_wn", true);
            $ms_suku 	            = $this->input->post("ms_suku", true);
            $ms_agama 	            = $this->input->post("ms_agama", true);
            $ms_gol_darah 	        = $this->input->post("ms_gol_darah", true);
            
            $ktp_alamat_user_data 	= $this->input->post("ktp_alamat_user_data", true);
            $ktp_provinsi 	        = $this->input->post("ktp_provinsi", true);
            $ktp_kota_user_data 	= $this->input->post("ktp_kota_user_data", true);
            $ktp_kec_user_data 	    = $this->input->post("ktp_kec_user_data", true);
            $ktp_kel_user_data 		= $this->input->post("ktp_kel_user_data", true);
            $ktp_rt_user_data 	    = $this->input->post("ktp_rt_user_data", true);
            $ktp_rw_user_data 	    = $this->input->post("ktp_rw_user_data", true);
            
            $d_alamat_user_data 	= $this->input->post("d_alamat_user_data", true);
            $d_provinsi 	        = $this->input->post("d_provinsi", true);
            $d_kota_user_data 	    = $this->input->post("d_kota_user_data", true);
            $d_kec_user_data 	    = $this->input->post("d_kec_user_data", true);
            $d_kel_user_data 	    = $this->input->post("d_kel_user_data", true);
            $d_rt_user_data 	    = $this->input->post("d_rt_user_data", true);
            $d_rw_user_data 	    = $this->input->post("d_rw_user_data", true);

            // $ms_pd_terakhir 	= $this->input->post("ms_pd_terakhir", true);
            // $akte_alasan_user 	= $this->input->post("akte_alasan_user", true);
            // $q_sts_tinggal 	= $this->input->post("q_sts_tinggal", true);
            // $q_lama_tinggal 	= $this->input->post("q_lama_tinggal", true);
            // $q_ingin_tinggal 	= $this->input->post("q_ingin_tinggal", true);
            // $wn_almt_user_data 	= $this->input->post("wn_almt_user_data", true);

            $crt_time 	= $this->input->post("crt_time", true);

            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [
                                [$type_pattern, $nik_user],
                                // [$type_pattern, $nik_user],
                                // [$type_pattern, $kk_user],
                                // [$type_pattern, $ms_jk],
                                // [$type_pattern, $ms_tgl_lhr],
                                // [$type_pattern, $ms_tmp_lhr],
                                // [$type_pattern, $ms_sts_pernikahan],
                                // [$type_pattern, $srt_nikah_no],
                                // [$type_pattern, $srt_akta_cerai],
                                // [$type_pattern, $srt_kematian_pasangan],
                                // [$type_pattern, $q_akte_user],
                                // [$type_pattern, $akte_no_user],
                                // [$type_pattern, $akte_alasan_user],
                                // [$type_pattern, $wn_no_paspor],
                                // [$type_pattern, $ms_wn],
                                // [$type_pattern, $ms_suku],
                                // [$type_pattern, $ms_agama],
                                // [$type_pattern, $ms_gol_darah],
                                
                                // [$type_pattern, $ktp_alamat_user_data],
                                // [$type_pattern, $ktp_provinsi],
                                // [$type_pattern, $ktp_kota_user_data],
                                // [$type_pattern, $ktp_kec_user_data],
                                // [$type_pattern, $ktp_kel_user_data],
                                // [$type_pattern, $ktp_rt_user_data],
                                // [$type_pattern, $ktp_rw_user_data],

                                // [$type_pattern, $d_alamat_user_data],
                                // [$type_pattern, $d_provinsi],
                                // [$type_pattern, $d_kota_user_data],
                                // [$type_pattern, $d_kec_user_data],
                                // [$type_pattern, $d_kel_user_data],
                                // [$type_pattern, $d_rt_user_data],
                                // [$type_pattern, $d_rw_user_data],
                                
                                // [$type_pattern, $ms_pd_terakhir],
                                // [$type_pattern, $akte_alasan_user],
                                // [$type_pattern, $q_sts_tinggal],
                                // [$type_pattern, $q_lama_tinggal],
                                // [$type_pattern, $q_ingin_tinggal],
                                // [$type_pattern, $wn_almt_user_data],
                            ];


            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek            = $this->cr->cek_data($this->tbl_main, "id_user", $id_user);
                
                // UPDATE NIK & KK USER
                $where_user     = array("id_user" => $id_user);
                $nik_kk         = array("nik_user" => $nik_user, "kk_user" => $kk_user);
                $this->mm->update_data($this->tbl_user, $nik_kk, $where_user);

                if ($cek->num_rows()>0) {
                    $set = array(
                        "ds_nama_dasawisma"     => $ds_nama_dasawisma,
                        "ms_jk"                 => $ms_jk,
                        "ms_tgl_lhr"            => $ms_tgl_lhr,
                        "ms_tmp_lhr"            => $ms_tmp_lhr,
                        "ms_sts_pernikahan"     => $ms_sts_pernikahan,
                        "srt_nikah_no"          => $srt_nikah_no,
                        "srt_akta_cerai"        => $srt_akta_cerai,
                        "srt_kematian_pasangan" => $srt_kematian_pasangan,
                        "q_akte_user"           => $q_akte_user,
                        "akte_no_user"          => $akte_no_user,
                        "akte_alasan_user"      => $akte_alasan_user,
                        "wn_no_paspor"          => $wn_no_paspor,
                        "ms_wn"                 => $ms_wn,
                        "ms_suku"               => $ms_suku,
                        "ms_agama"              => $ms_agama,
                        "ms_gol_darah"          => $ms_gol_darah,
                        
                        "ktp_alamat_user_data"  => $ktp_alamat_user_data,
                        "ktp_provinsi"          => $ktp_provinsi,
                        "ktp_kota_user_data"    => $ktp_kota_user_data,
                        "ktp_kec_user_data"     => $ktp_kec_user_data,
                        "ktp_kel_user_data"     => $ktp_kel_user_data,
                        "ktp_rt_user_data"      => $ktp_rt_user_data,
                        "ktp_rw_user_data"      => $ktp_rw_user_data,
                        
                        "d_alamat_user_data"    => $d_alamat_user_data,
                        "d_provinsi"            => $d_provinsi,
                        "d_kota_user_data"      => $d_kota_user_data,
                        "d_kec_user_data"       => $d_kec_user_data,
                        "d_kel_user_data"       => $d_kel_user_data,
                        "d_rt_user_data"        => $d_rt_user_data,
                        "d_rw_user_data"        => $d_rw_user_data,

                        // "ms_pd_terakhir"    => $ms_pd_terakhir,
                        // "akte_alasan_user"=>$akte_alasan_user,
                        // "q_sts_tinggal"=>$q_sts_tinggal,
                        // "q_lama_tinggal"=>$q_lama_tinggal,
                        // "q_ingin_tinggal"=>$q_ingin_tinggal,
                        // "wn_almt_user_data"=>$wn_almt_user_data,

                        "r_up_by"   => $id_user,
                        "r_up_time" => $crt_time,
                    );
                    $where = array("id_user"=>$id_user);
                    $update = $this->mm->update_data($this->tbl_main, $set, $where);
                    if($update){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                    }
                }else{
                    $data = [
                        "id_user_data"          => "",
                        "id_user"               => $id_user,
                        "ds_nama_dasawisma"     => $ds_nama_dasawisma,
                        "ms_jk"                 => $ms_jk,
                        "ms_tgl_lhr"            => $ms_tgl_lhr,
                        "ms_tmp_lhr"            => $ms_tmp_lhr,
                        "ms_sts_pernikahan"     => $ms_sts_pernikahan,
                        "srt_nikah_no"          => $srt_nikah_no,
                        "srt_akta_cerai"        => $srt_akta_cerai,
                        "srt_kematian_pasangan" => $srt_kematian_pasangan,
                        "q_akte_user"           => $q_akte_user,
                        "akte_no_user"          => $akte_no_user,
                        "akte_alasan_user"      => $akte_alasan_user,
                        "wn_no_paspor"          => $wn_no_paspor,
                        "ms_wn"                 => $ms_wn,
                        "ms_suku"               => $ms_suku,
                        "ms_agama"              => $ms_agama,
                        "ms_gol_darah"          => $ms_gol_darah,

                        "ktp_alamat_user_data"  => $ktp_alamat_user_data,
                        "ktp_provinsi"          => $ktp_provinsi,
                        "ktp_kota_user_data"    => $ktp_kota_user_data,
                        "ktp_kec_user_data"     => $ktp_kec_user_data,
                        "ktp_kel_user_data"     => $ktp_kel_user_data,
                        "ktp_rt_user_data"      => $ktp_rt_user_data,
                        "ktp_rw_user_data"      => $ktp_rw_user_data,

                        "d_alamat_user_data"    => $d_alamat_user_data,
                        "d_provinsi"            => $d_provinsi,
                        "d_kota_user_data"      => $d_kota_user_data,
                        "d_kec_user_data"       => $d_kec_user_data,
                        "d_kel_user_data"       => $d_kel_user_data,
                        "d_rt_user_data"        => $d_rt_user_data,
                        "d_rw_user_data"        => $d_rw_user_data,

                        // "ms_pd_terakhir"=>$ms_pd_terakhir,
                        // "akte_alasan_user"=>$akte_alasan_user,
                        // "q_sts_tinggal"=>$q_sts_tinggal,
                        // "q_lama_tinggal"=>$q_lama_tinggal,
                        // "q_ingin_tinggal"=>$q_ingin_tinggal,
                        // "wn_almt_user_data"=>$wn_almt_user_data,

                        "r_crt_by"   => $id_user,
                        "r_crt_time" => $crt_time,
                    ];
                    //test
                    // $msg_main = array("status"=>true, "msg"=>$_POST);
                    $insert = $this->mm->insert_data($this->tbl_main, $data);
                    if($insert){
                        $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                    }
                }
            }
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            $msg_detail["ds_nama_dasawisma"]    = strip_tags(form_error('ds_nama_dasawisma'));
            $msg_detail["nik_user"]             = strip_tags(form_error('nik_user'));
            $msg_detail["kk_user"]              = strip_tags(form_error('kk_user'));
            $msg_detail["ms_jk"]                = strip_tags(form_error('ms_jk'));
            $msg_detail["ms_tgl_lhr"] 	        = strip_tags(form_error('ms_tgl_lhr'));
            $msg_detail["ms_tmp_lhr"] 		    = strip_tags(form_error('ms_tmp_lhr'));
            $msg_detail["ms_sts_pernikahan"] 	= strip_tags(form_error('ms_sts_pernikahan')); 
            $msg_detail["srt_nikah_no"] 	    = strip_tags(form_error('srt_nikah_no'));
            $msg_detail["srt_akta_cerai"] 	    = strip_tags(form_error('srt_akta_cerai'));
            $msg_detail["srt_kematian_pasangan"]= strip_tags(form_error('srt_kematian_pasangan'));
            $msg_detail["q_akte_user"] 	        = strip_tags(form_error('q_akte_user'));
            $msg_detail["akte_no_user"] 	    = strip_tags(form_error('akte_no_user'));
            $msg_detail["akte_alasan_user"] 	= strip_tags(form_error('akte_alasan_user'));
            $msg_detail["wn_no_paspor"] 	    = strip_tags(form_error('wn_no_paspor'));
            $msg_detail["ms_wn"] 	            = strip_tags(form_error('ms_wn'));    
            $msg_detail["ms_suku"] 	            = strip_tags(form_error('ms_suku'));
            $msg_detail["ms_agama"] 	        = strip_tags(form_error('ms_agama')); 
            $msg_detail["ms_gol_darah"] 	    = strip_tags(form_error('ms_gol_darah')); 

            $msg_detail["ktp_alamat_user_data"] = strip_tags(form_error('ktp_alamat_user_data'));
            $msg_detail["ktp_provinsi"] 	    = strip_tags(form_error('ktp_provinsi'));
            $msg_detail["ktp_kota_user_data"] 	= strip_tags(form_error('ktp_kota_user_data'));
            $msg_detail["ktp_kec_user_data"] 	= strip_tags(form_error('ktp_kec_user_data'));
            $msg_detail["ktp_kel_user_data"] 	= strip_tags(form_error('ktp_kel_user_data'));
            $msg_detail["ktp_rt_user_data"] 	= strip_tags(form_error('ktp_rt_user_data'));
            $msg_detail["ktp_rw_user_data"] 	= strip_tags(form_error('ktp_rw_user_data'));
            
            $msg_detail["d_alamat_user_data"] 	= strip_tags(form_error('d_alamat_user_data')); 
            $msg_detail["d_provinsi"] 	        = strip_tags(form_error('d_provinsi')); 
            $msg_detail["d_kota_user_data"] 	= strip_tags(form_error('d_kota_user_data'));
            $msg_detail["d_kec_user_data"] 	    = strip_tags(form_error('d_kec_user_data')); 
            $msg_detail["d_kel_user_data"] 	    = strip_tags(form_error('d_kel_user_data'));
            $msg_detail["d_rt_user_data"] 	    = strip_tags(form_error('d_rt_user_data'));
            $msg_detail["d_rw_user_data"] 	    = strip_tags(form_error('d_rw_user_data'));
            
            // $msg_detail["ms_pd_terakhir"] 	= strip_tags(form_error('ms_pd_terakhir')); 
            // $msg_detail["akte_alasan_user"] 	= strip_tags(form_error('akte_alasan_user'));
            // $msg_detail["q_sts_tinggal"] 	= strip_tags(form_error('q_sts_tinggal'));
            // $msg_detail["q_lama_tinggal"] 	= strip_tags(form_error('q_lama_tinggal'));
            // $msg_detail["q_ingin_tinggal"] 	= strip_tags(form_error('q_ingin_tinggal'));      
            // $msg_detail["wn_almt_user_data"] 	= strip_tags(form_error('wn_almt_user_data'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
