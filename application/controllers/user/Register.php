<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {
    public $title = "Register Masyarakat";
    public $tbl_main;

	public function __construct(){
		parent::__construct();
		$this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');

		$this->load->library("response_message");
        $this->load->library("magic_pattern");
        // $this->auth_v0->auth_login();

        $this->tbl_main = "user";
        $this->load->helper('string');
	}

	public function index(){
        // print_r($_SESSION);
        $data["page"] = "register_new";
        $data["title"] = $this->title;
		$this->load->view('user/register_new', $data);
	}

	private function val_form_insert_register(){
        $config_val_input = array(
                array(
                    'field'=>'nama_user',
                    'label'=>'nama_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                       
                ),
                array(
                    'field'=>'nik_user',
                    'label'=>'nik_user',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s harus berupa angka"
                    )
                       
                ),
                array(
                    'field'=>'kk_user',
                    'label'=>'kk_user',
                    'rules'=>'required|numeric',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'numeric'=>"%s harus berupa angka"
                    )
                       
                ),
                array(
                    'field'=>'tlp_user',
                    'label'=>'tlp_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                        
                ),
                array(
                    'field'=>'email_user',
                    'label'=>'email_user',
                    'rules'=>'required|valid_email',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'valid_email'=>"%s tidak sesuai format"
                    )
                        
                ),
                array(
                    'field'=>'username_user',
                    'label'=>'username_user',
                    'rules'=>'required',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                    )
                        
                ),
                array(
                    'field'=>'password_user',
                    'label'=>'password_user',
                    'rules'=>'required|min_length[5]|max_length[20]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'min_length'=>"%s Minimal 5 karakter",
                        'max_length'=>"%s Maximal 20 karakter"
                    )      
                ),
                array(
                    'field'=>'repassword',
                    'label'=>'repassword',
                    'rules'=>'required|matches[password_user]',
                    'errors'=>array(
                        'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED"),
                        'matches'=>"%s ".$this->response_message->get_error_msg("RE_PASSWORD_FAIL")
                    )      
                ),
            );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert_register(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = [
            "id_user"=>"",
            "email_user"=>"",
            "username_user"=>"",
            "password_user"=>"",
            "nama_user"=>"",
            "nik_user"=>"",
            "kk_user"=>"",
            "tlp_user"=>""
        ];
               

        // print_r($_POST);
        // die();
        if($this->val_form_insert_register()){
            $captcha = $this->input->post("captcha", true);
            $email_user = $this->input->post("email_user", true);
            $username_user = $this->input->post("username_user", true);
            $password_user = $this->input->post("password_user", true);
            $nama_user = $this->input->post("nama_user", true);
            $nik_user = $this->input->post("nik_user", true);
            $kk_user = $this->input->post("kk_user", true);
            $tlp_user = $this->input->post("tlp_user", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern  = [[$type_pattern, $email_user],
                                [$type_pattern, $username_user],
                                [$type_pattern, $password_user],
                                [$type_pattern, $nama_user],
                                [$type_pattern, $nik_user],
                                [$type_pattern, $kk_user],
                                [$type_pattern, $tlp_user],
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else{
                $cek = $this->cr->cek_data($this->tbl_main, "username_user", $username_user);
                if ($cek->num_rows()>0) {
                    $msg_main = array("status"=>false, "msg"=>"Username ".$username_user." Sudah terdaftar pada sistem");
                } else {
                    $secret = '6LcA-ewUAAAAAFuTZIIDrBgzrNUsi2dGjgDRVP4M';
                    $verifyResponse = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$captcha);
                    $responseData = json_decode($verifyResponse);
                    if($cek)
                    {
                        $kode_aktifasi = random_string('alnum',50);
                        $data = [
                            "id_user"=> 0,
                            "email_user"=> $email_user,
                            "username_user"=> $username_user,
                            "password_user"=> hash("sha256", $password_user),
                            "nama_user"=> $nama_user,
                            "nik_user"=> $nik_user,
                            "kk_user"=> $kk_user,
                            "tlp_user"=> $tlp_user,
                            "status_active_user" => 1,
                            "kode_aktifasi" => $kode_aktifasi
                            

                        ];

                       
                        //test
                        // $msg_main = array("status"=>true, "msg"=>$_POST);
                        $insert = $this->mm->insert_data($this->tbl_main, $data);
                        if($insert){
                            $msg_main = array("status"=>true, "msg"=>$this->_email($email_user, $kode_aktifasi));
                            // 
                        }
                    }
                    else{
                        $msg_main = array("status"=>false, "msg"=>"Captcha Salah!");
                    }
                }
            }
                
            
        }else{
            $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
            
            $msg_detail["email_user"]= strip_tags(form_error('email_user'));

            $msg_detail["username_user"]= strip_tags(form_error('username_user'));
            
            $msg_detail["password_user"]= strip_tags(form_error('password_user'));
            $msg_detail["nama_user"]= strip_tags(form_error('nama_user'));
            $msg_detail["nik_user"]= strip_tags(form_error('nik_user'));
            $msg_detail["kk_user"]= strip_tags(form_error('kk_user'));
            $msg_detail["tlp_user"]= strip_tags(form_error('tlp_user'));
            $msg_detail["repassword"]= strip_tags(form_error('repassword'));

        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }

    public function aktifasi()
    {
        $code   = $this->input->get('code');
        $email  = $this->input->get('email_user');

        if (strlen($code)==50 && !empty($email))
        {
            $this->load->model('model_activate');

            if ($this->model_activate->activate($code, $email))
            {
                $this->session->set_flashdata('info', '<script>swal({title: "Success", text: "Your account has been activated", timer: 10000, icon: "success", button: false});</script>');
                redirect(base_url(), 'refresh');
            }
            else
            {
                $this->session->set_flashdata('info', '<script>swal({title: "Error", text: "Your activation code link is incorrect", timer: 10000, icon: "error", button: false});</script>');
                redirect(base_url(), 'refresh');
            }
        }
        else if(empty($code) && empty($email))
        {
            show_404();
        }
        else
        {
            $this->session->set_flashdata('info', '<script>swal({title: "Error", text: "Your activation code link is incorrect", timer: 10000, icon: "error", button: false});</script>');
            redirect(base_url(), 'refresh');
        }
    }

    function _email($email,$kode_aktifasi){
         $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'meilinda41@gmail.com', // change it to yours
          'smtp_pass' => 'Leona311', // change it to yours
          'mailtype' => 'html',
          'charset' => 'iso-8859-1',
          'wordwrap' => TRUE
        );

              $html = '
            <div style="margin-bottom: 20px; font-weight: bold;">Hello There,</div>

            <div>Terima kasih telah bergabung dengan SAM GEPUN BASA KOTA MALANG.</div>
            <div style="margin-bottom: 10px;">To activate your account, please click this link and we will redirect you to login page: <a href="'.base_url('activate?code='.$kode_aktifasi.'&email_user='.$email).'">Confirm my email address</a>.
            </div>
            <div>If that link cannot working you can access this URL from your browser</div>
            <div><a href="'.base_url('activate?code='.$kode_aktifasi.'&email_user='.$email).'">'.base_url('activate?code='.$kode_aktifasi.'&email_user='.$email).'</a></div>

            <div style="margin-bottom: 20px; margin-top: 10px;">If you have a question reply this email and we will contact you as soon as possible, thank you!</div>
        ';

              $this->load->library('email', $config);
              $this->email->set_newline("\r\n");
              $this->email->from('meilinda41@gmail.com'); // change it to yours
              $this->email->to($email);// change it to yours
              $this->email->subject('Verifikasi Email');
              $this->email->message($html);
              if($this->email->send())
             {
              
             }
             else
            {
          
            }   
    }


}
