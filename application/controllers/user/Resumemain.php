<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Resumemain extends CI_Controller {
    public $title = "Beranda";
    public $tbl_main = "user";

    public $sts_list_data;

    public $id_user_ses;

    public $date_now;

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];

        // print_r("<pre>");
        // print_r($_SESSION);
        // die();
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
        
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){

		$data["page"] = "resume_main";
        $data["title"] = $this->title;

        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

        $data["user"] = $this->mm->get_data_each("user", ["id_user"=> $this->id_user_ses]);
        $data["user_data"] = $this->mm->get_data_each("user_data", ["id_user"=> $this->id_user_ses]);

        $data["user_kerja"] = $this->mm->get_data_each("user_kerja", ["id_user"=> $this->id_user_ses]);
        $data["user_kesehatan"] = $this->mm->get_data_each("user_kesehatan", ["id_user"=> $this->id_user_ses]);
        $data["user_keuangan"] = $this->mm->get_data_each("user_keuangan", ["id_user"=> $this->id_user_ses]);
        $data["user_or"] = $this->mm->get_data_each("user_or", ["id_user"=> $this->id_user_ses]);
        $data["user_pendidikan"] = $this->mm->get_data_each("user_pendidikan", ["id_user"=> $this->id_user_ses]);
        $data["user_sosial"] = $this->mm->get_data_each("user_sosial", ["id_user"=> $this->id_user_ses]);
        $data["user_tmpt"] = $this->mm->get_data_each("user_tmpt", ["id_user"=> $this->id_user_ses]);
        $data["user_transport"] = $this->mm->get_data_each("user_transport", ["id_user"=> $this->id_user_ses]);
        // print_r("<pre>");
        // print_r($data);
        // die();
		// $data["list_data"] = $this->mm->get_data_all_where("admin", array("is_delete"=>"0"));
		$this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================


#===============================================================================
#-----------------------------------submit_data---------------------------------
#===============================================================================

    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'ck_profile',
                'label'=>'ck_profile',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_kerja',
                'label'=>'ck_user_kerja',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'ck_user_kesehatan',
                'label'=>'ck_user_kesehatan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'ck_user_keuangan',
                'label'=>'ck_user_keuangan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_or',
                'label'=>'ck_user_or',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_pendidikan',
                'label'=>'ck_user_pendidikan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_sosial',
                'label'=>'ck_user_sosial',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_tmpt',
                'label'=>'ck_user_tmpt',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ck_user_transport',
                'label'=>'ck_user_transport',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    function insert(){

        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(   
                    "ck_profile"=>"",
                    "ck_user_kerja"=>"",
                    "ck_user_kesehatan"=>"",
                    "ck_user_keuangan"=>"",
                    "ck_user_or"=>"",
                    "ck_user_pendidikan"=>"",
                    "ck_user_sosial"=>"",
                    "ck_user_tmpt"=>"",
                    "ck_user_transport"=>""
                );

        if($this->val_form_insert()){
            
            $ck_profile 	    = $this->input->post("ck_profile", true);
            $ck_user_kerja 	    = $this->input->post("ck_user_kerja", true);
            $ck_user_kesehatan 	= $this->input->post("ck_user_kesehatan", true);
            $ck_user_keuangan 	= $this->input->post("ck_user_keuangan", true);
            $ck_user_or 	    = $this->input->post("ck_user_or", true);
            $ck_user_pendidikan = $this->input->post("ck_user_pendidikan", true);
            $ck_user_sosial 	= $this->input->post("ck_user_sosial", true);
            $ck_user_tmpt 	    = $this->input->post("ck_user_tmpt", true);
            $ck_user_transport 	= $this->input->post("ck_user_transport", true);
            
            $where = ["id_user"=>$this->id_user_ses];
            $set = [
                "sts_submit" => "1"
            ];

            $inject_list = $this->injectlist->check_list_data($this->id_user_ses);
            if($inject_list){
                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }else{
                $msg_main = array("status"=>false, "msg"=>"Proses Submit gagal, Mohon lengkapi dulu data yg tersedia pada form isian.!");
            }
            
        }else {
            $msg_main = array("status" => false, "msg" => "Centang seluruh persetujuan untuk melaukan submit data");

            $msg_detail["ck_profile"]  = strip_tags(form_error('ck_profile'));
            $msg_detail["ck_user_kerja"]  = strip_tags(form_error('ck_user_kerja'));
            $msg_detail["ck_user_kesehatan"]  = strip_tags(form_error('ck_user_kesehatan'));
            $msg_detail["ck_user_keuangan"]  = strip_tags(form_error('ck_user_keuangan'));
            $msg_detail["ck_user_or"]  = strip_tags(form_error('ck_user_or'));
            $msg_detail["ck_user_pendidikan"]  = strip_tags(form_error('ck_user_pendidikan'));
            $msg_detail["ck_user_sosial"]  = strip_tags(form_error('ck_user_sosial'));
            $msg_detail["ck_user_tmpt"]  = strip_tags(form_error('ck_user_tmpt'));
            $msg_detail["ck_user_transport"]  = strip_tags(form_error('ck_user_transport'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
        


    }
#===============================================================================
#-----------------------------------submit_data---------------------------------
#===============================================================================

}
