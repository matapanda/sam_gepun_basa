<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sosialuser extends CI_Controller {
    public $title = "Kegiatan Sosial Masyarakat";
    public $tbl_main = "user_sosial";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "MAKANAN_POKOK"=>"80",
        "SKALA_USAHA"=>"37",
        "BIDANG_USAHA"=>"81"
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        
		$data["page"]       = "user_sosial";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);

		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$this->id_user_ses));
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);
		
        $data["makanan_pokok"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["MAKANAN_POKOK"]]);
        $data["skala_usaha"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["SKALA_USAHA"]]);
        $data["bidang_usaha"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["BIDANG_USAHA"]]);
        $this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();
        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'q_anggota_dasawisma',
                'label'=>'q_anggota_dasawisma',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'dw_nama',
                'label'=>'dw_nama',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_activitas_pyd',
                'label'=>'q_activitas_pyd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'pyd_frequensi',
                'label'=>'pyd_frequensi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_program_bina_balita',
                'label'=>'q_program_bina_balita',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_kel_belajar',
                'label'=>'q_ikut_kel_belajar',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_paud',
                'label'=>'q_paud',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ikut_kooperasi',
                'label'=>'q_ikut_kooperasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_punya_tabungan',
                'label'=>'q_punya_tabungan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_makan_pokok',
                'label'=>'q_makan_pokok',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_punya_usaha',
                'label'=>'q_punya_usaha',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    // "id_user_sosial" => "",
                    // "id_user" => $this->id_user_ses,
                    "q_anggota_dasawisma" => "",
                    "dw_nama" => "",
                    "q_activitas_pyd" => "",
                    "pyd_frequensi" => "",
                    "q_program_bina_balita" => "",
                    "q_ikut_kel_belajar" => "",
                    "q_paud" => "",
                    "q_ikut_kooperasi" => "",
                    "q_punya_tabungan" => "",
                    "q_makan_pokok" => "",
                    "q_punya_usaha" => "",
                    "usaha_skala" => "",
                    "usaha_no" => "",
                    "usaha_bidang" => ""
                );

        if($this->val_form_insert()){
            // $id_user_sosial = $this->input->post("id_user_sosial", true);

            $q_anggota_dasawisma = $this->input->post("q_anggota_dasawisma", true);
            $dw_nama = $this->input->post("dw_nama", true);
            $q_activitas_pyd = $this->input->post("q_activitas_pyd", true);
            $pyd_frequensi = $this->input->post("pyd_frequensi", true);
            $q_program_bina_balita = $this->input->post("q_program_bina_balita", true);
            $q_ikut_kel_belajar = $this->input->post("q_ikut_kel_belajar", true);
            $q_paud = $this->input->post("q_paud", true);
            $q_ikut_kooperasi = $this->input->post("q_ikut_kooperasi", true);
            $q_punya_tabungan = $this->input->post("q_punya_tabungan", true);
            $q_makan_pokok = $this->input->post("q_makan_pokok", true);
            $q_punya_usaha = $this->input->post("q_punya_usaha", true);
            $usaha_skala = $this->input->post("usaha_skala", true);
            $usaha_no = $this->input->post("usaha_no", true);
            $usaha_bidang = $this->input->post("usaha_bidang", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_sosial" => "",
                    "id_user" => $this->id_user_ses,
                    "q_anggota_dasawisma" => $q_anggota_dasawisma,
                    "dw_nama" => $dw_nama,
                    "q_activitas_pyd" => $q_activitas_pyd,
                    "pyd_frequensi" => $pyd_frequensi,
                    "q_program_bina_balita" => $q_program_bina_balita,
                    "q_ikut_kel_belajar" => $q_ikut_kel_belajar,
                    "q_paud" => $q_paud,
                    "q_ikut_kooperasi" => $q_ikut_kooperasi,
                    "q_punya_tabungan" => $q_punya_tabungan,
                    "q_makan_pokok" => $q_makan_pokok,
                    "q_punya_usaha" => $q_punya_usaha,
                    "usaha_skala" => $usaha_skala,
                    "usaha_no" => $usaha_no,
                    "usaha_bidang" => $usaha_bidang,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["q_anggota_dasawisma"]  = strip_tags(form_error('q_anggota_dasawisma'));
            $msg_detail["dw_nama"]  = strip_tags(form_error('dw_nama'));
            $msg_detail["q_activitas_pyd"]  = strip_tags(form_error('q_activitas_pyd'));
            $msg_detail["pyd_frequensi"]  = strip_tags(form_error('pyd_frequensi'));
            $msg_detail["q_program_bina_balita"]  = strip_tags(form_error('q_program_bina_balita'));
            $msg_detail["q_ikut_kel_belajar"]  = strip_tags(form_error('q_ikut_kel_belajar'));
            $msg_detail["q_paud"]  = strip_tags(form_error('q_paud'));
            $msg_detail["q_ikut_kooperasi"]  = strip_tags(form_error('q_ikut_kooperasi'));
            $msg_detail["q_punya_tabungan"]  = strip_tags(form_error('q_punya_tabungan'));
            $msg_detail["q_makan_pokok"]  = strip_tags(form_error('q_makan_pokok'));
            $msg_detail["q_punya_usaha"]  = strip_tags(form_error('q_punya_usaha'));
            $msg_detail["usaha_skala"]  = strip_tags(form_error('usaha_skala'));
            $msg_detail["usaha_no"]  = strip_tags(form_error('usaha_no'));
            $msg_detail["usaha_bidang"]  = strip_tags(form_error('usaha_bidang'));
            $msg_detail["t_penghasilan"]  = strip_tags(form_error('t_penghasilan'));
            $msg_detail["t_pengeluaran_rutin"]  = strip_tags(form_error('t_pengeluaran_rutin'));
            $msg_detail["t_pengeluaran_lainnya"]  = strip_tags(form_error('t_pengeluaran_lainnya'));    
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            
            array(
                'field'=>'q_anggota_dasawisma',
                'label'=>'q_anggota_dasawisma',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'dw_nama',
                'label'=>'dw_nama',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_activitas_pyd',
                'label'=>'q_activitas_pyd',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'pyd_frequensi',
                'label'=>'pyd_frequensi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_program_bina_balita',
                'label'=>'q_program_bina_balita',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_ikut_kel_belajar',
                'label'=>'q_ikut_kel_belajar',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_paud',
                'label'=>'q_paud',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_ikut_kooperasi',
                'label'=>'q_ikut_kooperasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_punya_tabungan',
                'label'=>'q_punya_tabungan',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_makan_pokok',
                'label'=>'q_makan_pokok',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_punya_usaha',
                'label'=>'q_punya_usaha',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_anggota_dasawisma" => "",
                    "dw_nama" => "",
                    "q_activitas_pyd" => "",
                    "pyd_frequensi" => "",
                    "q_program_bina_balita" => "",
                    "q_ikut_kel_belajar" => "",
                    "q_paud" => "",
                    "q_ikut_kooperasi" => "",
                    "q_punya_tabungan" => "",
                    "q_makan_pokok" => "",
                    "q_punya_usaha" => "",
                    "usaha_skala" => "",
                    "usaha_no" => "",
                    "usaha_bidang" => "",
                    "t_penghasilan" => "",
                    "t_pengeluaran_rutin" => "",
                    "t_pengeluaran_lainnya" => ""
                );

        if($this->val_form_update()){
            // $id_user_sosial = $this->input->post("id_user_sosial", true);

            $q_anggota_dasawisma = $this->input->post("q_anggota_dasawisma", true);
            $dw_nama = $this->input->post("dw_nama", true);
            $q_activitas_pyd = $this->input->post("q_activitas_pyd", true);
            $pyd_frequensi = $this->input->post("pyd_frequensi", true);
            $q_program_bina_balita = $this->input->post("q_program_bina_balita", true);
            $q_ikut_kel_belajar = $this->input->post("q_ikut_kel_belajar", true);
            $q_paud = $this->input->post("q_paud", true);
            $q_ikut_kooperasi = $this->input->post("q_ikut_kooperasi", true);
            $q_punya_tabungan = $this->input->post("q_punya_tabungan", true);
            $q_makan_pokok = $this->input->post("q_makan_pokok", true);
            $q_punya_usaha = $this->input->post("q_punya_usaha", true);
            $usaha_skala = $this->input->post("usaha_skala", true);
            $usaha_no = $this->input->post("usaha_no", true);
            $usaha_bidang = $this->input->post("usaha_bidang", true);
           
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "q_anggota_dasawisma" => $q_anggota_dasawisma,
                    "dw_nama" => $dw_nama,
                    "q_activitas_pyd" => $q_activitas_pyd,
                    "pyd_frequensi" => $pyd_frequensi,
                    "q_program_bina_balita" => $q_program_bina_balita,
                    "q_ikut_kel_belajar" => $q_ikut_kel_belajar,
                    "q_paud" => $q_paud,
                    "q_ikut_kooperasi" => $q_ikut_kooperasi,
                    "q_punya_tabungan" => $q_punya_tabungan,
                    "q_makan_pokok" => $q_makan_pokok,
                    "q_punya_usaha" => $q_punya_usaha,
                    "usaha_skala" => $usaha_skala,
                    "usaha_no" => $usaha_no,
                    "usaha_bidang" => $usaha_bidang,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["q_anggota_dasawisma"]  = strip_tags(form_error('q_anggota_dasawisma'));
            $msg_detail["dw_nama"]  = strip_tags(form_error('dw_nama'));
            $msg_detail["q_activitas_pyd"]  = strip_tags(form_error('q_activitas_pyd'));
            $msg_detail["pyd_frequensi"]  = strip_tags(form_error('pyd_frequensi'));
            $msg_detail["q_program_bina_balita"]  = strip_tags(form_error('q_program_bina_balita'));
            $msg_detail["q_ikut_kel_belajar"]  = strip_tags(form_error('q_ikut_kel_belajar'));
            $msg_detail["q_paud"]  = strip_tags(form_error('q_paud'));
            $msg_detail["q_ikut_kooperasi"]  = strip_tags(form_error('q_ikut_kooperasi'));
            $msg_detail["q_punya_tabungan"]  = strip_tags(form_error('q_punya_tabungan'));
            $msg_detail["q_makan_pokok"]  = strip_tags(form_error('q_makan_pokok'));
            $msg_detail["q_punya_usaha"]  = strip_tags(form_error('q_punya_usaha'));
            $msg_detail["usaha_skala"]  = strip_tags(form_error('usaha_skala'));
            $msg_detail["usaha_no"]  = strip_tags(form_error('usaha_no'));
            $msg_detail["usaha_bidang"]  = strip_tags(form_error('usaha_bidang'));
            $msg_detail["t_penghasilan"]  = strip_tags(form_error('t_penghasilan'));
            $msg_detail["t_pengeluaran_rutin"]  = strip_tags(form_error('t_pengeluaran_rutin'));
            $msg_detail["t_pengeluaran_lainnya"]  = strip_tags(form_error('t_pengeluaran_lainnya'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
