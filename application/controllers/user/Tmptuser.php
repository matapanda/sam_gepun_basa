<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tmptuser extends CI_Controller {
    public $title = "Data Tempat Tinggal Masyarakat";
    public $tbl_main = "user_tmpt";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "STS_RUMAH"=>"46",
        "KONDISI_RUMAH"=>"52",
        "JN_LANTAI"=>"55",
        "KRITERIA_RUMAH"=>"56",
        "JN_LISTRIK"=>"66",
        "JN_SANITASI"=>"71",
        "SUMBER_AIR"=>"75"
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");
        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        $id_user 	        = $this->id_user_ses;
		$data["page"]       = "user_tmpt";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$this->id_user_ses));
        
        $data["sts_rumah"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["STS_RUMAH"]]);
        $data["kondisi_rumah"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["KONDISI_RUMAH"]]);
        $data["jn_lantai"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_LANTAI"]]);
        $data["kriteria_rumah"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["KRITERIA_RUMAH"]]);
        $data["jn_listrik"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_LISTRIK"]]);
        $data["jn_sanitasi"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_SANITASI"]]);
        $data["sumber_air"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["SUMBER_AIR"]]);
		
        $this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'q_punya_rumah',
                'label'=>'q_punya_rumah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'py_rmh_sts',
                'label'=>'py_rmh_sts',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'py_rmh_kondisi',
                'label'=>'py_rmh_kondisi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'py_rmh_jn_lantai',
                'label'=>'py_rmh_jn_lantai',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'py_rmh_kriteria_rmh',
                'label'=>'py_rmh_kriteria_rmh',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ada_listrik',
                'label'=>'q_ada_listrik',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'lstr_jenis',
                'label'=>'lstr_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_ada_sanitasi',
                'label'=>'q_ada_sanitasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'sn_jenis',
                'label'=>'sn_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_saluran_limbah',
                'label'=>'q_saluran_limbah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_air_bersih',
                'label'=>'q_air_bersih',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'ab_sumber_air',
                'label'=>'ab_sumber_air',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_pembuangan_sampah',
                'label'=>'q_pembuangan_sampah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_punya_rumah" => "",
                    "py_rmh_sts" => "",
                    "py_rmh_kondisi" => "",
                    "py_rmh_jn_lantai" => "",
                    "py_rmh_kriteria_rmh" => "",
                    "q_ada_listrik" => "",
                    "lstr_jenis" => "",
                    "q_ada_sanitasi" => "",
                    "sn_jenis" => "",
                    "q_saluran_limbah" => "",
                    "q_air_bersih" => "",
                    "ab_sumber_air" => "",
                    "q_pembuangan_sampah" => ""
                );

        if($this->val_form_insert()){
            // $id_user_tmpt = $this->input->post("id_user_tmpt", true);

            $q_punya_rumah = $this->input->post("q_punya_rumah", true);
            $py_rmh_sts = $this->input->post("py_rmh_sts", true);
            $py_rmh_kondisi = $this->input->post("py_rmh_kondisi", true);
            $py_rmh_jn_lantai = $this->input->post("py_rmh_jn_lantai", true);
            $py_rmh_kriteria_rmh = $this->input->post("py_rmh_kriteria_rmh", true);
            $q_ada_listrik = $this->input->post("q_ada_listrik", true);
            $lstr_jenis = $this->input->post("lstr_jenis", true);
            $q_ada_sanitasi = $this->input->post("q_ada_sanitasi", true);
            $sn_jenis = $this->input->post("sn_jenis", true);
            $q_saluran_limbah = $this->input->post("q_saluran_limbah", true);
            $q_air_bersih = $this->input->post("q_air_bersih", true);
            $ab_sumber_air = $this->input->post("ab_sumber_air", true);
            $q_pembuangan_sampah = $this->input->post("q_pembuangan_sampah", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_tmpt" => "",
                    "id_user" => $this->id_user_ses,
                    "q_punya_rumah" => $q_punya_rumah,
                    "py_rmh_sts" => $py_rmh_sts,
                    "py_rmh_kondisi" => $py_rmh_kondisi,
                    "py_rmh_jn_lantai" => $py_rmh_jn_lantai,
                    "py_rmh_kriteria_rmh" => $py_rmh_kriteria_rmh,
                    "q_ada_listrik" => $q_ada_listrik,
                    "lstr_jenis" => $lstr_jenis,
                    "q_ada_sanitasi" => $q_ada_sanitasi,
                    "sn_jenis" => $sn_jenis,
                    "q_saluran_limbah" => $q_saluran_limbah,
                    "q_air_bersih" => $q_air_bersih,
                    "ab_sumber_air" => $ab_sumber_air,
                    "q_pembuangan_sampah" => $q_pembuangan_sampah,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["q_punya_rumah"]  = strip_tags(form_error('q_punya_rumah'));
            $msg_detail["py_rmh_sts"]  = strip_tags(form_error('py_rmh_sts'));
            $msg_detail["py_rmh_kondisi"]  = strip_tags(form_error('py_rmh_kondisi'));
            $msg_detail["py_rmh_jn_lantai"]  = strip_tags(form_error('py_rmh_jn_lantai'));
            $msg_detail["py_rmh_kriteria_rmh"]  = strip_tags(form_error('py_rmh_kriteria_rmh'));
            $msg_detail["q_ada_listrik"]  = strip_tags(form_error('q_ada_listrik'));
            $msg_detail["lstr_jenis"]  = strip_tags(form_error('lstr_jenis'));
            $msg_detail["q_ada_sanitasi"]  = strip_tags(form_error('q_ada_sanitasi'));
            $msg_detail["sn_jenis"]  = strip_tags(form_error('sn_jenis'));
            $msg_detail["q_saluran_limbah"]  = strip_tags(form_error('q_saluran_limbah'));
            $msg_detail["q_air_bersih"]  = strip_tags(form_error('q_air_bersih'));
            $msg_detail["ab_sumber_air"]  = strip_tags(form_error('ab_sumber_air'));
            $msg_detail["q_pembuangan_sampah"]  = strip_tags(form_error('q_pembuangan_sampah'));
             
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            
            array(
                'field'=>'q_punya_rumah',
                'label'=>'q_punya_rumah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'py_rmh_sts',
                'label'=>'py_rmh_sts',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'py_rmh_kondisi',
                'label'=>'py_rmh_kondisi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'py_rmh_jn_lantai',
                'label'=>'py_rmh_jn_lantai',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'py_rmh_kriteria_rmh',
                'label'=>'py_rmh_kriteria_rmh',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_ada_listrik',
                'label'=>'q_ada_listrik',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'lstr_jenis',
                'label'=>'lstr_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_ada_sanitasi',
                'label'=>'q_ada_sanitasi',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'sn_jenis',
                'label'=>'sn_jenis',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_saluran_limbah',
                'label'=>'q_saluran_limbah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_air_bersih',
                'label'=>'q_air_bersih',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'ab_sumber_air',
                'label'=>'ab_sumber_air',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_pembuangan_sampah',
                'label'=>'q_pembuangan_sampah',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_punya_rumah" => "",
                    "py_rmh_sts" => "",
                    "py_rmh_kondisi" => "",
                    "py_rmh_jn_lantai" => "",
                    "py_rmh_kriteria_rmh" => "",
                    "q_ada_listrik" => "",
                    "lstr_jenis" => "",
                    "q_ada_sanitasi" => "",
                    "sn_jenis" => "",
                    "q_saluran_limbah" => "",
                    "q_air_bersih" => "",
                    "ab_sumber_air" => "",
                    "q_pembuangan_sampah" => ""
                );

        if($this->val_form_update()){
            // $id_user_tmpt = $this->input->post("id_user_tmpt", true);

            $q_punya_rumah = $this->input->post("q_punya_rumah", true);
            $py_rmh_sts = $this->input->post("py_rmh_sts", true);
            $py_rmh_kondisi = $this->input->post("py_rmh_kondisi", true);
            $py_rmh_jn_lantai = $this->input->post("py_rmh_jn_lantai", true);
            $py_rmh_kriteria_rmh = $this->input->post("py_rmh_kriteria_rmh", true);
            $q_ada_listrik = $this->input->post("q_ada_listrik", true);
            $lstr_jenis = $this->input->post("lstr_jenis", true);
            $q_ada_sanitasi = $this->input->post("q_ada_sanitasi", true);
            $sn_jenis = $this->input->post("sn_jenis", true);
            $q_saluran_limbah = $this->input->post("q_saluran_limbah", true);
            $q_air_bersih = $this->input->post("q_air_bersih", true);
            $ab_sumber_air = $this->input->post("ab_sumber_air", true);
            $q_pembuangan_sampah = $this->input->post("q_pembuangan_sampah", true);
           
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "q_punya_rumah" => $q_punya_rumah,
                    "py_rmh_sts" => $py_rmh_sts,
                    "py_rmh_kondisi" => $py_rmh_kondisi,
                    "py_rmh_jn_lantai" => $py_rmh_jn_lantai,
                    "py_rmh_kriteria_rmh" => $py_rmh_kriteria_rmh,
                    "q_ada_listrik" => $q_ada_listrik,
                    "lstr_jenis" => $lstr_jenis,
                    "q_ada_sanitasi" => $q_ada_sanitasi,
                    "sn_jenis" => $sn_jenis,
                    "q_saluran_limbah" => $q_saluran_limbah,
                    "q_air_bersih" => $q_air_bersih,
                    "ab_sumber_air" => $ab_sumber_air,
                    "q_pembuangan_sampah" => $q_pembuangan_sampah,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["q_punya_rumah"]  = strip_tags(form_error('q_punya_rumah'));
            $msg_detail["py_rmh_sts"]  = strip_tags(form_error('py_rmh_sts'));
            $msg_detail["py_rmh_kondisi"]  = strip_tags(form_error('py_rmh_kondisi'));
            $msg_detail["py_rmh_jn_lantai"]  = strip_tags(form_error('py_rmh_jn_lantai'));
            $msg_detail["py_rmh_kriteria_rmh"]  = strip_tags(form_error('py_rmh_kriteria_rmh'));
            $msg_detail["q_ada_listrik"]  = strip_tags(form_error('q_ada_listrik'));
            $msg_detail["lstr_jenis"]  = strip_tags(form_error('lstr_jenis'));
            $msg_detail["q_ada_sanitasi"]  = strip_tags(form_error('q_ada_sanitasi'));
            $msg_detail["sn_jenis"]  = strip_tags(form_error('sn_jenis'));
            $msg_detail["q_saluran_limbah"]  = strip_tags(form_error('q_saluran_limbah'));
            $msg_detail["q_air_bersih"]  = strip_tags(form_error('q_air_bersih'));
            $msg_detail["ab_sumber_air"]  = strip_tags(form_error('ab_sumber_air'));
            $msg_detail["q_pembuangan_sampah"]  = strip_tags(form_error('q_pembuangan_sampah'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
