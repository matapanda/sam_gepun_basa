<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Transkomuser extends CI_Controller {
    public $title = "Transportasi dan Komunikasi Masyarakat";
    public $tbl_main = "user_transport";
    public $tbl_jn_all = "ms_jenis_all";

    public $id_user_ses;

    public $date_now;

    public $arr_jn = [
        "JN_KENDARAAN"=>"23",
        "STS_KEPEMILIKAN"=>"24",
        "JN_INTERNET"=>"25",
        "JN_PERANGKAT"=>"26",
    ];

	public function __construct(){
        parent::__construct(); 
        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('custom/registercustom', 'cr');
        // $this->load->model('main/store_insert_auto_key', 'ma');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");

        $this->load->library("injectlist");

        $this->id_user_ses = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
        // $this->id_user_ses = "4";

        $this->date_now = date("Y-m-d H:i:s");
    }

#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================
	public function index(){
        // $id_user 	        = $this->session->userdata("ngadi_ngadi_aja")["id_user"];
		$data["page"]       = "user_transport";
        $data["title"]      = $this->title;
        $data["inject_list"] = $this->injectlist->check_list_data($this->id_user_ses);
        $data["resume"] = $this->injectlist->check_sts_submit($this->id_user_ses);

		$data["list_data"]  = $this->mm->get_data_each($this->tbl_main, array("id_user"=>$this->id_user_ses));

        $data["jenis_transportasi"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_KENDARAAN"]]);
        $data["sts_kepemilikan"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["STS_KEPEMILIKAN"]]);
        $data["jenis_internet"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_INTERNET"]]);
        $data["jenis_perangkat"]   = $this->mm->get_data_all_where($this->tbl_jn_all, ["ms_jenis_all_id"=>$this->arr_jn["JN_PERANGKAT"]]);
		
        $this->load->view('index_user', $data);
	}
#===============================================================================
#-----------------------------------home_admin----------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================
    public function act_controller(){
        // print_r("<pre>");
        // print_r($_POST);
        // die();
        $cek_data = $this->mm->get_data_each($this->tbl_main, ["id_user"=> $this->id_user_ses]);
        if($cek_data){
            // data telah diisi maka update
            // print_r("ada");
            $this->update();
        }else{
            // data belum ada maka input
            // print_r("tidak ada");
            $this->insert();
        }
    }
#===============================================================================
#-----------------------------------controller----------------------------------
#===============================================================================



#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================
    public function val_form_insert(){
        $config_val_input = array(
            array(
                'field'=>'q_punya_tr',
                'label'=>'q_punya_tr',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'tr_detail',
                'label'=>'tr_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_akses_internet',
                'label'=>'q_akses_internet',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'jn_internet_akses',
                'label'=>'jn_internet_akses',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'q_punya_perangkat',
                'label'=>'q_punya_perangkat',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            ),array(
                'field'=>'prng_detail',
                'label'=>'prng_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function insert(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_punya_tr" => "",
                    "tr_detail" => "",
                    "q_akses_internet" => "",
                    "jn_internet_akses" => "",
                    "q_punya_perangkat" => "",
                    "prng_detail" => ""
                );

        if($this->val_form_insert()){
            // $id_user_transport = $this->input->post("id_user_transport", true);

            $q_punya_tr = $this->input->post("q_punya_tr", true);
            $tr_detail = $this->input->post("tr_detail", true);
            $q_akses_internet = $this->input->post("q_akses_internet", true);
            $jn_internet_akses = $this->input->post("jn_internet_akses", true);
            $q_punya_perangkat = $this->input->post("q_punya_perangkat", true);
            $prng_detail = $this->input->post("prng_detail", true);
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $data = [
                    "id_user_transport" => "",
                    "id_user" => $this->id_user_ses,
                    "q_punya_tr" => $q_punya_tr,
                    "tr_detail" => $tr_detail,
                    "q_akses_internet" => $q_akses_internet,
                    "jn_internet_akses" => $jn_internet_akses,
                    "q_punya_perangkat" => $q_punya_perangkat,
                    "prng_detail" => $prng_detail,
                    "r_crt_by" => $this->id_user_ses,
                    "r_crt_time" => $this->date_now,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];


                $act = $this->mm->insert_data($this->tbl_main, $data);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("INSERT_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));

            $msg_detail["q_punya_tr"]  = strip_tags(form_error('q_punya_tr'));
            $msg_detail["tr_detail"]  = strip_tags(form_error('tr_detail'));
            $msg_detail["q_akses_internet"]  = strip_tags(form_error('q_akses_internet'));
            $msg_detail["jn_internet_akses"]  = strip_tags(form_error('jn_internet_akses'));
            $msg_detail["q_punya_perangkat"]  = strip_tags(form_error('q_punya_perangkat'));
            $msg_detail["prng_detail"]  = strip_tags(form_error('prng_detail'));
             
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------insert_data---------------------------------
#===============================================================================

#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================
    public function val_form_update(){
        $config_val_input = array(
            
            array(
                'field'=>'q_punya_tr',
                'label'=>'q_punya_tr',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'tr_detail',
                'label'=>'tr_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                    
            ),array(
                'field'=>'q_akses_internet',
                'label'=>'q_akses_internet',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'jn_internet_akses',
                'label'=>'jn_internet_akses',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'q_punya_perangkat',
                'label'=>'q_punya_perangkat',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            ),array(
                'field'=>'prng_detail',
                'label'=>'prng_detail',
                'rules'=>'required',
                'errors'=>array(
                    'required'=>"%s ".$this->response_message->get_error_msg("REQUIRED")
                )
                   
            )
        );
        $this->form_validation->set_rules($config_val_input); 
        return $this->form_validation->run();
    }

    public function update(){
        $msg_main = array("status"=>false, "msg"=>$this->response_message->get_error_msg("INPUT_FAIL"));
        $msg_detail = array(
                    "q_punya_tr" => "",
                    "tr_detail" => "",
                    "q_akses_internet" => "",
                    "jn_internet_akses" => "",
                    "q_punya_perangkat" => "",
                    "prng_detail" => ""
                );

        if($this->val_form_update()){
            // $id_user_transport = $this->input->post("id_user_transport", true);

            $q_punya_tr = $this->input->post("q_punya_tr", true);
            $tr_detail = $this->input->post("tr_detail", true);
            $q_akses_internet = $this->input->post("q_akses_internet", true);
            $jn_internet_akses = $this->input->post("jn_internet_akses", true);
            $q_punya_perangkat = $this->input->post("q_punya_perangkat", true);
            $prng_detail = $this->input->post("prng_detail", true);           
            
            $type_pattern   = "allowed_general_char";

            $arr_pattern    = [
                                [$type_pattern, $this->id_user_ses]
                            ];

            // $data =

            if($this->magic_pattern->set_list_pattern($arr_pattern )){
                $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("CHAR_NOT_COMFIRMED_GENERAL"));
            } else {
                
                $set = [
                    "q_punya_tr" => $q_punya_tr,
                    "tr_detail" => $tr_detail,
                    "q_akses_internet" => $q_akses_internet,
                    "jn_internet_akses" => $jn_internet_akses,
                    "q_punya_perangkat" => $q_punya_perangkat,
                    "prng_detail" => $prng_detail,
                    "r_up_by" => $this->id_user_ses,
                    "r_up_time" => $this->date_now
                ];

                $where = [
                    "id_user" => $this->id_user_ses
                ];

                $act = $this->mm->update_data($this->tbl_main, $set, $where);
                if($act){
                    $msg_main = array("status"=>true, "msg"=>$this->response_message->get_success_msg("UPDATE_SUC"));
                }
            }
        }else {
            $msg_main = array("status" => false, "msg" => $this->response_message->get_error_msg("INPUT_FAIL"));
        
            $msg_detail["q_punya_tr"]  = strip_tags(form_error('q_punya_tr'));
            $msg_detail["tr_detail"]  = strip_tags(form_error('tr_detail'));
            $msg_detail["q_akses_internet"]  = strip_tags(form_error('q_akses_internet'));
            $msg_detail["jn_internet_akses"]  = strip_tags(form_error('jn_internet_akses'));
            $msg_detail["q_punya_perangkat"]  = strip_tags(form_error('q_punya_perangkat'));
            $msg_detail["prng_detail"]  = strip_tags(form_error('prng_detail'));
        }

        $res_msg = $this->response_message->default_mgs($msg_main, $msg_detail);
        print_r(json_encode($res_msg));
    }
#===============================================================================
#-----------------------------------update_admin--------------------------------
#===============================================================================

}
