 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_v0 {
    public $tbl_admin = "admin";
    public $tbl_admin_tipe = "admin_tipe";
    public $tbl_admin_role = "admin_role";
    public $tbl_admin_role_tipe = "admin_role_tipe";

    public $default_redirect;

    public $arr_sess_param = [
        "admin"=>"aduhduh_ketauan",
        "petugas"=>"ih_mau_bro"
    ];

    public function __construct(){
        $this->load->library('session');

        $this->load->model('main/mainmodel', 'mm');
        $this->load->model('login/admin_main', 'am');

        $this->default_redirect = base_url()."";
    }

    public function __get($var){
        return get_instance()->$var;
    }

    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================

        public function set_session($param = null){
        	// print_r($param);
            $status = false;
            if($param != null){
        		$this->session->set_userdata($this->arr_sess_param["admin"], $param);
                $status = true;
        	}
            return $status;
        }

        public function destroy_session($redirect = "login"){
            $this->session->sess_destroy();
            redirect($redirect);
        }

        public function get_session(){
            $data_session = array();
            if(isset($_SESSION[$this->arr_sess_param["admin"]])){
                $data_session = $_SESSION[$this->arr_sess_param["admin"]];
            }
            return $data_session;
        }

        public function auth_login(){
            if(isset($_SESSION[$this->arr_sess_param["admin"]])){
                if($_SESSION[$this->arr_sess_param["admin"]]["status_log"]){
                    $id_admin_tipe = $_SESSION[$this->arr_sess_param["admin"]]["id_admin_tipe"];
                    $data_tipe = $this->mm->get_data_each($this->tbl_admin_tipe, ["id_admin_tipe"=> $id_admin_tipe]);

                    if($data_tipe["redirect"]){
                        redirect(base_url().$data_tipe["redirect"]);
                    }else{
                        redirect(base_url()."login");
                    }
                }
            }
        }

        public function check_session_page($rules = null){
            if(isset($_SESSION[$this->arr_sess_param["admin"]])){
                if($_SESSION[$this->arr_sess_param["admin"]]["status_log"] == true){
                    $data_session = $_SESSION[$this->arr_sess_param["admin"]];

                    // print_r("<pre>");
                    // print_r($data_session);
                    if($rules != null){
                        $id_admin = $data_session["id_admin"];
                        $id_admin_tipe = $data_session["id_admin_tipe"];
                        // die();

                        $cek_data = $this->am->check_access(["art.id_admin_tipe"=>$id_admin_tipe, "kd_role"=>$rules]);
                        if($cek_data){

                        }else{
                            redirect(base_url()."login");
                        }
                        // print_r("<pre>");
                        // print_r($cek_data);
                        // die();
                    }
                }else {
                    redirect(base_url()."login");
                }
            }else {
                redirect(base_url()."login");
            }
        }

    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================
}