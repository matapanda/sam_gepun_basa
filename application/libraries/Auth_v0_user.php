<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth_v0_user {
    public function __construct(){
        $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================

        public function set_session($param = null){

            // print_r($param);
        	$status = false;
            if($param != null){
        		$this->session->set_userdata("ngadi_ngadi_aja", $param);
                $status = true;
        	}
            return $status;
        }

        public function destroy_session($redirect = "user/login"){
            $this->session->sess_destroy();
            redirect($redirect);
        }

        public function get_session(){
            $data_session = array();
            if(isset($_SESSION["ngadi_ngadi_aja"])){
                $data_session = $_SESSION["ngadi_ngadi_aja"];
            }
            return $data_session;
        }

        public function auth_login(){
            if(isset($_SESSION["ngadi_ngadi_aja"])){
                if($_SESSION["ngadi_ngadi_aja"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["ngadi_ngadi_aja"]["status_log"]) {
                        case true:
                            redirect(base_url()."user/beranda");
                            
                            break;

                        default:
                            break;
                    }
                }
            }
        }

        public function check_session_active(){
            if(isset($_SESSION["ngadi_ngadi_aja"])){
                if($_SESSION["ngadi_ngadi_aja"]["status_log"] == true){
                    // print_r("login");
                    switch ($_SESSION["ngadi_ngadi_aja"]["status_log"]) {
                        case true:
                            // redirect(base_url()."admin/data_admin");
                            // print_r("admin");
                            break;
                    
                        default:
                            redirect(base_url()."user/login");
                            break;
                    }
                }else {
                    redirect(base_url()."user/login");
                }
            }else {
                redirect(base_url()."user/login");
            }
        }
    // ===========================================================================
    // ----------------------------------Admin_Login------------------------------
    // ===========================================================================

}