<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Injectlist {

    public function __construct(){
        $this->load->library('session');

        $this->load->model('main/mainmodel', 'mm');

        $this->load->library("response_message");
        $this->load->library("Auth_v0_user");
        $this->load->library("magic_pattern");
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function check_list_data($id){
		$data = [
            "data_diri"=>count($this->mm->get_data_all_where("user_data", ["id_user"=>$id])),
            "ketenagakerjaan"=>count($this->mm->get_data_all_where("user_kerja", ["id_user"=>$id])),
            "kesehatan"=>count($this->mm->get_data_all_where("user_kesehatan", ["id_user"=>$id])),
            "keuangan"=>count($this->mm->get_data_all_where("user_keuangan", ["id_user"=>$id])),
            "pendidikan"=>count($this->mm->get_data_all_where("user_pendidikan", ["id_user"=>$id])),
            "kegiatan_organisasi"=>count($this->mm->get_data_all_where("user_or", ["id_user"=>$id])),
            "sosial_ekonomi"=>count($this->mm->get_data_all_where("user_sosial", ["id_user"=>$id])),
            "transportasi"=>count($this->mm->get_data_all_where("user_transport", ["id_user"=>$id])),
            "lingkungan"=>count($this->mm->get_data_all_where("user_tmpt", ["id_user"=>$id])),
            // "resume"=>count($this->mm->get_data_all_where("user", ["id_user"=>$id, "sts_submit"=>"1"]))
        ];

        // print_r("<pre>");
        // print_r($data);
        // die();

        return $data;
	}

    public function check_sts_submit($id){
		$data = $this->mm->get_data_each("user", ["id_user"=>$id, "sts_submit"=>"1"]);

        return $data;
	}

}