<?php
/**
 * @author Surya Hanggara (Filosofi_code)
 * @copyright 2019
 */
class Response_message{
    
    public function get_error_msg($id_message){
        $errors_msg = array(
            "REQUIRED"=>"Tidak boleh kosong",
            "MEMBER_NOT_FOUND"=>"Status keanggotaan dengan user anda tidak diketemukan silahkan hubungi admin terkait",

            "NUMBER"=>"Input Salah, Input harus berupa angka",
            "NUMBER_CHAR"=>"Input Salah, Input tidak diperkenankan selain berupa angka dan huruf",

            "EMAIL"=>"Input Email tidak valid, mohon input dengan benar",
            "EMAIL_AVAIL"=>"Email sudah terdaftar, satu Email tidak bisa di gunakan untuk 2 akun berbeda",
            "EMAIL_NOT_AVAIL"=>"Email tidak terdaftar",

            "USERNAME_LENGHT"=>"Input tidak sesuai, minimal karakter adalah 8",
            "USERNAME_AVAIL"=>"username sudah terdaftar, satu username tidak bisa di gunakan untuk 2 akun berbeda",

            "TLP_FAIL_MAX"=>"nomor tlp tidak boleh lebih dari 13 digit",
            "TLP_FAIL_MIN"=>"nomor tlp tidak boleh kurang dari 10 digit",

            "TLP_AVAIL"=>"Telephon sudah terdaftar, satu Telephon tidak bisa di gunakan untuk 2 akun berbeda",

            "PASSWORD_LENGHT"=>"Input tidak sesuai, minimal karakter adalah 6",
            "RE_PASSWORD_FAIL"=>"Mohon Ulangi password anda dengan benar",

            "CHANGE_PASS_FAIL"=>"Mohon maaf proses pergantian password akun anda gagal dilakukan, mohon periksa jaringan saudara atau coba beberapa saat lagi",
            "CHANGE_EMAIL_FAIL"=>"Mohon maaf proses perubahan email anda gagal dilakukan, mohon periksa jaringan saudara atau coba beberapa saat lagi",

            

            "USER_IN_TOKO_AVAIL"=>"User telah terdaftar pada toko lain, gunakan user lain untuk menyimpan data ini",
            
            "INPUT_FAIL"=>"Input tidak tepat, mohon periksa input saudara kembali",

            "STOCK_READY_FAIL"=>"Stok tidak tersedia",
            
            "PENDAFTARAN_FAIL"=>"Proses pendaftaran gagal, silahkan periksa kelengkapan data dan jaringan anda lagi..!",
            "PENDAFTARAN_DOC_FAIL"=>"Mohon maaf proses pendaftaran di batalkan, Silahkan periksa kelengkapan dokumen saudara dan Lakukan pendafatran ulang..",
            "PENDAFTARAN_AVAIL_FAIL"=>"Mohon maaf, pendaftaran Gagal di terima.. Anda masih memeiliki program proposal atau magang yang belum di selesaikan..",

            "REGISTER_FAIL"=>"Mohon maaf register anda gagal, silahkan periksa kembali input saudara.",
            "ACTIVATION_FAIL"=>"Aktivasi gagal, silahkan lakukan aktivasi ulang untuk mengaktifkan akun anda.",
            "DATE_START_FAIL"=> "Mohon maaf tanggal mulai harus lebih kecil dari tanggal berakhir.",
            
            "UPDATE_FAIL"=>"Mohon maaf, Proses update gagal silahkan perikasa input saudara lagi",
            "INSERT_FAIL"=>"Mohon maaf, Proses insert gagal silahkan perikasa input saudara lagi",
            "DELETE_FAIL"=>"Mohon maaf, Proses delete gagal silahkan perikasa input saudara lagi",
            "UPLOAD_FAIL"=>"Mohon maaf, uploade file gagal, silahkan update kembali file saudara",

            "GET_FAIL"=>"Mohon maaf, proses pengambilan ini gagal. Silahkan periksa jaringan saudara atau coba lagi nanti",
            "ACCESS_FAIL"=>"Mohon maaf, anda tidak mendapat autorisasi untuk akses ke halaman ini. Untuk infromasi lebih lanjut silahkan Admin :)",
            
            "LOG_FAIL"=>"Mohon maaf, Login gagal, silahkan patikan username dan password saudara telah sesuai",

            "USER_DELETE"=>"Mohon maaf, status keanggotaan anda telah di hapus silahkan hubungi admin untuk mengaktifkan kembali.",
            "USER_UNACTIVE"=>"Mohon maaf, status keanggotaan anda telah di nonaktifkan silahkan hubungi admin untuk mengaktifkan kembali.",


            "DELETE_ALL_FAIL"=>"Mohon maaf, data ini merupakan data inti dan digunakan oleh data lain sebagai sumber, silahkan menghapus semua data yang berhubungan dengan data ini sebelum menghapus data ini..",

            "CHAR_NOT_COMFIRMED_GENERAL"=>"Karakter '|\"|;|&|@|#|`|#|$|<|>|\||\|\/|enter|=,Silahkan periksa input anda sekali lagi.",

            "CHAR_NOT_COMFIRMED_GENERAL_WITH_ENTER"=>"Karakter '|\"|;|&|@|#|`|#|$|<|>|\||\|\/|=,Silahkan periksa input anda sekali lagi.",
            
        );
        
        return $errors_msg[$id_message];
    }
    
    public function get_success_msg($id_message){
        $succes_msg = array(
            "REG_SUC"=>"Permintaan registrasi saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            "LOG_SUC"=>"Login Berhasil, Selamat datang di halaman admin",
            // "LOG_SUC"=>"Login Berhasil, Selamat datang di halaman admin",
            "LOG_SUC_USER"=>"Login Berhasil, Selamat datang di halaman survey masyarakat",

            "CHANGE_PASS_SUC"=>"Proses pergantian password telah berhasil, aplikasi akan melakukan reset dan anda di wajibkan untuk login kembali..",
            "CHANGE_EMAIL_SUC"=>"Proses perubahan email telah berhasil, silahkan verifikasi akun anda kembali untuk mengaktifkan aplikasi ini..",
            
            "UPDATE_PROF_SUC"=>"Permintaan perubahan saudara telah di terima",
            "UPDATE_PROF_SUC_EMAIL"=>"Permintaan perubahan saudara telah di terima, Silahkan buka email saudara dan klik tautan yang sudah kami kirim ke email saudara",
            
            "PENDAFTARAN_SUC"=>"Proses pendaftaran berhasil, silahkan masuk ke halaman riwayat untuk melakukan cetak surat pernyataan..!",

            "REGISTER_SUC"=>"Register anda berhasil, kami sudah mengirimkan email verifikasi kepada email saudara.",
            "VERT_SUC"=>"Verifikasi anda berhasil, kami sudah mengirimkan email verifikasi kepada email saudara.",

            "STOCK_READY_SUC"=>"Stok tersedia",

            "ACTIVATION_SUC"=>"Aktivasi sukses, silahkan login untuk mendaftar.",

            "FORGET_PASS_SUC"=>"Permintaan untuk lupa password saudara telah berhasil, silahkan leihat email saudara untuk mendapatkan proses selanjutnya..",

            "GET_SUC"=>"get data success",
            "ACCESS_SUC"=>"auth success",

            "UPDATE_SUC"=>"Update berhasil",
            "INSERT_SUC"=>"Insert berhasil",
            "DELETE_SUC"=>"Delete berhasil"
        );
        
        return $succes_msg[$id_message];
    }
    
    public function default_mgs($msg_main, $msg_detail){
        return array(
                    "msg_main"=>$msg_main,
                    "msg_detail"=>$msg_detail
                );
    }
}
?>