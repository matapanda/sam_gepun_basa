 <?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Uploadfilev0 {
    public function __construct(){
        // $this->load->library('session');
    }

    public function __get($var){
        return get_instance()->$var;
    }

    public function do_upload($config, $input_name){
        if(file_exists($config['upload_path'].$config['file_name'])){
            unlink($config['upload_path'].$config['file_name']);    
        }
        //print_r($config);
        $this->load->library("upload", $config);
        // $this->load->library('upload', $config);
        $this->upload->initialize($config);

        $return_array = array("status"=>"",
                                "main_msg"=>"",
                                "main_data"=>"");
        
        if (!$this->upload->do_upload($input_name)){
            $return_array["status"] = false;
            $return_array["main_msg"] = array('error' => $this->upload->display_errors());
            $return_array["main_data"] = null;
        }else{
            $return_array["status"] = true;
            $return_array["main_msg"] = "upload success";
            $return_array["main_data"] = array('upload_data' => $this->upload->data());
        }

        return $return_array;
    }


    public function delete_file($upload_path, $file_name){
        $status = false;
        // print_r($upload_path.$file_name);
        if(file_exists($upload_path.$file_name)){
            unlink($upload_path.$file_name);
            $status = true;
        }

        return $status;
    }
}