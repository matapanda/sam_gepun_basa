<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_password extends CI_Model {

	public function cek($email, $kode_aktifasi)
	{
		$this->db->where('email_user', $email);

		if ($this->db->count_all_results('user')>0)
		{
			$this->load->helper('string');
			$data = array(
				'kode_aktifasi' => $kode_aktifasi
			);

			$this->db->where('email_user', $email);
			$this->db->update('user', $data);

			return TRUE;
		} else return FALSE;
	}

	public function reset($code, $email)
	{
		$this->db->select('kode_aktifasi');
		$this->db->from('user');
		$this->db->where('email_user', $email);

		$kode_aktifasi = $this->db->get()->row('kode_aktifasi');

		if ($kode_aktifasi === $code) return TRUE;
		else return FALSE;
	}

	public function renew($email, $password)
	{
		$data = array(
			'password_user'		=> hash("sha256", $password),
			'kode_aktifasi' => NULL,
		);

		$this->db->where('email_user', $email);
		$this->db->update('user', $data);
	}
}

/* End of file model_password.php */
/* Location: ./application/models/model_password.php */