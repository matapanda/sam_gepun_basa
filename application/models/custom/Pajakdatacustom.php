<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pajakdatacustom extends CI_Model{
    public $db_custom;

    public function __construct(){
        parent::__construct(); 
        
        $this->db_custom = $this->load->database('data_pajak', TRUE);
    }
    
    
    public function get_data_excel_vs_seris(){
        $data = $this->db_custom->query("SELECT *, @id_file_pajak:=id_file_pajak, (SELECT COUNT(*) as c_data FROM pajak_d_seris WHERE id_file_pajak = @id_file_pajak) as c_data FROM `dt_pajak_file`");
        return $data->result();
    }

}
?>