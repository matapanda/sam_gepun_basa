<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_main extends CI_Model{

	public function select_admin($where, $where_or){
		$this->db->select("
            ad.id_admin,
            ad.id_admin_tipe,
            ad.email,
            ad.username,
            ad.tlp,
            
            ad.nama_admin,
            ad.sts_act,
            ad.is_del,

            at.tipe,
            at.ket,
            at.redirect,
            
        ");

        $this->db->join("admin_tipe at", "ad.id_admin_tipe = at.id_admin_tipe", "left");
        $this->db->or_where("(username = '".$where_or["username"]."'");
        $this->db->or_where("email = '".$where_or["email"]."')");
        // $this->db->where($where);

        $data = $this->db->get_where("admin ad", $where)->row_array();
        return $data;
	}

    function check_access($where){
        $this->db->join("admin_tipe at", "art.id_admin_tipe = at.id_admin_tipe");
        $this->db->join("admin_role ar", "art.id_role = ar.id_role");
        return $this->db->get_where("admin_role_tipe art", $where)->row_array();
    }
}
?>