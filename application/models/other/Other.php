<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Other extends CI_Model{

   
    
    # get_admin
    
        public function get_admin_full($where){
            $this->db->join("m_pp pp", "ad.id_pp = pp.id_pp", "left");
            $this->db->order_by("id_admin", "DESC");
            $data = $this->db->get_where("admin ad", $where);
            return $data->result();
        }

        public function get_admin_like($fields, $param, $start, $length){
            $str_fields = "";
            if($fields){
                $str_fields = implode(", ",$fields);
            }

            return $this->db->query("SELECT id_admin
                id_tipe_admin,
                jn_admin,
                kd_admin,
                id_pp,
                email,
                username,
                status_active,
                nama_admin
            FROM admin 
            WHERE CONCAT(".$str_fields.") LIKE \"%".$param."%\"
            ORDER BY id_admin DESC
            LIMIT ".$start.", ".$length)->result();
        }

        public function get_admin_like_all($start, $length){
            return $this->db->query("SELECT id_admin,
                id_tipe_admin,
                jn_admin,
                kd_admin,
                id_pp,
                email,
                username,
                status_active,
                nama_admin
            FROM admin
            ORDER BY id_admin DESC
            LIMIT ".$start.", ".$length)->result();
        }

        public function admin_count(){
            return $this->db->query("SELECT COUNT(id_admin) as count_data FROM admin")->row_array();
        }
        

    # get_user
    
        public function get_user_full($where){
            $this->db->join("user_data ud", "us.id_user = ud.id_user", "left");
            $this->db->order_by("id_user", "DESC");
            $data = $this->db->get_where("user us", $where);
            return $data->result();
        }


        public function get_user_single($where){
            $this->db->join("user_data ud", "us.id_user = ud.id_user", "left");
            $this->db->order_by("us.id_user", "DESC");
            $data = $this->db->get_where("user us", $where);
            return $data->row_array();
        }

        public function find_user_by_nik_id($param){
            return $this->db->query("select 
                id_user,
                id_tipe_user,
                jn_user,
                kd_user,
                id_pp,
                email,
                no_tlp_user,
                username,
                nik_user,
                nama_user,
                tmp_lhr,
                tgl_lhr,
                jk_user,
                almt_user,
                agama_user,
                sts_kawin,
                pekerjaan_user,
                photo_user,
                keahlian_khusus_user,
                status_active,
                ket_non_active_user
            from m_user
            WHERE id_user = \"".$param."\" or nik_user = \"".$param."\" or no_tlp_user = \"".$param."\" or username = \"".$param."\"")->row_array();
        }

        public function get_user_like($fields, $param, $start, $length){
            $str_fields = "";
            if($fields){
                $str_fields = implode(", ",$fields);
            }

            return $this->db->query("SELECT 
                            @id_user := us.id_user as id_user,
                            us.email_user,
                            us.username_user,
                            us.status_active_user,
                            us.nama_user,
                            us.nik_user,
                            us.kk_user,
		                    us.tlp_user,
                            
                            ud.ds_nama_dasawisma,
                            ud.ms_jk,
                            ud.d_alamat_user_data,
                            ud.d_kec_user_data,
                            ud.d_kel_user_data,
                            ud.d_rt_user_data,
                            ud.d_rw_user_data,
                            ud.d_kota_user_data,
                            ud.d_provinsi
                    FROM user us
                    LEFT JOIN user_data ud ON us.id_user = ud.id_user
                    WHERE CONCAT(".$str_fields.") LIKE \"%".$param."%\"
                    ORDER BY id_user
                    LIMIT ".$start.", ". $length)->result();
        }

        public function get_user_all($start, $length){
            return $this->db->query("SELECT 
                            @id_user := us.id_user as id_user,
                            us.email_user,
                            us.username_user,
                            us.status_active_user,
                            us.nama_user,
                            us.nik_user,
                            us.kk_user,
		                    us.tlp_user,
                            
                            ud.ds_nama_dasawisma,
                            ud.ms_jk,
                            ud.d_alamat_user_data,
                            ud.d_kec_user_data,
                            ud.d_kel_user_data,
                            ud.d_rt_user_data,
                            ud.d_rw_user_data,
                            ud.d_kota_user_data,
                            ud.d_provinsi
                    FROM user us
                    LEFT JOIN user_data ud ON us.id_user = ud.id_user
                    ORDER BY id_user
                    LIMIT ".$start.", ". $length)->result();
        }

        public function user_count(){
            return $this->db->query("SELECT COUNT(id_user) as count_data FROM user")->row_array();
        }

    # user report
        function get_user_report($start, $length){

            return $this->db->query("SELECT 
                @id_user := us.id_user as id_user,
                us.email_user,
                us.username_user,
                us.status_active_user,
                us.nama_user,
                us.nik_user,
                us.kk_user,
		        us.tlp_user,
                sts_submit,
                
                ud.ds_nama_dasawisma,
                ud.ms_jk,
                ud.d_alamat_user_data,
                ud.d_kec_user_data,
                ud.d_kel_user_data,
                ud.d_rt_user_data,
                ud.d_rw_user_data,
                ud.d_kota_user_data,
                ud.d_provinsi,

                (SELECT COUNT(*) FROM user_data WHERE id_user = @id_user) as user_data,
                (SELECT COUNT(*) FROM user_or WHERE id_user = @id_user) as user_or,
                (SELECT COUNT(*) FROM user_kerja WHERE id_user = @id_user) as user_kerja,
                (SELECT COUNT(*) FROM user_tmpt WHERE id_user = @id_user) as user_tmpt,
                (SELECT COUNT(*) FROM user_sosial WHERE id_user = @id_user) as user_sosial,
                (SELECT COUNT(*) FROM user_keuangan WHERE id_user = @id_user) as user_keuangan,
                (SELECT COUNT(*) FROM user_kesehatan WHERE id_user = @id_user) as user_kesehatan,
                (SELECT COUNT(*) FROM user_transport WHERE id_user = @id_user) as user_transport,
                (SELECT COUNT(*) FROM user_pendidikan WHERE id_user = @id_user) as user_pendidikan
            FROM user us
            LEFT JOIN user_data ud ON us.id_user = ud.id_user
            ORDER BY id_user
            LIMIT ".$start.", ".$length)->result();
        }

        function get_user_report_like($fields, $param, $start, $length){
            $str_fields = "";
            if($fields){
                $str_fields = implode(", ",$fields);
            }

            return $this->db->query("SELECT 
                    @id_user := us.id_user as id_user,
                    us.email_user,
                    us.username_user,
                    us.status_active_user,
                    us.nama_user,
                    us.nik_user,
                    us.kk_user,
                    us.tlp_user,
                    sts_submit,
                    
                    ud.ds_nama_dasawisma,
                    ud.ms_jk,
                    ud.d_alamat_user_data,
                    ud.d_kec_user_data,
                    ud.d_kel_user_data,
                    ud.d_rt_user_data,
                    ud.d_rw_user_data,
                    ud.d_kota_user_data,
                    ud.d_provinsi,
            
                    (SELECT COUNT(*) FROM user_data WHERE id_user = @id_user) as user_data,
                    (SELECT COUNT(*) FROM user_or WHERE id_user = @id_user) as user_or,
                    (SELECT COUNT(*) FROM user_kerja WHERE id_user = @id_user) as user_kerja,
                    (SELECT COUNT(*) FROM user_tmpt WHERE id_user = @id_user) as user_tmpt,
                    (SELECT COUNT(*) FROM user_sosial WHERE id_user = @id_user) as user_sosial,
                    (SELECT COUNT(*) FROM user_keuangan WHERE id_user = @id_user) as user_keuangan,
                    (SELECT COUNT(*) FROM user_kesehatan WHERE id_user = @id_user) as user_kesehatan,
                    (SELECT COUNT(*) FROM user_transport WHERE id_user = @id_user) as user_transport,
                    (SELECT COUNT(*) FROM user_pendidikan WHERE id_user = @id_user) as user_pendidikan
            FROM user us
            LEFT JOIN user_data ud ON us.id_user = ud.id_user
            WHERE CONCAT(".$str_fields.") LIKE \"%".$param."%\"
            ORDER BY id_user
            LIMIT ".$start.", ".$length)->result();
        }

    # get_qolwat
    
        public function get_qolwat_full($where){
            $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
            $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
            $this->db->join("m_kamar km", "tq.id_kamar = km.id_kamar", "left");
            $this->db->order_by("date_tr_qolwat", "DESC");
            $data = $this->db->get_where("tr_qolwat tq", $where);
            return $data->result();
        }


        public function get_qolwat_single($where){
            $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
            $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
            $this->db->order_by("date_tr_qolwat", "DESC");
            $data = $this->db->get_where("tr_qolwat tq", $where);
            return $data->row_array();
        }

        public function get_qolwat_between($where, $date_start, $date_finish){
            $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
            $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
            $this->db->join("m_kamar km", "tq.id_kamar = km.id_kamar", "left");
            $this->db->where('date_tr_qolwat >=',  $date_start);
            $this->db->where('date_tr_qolwat <=',  $date_finish);
            $this->db->or_where('DATE_FORMAT(date_tr_qolwat, \'%Y-%m-%d\') =',  $date_finish);
            $this->db->order_by("date_tr_qolwat", "DESC");
            $data = $this->db->get_where("tr_qolwat tq", $where);
            return $data->result();
        }
        

   
}
?>