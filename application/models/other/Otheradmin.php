<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Otheradmin extends CI_Model
{

    # get_report_qolwat

    public function get_report_qolwat()
    {
        $data = $this->db->query("SELECT *, 
        @id_user := id_user as id_user, 
        nm_pp,
        (SELECT COUNT(*) FROM tr_qolwat WHERE id_user = @id_user) as jml_qolwat 
        FROM m_user u
        JOIN m_pp pp ON u.id_pp = pp.id_pp
        WHERE is_delete = '0'");

        return $data->result();
    }

    # get_qolwat

    public function get_qolwat_full($where)
    {
        $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
        $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
        $this->db->join("m_kamar km", "tq.id_kamar = km.id_kamar", "left");
        $this->db->order_by("date_tr_qolwat", "DESC");
        $data = $this->db->get_where("tr_qolwat tq", $where);
        return $data->result();
    }


    public function get_qolwat_single($where)
    {
        $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
        $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
        $this->db->join("m_kamar km", "tq.id_kamar = km.id_kamar", "left");
        $this->db->order_by("date_tr_qolwat", "DESC");
        $data = $this->db->get_where("tr_qolwat tq", $where);
        return $data->row_array();
    }

    public function get_qolwat_between($where, $date_start, $date_finish)
    {
        $this->db->join("m_petugas mp", "tq.id_petugas = mp.id_petugas", "left");
        $this->db->join("m_user mu", "tq.id_user = mu.id_user", "left");
        $this->db->join("m_kamar km", "tq.id_kamar = km.id_kamar", "left");
        $this->db->where('date_tr_qolwat >=',  $date_start);
        $this->db->where('date_tr_qolwat <=',  $date_finish);
        $this->db->or_where('DATE_FORMAT(date_tr_qolwat, \'%Y-%m-%d\') =',  $date_finish);
        $this->db->order_by("date_tr_qolwat", "DESC");
        $data = $this->db->get_where("tr_qolwat tq", $where);
        return $data->result();
    }
}
