<?php
include 'jwt/JWT.php';
// 
class Mainjwt {
    private $main_jwt;

    private $KEY = "fc74a3f699461cca1c2e45bd54b9ce70b6df2ad71c789e81bec99940501fe848";
    private $ALGO = ['HS256'];

    private $name_main_coockie = "mlg_keren";

    public function __construct(){
        $this->main_jwt = new JWT();
    }

    public function encode_jwt($payload){
        return $this->main_jwt->encode($payload, $this->KEY);
    }

    public function set_coockie($payload){
        $cookie = array(
            'name'   => $this->name_main_coockie,
            'value'  => $this->encode_jwt($payload),
            'expire' => '3600',
            // 'domain' => 'localhost',
            'path'   => '/',
            // 'prefix' => 'myprefix_',
            'secure' => TRUE,
            'httponly' => TRUE
        );
        set_cookie($cookie);
        return $this->encode_jwt($payload);
    }

    public function get_coockie(){
        try{
            $jwtData = [
                    "sts"=>true,
                    "msg"=>"success",
                    "jwt"=>(array)$this->main_jwt->decode(get_cookie($this->name_main_coockie), $this->KEY, $this->ALGO)
            ];
        }catch (Exception $e){
            $jwtData = [
                    "sts"=>false,
                    "msg"=>$e,
                    "jwt"=>[]
            ];
        }
        return $jwtData;
    }
}
?>