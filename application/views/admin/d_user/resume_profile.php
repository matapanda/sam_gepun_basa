<?php
    $id_user = "";
    $email_user = "";
    $username_user = "";
    $password_user = "";
    $status_active_user = "";
    $nama_user = "";
    $nik_user = "";
    $kk_user = "";
    $tlp_user = "";
    $sts_submit = "";
    $is_del_user = "";
    $kode_aktifasi = "";
    $aktif = "";
    
    if(isset($user)){
        if($user){
            $id_user = $user["id_user"];
            $email_user = $user["email_user"];
            $username_user = $user["username_user"];
            $password_user = $user["password_user"];
            $status_active_user = $user["status_active_user"];
            $nama_user = $user["nama_user"];
            $nik_user = $user["nik_user"];
            $kk_user = $user["kk_user"];
            $tlp_user = $user["tlp_user"];
            $sts_submit = $user["sts_submit"];
            $is_del_user = $user["is_del_user"];
            $kode_aktifasi = $user["kode_aktifasi"];
            $aktif = $user["aktif"];
        }
    }


    $id_user_data = "";
    $id_user = "";
    $ds_nama_dasawisma = "";
    $ms_jk = "";
    $ms_tmp_lhr = "";
    $ms_tgl_lhr = "";
    $ms_pd_terakhir = "";
    $ms_sts_pernikahan = "";
    $ms_wn = "";
    $ms_agama = "";
    $ms_suku = "";
    $ms_gol_darah = "";
    $ktp_alamat_user_data = "";
    $ktp_kec_user_data = "";
    $ktp_kel_user_data = "";
    $ktp_rt_user_data = "";
    $ktp_rw_user_data = "";
    $ktp_kota_user_data = "";
    $ktp_provinsi = "";
    $q_akte_user = "";
    $akte_alasan_user = "";
    $akte_no_user = "";
    $q_d_alamat_sesuai_ktp = "";
    $d_alamat_user_data = "";
    $d_kec_user_data = "";
    $d_kel_user_data = "";
    $d_rt_user_data = "";
    $d_rw_user_data = "";
    $d_kota_user_data = "";
    $d_provinsi = "";
    $wn_almt_user_data = "";
    $wn_no_paspor = "";
    $srt_nikah_no = "";
    $srt_akta_cerai = "";
    $srt_kematian_pasangan = "";
    $q_ingin_tinggal = "";
    $q_sts_tinggal = "";
    $akte_user = "";
    $q_lama_tinggal = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    if(isset($user_data)){
        if($user_data){
            $id_user_data = $user_data["id_user_data"];
            $id_user = $user_data["id_user"];
            $ds_nama_dasawisma = $user_data["ds_nama_dasawisma"];
            $ms_jk = $user_data["ms_jk"];
            $ms_tmp_lhr = $user_data["ms_tmp_lhr"];
            $ms_tgl_lhr = $user_data["ms_tgl_lhr"];
            $ms_pd_terakhir = $user_data["ms_pd_terakhir"];
            $ms_sts_pernikahan = $user_data["ms_sts_pernikahan"];
            $ms_wn = $user_data["ms_wn"];
            $ms_agama = $user_data["ms_agama"];
            $ms_suku = $user_data["ms_suku"];
            $ms_gol_darah = $user_data["ms_gol_darah"];
            $ktp_alamat_user_data = $user_data["ktp_alamat_user_data"];
            $ktp_kec_user_data = $user_data["ktp_kec_user_data"];
            $ktp_kel_user_data = $user_data["ktp_kel_user_data"];
            $ktp_rt_user_data = $user_data["ktp_rt_user_data"];
            $ktp_rw_user_data = $user_data["ktp_rw_user_data"];
            $ktp_kota_user_data = $user_data["ktp_kota_user_data"];
            $ktp_provinsi = $user_data["ktp_provinsi"];
            $q_akte_user = $user_data["q_akte_user"];
            $akte_alasan_user = $user_data["akte_alasan_user"];
            $akte_no_user = $user_data["akte_no_user"];
            $q_d_alamat_sesuai_ktp = $user_data["q_d_alamat_sesuai_ktp"];
            $d_alamat_user_data = $user_data["d_alamat_user_data"];
            $d_kec_user_data = $user_data["d_kec_user_data"];
            $d_kel_user_data = $user_data["d_kel_user_data"];
            $d_rt_user_data = $user_data["d_rt_user_data"];
            $d_rw_user_data = $user_data["d_rw_user_data"];
            $d_kota_user_data = $user_data["d_kota_user_data"];
            $d_provinsi = $user_data["d_provinsi"];
            $wn_almt_user_data = $user_data["wn_almt_user_data"];
            $wn_no_paspor = $user_data["wn_no_paspor"];
            $srt_nikah_no = $user_data["srt_nikah_no"];
            $srt_akta_cerai = $user_data["srt_akta_cerai"];
            $srt_kematian_pasangan = $user_data["srt_kematian_pasangan"];
            $q_ingin_tinggal = $user_data["q_ingin_tinggal"];
            $q_sts_tinggal = $user_data["q_sts_tinggal"];
            $akte_user = $user_data["akte_user"];
            $q_lama_tinggal = $user_data["q_lama_tinggal"];
            $r_crt_by = $user_data["r_crt_by"];
            $r_crt_time = $user_data["r_crt_time"];
            $r_up_by = $user_data["r_up_by"];
            $r_up_time = $user_data["r_up_time"];

        }
    }

?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Nama Lengkap</label>
                <div class="col-md-8">
                    <?= strtoupper($nama_user)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">NIK/KIA</label>
                <div class="col-md-8">
                    <?= strtoupper($nik_user)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Nomor KK</label>
                <div class="col-md-8">
                <?= strtoupper($kk_user)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jenis Kelamin</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_jk)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Tanggal Lahir</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_tgl_lhr)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Tempat Lahir</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_tmp_lhr)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Status Kawin</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_sts_pernikahan)?>
                </div>
            </div>
            <div class="form-group row mb-0" id="div-no-akta-nikah">
                <label for="message-text" class="control-label col-md-4">No. Akta Nikah</label>
                <div class="col-md-8">
                    <?= strtoupper($srt_nikah_no)?>
                </div>
            </div>
            <div class="form-group row mb-0" id="div-no-akta-cerai">
                <label for="message-text" class="control-label col-md-4">No. Akta Cerai</label>
                <div class="col-md-8">
                    <?= strtoupper($srt_akta_cerai)?>
                </div>
            </div>
            <div class="form-group row mb-0" id="div-no-akta-cerai-mati">
                <label for="message-text" class="control-label col-md-4">No. Kematian Pasangan</label>
                <div class="col-md-8">
                    <?= strtoupper($srt_kematian_pasangan)?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Akta Kelahiran</label>
                <div class="col-md-8">
                    <?= strtoupper($q_akte_user)?>
                </div>
            </div>
            <div class="form-group row mb-0" id="div-no-akte-kelahiran">
                <label for="message-text" class="control-label col-md-4">No. Akta Kelahiran</label>
                <div class="col-md-8">
                    <?= strtoupper($akte_no_user)?>
                </div>
            </div>
            <div class="form-group row mb-0" id="div-no-akte-keterangan" hidden>
                <label for="message-text" class="control-label col-md-4">Keterangan</label>
                <div class="col-md-8">
                    <?= strtoupper($nama_user)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Nomor Paspor</label>
                <div class="col-md-8">
                    <?= strtoupper($wn_no_paspor)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kewarganegaraan</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_wn)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Suku Bangsa</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_suku)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Agama</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_agama)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Golongan Darah</label>
                <div class="col-md-8">
                    <?= strtoupper($ms_gol_darah)?>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Alamat Sesuai KTP</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_alamat_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Provinsi</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_provinsi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kota</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_kota_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kecamatan</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_kec_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kelurahan</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_kel_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">RT / RW</label>
                <div class="col-md-8">
                    <?= strtoupper($ktp_rt_user_data)?> / <?= strtoupper($ktp_rw_user_data)?>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Domisili Saat ini</label>
                <div class="col-md-8">
                    <?= strtoupper($q_d_alamat_sesuai_ktp)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Alamat Domisili</label>
                <div class="col-md-8">
                    <?= strtoupper($d_alamat_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Provinsi</label>
                <div class="col-md-8">
                    <?= strtoupper($d_provinsi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kota</label>
                <div class="col-md-8">
                    <?= strtoupper($d_kota_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kecamatan</label>
                <div class="col-md-8">
                    <?= strtoupper($d_kec_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kelurahan</label>
                <div class="col-md-8">
                    <?= strtoupper($d_kel_user_data)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">RT / RW</label>
                <div class="col-md-8">
                    <?= strtoupper($d_rt_user_data)?> / <?= strtoupper($d_rw_user_data)?> 
                </div>
            </div>
        </div>

        
    </div>
