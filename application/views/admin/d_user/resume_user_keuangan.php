<?php
    $id_user_keuangan = "";
    $id_user = "";
    $q_harta_tidak_bergerak = "";
    $htb_detail = "";
    $q_harta_bergerak = "";
    $hb_detail = "";
    $q_deposito = "";
    $q_piutang = "";
    $q_hutang = "";
    $ht_nominal = "0";
    $t_kekayaan = "0";
    $q_penghasilan_per_th = "";
    $q_penghasilan_profesi = "";
    $q_penghasilan_usaha = "";
    $q_penghasilan_hibah = "";
    $t_penghasilan = "0";
    $t_pengeluaran_rutin = "0";
    $t_pengeluaran_lainnya = "0";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    
    
    $str_htb_detail = "";
    $str_hb_detail = "";

    $t_htb_detail = 0;
    $t_hb_detail = 0;

    $t_all_kekayaan = 0;

    $t_all_pengeluaran = 0;

    $t_penghasilan_all = 0;

    if(isset($user_keuangan)){
        if($user_keuangan){
            $id_user_keuangan = $user_keuangan["id_user_keuangan"];
            $id_user = $user_keuangan["id_user"];
            $q_harta_tidak_bergerak = $user_keuangan["q_harta_tidak_bergerak"];
            
            $htb_detail = json_decode($user_keuangan["htb_detail"]);
            if($htb_detail){
                $no = 1;
                foreach ($htb_detail as $key => $value) {
                    $str_htb_detail .= "<tr>
                                            <td>".$no."</td>
                                            <td>".$value->jn_htb_detail."</td>
                                            <td>Rp. ".number_format($value->n_htb_detail, 0, ',', '.')."</td>
                                        </tr>";
                    $t_htb_detail += (int) $value->n_htb_detail;
                    $no++;
                }
            }
            
            $q_harta_bergerak = $user_keuangan["q_harta_bergerak"];
            
            $hb_detail = json_decode($user_keuangan["hb_detail"]);
            if($hb_detail){
                $no = 1;
                foreach ($hb_detail as $key => $value) {
                    $str_htb_detail .= "<tr>
                                            <td>".$no."</td>
                                            <td>".$value->jn_hb_detail."</td>
                                            <td>Rp. ".number_format($value->n_hb_detail, 0, ',', '.')."</td>
                                        </tr>";
                    $t_hb_detail += (int) $value->n_hb_detail;
                    $no++;
                }
            }
            
            $q_deposito = $user_keuangan["q_deposito"];
            $q_piutang = $user_keuangan["q_piutang"];
            $q_hutang = $user_keuangan["q_hutang"];
            $ht_nominal = $user_keuangan["ht_nominal"];
            $t_kekayaan = $user_keuangan["t_kekayaan"];

            $t_all_kekayaan = $t_hb_detail + $t_htb_detail + $t_kekayaan - $ht_nominal;

            $q_penghasilan_per_th = $user_keuangan["q_penghasilan_per_th"];
            $q_penghasilan_profesi = $user_keuangan["q_penghasilan_profesi"];
            $q_penghasilan_usaha = $user_keuangan["q_penghasilan_usaha"];
            $q_penghasilan_hibah = $user_keuangan["q_penghasilan_hibah"];

            $t_penghasilan = $user_keuangan["t_penghasilan"];
            
            $t_pengeluaran_rutin = $user_keuangan["t_pengeluaran_rutin"];
            $t_pengeluaran_lainnya = $user_keuangan["t_pengeluaran_lainnya"];
            $t_all_pengeluaran = $t_pengeluaran_rutin + $t_pengeluaran_lainnya;

            $t_penghasilan_all = $t_penghasilan - $t_all_pengeluaran;

            $r_crt_by = $user_keuangan["r_crt_by"];
            $r_crt_time = $user_keuangan["r_crt_time"];
            $r_up_by = $user_keuangan["r_up_by"];
            $r_up_time = $user_keuangan["r_up_time"];
        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Harta Tidak Bergerak (Tanah dan Bangunan)</label>
                <div class="col-md-8">
                    <?= strtoupper($q_harta_tidak_bergerak)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Detail Harta Tidak Bergerak (Alat Transportasi, Peternakan, Logam Mulia,dll)</label>
                <div class="col-md-8">
                    
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Jenis Harta</td>
                                <td>Nilai Harta</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_htb_detail?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Harta Bergerak (Alat Transportasi, Peternakan, Logam Mulia,dll)</label>
                <div class="col-md-8">
                    <?= strtoupper($q_harta_bergerak)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Detail Harta Bergerak (Alat Transportasi, Peternakan, Logam Mulia,dll)</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Jenis Harta</td>
                                <td>Nilai Harta</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_hb_detail?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Uang Tunai, Deposito, Giro, Tabungan dan kas Lainnya:</label>
                <div class="col-md-8">
                <?= strtoupper($q_deposito)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Piutang (Barang, Uang):</label>
                <div class="col-md-8">
                    <?= strtoupper($q_piutang)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah anda memiliki hutang ? (Barang, Uang):</label>
                <div class="col-md-8">
                    <?= strtoupper($q_hutang)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Hutang</label>
                <div class="col-md-8">
                    <?="Rp. ".number_format($ht_nominal, 0, ',', '.')?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Total Harta Kekayaan</label>
                <div class="col-md-8">
                    
                    <?="Rp. ".number_format($t_kekayaan, 0, ',', '.')?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Total Harta :</label>
                <div class="col-md-8">
                    <?="Rp. ".number_format($t_all_kekayaan, 0, ',', '.')?>
                </div>
            </div>

            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Penghasilan dari (Per Tahun)</label>
                <div class="col-md-8">
                    <?= strtoupper($q_penghasilan_per_th)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Penghasilan dari Profesi/Keahlian (Per Tahun):</label>
                <div class="col-md-8">
                    <?= strtoupper($q_penghasilan_profesi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Penghasilan dari Usaha Lainnya (Per tahun):</label>
                <div class="col-md-8">
                    <?= strtoupper($q_penghasilan_usaha)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Penghasilan dari Hibah Lainnya</label>
                <div class="col-md-8">
                    
                    <?= strtoupper($q_penghasilan_hibah)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Total Penghasilan</label>
                <div class="col-md-8">
                <?="Rp. ".number_format($t_penghasilan, 0, ',', '.')?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Pengeluaran Rutin</label>
                <div class="col-md-8">
                <?="Rp. ".number_format($t_pengeluaran_rutin, 0, ',', '.')?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Pengeluaran Lainnya</label>
                <div class="col-md-8">
                <?="Rp. ".number_format($t_pengeluaran_lainnya, 0, ',', '.')?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Total Pengeluaran</label>
                <div class="col-md-8">
                <?="Rp. ".number_format($t_penghasilan_all, 0, ',', '.')?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Pengehasilan Bersih</label>
                <div class="col-md-8">
                <?="Rp. ".number_format($t_penghasilan_all, 0, ',', '.')?>
                </div>
            </div>
            
        </div>
    </div>
    