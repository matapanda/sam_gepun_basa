<?php
    $id_parent = "0";
    if($this->uri->segment(3)){
        $id_parent = $this->uri->segment(3);
    }

    $url_list_app = base_url()."admin/list-jenis-all/";
    $url_add_app = base_url()."admin/list-jenis-all/".$id_parent;
    $url_up_app = base_url()."admin/list-jenis-all/";

    $main_controller = base_url()."admin/listjenis/";

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    <!-- <a href=""></a> -->

    <div class="col-md-6 text-right">
        <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_data"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data</button>

    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <?php
        if($id_parent != "0"){
    ?>
            <div class="row">
                <div class="col-md-12 align-self-center">
                    <ol class="breadcrumb">
                        <!-- <li class="breadcrumb-item"><a href="<?=$url_list_app."0"?>">Main</a></li> -->
                        <?php
                            if(isset($patern)){
                                if($patern){
                                    foreach ($patern as $key => $value) {

                                        // print_r($value);
                                        // die();
                                        echo '<li class="breadcrumb-item"><a href="'.$url_list_app.$value["id_ms_jenis_all"].'">'.$value["nm_jenis"].'</a></li>';
                                    }
                                }
                            }
                            if(isset($parent)){
                                if($parent){
                                    echo '<li class="breadcrumb-item">'.$parent["nm_jenis"].'</li>';
                                }
                            }
                        ?>
                    </ol>
                </div>
            </div>
    <?php 
        }
    ?>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="10%">ID Jenis. </th>
                                            <th width="15%">KODE Jenis</th>
                                            <th width="20%">Nama</th>
                                            <th width="25%">Deskripsi</th>
                                            <th width="15%">Sts. Aktif</th>
                                            <th width="15%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r => $v) {
                                                    
                                                    echo "<tr>
                                                            <td>".$v->id_ms_jenis_all."</td>
                                                            <td><a href=\"".$url_list_app.$v->id_ms_jenis_all."\">".$v->kode_jenis."</td>
                                                            <td>".$v->nm_jenis."</td>
                                                            <td>".$v->desc."</td>
                                                            <td>".$v->sts_act."</td>
                                                            <td>
                                                                <center>
                                                                <button class=\"btn btn-info\" id=\"up_data\" onclick=\"update_data('".$v->id_ms_jenis_all ."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;
                                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$v->id_ms_jenis_all ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                            </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form class="form-horizontal form-material" id="form_ins" method="post" action="javascript:void(0)"> -->
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Tambah <?=$title?></h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>

                <div class="modal-body">
                    <div class="col-md-12">
                        <form class="form-horizontal form-material" id="form_ins" method="post" action="javascript:void(0)">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="recipient-name" class="control-label">Kode Jenis<span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="kode_jenis" name="kode_jenis" required="">
                                        <p id="msg_kode_jenis" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Nama Jenis <span style="color: red;">*</span></label>
                                        <input type="text" class="form-control" id="nm_jenis" name="nm_jenis" required="">
                                        <p id="msg_nm_jenis" style="color: red;"></p>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label for="message-text" class="control-label">Deskripsi <span style="color: red;"></span></label>
                                        <input type="text" class="form-control" id="desc" name="desc" value="" required="">
                                        <p id="msg_desc" style="color: red;"></p>
                                    </div>
                                </div>

                                <div class="col-md-12">
                                    <div class="form-group text-right">
                                        <button type="submit" id="btn_add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan Data</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>         
                </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_data------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_data" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <form class="form-horizontal form-material" id="form_up" method="post" action="javascript:void(0)">
                <div class="modal-header">
                    <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data Admin</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <!-- <form action="<?= base_url()."admin_super/superadmin/update_data";?>" method="post"> -->
                <div class="modal-body">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="recipient-name" class="control-label">Kode Jenis<span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="_kode_jenis" name="kode_jenis" required="">
                                    <p id="_msg_kode_jenis" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Nama Jenis <span style="color: red;">*</span></label>
                                    <input type="text" class="form-control" id="_nm_jenis" name="nm_jenis" required="">
                                    <p id="_msg_nm_jenis" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label for="message-text" class="control-label">Deskripsi <span style="color: red;"></span></label>
                                    <input type="text" class="form-control" id="_desc" name="desc" value="" required="">
                                    <p id="_msg_desc" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group text-right">
                                    <button type="submit" id="btn_up_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
                                </div>
                            </div>
                        </div>
                    </div>         
                </div>
            </form>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_data------------------------ -->
<!-- ============================================================== -->

<!-- id_ms_jenis_all
ms_jenis_all_id
pattern
tipe
kode_jenis
nm_jenis
desc
sts_act -->



<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert_data--------------------------//
    //=========================================================================//

        function add_data(){
            var data_main = new FormData();
            data_main.append('ms_jenis_all_id'  , "<?=$id_parent?>");
            data_main.append('nm_jenis'         , $("#nm_jenis").val());
            data_main.append('kode_jenis'       , $("#kode_jenis").val());
            data_main.append('desc'             , $("#desc").val());

            $.ajax({
                url: "<?=$main_controller?>insert_data",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        }
        

        $("#form_ins").submit(function(e){
            e.preventDefault();
            add_data();
            
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_data').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?= $url_list_app.$id_parent?>");
            } else {
                $("#msg_nm_jenis").html(detail_msg.nm_jenis);
                $("#msg_kode_jenis").html(detail_msg.kode_jenis);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#nm_jenis").val("");
            $("#kode_jenis").val("");
            $("#desc").val("");

            $("#msg_nm_jenis").html("");
            $("#msg_kode_jenis").html("");
            $("#msg_desc").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_data--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_nm_jenis").val("");
            $("#_kode_jenis").val("");
            $("#_desc").val("");

            $("#_msg_nm_jenis").html("");
            $("#_msg_kode_jenis").html("");
            $("#_msg_desc").html("");
        }

        function update_data(id_ms_jenis_all) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_ms_jenis_all', id_ms_jenis_all);

            $.ajax({
                url: "<?=$main_controller?>get_data",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_ms_jenis_all);
                    $("#update_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_ms_jenis_all) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_ms_jenis_all;

                $("#_nm_jenis").val(list_data.nm_jenis);
                $("#_kode_jenis").val(list_data.kode_jenis);
                $("#_desc").val(list_data.desc);
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        function up_data() {
            var data_main = new FormData();
            data_main.append('id_ms_jenis_all', id_cache);
            data_main.append('ms_jenis_all_id'  , "<?=$id_parent?>");

            data_main.append('nm_jenis'         , $("#_nm_jenis").val());
            data_main.append('kode_jenis'       , $("#_kode_jenis").val());
            data_main.append('desc'             , $("#_desc").val());

            $.ajax({
                url: "<?= $main_controller?>update_data",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        }

        $("#form_up").submit(function(e){
            e.preventDefault();
            up_data();
            
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_admin').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$url_list_app.$id_parent?>");
            } else {
                $("#_msg_nm_jenis").html(detail_msg.nm_jenis);
                $("#_msg_kode_jenis").html(detail_msg.kode_jenis);
                $("#_msg_desc").html(detail_msg.desc);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_ms_jenis_all ){
            var data_main = new FormData();
            data_main.append('id_ms_jenis_all', id_ms_jenis_all );

            $.ajax({
                url: "<?= $main_controller."delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_jn_mcctv ) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_jn_mcctv );
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?= $url_list_app.$id_parent;?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//
</script>


