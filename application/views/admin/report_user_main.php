<?php
    $main_url_user = base_url()."admin/report-data-user/";
    // $main_list_table = base_url()."admin/u";
    
    $main_controller = base_url()."admin/reportusermain/";
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel <?=$title?></h4>

                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data User</button>

                    <div class="table-responsive m-t-40">
                        <table id="data_list" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th width="5%">ID. User</th>
                                    <!-- <th width="10%">Username</th> -->
                                    <th width="20%">Nama User</th>
                                    <th width="25%">Email, Tlp, Username</th>
                                    <th width="20%">Alamat</th>
                                    <th width="10%">Pengisian Data</th>
                                    <th width="10%">Status User</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="main_table_content">
        
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="detail_user" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;" style="width: 80%;">
    <div class="modal-dialog modal-lg" role="document" style="max-width: 80%;">
        <div class="modal-content" >
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Detail Data User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="vtabs ">
                            <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="link_profil" data-toggle="tab" href="#out_profil" role="tab" aria-selected="true" id="ls_ck_profile">
                                        <span><i class="mdi mdi-home-variant"></i></span>&nbsp;&nbsp;Data Kependudukan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_ketenagakerjaan" data-toggle="tab" href="#out_ketenagakerjaan" role="tab" aria-selected="true" id="ls_user_kerja">
                                        <span><i class="mdi mdi-account-edit"></i></span>&nbsp;&nbsp;Data Ketenagakerjaan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_kesehatan" data-toggle="tab" href="#out_kesehatan" role="tab" aria-selected="true" id="ls_user_kesehatan">
                                        <span><i class="mdi mdi-ambulance"></i></span>&nbsp;&nbsp;Data Kesehatan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_keuangan" data-toggle="tab" href="#out_keuangan" role="tab" aria-selected="true" id="ls_user_keuangan">
                                        <span><i class="mdi mdi-cash-usd"></i></span>&nbsp;&nbsp;Data Keuangan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_or" data-toggle="tab" href="#out_or" role="tab" aria-selected="true" id="ls_user_or">
                                        <span><i class="mdi mdi-school"></i></span>&nbsp;&nbsp;Data Kegiatan dan Organisasi
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_pendidikan" data-toggle="tab" href="#out_pendidikan" role="tab" aria-selected="true" id="ls_user_pendidikan">
                                        <span><i class="mdi mdi-school"></i></span>&nbsp;&nbsp;Data Pendidikan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_sosial" data-toggle="tab" href="#out_sosial" role="tab" aria-selected="true" id="ls_user_sosial">
                                        <span><i class="mdi mdi-cash"></i></span>&nbsp;&nbsp;Data Sosial Ekonomi
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_tmpt" data-toggle="tab" href="#out_tmpt" role="tab" aria-selected="true" id="ls_user_tmpt">
                                        <span><i class="mdi mdi-car"></i></span>&nbsp;&nbsp;Data Lingkungan
                                    </a>
                                </li>

                                <li class="nav-item">
                                    <a class="nav-link" id="link_transport" data-toggle="tab" href="#out_transport" role="tab" aria-selected="true" id="ls_user_transport">
                                        <span><i class="mdi mdi-hops"></i></span>&nbsp;&nbsp;Data Transportasi dan Komunikasi
                                    </a>
                                </li>

                                
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content" style="width: 100%;">
                                <div class="tab-pane active" id="out_profil" role="tabpanel">
                                    
                                </div>
                                <div class="tab-pane" id="out_ketenagakerjaan" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_kesehatan" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_keuangan" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_or" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_pendidikan" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_sosial" role="tabpanel">
    
                                </div>
                                <div class="tab-pane" id="out_tmpt" role="tabpanel">

                                </div>
                                <div class="tab-pane" id="out_transport" role="tabpanel">

                                </div>
                            </div>
                        </div>        
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    let def_datatables = (req) => {
        return {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url" : "<?=$main_controller?>index_list_data",
                "deferRender": true,
                "type" : "POST",
                "data" : {
                    "keterangan" : req
                }
            },
            "columns": [
                    {
                        data: null,
                        render: function (data, type, full, meta) {
                            console.log(data)
                            var id_user = data.id_user
                            // Combine the first and last names into a single table field
                            return id_user;
                        }
                    },
                    {
                        data: null,
                        render: function (data, type, full, meta) {
                            var nama_user = data.nama_user;
                            var nik_user  = data.nik_user ;
                            // var id_user = data.id_user;
                            return nik_user+" ("+nama_user+")";
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var username = data.username_user;
                            var email = data.email_user;
                            var tlp_user = data.tlp_user;

                            return "Username: "+username+"<br>Email: "+email+"<br>No. Tlp: "+tlp_user+"";
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var d_alamat_user_data = data.d_alamat_user_data ? data.d_alamat_user_data : ""
                            var d_kec_user_data = data.d_kec_user_data ? data.d_kec_user_data : ""
                            var d_kel_user_data = data.d_kel_user_data ? data.d_kel_user_data : ""
                            var d_rt_user_data = data.d_rt_user_data ? data.d_rt_user_data : ""
                            var d_rw_user_data = data.d_rw_user_data ? data.d_rw_user_data : ""
                            var d_kota_user_data = data.d_kota_user_data ? data.d_kota_user_data : ""
                            var d_provinsi = data.d_provinsi ? data.d_provinsi : ""
                            
                            return d_alamat_user_data+", "+
                                    d_rt_user_data+", "+
                                    d_rw_user_data+", "+
                                    d_kel_user_data+", "+
                                    d_kec_user_data+", "+
                                    d_kota_user_data+", "+
                                    d_provinsi+", "
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user
                            var sts_submit = data.sts_submit

                            var str_active = "<span class=\"label label-warning\">belum disubmit</span>";
                            if(sts_submit == "1"){
                                str_active = "<span class=\"label label-info\">sudah disubmit</span>";
                            }

                            // var btn_pass = "<button class=\"btn btn-sm btn-block btn-primary\" id=\"btn_ch_pass\" onclick=\"ch_pass('"+id_user+"')\">Ubah Password</button>"

                            var user_data = data.user_data
                            var user_or = data.user_or
                            var user_kerja = data.user_kerja
                            var user_tmpt = data.user_tmpt
                            var user_sosial = data.user_sosial
                            var user_keuangan = data.user_keuangan
                            var user_kesehatan = data.user_kesehatan
                            var user_transport = data.user_transport
                            var user_pendidikan = data.user_pendidikan

                            console.log(data)

                            var count_all = 9
                            var hasil = (parseInt(user_data) + parseInt(user_or) + parseInt(user_kerja) + parseInt(user_tmpt) + parseInt(user_sosial) + parseInt(user_keuangan) + parseInt(user_kesehatan) + parseInt(user_transport) + parseInt(user_pendidikan)) / count_all * 100

                            return hasil.toFixed(2) + " % <br>" + str_active
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user;
                            var status_active = data.status_active_user;

                            var str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                            if(status_active == "1"){
                                str_active = "<span class=\"label label-info\">aktif</span>";
                            }else if (status_active == "2") {
                                str_active = "<span class=\"label label-danger\">di blockir</span>";
                            }

                            return str_active;
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user;
                            
                            return "<center>"+
                            
                                "<button class=\"btn btn-info\" id=\"d_user\" onclick=\"detail_user('"+id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-list\"></i></button>&nbsp;&nbsp;"+
                                "</center>";
                        }
                    }
                ]
        }
    }

    $(document).ready(function(){
        $('#data_list').DataTable(def_datatables("all"));

        // send("out_profil", "out_profil")
    });

    function detail_user(id){
        $("#detail_user").modal('show');

        id_ch_cache = id

        send("out_profil", "out_profil")
    }


    $("#link_profil").click(function(){
        send("out_profil", "out_profil")
    });

    $("#link_ketenagakerjaan").click(function(){
        send("out_ketenagakerjaan", "out_ketenagakerjaan")
    });

    $("#link_kesehatan").click(function(){
        send("out_kesehatan", "out_kesehatan")
    });

    $("#link_keuangan").click(function(){
        send("out_keuangan", "out_keuangan")
    });

    $("#link_or").click(function(){
        send("out_or", "out_or")
    });

    $("#link_pendidikan").click(function(){
        send("out_pendidikan", "out_pendidikan")
    });

    $("#link_sosial").click(function(){
        send("out_sosial", "out_sosial")
    });

    $("#link_tmpt").click(function(){
        send("out_tmpt", "out_tmpt")
    });

    $("#link_transport").click(function(){
        send("out_transport", "out_transport")
    });

    function send(url_param, out_html){
        var data_main = new FormData();
        // data_main.append('id_user', id_user);

        $.ajax({
            url: "<?=$main_controller?>"+url_param+"/"+id_ch_cache,
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            success: function(res) {

                $("#"+out_html).html(res);
                // console.log(res);
                // response_active_admin(res);
            }
        });
    }


</script>