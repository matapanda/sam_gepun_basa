<?php
    $main_url_user = base_url()."admin/user/";
    // $main_list_table = base_url()."admin/u";
    
    $main_controller = base_url()."admin/usermain/";

    $path_img = base_url()."assets/prw/img/icon/";
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor">Data User</h3>
    </div>
    
    <div>
        <button class="right-side-toggle waves-effect waves-light btn-inverse btn btn-circle btn-sm pull-right m-l-10"><i class="ti-settings text-white"></i></button>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <div class="row">
        <div class="col-12 m-t-30">
            <!-- Card -->
            <div class="card card-outline-info">
                <div class="card-header">
                    <h4 class="m-b-0 text-white">Tabel Data User</h4>

                </div>
                <div class="card-body">
                    <button type="button" class="btn btn-rounded btn-info" data-toggle="modal" data-target="#insert_admin"><i class="fa fa-download"></i>&nbsp;&nbsp;&nbsp;Tambah Data User</button>

                    <div class="table-responsive m-t-40">
                        <table id="data_list" class="table table-bordered table-striped" style="width: 100%;">
                            <thead>
                                <tr>
                                    <th width="5%">ID. User</th>
                                    <!-- <th width="10%">Username</th> -->
                                    <th width="15%">Nama User</th>
                                    <th width="25%">Email, Tlp, Username</th>
                                    <th width="20%">Alamat</th>
                                    <th width="10%">Ubah Password</th>
                                    <th width="10%">Status User</th>
                                    <th width="15%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody id="main_table_content">
        
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="card-body">
                    <div class="text-right">
                        <label class="form-label">Keterangan Tombol Aksi ==> </label>

                        <a class="btn btn-info" style="width: 40px;"><i class="fa fa-pencil-square-o" style="color: white;"></i></a>
                        <label class="form-label text-info">Update Data</label>,&nbsp;
                        <a class="btn btn-danger" style="width: 40px;"><i class="fa fa-trash-o" style="color: white;"></i></a>
                        <label class="form-label text-danger">Delete Data</label>
                    </div>
                </div>
            </div>
            <!-- Card -->
        </div>
    </div>
    <!-- End Row -->
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-md" tabindex="-1" role="dialog" id="change_pass_modal" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Ubah Password User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password :</label>
                                <input type="Password" class="form-control" id="ch_pass" name="pass" required="">
                                <p id="_msg_ch_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password :</label>
                                <input type="Password" class="form-control" id="ch_repass" name="repass" required="">
                                <p id="_msg_ch_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="add_pass_new" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------change_pass_modal------------------- -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="insert_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <!-- <form method="post" action="<?= base_url()."admin_super/superadmin/insert_admin";?>"> -->
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Tambah User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>

            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tlp <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="tlp_user" name="tlp_user" required="">
                                <p id="msg_tlp_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="email_user" name="email_user" required="">
                                <p id="msg_email_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nama_user" name="nama_user" required="">
                                <p id="msg_nama_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="nik_user" name="nik_user" value="0" required="">
                                <p id="msg_nik_user" style="color: red;"></p>
                            </div>
                        </div>
                        
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">username_user <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="username_user" name="username_user" required="">
                                <p id="msg_username_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="pass" name="pass" required="">
                                <p id="msg_pass" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Ulangi Password <span style="color: red;">*</span></label>
                                <input type="Password" class="form-control" id="repass" name="repass" required="">
                                <p id="msg_repass" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="submit" id="add_admin" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------insert_admin------------------------ -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->
<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" id="update_admin" aria-labelledby="myLargeModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="exampleModalLabel1">Form Ubah Data User</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <!-- <form action="<?= base_url()."admin_super/superadmin/update_admin";?>" method="post"> -->
            <div class="modal-body">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Tlp <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_tlp_user" name="tlp_user" required="">
                                <p id="_msg_tlp_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Email <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_email_user" name="email_user" required="">
                                <p id="_msg_email_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nama_user" name="nama_user" required="">
                                <p id="_msg_nama_user" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6" hidden="">
                            <div class="form-group">
                                <label for="message-text" class="control-label">NIK User <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_nik_user" name="nik_user" value="a" readonly="" required="">
                                <p id="_msg_nik_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>         
                
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <hr>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="recipient-name" class="control-label">Username <span style="color: red;">*</span></label>
                                <input type="text" class="form-control" id="_username_user" name="username_user" required="">
                                <p id="_msg_username_user" style="color: red;"></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" id="btn_update_data" class="btn waves-effect waves-light btn-rounded btn-info">Ubah Data</button>
            </div>
            <!-- </form> -->
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------update_admin------------------------ -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    let def_datatables = (req) => {
        return {
            "processing": true,
            "serverSide": true,
            "ajax": {
                "url" : "<?=$main_controller?>index_list_data",
                "deferRender": true,
                "type" : "POST",
                "data" : {
                    "keterangan" : req
                }
            },
            "columns": [
                    {
                        data: null,
                        render: function (data, type, full, meta) {
                            console.log(data)
                            var id_user = data.id_user
                            // Combine the first and last names into a single table field
                            return id_user;
                        }
                    },
                    {
                        data: null,
                        render: function (data, type, full, meta) {
                            var nama_user = data.nama_user;
                            var nik_user  = data.nik_user ;
                            // var id_user = data.id_user;
                            return nik_user+" ("+nama_user+")";
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var username = data.username_user;
                            var email = data.email_user;
                            var tlp_user = data.tlp_user;

                            return "Username: "+username+"<br>Email: "+email+"<br>No. Tlp: "+tlp_user+"";
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var d_alamat_user_data = data.d_alamat_user_data ? data.d_alamat_user_data : ""
                            var d_kec_user_data = data.d_kec_user_data ? data.d_kec_user_data : ""
                            var d_kel_user_data = data.d_kel_user_data ? data.d_kel_user_data : ""
                            var d_rt_user_data = data.d_rt_user_data ? data.d_rt_user_data : ""
                            var d_rw_user_data = data.d_rw_user_data ? data.d_rw_user_data : ""
                            var d_kota_user_data = data.d_kota_user_data ? data.d_kota_user_data : ""
                            var d_provinsi = data.d_provinsi ? data.d_provinsi : ""
                            
                            return d_alamat_user_data+", "+
                                    d_rt_user_data+", "+
                                    d_rw_user_data+", "+
                                    d_kel_user_data+", "+
                                    d_kec_user_data+", "+
                                    d_kota_user_data+", "+
                                    d_provinsi+", "
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user

                            var btn_pass = "<button class=\"btn btn-sm btn-block btn-primary\" id=\"btn_ch_pass\" onclick=\"ch_pass('"+id_user+"')\">Ubah Password</button>"

                            // var user_data = data.user_data
                            // var user_or = data.user_or
                            // var user_kerja = data.user_kerja
                            // var user_tmpt = data.user_tmpt
                            // var user_sosial = data.user_sosial
                            // var user_keuangan = data.user_keuangan
                            // var user_kesehatan = data.user_kesehatan
                            // var user_transport = data.user_transport
                            // var user_pendidikan = data.user_pendidikan

                            // console.log(data)

                            // var count_all = 9
                            // var hasil = (parseInt(user_data) + parseInt(user_or) + parseInt(user_kerja) + parseInt(user_tmpt) + parseInt(user_sosial) + parseInt(user_keuangan) + parseInt(user_kesehatan) + parseInt(user_transport) + parseInt(user_pendidikan)) / count_all * 100

                            return btn_pass
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user;
                            var status_active = data.status_active_user;

                            var str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                            if(status_active == "1"){
                                str_active = "<span class=\"label label-info\">aktif</span>";
                            }else if (status_active == "2") {
                                str_active = "<span class=\"label label-danger\">di blockir</span>";
                            }

                            return str_active;
                        }
                    },
                    {
                        data: null,
                        render: function ( data, type, full, meta ) {
                            var id_user = data.id_user;
                            var status_active = data.status_active_user;

                            var str_btn_active = "<button class=\"btn btn-primary\" id=\"un_ac_admin\" onclick=\"disabled_data('"+id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";

                            if(status_active != "1"){
                                str_btn_active = "<button class=\"btn btn-success\" id=\"ac_admin\" onclick=\"active_data('"+id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                            }

                            return "<center>"+str_btn_active+
                                "<button class=\"btn btn-info\" id=\"up_admin\" onclick=\"update_data('"+id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                                "<button class=\"btn btn-danger\" id=\"del_admin\" onclick=\"delete_admin('"+id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>&nbsp;&nbsp;"+
                                "</center>";
                        }
                    }
                ]
        }
    }

    $(document).ready(function(){

        $('#data_list').DataTable(def_datatables("all"));
    });

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_admin").click(function() {
            var data_main = new FormData();
            data_main.append('email_user'   , $("#email_user").val());
            data_main.append('username_user', $("#username_user").val());
            data_main.append('tlp_user'     , $("#tlp_user").val());
            data_main.append('nik_user'     , $("#nik_user").val());
            
            // data_main.append('username_user'   , $("#username_user").val());
            data_main.append('password'     , $("#pass").val());
            data_main.append('repassword'   , $("#repass").val());

            data_main.append('nama_user'   , $("#nama_user").val());

            $.ajax({
                url: "<?php echo $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
                $("#msg_email_user").html(detail_msg.email_user);
                $("#msg_username_user").html(detail_msg.username_user);
                $("#msg_tlp_user").html(detail_msg.tlp_user);
                $("#msg_nik_user").html(detail_msg.nik_user);

                $("#msg_pass").html(detail_msg.pass);
                $("#msg_repass").html(detail_msg.repass);
                
                $("#msg_nama_user").html(detail_msg.nama_user);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            // $("#id_tipe_admin").val("0");
            $("#email_user").val("");
            $("#username_user").val("");
            $("#tlp_user").val("");
            $("#nik_user").val("");

            $("#pass").val("");
            $("#repass").val("");
            
            $("#nama_user").val("");
            
            
            $("#msg_email_user").html("");
            $("#msg_username_user").html("");
            $("#msg_tlp_user").html("");
            $("#msg_nik_user").html("");

            $("#msg_pass").html("");
            $("#msg_repass").html("");
            
            $("#msg_nama_user").html("");
            
        }

        function render_data(data){
            var str_table = "";
            var no = 1;
            for (let i in data) {
                var str_active = "<span class=\"label label-warning\">belum diaktifkan</span>";
                    if(data[i].status_active_user == "1"){
                        str_active = "<span class=\"label label-info\">aktif</span>";
                    }else if (data[i].status_active_user == "2") {
                        str_active = "<span class=\"label label-danger\">di blockir</span>";
                    }

                var str_lv = "Super User";
                    if(data[i].id_tipe_admin == "1"){
                        str_lv = "Admin Frontliner";
                    }

                var str_btn_active = "<button class=\"btn btn-primary\" id=\"un_ac_admin\" onclick=\"disabled_admin('"+data[i].id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                    if(data[i].status_active_user != "1"){
                        str_btn_active = "<button class=\"btn btn-success\" id=\"ac_admin\" onclick=\"active_admin('"+data[i].id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                    }

                 var str_btn_ch_pass = "<button class=\"btn btn-info\" id=\"btn_ch_pass\" onclick=\"ch_pass('"+data[i].id_user+"')\">Ubah Password</button>";

                str_table += "<tr>"+
                    "<td>"+no+"</td>"+
                    "<td>"+data[i].username_user+"</td>"+
                    "<td>"+data[i].nama_user+"</td>"+
                    "<td>"+str_lv+"</td>"+
                    "<td>"+str_btn_ch_pass+"</td>"+
                    "<td>"+str_active+"</td>"+
                    "<td>"+
                        "<center>"+
                        str_btn_active+
                        "<button class=\"btn btn-info\" id=\"up_admin\" onclick=\"update_admin('"+data[i].id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;&nbsp;"+
                        "<button class=\"btn btn-danger\" id=\"del_admin\" onclick=\"delete_admin('"+data[i].id_user+"')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>"+
                        "</center>"+
                    "</td>"+
                    "</tr>";
                    no++;
                // str_table += "";
            }
            $("#main_table_content").html(str_table);
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("#_email_user").val("");
            $("#_username_user").val("");
            $("#_tlp_user").val("");
            $("#_nik_user").val("");
            
            $("#_nama_user").val("");


            $("#_msg_email_user").html("");
            $("#_msg_username_user").html("");
            $("#_msg_tlp_user").html("");
            $("#_msg_nik_user").html("");
            
            $("#_msg_nama_user").html("");
        }

        function update_admin(id_data) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_user', id_data);

            $.ajax({
                url: "<?= $main_controller."get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_data);
                    $("#update_admin").modal('show');
                }
            });
        }

        function set_val_update(res, id_data) {
            var data_json = JSON.parse(res);
            console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_data;

                $("#_email_user").val(list_data.email_user);
                $("#_username_user").val(list_data.username_user);
                $("#_tlp_user").val(list_data.tlp_user);
                $("#_nik_user").val(list_data.nik_user);
                
                $("#_nama_user").val(list_data.nama_user);
                
            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#btn_update_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_user', id_cache);

            data_main.append('email_user'   , $("#_email_user").val());
            data_main.append('username_user', $("#_username_user").val());
            data_main.append('tlp_user'     , $("#_tlp_user").val());
            data_main.append('nik_user'     , $("#_nik_user").val());
        
            data_main.append('nama_user'   , $("#_nama_user").val());

            $.ajax({
                url: "<?= $main_controller."update_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#update_admin').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
                $("#_msg_email_user").html(detail_msg.email_user);
                $("#_msg_username_user").html(detail_msg.username_user);
                $("#_msg_tlp_user").html(detail_msg.tlp_user);
                $("#_msg_nik_user").html(detail_msg.nik_user);
                
                $("#_msg_nama_user").html(detail_msg.nama_user);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_user){
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?= $main_controller."delete_admin";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_admin(id_user) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_user);
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }



        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//
        $("#add_pass_new").click(function() {
            var data_main = new FormData();
            data_main.append('id_user', id_ch_cache);

            data_main.append('password'     , $("#ch_pass").val());
            data_main.append('repassword'   , $("#ch_repass").val());

            $.ajax({
                url: "<?=$main_controller."change_pass_admin";?>",
                dataType: 'html',
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_pass_new(res);
                    // response_change_pass(res, id_user_op);
                }
            });
        });

        function response_pass_new(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");

                clear_ch_pass();
                $('#change_pass_modal').modal('toggle');;
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");

                $("#_msg_ch_pass").val(detail_msg.ch_pass);
                $("#_msg_ch_repass").val(detail_msg.ch_repass);
            }
        }

        function ch_pass(id_user) {
            $('#change_pass_modal').modal('show');
            id_ch_cache = id_user;
        }

        function clear_ch_pass(){
            $("#ch_pass").val("");
            $("#ch_repass").val("");
        }
    //=========================================================================//
    //-----------------------------------change_pass---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------dasabled_admin------------------------//
    //=========================================================================//
        function disabled_admin(id_user) {
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?=$main_controller."disabled_admin/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_disabled(res);
                }
            });
        }

        function response_disabled(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }   
    //=========================================================================//
    //-----------------------------------dasabled_admin------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------activate_admin------------------------//
    //=========================================================================//

        function active_admin(id_user) {
            
            var data_main = new FormData();
            data_main.append('id_user', id_user);

            $.ajax({
                url: "<?=$main_controller."activate_admin/";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    console.log(res);
                    response_active_admin(res);
                }
            });
                       
        }

        function response_active_admin(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------activate_admin------------------------//
    //=========================================================================//
</script>