<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>SAM GEPUN BASA</title>
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url(); ?>/assets/new_template/images/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192" href="<?= base_url(); ?>/assets/new_template/images/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url(); ?>/assets/new_template/images/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url(); ?>/assets/new_template/images/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url(); ?>/assets/new_template/images/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?= base_url(); ?>/assets/new_template/images/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/new_template/css/style.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/new_template/css/responsive.css">
    <style type="text/css">
        video {
  max-width: 100%;
  height: auto;
}
    </style>
</head>

<body>
    <div class="preloader"></div><!-- /.preloader -->
    <div class="page-wrapper">
         <header class="site-header header-one ">
            <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
                <div class="container clearfix">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="logo-box clearfix">
                        <a class="navbar-brand" href="#">
                            <img src="<?= base_url(); ?>/assets/new_template/images/logo-1-1.png" class="main-logo" width="97" alt="Awesome Image" />
                        </a>
                        <button class="menu-toggler" data-target=".main-navigation">
                            <span class="fa fa-bars"></span>
                        </button>
                    </div><!-- /.logo-box -->
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="main-navigation">
                        <ul class=" navigation-box one-page-scroll-menu ">
                            <li class="current scrollToLink">
                                <a href="#home">Home</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#data">Data</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#program">Program</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#cara_daftar">Cara Daftar</a>
                            </li>
                            <li class="scrollToLink">
                                <a href="#kontak">Kontak</a>
                                <ul class="sub-menu">
                                    <li><a href="https://pkk.malangkota.go.id/">TP-PKK Kota Malang</a></li>
                                   
                                </ul><!-- /.sub-menu -->
                            </li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                    <div class="right-side-box">
                        <a href="<?= base_url(); ?>user/login" class="thm-btn header-one__btn">Login</a>
                    </div><!-- /.right-side-box -->
                </div>
                <!-- /.container -->
            </nav>
        </header><!-- /.header-one -->
        <section class="banner-one" id="home">
            <div class="container">
                <div class="banner-one__moc-wrap">
                    <img src="<?= base_url(); ?>/assets/new_template/images/banner-1-1.png" class="banner-one__moc" alt="Awesome Image" />
                </div><!-- /.banner-one__moc-wrap -->
                <div class="row justify-content-end">
                    <div class="col-lg-5 col-md-6">
                        <div class="banner-one__content">
                            <h4 class="banner-one__title">SAM GEPUN BASA <br> KOTA MALANG</h4>
                            <!-- /.banner-one__title -->
                            <p class="banner-one__text">Mendorong penguatan peran dasa wisma melalui SAM GEPUN BASA (Smart City Malang Gerakan Menghimpun Data Berbasis Dasa Wisma) dalam penyediaan data bagi perumusan kebijakan</p><!-- /.banner-one__text -->
                            <a href="<?= base_url(); ?>user/register" class="banner-one__btn thm-btn">DAFTAR</a>
                        </div><!-- /.banner-one__content -->
                    </div><!-- /.col-lg-5 -->
                </div><!-- /.row justify-content-end -->
            </div><!-- /.container -->
        </section><!-- /.banner-one -->
        <section class="funfact-one" id="data">
            <div class="container">
                <div class="block-title text-center">
                    <h3 class="block-title__title">Cakupan Data <br> </h3>
                    <p>Cakupan pendataan warga Kota Malang menggunakan aplikasi SAM GEPUN BASA, yang dilakukan oleh kader Dasawisma se-Kota Malang. Pendataan SAM GEPUN BASA mencakup empat aspek yaitu: 1. Pendataan Bangunan, 2. Pendataan Individu, 3. Pendataan Keluarga, dan 4. Kesehatan.</p><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <div class="row">
                    <div class="col-lg-3 col-md-6">
                        <div class="funfact-one__single">
                            <div class="inner">
                                <h3 class="funfact-one__title counter">0</h3><!-- /.funfact-one__title -->
                                <p class="funfact-one__text">Pendataan<br>Bangunan</p><!-- /.funfact-one__text -->
                            </div><!-- /.inner -->
                        </div><!-- /.funfact-one__single -->
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="funfact-one__single">
                            <div class="inner">
                                <h3 class="funfact-one__title counter">0</h3><!-- /.funfact-one__title -->
                                <p class="funfact-one__text">Pendataan<br>Individu</p><!-- /.funfact-one__text -->
                            </div><!-- /.inner -->
                        </div><!-- /.funfact-one__single -->
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3 col-md-6 d-flex justify-content-center">
                        <div class="funfact-one__single">
                            <div class="inner">
                                <h3 class="funfact-one__title counter">0</h3><!-- /.funfact-one__title -->
                                <p class="funfact-one__text">Pendataan<br>Keluarga</p><!-- /.funfact-one__text -->
                            </div><!-- /.inner -->
                        </div><!-- /.funfact-one__single -->
                    </div><!-- /.col-lg-3 -->
                    <div class="col-lg-3 col-md-6 d-flex justify-content-end">
                        <div class="funfact-one__single">
                            <div class="inner">
                                <h3 class="funfact-one__title counter">0</h3><!-- /.funfact-one__title -->
                                <p class="funfact-one__text">Kesehatan<br>Masyarakat</p><!-- /.funfact-one__text -->
                            </div><!-- /.inner -->
                        </div><!-- /.funfact-one__single -->
                    </div><!-- /.col-lg-3 -->
                </div><!-- /.row -->
                <center>
                 <div class='tableauPlaceholder' id='viz1630398128331' style='position: relative'><noscript><a href='#'><img alt='Dashboard 1 ' src='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Da&#47;DataDasawismaKotaMalang&#47;Dashboard1&#47;1_rss.png' style='border: none' /></a></noscript><object class='tableauViz'  style='display:none;'><param name='host_url' value='https%3A%2F%2Fpublic.tableau.com%2F' /> <param name='embed_code_version' value='3' /> <param name='site_root' value='' /><param name='name' value='DataDasawismaKotaMalang&#47;Dashboard1' /><param name='tabs' value='no' /><param name='toolbar' value='yes' /><param name='static_image' value='https:&#47;&#47;public.tableau.com&#47;static&#47;images&#47;Da&#47;DataDasawismaKotaMalang&#47;Dashboard1&#47;1.png' /> <param name='animate_transition' value='yes' /><param name='display_static_image' value='yes' /><param name='display_spinner' value='yes' /><param name='display_overlay' value='yes' /><param name='display_count' value='yes' /><param name='language' value='en-US' /><param name='filter' value='publish=yes' /></object></div>                <script type='text/javascript'>                    var divElement = document.getElementById('viz1630398128331');                    var vizElement = divElement.getElementsByTagName('object')[0];                    if ( divElement.offsetWidth > 800 ) { vizElement.style.width='1000px';vizElement.style.height='827px';} else if ( divElement.offsetWidth > 500 ) { vizElement.style.width='1000px';vizElement.style.height='827px';} else { vizElement.style.width='100%';vizElement.style.height='727px';}                     var scriptElement = document.createElement('script');                    scriptElement.src = 'https://public.tableau.com/javascripts/api/viz_v1.js';                    vizElement.parentNode.insertBefore(scriptElement, vizElement);                </script></center>
            </div><!-- /.container -->

        </section><!-- /.funfact-one -->
        
       

        <section class="cta-one" id="program">
            <div class="container">
                <img src="<?= base_url(); ?>/assets/new_template/images/cta-moc-1-1.png" class="cta-one__moc" alt="Awesome Image" />
                <div class="row">
                    <div class="col-lg-6">
                        <div class="cta-one__content">
                            <div class="block-title text-left">
                                <h2 class="block-title__title">PROGRAM TP-PKK <BR>KOTA MALANG</h2>
                                <!-- /.block-title__title -->
                            </div><!-- /.block-title -->
                            <p class="cta-one__text">10 Program Pokok PKK pada hakekatnya merupakan kebutuhan dasar manusia, yaitu :</p><!-- /.cta-one__text -->
                            <ul class="cta-one__list">
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Penghayatan dan Pengamalan Pancasila </li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Gotong Royong</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Pangan</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Sandang</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Perumahan dan Tatalaksana Rumah Tangga</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Pendidikan dan Ketrampilan</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Kesehatan</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Pengembangan Kehidupan Berkoperasi</li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Kelestarian Lingkungan Hidup  </li><!-- /.cta-one__list-item -->
                                <li class="cta-one__list-item"><i class="fa fa-check"></i>Perencanaan Sehat</li><!-- /.cta-one__list-item -->
                            </ul><!-- /.cta-one__list -->
                            <a href="https://pkk.malangkota.go.id/profil__trashed/10-program-pokok-pkk/" class="cta-one__btn thm-btn">Baca Selengkapnya</a>
                        </div><!-- /.cta-one__content -->
                    </div><!-- /.col-lg-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.cta-one -->
       
        <section class="app-screen-one" id="cara_daftar">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title">CARA DAFTAR APLIKASI<br> SAM GEPUN BASA</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
                <div class="row">
                    <div class="col-lg-4">
                        <div class="pricing-one__single">
                            <div class="inner">
                                <div class="pricing-one__top">
                                    <div class="service-one__icon">
                                        <i class="fa fa-user-plus" aria-hidden="true"></i>
                                    </div><!-- /.service-one__icon -->
                                    <h3>Daftar</h3>
                                </div><!-- /.pricing-one__top -->
                                <ul class="pricing-one__list">
                                    <p>Klik tombol Daftar, Isi Form yang sudah disediakan, kemudian
                                    <b>Verifikasi Email</b> dan Klik Aktifasi Akun</p>
                                </ul><!-- /.pricing-one__list -->
                                <a href="<?= base_url(); ?>user/register" class="pricing-one__btn thm-btn">Daftar</a>
                            </div><!-- /.inner -->
                        </div><!-- /.pricing-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="pricing-one__single">
                            <div class="inner">
                                <div class="pricing-one__top">
                                      <div class="service-one__icon">
                                        <i class="fa fa-sign-in" aria-hidden="true"></i>
                                    </div><!-- /.service-one__icon -->
                                     <h3>Login</h3>
                                </div><!-- /.pricing-one__top -->
                                <ul class="pricing-one__list">
                                    <p>Klik Tombol Login, kemudian masukkan username anda, dan password anda <br> yang sudah Anda daftarkan sebelumnya</p>
                                </ul><!-- /.pricing-one__list -->
                                <a href="<?= base_url(); ?>user/login" class="pricing-one__btn thm-btn">Login</a>
                            </div><!-- /.inner -->
                        </div><!-- /.pricing-one__single -->
                    </div><!-- /.col-lg-4 -->
                    <div class="col-lg-4">
                        <div class="pricing-one__single">
                            <div class="inner">
                                <div class="pricing-one__top">
                                  <div class="service-one__icon">
                                        <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                    </div><!-- /.service-one__icon -->
                                     <h3>Isi Survey</h3>
                                </div><!-- /.pricing-one__top -->
                                <ul class="pricing-one__list">
                                    <p>Diharapakan mengisi data pada masing-masing Menu Kategori yang sudah disediakan, Terima kasih atas partisipasinya</p>
                                </ul><!-- /.pricing-one__list -->
                                <a href="#data" class="pricing-one__btn thm-btn">Data</a>
                            </div><!-- /.inner -->
                        </div><!-- /.pricing-one__single -->
                    </div><!-- /.col-lg-4 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </section><!-- /.pricing-one -->
        
       
        <div class="video-one">
            <div class="container">
                <div class="block-title text-center">
                    <h2 class="block-title__title">TP-PKK <br> KOTA MALANG</h2><!-- /.block-title__title -->
                </div><!-- /.block-title -->
              <center>
                <div class="video">
                <video  loop="true" autoplay="autoplay" controls="controls" id="vid" muted>
                         <source type="video/mp4" src="<?= base_url(); ?>/assets/video.mp4"></source>
                         <source type="video/ogg" src="video_file.ogg"></source>
                </video>
               </div>
            </div><!-- /.container -->
        </div><!-- /.video-one -->
      
        <footer class="site-footer" id="kontak">
            <div class="site-footer__upper">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-5">
                            <div class="footer-widget">
                                <a href="#" class="footer-widget__logo"><img src="<?= base_url(); ?>/assets/new_template/images/logo-1-1.png" width="132" alt="Awesome Image" /></a>
                                <div class="address-holder" style="color: white;">
                                <div class="phone"><i class="fa fa-phone-square" aria-hidden="true"></i> +62 341 351964</div>
                                <div class="email"><i class="fa fa-envelope" aria-hidden="true"></i> tppkkmalangkota@gmail.com</div>
                                <div class="address">
                                    
                                    <div> <i class="fa fa-map-marker" aria-hidden="true"></i> Jln. Tangkuban Perahu<br>
                                        No. 1B <br>
                                        Kota Malang<br>
                                       </div>
                                </div>
                            </div>
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-5 -->
                        <div class="col-lg-4">
                            <div class="footer-widget">
                                <ul class="footer-widget__links">
                                    <li class="footer-widget__links-item"><a href="https://pkk.malangkota.go.id/category/artikel/">Artikel</a></li>
                                    <li class="footer-widget__links-item"><a href="https://pkk.malangkota.go.id/category/berita/">Berita</a></li>
                                    <li class="footer-widget__links-item"><a href="https://pkk.malangkota.go.id/galleries/l">Galeri Foto</a></li>
                                    <li class="footer-widget__links-item"><a href="https://pkk.malangkota.go.id/pokja-2/">Program Kerja</a></li>
                                </ul><!-- /.footer-widget__links -->
                                <ul class="footer-widget__links">
                                    <li class="footer-widget__links-item"><a href="https://malangkota.go.id/">Website Kota Malang</a></li>
                                    <li class="footer-widget__links-item"><a href="https://smartcity.malangkota.go.id/">Website Smartcity</a></li>
                                    <li class="footer-widget__links-item"><a href="https://tppkk-pusat.org/home/">TP PKK Pusat</a></li>
                                    <li class="footer-widget__links-item"><a href="https://pkk.malangkota.go.id/">TP PKK Kota Malang</a></li>        
                                </ul><!-- /.footer-widget__links -->
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-4 -->
                        <div class="col-lg-3">
                            <div class="footer-widget">
                                <a href="<?= base_url(); ?>user/login" class="footer-widget__btn">
                                    <i class="fa fa-play"></i>
                                    <span class="footer-widget__btn-text">
                                        Get in <span class="footer-widget__btn-text-highlight">LOGIN</span>
                                    </span>
                                </a>
                                <a href="<?= base_url(); ?>user/register" class="footer-widget__btn">
                                    <i class="fa fa-play"></i>
                                    <span class="footer-widget__btn-text">
                                        Get in <span class="footer-widget__btn-text-highlight">DAFTAR</span>
                                    </span>
                                </a>
                            </div><!-- /.footer-widget -->
                        </div><!-- /.col-lg-3 -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__upper -->
            <div class="site-footer__bottom">
                <div class="container">
                    <div class="site-footer__social">
                        <a href="https://www.facebook.com/TP-PKK-Kota-Malang-342327463273806"><i class="fa fa-facebook-square"></i></a>
                        <a href="https://twitter.com/PemkotMalang"><i class="fa fa-twitter"></i></a>
                        <a href="https://www.instagram.com/pkkkotamalang/"><i class="fa fa-instagram"></i></a>
                        <a href="https://www.youtube.com/channel/UCxe5kt4wDPxNYc2Z89az32Q/videos"><i class="fa fa-youtube"></i></a>
                    </div><!-- /.site-footer__social -->
                    <p class="site-footer__copy-text"><i class="fa fa-copyright"></i>copyright 2021 by <a
                            href="<?= base_url(); ?>/assets/new_template/#">TP-PKK Kota Malang | Dinas Komunikasi dan Informatika Pemerintah Kota Malang</a></p><!-- /.site-footer__copy-text -->
                </div><!-- /.container -->
            </div><!-- /.site-footer__bottom -->
        </footer><!-- /.site-footer -->
    </div><!-- /.page-wrapper -->
    <a href="<?= base_url(); ?>/assets/new_template/#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>
    <!-- /.scroll-to-top -->
    <script src="<?= base_url(); ?>/assets/new_template/js/jquery.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/waypoints.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/jquery.counterup.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/owl.carousel.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/swiper.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/jquery.magnific-popup.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/jquery.easing.min.js"></script>
    <script src="<?= base_url(); ?>/assets/new_template/js/theme.js"></script>
</body>

</html>