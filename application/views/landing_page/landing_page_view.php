<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">

<title>SAM GEPUN BASA -TP PKK</title>
<link rel="shortcut icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">
<link rel="icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">

<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/style.css">

</head>

<body>
    
<nav id="nav" class="navbar navbar-default navbar-full navbar-fixed-top">   
    <div class="container container-nav">
        <div class="navbar-header">
            <button aria-expanded="false" type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand page-scroll" href="<?= base_url(); ?>landing_page/Landingpage">
             <img class="logo" src="<?= base_url(); ?>/assets/template_front/images/logo.png" style="width: 100%" alt="Hostino">
            </a>


        </div>
        <div style="height: 1px;" role="main" aria-expanded="false" class="navbar-collapse collapse" id="bs">
             <ul class="nav navbar-nav navbar-right">
                <li><a href="#top-contente">Home</a></li>
                <li><a href="#info">Tentang</a></li>
                <li><a href="#footer">Kontak</a></li>
                <li><a class="signin-button" href="<?= base_url(); ?>user/login">Login</a></li>
                <li><a class="chat-button" href="<?= base_url(); ?>user/register">Daftar</a></li>
            </ul>
        </div>
    </div>
</nav>
<div id="top-content" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <div id="main-slider">
                    <div class="slide info-slide1" title="Daftar">
                        <div class="image-holder"><img src="<?= base_url(); ?>assets/template_front/images/main-slide-img4.png" alt="" /></div>
                        <div class="text-holder txt">SAM GEPUN BASA<br>
                        Smart City Kota Malang Gerakan Menghimpun Data Berbasis Dasa Wisma</div>
                        <div class="button-holder"><a href="<?= base_url(); ?>user/register" class="blue-button">Daftar Sekarang</a></div>
                    </div>

                    <div class="slide domainsearch-slide" title="Inovatif">
                        <div class="image-holder"><img src="<?= base_url(); ?>assets/template_front/images/main-slide-img6.png" alt="" /></div>
                        <div class="b-title txt">TP PKK<br>
                        telah menginisiasi sejumlah program inovatif</div><br>
                        <div class="domain-search-holder">
                           <!--  <form id="domain-search">
                                <input id="domain-text" type="text" name="domain" placeholder="Search a domain name here" />
                                <input id="search-btn" type="submit" name="submit" value="Search now" />
                            </form> -->
                        </div>
                    </div>
                    <div class="slide info-slide2" title="Login">
                        <div class="image-holder"><img src="<?= base_url(); ?>assets/template_front/images/main-slide-img5.png" alt="" /></div>
                        <div class="text-holder txt">Harap Login untuk melakukan e-survey</div>
                        <div class="button-holder"><a href="<?= base_url(); ?>user/login" class="blue-button">Login Sekarang</a></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="info" class="container-fluid">
    <canvas id="infobg" data-paper-resize="true"></canvas>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                 <img class="photo" src="<?= base_url(); ?>/assets/template_front/images/logopkk2.png" alt="">
                <div class="row-title"><h1>SAM GEPUN BASA</h1>  
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">

                <div class="info-text">Mendorong penguatan peran dasa wisma melalui SAM GEPUN BASA (Smart City Malang Gerakan Menghipun Data Berbasis Dasa Wisma) dalam penyediaan data bagi perumusan kebijakan. Artikel ini telah tayang di SuryaMalang.com dengan judul Peringati Hari Kartini, Ini Pesan Ketua TP PKK Kota Malang Widayati Sutiaji.
                Penulis: Mochammad Rifky Edgar Hidayatullah | Editor: eko darmoko</div>
                
                <a href="https://pkk.malangkota.go.id/" class="white-green-shadow-button">Website Resmi TP PKK Kota Malang</a>
            </div>
        </div>
    </div>
</div>
<div id="features" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row-title">Cara  Pendaftaran</div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-4 col-md-4">
                <div class="mfeature-box">
                    <div class="mfeature-icon">
                        <div class="icon-bg"><img src="<?= base_url(); ?>/assets/template_front/images/clouds-light.png" alt="" /></div>
                        <i class="fas fa-user-plus"></i>
                    </div>
                    <div class="mfeature-title">Klik Tombol Daftar</div>
                    <div class="mfeature-details">Terdapat di halaman utaman pojok kanan atas berwarna hijau, Isi Form yang sudah disediakan Klik Simpan<br>[Verifikasi Email yang telah Anda daftarkan Klik Aktifasi Akun]</div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="mfeature-box active">
                    <div class="mfeature-icon">
                        <div class="icon-bg"><img src="<?= base_url(); ?>/assets/template_front/images/clouds-light.png" alt="" /></div>
                        <i class="fas fa-sign-in-alt"></i>
                    </div>
                    <div class="mfeature-title">Klik Login</div>
                    <div class="mfeature-details">Isi username dan password yang sudah didaftarkan sebelumnya</div>
                </div>
            </div>
            <div class="col-sm-4 col-md-4">
                <div class="mfeature-box">
                    <div class="mfeature-icon">
                        <div class="icon-bg"><img src="<?= base_url(); ?>/assets/template_front/images/clouds-light.png" alt="" /></div>
                        <i class="fas fa-pencil-alt"></i>
                    </div>
                    <div class="mfeature-title">Isi e-survey Sam Gepun Basa</div>
                    <div class="mfeature-details">Diharpakan mengisi data di setiap Menu Kategori yang sudah disediakan, Terima kasih atas partisipasinya</div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <a class="light-blue-button" href="<?= base_url(); ?>user/register">Daftar</a>
                <a class="light-blue-button" href="<?= base_url(); ?>user/login">Login</a>
            </div>
        </div>    
    </div>
</div>

<div id="testimonials" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
                <div class="row-title"></div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <div id="testimonials-slider">
                    <div>
                        <div class="details-holder">
                            <img class="photo" src="<?= base_url(); ?>/assets/template_front/images/logopkk.png" alt="">
                            <h4>Hj. Widayati Sutiaji, S.Sos., MM</h4>
                            <h5>KETUA TP PKK KOTA MALANG</h5>
                            <p>Salah satunya menginisiasi sejumlah program inovatif, di antaranya ialah terlibat aktif dalam Kelompok Kerja PUG di tingkat kota hingga kecamatan dan kelurahan.
                                Artikel ini telah tayang di SuryaMalang.com dengan judul Peringati Hari Kartini, Ini Pesan Ketua TP PKK Kota Malang Widayati Sutiaji, https://suryamalang.tribunnews.com/2021/04/21/peringati-hari-kartini-ini-pesan-ketua-tp-pkk-kota-malang-widayati-sutiaji.
                                Penulis: Mochammad Rifky Edgar Hidayatullah | Editor: eko darmoko</p>
                        </div>
                    </div>
                    <div>
                        <div class="details-holder">
                           <img class="photo" src="<?= base_url(); ?>/assets/template_front/images/logopkk.png" alt="">
                            <h4>Hj. Widayati Sutiaji, S.Sos., MM</h4>
                            <h5>KETUA TP PKK KOTA MALANG</h5>
                            <p>Serta mendorong penguatan peran dasa wisma melalui SAM GEPUN BASA (Smart City Malang Gerakan Menghipun Data Berbasis Dasa Wisma) dalam penyediaan data bagi perumusan kebijakan.
                                Artikel ini telah tayang di SuryaMalang.com dengan judul Peringati Hari Kartini, Ini Pesan Ketua TP PKK Kota Malang Widayati Sutiaji, https://suryamalang.tribunnews.com/2021/04/21/peringati-hari-kartini-ini-pesan-ketua-tp-pkk-kota-malang-widayati-sutiaji.
                                Penulis: Mochammad Rifky Edgar Hidayatullah | Editor: eko darmoko</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="footer" class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer-menu-holder">
                    <h4>Company</h4>
                    <ul class="footer-menu">
                        <li><a href="#info">Tentang</a></li>
                        <li><a href="<?= base_url(); ?>user/register">Daftar</a></li>
                        <li><a href="<?= base_url(); ?>user/login">Login</a></li>
                        <li><a href="#footer">Kontak</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer-menu-holder">
                    <h4>Services</h4>
                    <ul class="footer-menu">
                        <li><a href="https://pkk.malangkota.go.id/category/artikel/">Artikel</a></li>
                        <li><a href="https://pkk.malangkota.go.id/category/berita/">Berita</a></li>
                        <li><a href="https://pkk.malangkota.go.id/galleries/l">Galeri Foto</a></li>
                        <li><a href="https://pkk.malangkota.go.id/pokja-2/">Program Kerja</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="footer-menu-holder">
                    <h4>Other links</h4>
                    <ul class="footer-menu">
                        <li><a href="https://malangkota.go.id/">Website Kota Malang</a></li>
                        <li><a href="https://smartcity.malangkota.go.id/">Website Smartcity</a></li>
                        <li><a href="https://tppkk-pusat.org/">TP PKK Pusat</a></li>
                        <li><a href="https://pkk.malangkota.go.id/">TP PKK Kota Malang</a></li>                   
                    </ul>
                </div>
            </div>
            <div class="col-xs-6 col-sm-3 col-md-3">
                <div class="address-holder">
                    <div class="phone"><i class="fas fa-phone"></i>+62 341 351964</div>
                    <div class="email"><i class="fas fa-envelope"></i> tppkkmalangkota@gmail.com</div>
                    <div class="address">
                        <i class="fas fa-map-marker"></i> 
                        <div>Jln. Takuban Perahu<br>
                            No. 1B <br>
                            Kota Malange<br>
                           </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="social row" style="text-align: center;">
            <div class="col-xs-2"></div>
            <div class="col-xs-2"><a href="https://www.facebook.com/TP-PKK-Kota-Malang-342327463273806"><i class="fab fa-facebook"></i></a></div>
            <div class="col-xs-2"><a href="https://twitter.com/PemkotMalang"><i class="fab fa-twitter"></i></a></div>
            <div class="col-xs-2"><a href="https://www.youtube.com/channel/UCxe5kt4wDPxNYc2Z89az32Q/videos"><i class="fab fa-youtube"></i></a></div>
            <div class="col-xs-2"><a href="https://www.instagram.com/pkkkotamalang/"><i class="fab fa-instagram"></i></a></div>

        </div>
    </div>
</div>
<script src="<?= base_url(); ?>/assets/template_front/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/slick.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/paper-full.min.js"></script>
<script type="text/paperscript" src="<?= base_url(); ?>/assets/template_front/js/metaball.js" data-paper-canvas="infobg"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/main.js"></script>
</body>
</html>
