<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">

<title>Login User</title>
<link rel="shortcut icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">
<link rel="icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/style.css">
<link href="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/skins/all.css" rel="stylesheet">
</head>

<body class="fullpage">
<script type="text/javascript">

</script>


<div id="form-section" class="container-fluid signin">
      <a class="website-logo" href="<?= base_url(); ?>landing_page/Landingpage">
        <img class="logo" src="<?= base_url(); ?>/assets/template_front/images/logo.png" style="width: 70%;" alt="Hostino">
    </a>
    <div class="menu-holder">
        <ul class="main-links">
            <li><a class="normal-link" href="<?= base_url(); ?>user/register">Tidak memiliki Akun?</a></li>
            <li><a class="sign-button" href="<?= base_url(); ?>user/register">Daftar <i class="hno hno-arrow-right"></i></a></li>
        </ul>
    </div>
    <div class="row">
        <div class="form-holder">
            <div class="signin-signup-form">
                <div class="form-items">
                    <h1 class="box-title m-b- text-center "><b>LOGIN</b></h1>
                        <h3 class="box-title m-b-20 text-center"><b>SAM GEPUN BASA</b></h3><br>
                         <?php if($this->session->flashdata('item')){?>
                              <div class="alert alert-success">      
                                <?php echo $this->session->flashdata('item')?>
                              </div>
                            <?php } ?>
                        <?php if($this->session->flashdata('renew')){?>
                              <div class="alert alert-success">      
                                <?php echo $this->session->flashdata('renew')?>
                              </div>
                        <?php } ?>
                    <form id="signinform">
                        <div class="form-text">
                             <input class="form-control" type="text" name="username" id="username" required="" placeholder="Username">
                                <p id="msg_username" style="color: red;"></p>
                        </div>
                        <div class="form-text">
                          <input class="form-control" type="password" name="password" id="password" required="" placeholder="Password">
                                <p id="msg_password" style="color: red;"></p>
                        </div>
                        <div class="form-button">
                             <div class="checkbox checkbox-primary pull-left p-t-0">
                                    <input id="checkbox-signup" type="checkbox">
                                    <label for="checkbox-signup"> Remember me </label>
                                </div> <a href="<?=base_url('password_reset')?>" id="to-recover" class="text-dark pull-right"><!-- <i class="fa fa-lock m-r-5"></i> --> Forgot password?</a> </div>
                        </div>
                        <div class="form-button">
                             <button class="btn btn-default" type="button" name="login" id="login">Log In<i class="hno hno-arrow-right"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

      
        <div class="info-slider-holder">
            <div class="info-holder">
                <div class="img-text-slider">
                    <div>
                        <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img4.png" alt="">
                        <p>Smart City Kota Malang Gerakan Menghimpun Data <br> Berbasis Dasa Wisma</p>
                    </div>
                    <div>
                         <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img5.png" alt="">
                        <p>Smart City Kota Malang Gerakan Menghimpun Data <br> Berbasis Dasa Wisma</p>
                    </div>
                    <div>
                        <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img6.png" alt="">
                        <p>Smart City Kota Malang Gerakan Menghimpun Data <br> Berbasis Dasa Wisma</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script src="<?= base_url(); ?>/assets/template_front/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/slick.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/main.js"></script>
<script src="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
<script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.init.js"></script>
<script type="text/javascript">

        var ok = "<?php echo $session_cek; ?>";
        if(ok == 'Sukses'){
            swal({
                            title: "Sukses",
                            text: "Berhasil Aktivasi Akun",
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        }, function() {
                            swal.close();
                            // window.location.href = window.location.href;
                        });
            
        <?php $this->session->unset_userdata('flash_welcome'); ?>
        }
        if(ok == 'Sudah'){
            swal({
                            title: "Warning",
                            text: "Link Sudah Digunakan / Expired",
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        }, function() {
                            swal.close();
                            // window.location.href = window.location.href;
                        });
            
        <?php $this->session->unset_userdata('flash_welcome'); ?>
        }

        $(document).ready(function() {

        });

        $("#login").click(function() {
            var data_main = new FormData();
            data_main.append('username'     , $("#username").val());
            data_main.append('password'     , $("#password").val());
            
            $.ajax({
                url: "<?php echo base_url()."login/Loginuser/get_auth";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_login(res);
                    console.log(res);
                }
            });
        });




        function response_login(res) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {

                    var data_json = JSON.parse(res);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                    if (main_msg.status) {
                        swal({
                            title: "Login Success",
                            text: main_msg.msg,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            window.location.href = window.location.href;
                        });
                    } else {
                        swal({
                            title: "Login Gagal",
                            text: main_msg.msg,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        }, function() {
                            swal.close();
                            // window.location.href = window.location.href;
                        });

                        $("#msg_username").html(detail_msg.username);
                        $("#msg_password").html(detail_msg.password);
                    }
                    
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
            
        }
    </script>
</body>
</html>
