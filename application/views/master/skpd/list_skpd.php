<?php
    $id_parent = "0";
    if($this->uri->segment(3)){
        $id_parent = $this->uri->segment(3);
    }

    $url_list_app = base_url()."master/list-skpd/";

    $main_controller = base_url()."master/skpdmaster/";

    $path_img = base_url()."assets/dash/img/icon/";
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6">
        <h3 class="text-themecolor">Data List <?=$title?></h3>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Input Data <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Nama <?=$title?></label>
                                <input type="text" class="form-control" id="nama_ms_skpd" name="nama_ms_skpd" required="">
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_nama_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Kode <?=$title?></label>
                                <input type="text" class="form-control" id="kd_ms_skpd" name="kd_ms_skpd" required="">
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_kd_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Singkatan <?=$title?></label>
                                <input type="text" class="form-control" id="singkatan_ms_skpd" name="singkatan_ms_skpd" required="">
                                <!-- <input type="text" class="form-control" id="nm_ms_img" name="nm_ms_img" required=""> -->
                                <p id="msg_singkatan_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Alamat <?=$title?></label>
                                <input type="text" class="form-control" id="alamat_ms_skpd" name="alamat_ms_skpd" required="">
                                <p id="msg_alamat_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Latitude</label>
                                <input type="text" class="form-control" id="lat_ms_skpd" name="lat_ms_skpd" required="">
                                <p id="msg_lat_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Longitude</label>
                                <input type="text" class="form-control" id="long_ms_skpd" name="long_ms_skpd" required="">
                                <p id="msg_long_ms_skpd" style="color: red;"></p>
                            </div>
                        </div>
                        <div class="col-md-12 text-right">
                            <button type="button" id="save_data" class="btn btn-info waves-effect text-left">Simpan</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">List <?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-lg-12 col-md-12">
                            <div class="table-responsive m-t-40">
                                <table id="myTable" class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th width="5%">No. </th>
                                            <th width="25%">Nama <?=$title?></th>
                                            <th width="15%">Singkatan <?=$title?></th>
                                            <th width="20%">Alamat <?=$title?></th>
                                            <th width="15%">Status Active</th>
                                            <th width="20%">Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody id="main_table_content">
                                        <?php
                                            
                                            if(!empty($list_data)){
                                                foreach ($list_data as $r => $v) {
                                                    $id_ms_skpd = $v->id_ms_skpd;
                                                    $ms_skpd_id = $v->ms_skpd_id;
                                                    $kd_ms_skpd = $v->kd_ms_skpd;
                                                    $patern_ms_skpd = $v->patern_ms_skpd;
                                                    $nama_ms_skpd = $v->nama_ms_skpd;
                                                    $singkatan_ms_skpd = $v->singkatan_ms_skpd;
                                                    $alamat_ms_skpd = $v->alamat_ms_skpd;
                                                    $loc_ms_skpd = $v->loc_ms_skpd;
                                                    $sts_ac_ms_skpd = $v->sts_ac_ms_skpd;
                                                    $is_del_ms_skpd = $v->is_del_ms_skpd;

                                                    $str_btn_active = "<button class=\"btn btn-primary\" id=\"ac_admin\" onclick=\"check_data('".$id_ms_skpd."', 'active')\" style=\"width: 40px;\"><i class=\"fa fa fa-window-close\" ></i></button>&nbsp;&nbsp;";
                                                    
                                                    $str_active = "<span class=\"label label-warning\">Tidak Aktif</span>";
                                                    if($sts_ac_ms_skpd == "1"){
                                                        $str_active = "<span class=\"label label-info\">Aktif</span>";
                                                        
                                                        $str_btn_active = "<button class=\"btn btn-success\" id=\"un_ac_admin\" onclick=\"check_data('".$id_ms_skpd."', 'non_active')\" style=\"width: 40px;\"><i class=\"fa fa-check\" ></i></button>&nbsp;&nbsp;";
                                                    }
                                                    
                                                   
                                                    echo "<tr>
                                                            <td>".($r+1)."</td>
                                                            <td>".$nama_ms_skpd."</td>
                                                            <td>".$singkatan_ms_skpd."</td>
                                                            <td>".$alamat_ms_skpd."</td>
                                                            <td>".$str_active."</td>
                                                            <td>
                                                                <center>
                                                                ".$str_btn_active."
                                                                <button class=\"btn btn-info\" onclick=\"update_data('".$id_ms_skpd ."')\" style=\"width: 40px;\"><i class=\"fa fa-pencil-square-o\" ></i></button>&nbsp;
                                                                <button class=\"btn btn-danger\" id=\"del_data\" onclick=\"delete_data('".$id_ms_skpd ."')\" style=\"width: 40px;\"><i class=\"fa fa-trash-o\"></i></button>
                                                                </center>
                                                            </td>
                                                        </tr>";
                                                }
                                            }
                                        ?>

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-lg" id="modal_up_data" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" style="display: none;" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?=$title?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Nama <?=$title?></label>
                            <input type="text" class="form-control" id="_nama_ms_skpd" name="nama_ms_skpd" required="">
                            <!-- <input type="text" class="form-control" id="_nm_ms_img" name="nm_ms_img" required=""> -->
                            <p id="_msg_nama_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kode <?=$title?></label>
                            <input type="text" class="form-control" id="_kd_ms_skpd" name="kd_ms_skpd" required="">
                            <!-- <input type="text" class="form-control" id="_nm_ms_img" name="nm_ms_img" required=""> -->
                            <p id="_msg_kd_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Singkatan <?=$title?></label>
                            <input type="text" class="form-control" id="_singkatan_ms_skpd" name="singkatan_ms_skpd" required="">
                            <!-- <input type="text" class="form-control" id="_nm_ms_img" name="nm_ms_img" required=""> -->
                            <p id="_msg_singkatan_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Alamat <?=$title?></label>
                            <input type="text" class="form-control" id="_alamat_ms_skpd" name="alamat_ms_skpd" required="">
                            <p id="_msg_alamat_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Latitude</label>
                            <input type="text" class="form-control" id="_lat_ms_skpd" name="lat_ms_skpd" required="">
                            <p id="_msg_lat_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Longitude</label>
                            <input type="text" class="form-control" id="_long_ms_skpd" name="long_ms_skpd" required="">
                            <p id="_msg_long_ms_skpd" style="color: red;"></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" id="up_data" class="btn btn-info waves-effect text-left">Ubah</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>




<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var id_cache = "";
    var id_ch_cache = "";

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#save_data").click(function() {
            var data_main = new FormData();
            data_main.append('nama_ms_skpd', $("#nama_ms_skpd").val());
            data_main.append('kd_ms_skpd', $("#kd_ms_skpd").val());
            data_main.append('singkatan_ms_skpd', $("#singkatan_ms_skpd").val());
            
            data_main.append('alamat_ms_skpd', $("#alamat_ms_skpd").val());
            data_main.append('lat_ms_skpd', $("#lat_ms_skpd").val());
            data_main.append('long_ms_skpd', $("#long_ms_skpd").val());
            
            $.ajax({
                url: "<?= $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                // $('#insert_admin').modal('toggle');
                clear_form_insert();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$url_list_app?>");
            } else {
                $("#msg_nama_ms_skpd").html(detail_msg.nama_ms_skpd);
                $("#msg_kd_ms_skpd").html(detail_msg.kd_ms_skpd);
                $("#msg_singkatan_ms_skpd").html(detail_msg.singkatan_ms_skpd);
                $("#msg_alamat_ms_skpd").html(detail_msg.alamat_ms_skpd);
                $("#msg_lat_ms_skpd").html(detail_msg.lat_ms_skpd);
                $("#msg_long_ms_skpd").html(detail_msg.long_ms_skpd);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#msg_nama_ms_skpd").html("");
            $("#msg_kd_ms_skpd").html("");
            $("#msg_singkatan_ms_skpd").html("");
            $("#msg_alamat_ms_skpd").html("");
            $("#msg_lat_ms_skpd").html("");
            $("#msg_long_ms_skpd").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//
        function clear_form_update(){
            $("_#msg_nama_ms_skpd").html("");
            $("_#msg_kd_ms_skpd").html("");
            $("_#msg_singkatan_ms_skpd").html("");
            $("_#msg_alamat_ms_skpd").html("");
            $("_#msg_lat_ms_skpd").html("");
            $("_#msg_long_ms_skpd").html("");
        }

        function update_data(id_ms_skpd) {
            clear_form_update();

            var data_main = new FormData();
            data_main.append('id_ms_skpd', id_ms_skpd);

            $.ajax({
                url: "<?= $main_controller."get_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    set_val_update(res, id_ms_skpd);
                    $("#modal_up_data").modal('show');
                }
            });
        }

        function set_val_update(res, id_ms_skpd ) {
            var data_json = JSON.parse(res);
            // console.log(data_json);
                var main_msg = data_json.msg_main;
                var detail_msg = data_json.msg_detail;
                    var list_data = data_json.msg_detail.list_data;
            if (main_msg.status) {
                id_cache = id_ms_skpd ;
                
                var loc_ms_skpd = JSON.parse(list_data.loc_ms_skpd);

                $("#_nama_ms_skpd").val(list_data.nama_ms_skpd);
                $("#_kd_ms_skpd").val(list_data.kd_ms_skpd);
                $("#_singkatan_ms_skpd").val(list_data.singkatan_ms_skpd);
                $("#_alamat_ms_skpd").val(list_data.alamat_ms_skpd);
                
                $("#_lat_ms_skpd").val(loc_ms_skpd[0]);
                $("#_long_ms_skpd").val(loc_ms_skpd[1]);

            }else {
                clear_form_update();
            }
        }
    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#up_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_ms_skpd', id_cache);
            data_main.append('nama_ms_skpd', $("#_nama_ms_skpd").val());
            data_main.append('kd_ms_skpd', $("#_kd_ms_skpd").val());
            data_main.append('singkatan_ms_skpd', $("#_singkatan_ms_skpd").val());
            
            data_main.append('alamat_ms_skpd', $("#_alamat_ms_skpd").val());
            data_main.append('lat_ms_skpd', $("#_lat_ms_skpd").val());
            data_main.append('long_ms_skpd', $("#_long_ms_skpd").val());

            $.ajax({
                url: "<?= $main_controller."update_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_update(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#modal_up_data').modal('toggle');
                clear_form_update();

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?= $url_list_app?>");
            } else {
                $("#_msg_nama_ms_skpd").html(detail_msg.nama_ms_skpd);
                $("#_msg_kd_ms_skpd").html(detail_msg.kd_ms_skpd);
                $("#_msg_singkatan_ms_skpd").html(detail_msg.singkatan_ms_skpd);
                $("#_msg_alamat_ms_skpd").html(detail_msg.alamat_ms_skpd);
                $("#_msg_lat_ms_skpd").html(detail_msg.lat_ms_skpd);
                $("#_msg_long_ms_skpd").html(detail_msg.long_ms_skpd);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------admin_delete--------------------------//
    //=========================================================================//

        function method_delete(id_ms_skpd ){
            var data_main = new FormData();
            data_main.append('id_ms_skpd', id_ms_skpd );

            $.ajax({
                url: "<?= $main_controller."delete_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }

        function delete_data(id_ms_skpd ) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {
                    swal({
                        title: "Pesan Konfirmasi.!!",
                        text: "Hapus ?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "Hapus",
                        closeOnConfirm: false
                    }, function() {
                        
                        // swal.close();
                        method_delete(id_ms_skpd );
                    });
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
        }

        function response_delete(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            // console.log(data_json);
            if (main_msg.status) {
                // console.log("true");
                create_sweet_alert_for_del("Proses Berhasil", main_msg.msg, "success", "<?=$url_list_app?>");
            } else {
                create_sweet_alert_for_del("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------admin_update--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//
        function check_data(id_ms_skpd, sts){
            var data_main = new FormData();
            data_main.append('id_ms_skpd', id_ms_skpd);
            data_main.append('param', sts);

            $.ajax({
                url: "<?= $main_controller."check_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    // console.log(res);
                    response_delete(res);
                }
            });
        }
    //=========================================================================//
    //-----------------------------------check---------------------------------//
    //=========================================================================//

    
</script>