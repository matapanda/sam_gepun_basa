<?php
    $main_controller = base_url()."user/sosialuser/";

    $path_img = base_url()."assets/prw/img/icon/";

    $url_resume = base_url()."user/resume";

    $inject_list = isset($inject_list) ? $inject_list : [];
    $count_list = count($inject_list);

    $count_isi_list = array_sum($inject_list);

    $prosentage = $count_isi_list / $count_list * 100;

    $color_pg_bar = "success";
    if($prosentage >= 0 and $prosentage < 30){
        $color_pg_bar = "danger";
    }elseif($prosentage > 30 and $prosentage < 70){
        $color_pg_bar = "warning";
    }elseif($prosentage > 70 and $prosentage < 100){
        $color_pg_bar = "success";
    }elseif($prosentage >= 100){
        $color_pg_bar = "info";
    }
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?> </h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">

    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">

                <?php if($resume){?>
                    <div class="alert alert-info">
                        <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> -->
                        <h3 class="text-info"><i class="fa fa-exclamation-circle"></i> Notifikasi Sistem !!!</h3> Data anda sudah ter-Submit kedalam Sistem. Terimakasih telah melakukan survey online melalui aplikasi ini.
                    </div>
                <?php
                    } else{
                ?>
                    <div class="alert alert-warning">
                        <!-- <button type="button" class="close" data-dismiss="alert" aria-label="Close"> <span aria-hidden="true">×</span> </button> -->
                        <h3 class="text-warning"><i class="fa fa-exclamation-triangle"></i> Peringatan !!!</h3> 
                        <h6>Data anda belum ter-Submit kedalam Sistem. Silahkan klik tombol berikut untuk melakukan submit.</h6>
                        <a href="<?=$url_resume?>" class="tst2 btn btn-warning">Submit Data Kita</a>
                    </div>

                <?php
                    }
                ?>
                    <h4 class="card-title">Status Pengisian Data <a class="get-code" data-toggle="collapse" href="#pgr2" aria-expanded="true"></a></h4>
                    
                    <div class="progress m-t-20">
                        <div class="progress-bar bg-<?=$color_pg_bar?>" style="width: <?=number_format($prosentage,0,'.', ',')?>%; height:15px;" role="progressbar"><?=number_format($prosentage,0,'.', ',')?>%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-5">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Detail Pengisian Data</h4>
                    <table class="table browser m-t-30 no-border">
                        <tbody>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-home-variant"></i></td>
                                <td>Data Kependudukan</td>
                                <td class="text-right">
                                    <?=$data_diri?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-account-edit"></i></td>
                                <td>Data Ketenagakerjaan</td>
                                <td class="text-right">
                                    <?=$ketenagakerjaan?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-ambulance"></i></td>
                                <td>Data Kesehatan</td>
                                <td class="text-right">
                                    <?=$kesehatan?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-cash-usd"></i></td>
                                <td>Data Keuangan</td>
                                <td class="text-right">
                                    <?=$keuangan?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-school"></i></td>
                                <td>Data Pendidikan</td>
                                <td class="text-right">
                                    <?=$pendidikan?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-chemical-weapon"></i></td>
                                <td>Data Kegiatan dan Organisasi</td>
                                <td class="text-right">
                                    <?=$kegiatan_organisasi?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-cash"></i></td>
                                <td>Data Sosial Ekonomi</td>
                                <td class="text-right">
                                    <?=$sosial_ekonomi?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-car"></i></td>
                                <td>Data Transport dan Komunikasi</td>
                                <td class="text-right">
                                    <?=$transportasi?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                            <tr>
                                <td style="width:40px"><i class="mdi mdi-hops"></i></td>
                                <td>Data Lingkungan</td>
                                <td class="text-right">
                                    <?=$lingkungan?'<span class="label label-info">Sudah Terisi</span>':'<span class="label label-danger">Belum Terisi</span>'?>
                                <!-- <span class="label label-info">Sudah Terisi</span></td> -->
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="col-lg-7">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-body">
                    <center>
                    <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img5.png" style="width: 40%; text-align: center;" alt="">
                    <H3>Program SAM GEPUN BASA (Smart City Gerakan Menghimpun Data Berbasis Dasa Wisma)</H3>
                    </center>
                </div>
                
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->
