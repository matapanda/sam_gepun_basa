<?php
    $url_main = base_url()."user/kesehatan";

    $main_controller = base_url()."user/kesehatanuser/";

    $str_op_jn_kb = "";
    if(isset($jenis_kb)){
        if($jenis_kb){
            foreach ($jenis_kb as $key => $value) {
                $str_op_jn_kb .= "<option value=\"".$value->nm_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_vaksin = "";
    if(isset($jenis_vaksin)){
        if($jenis_vaksin){
            foreach ($jenis_vaksin as $key => $value) {
                $str_op_jn_vaksin .= "<option value=\"".$value->nm_jenis."\">".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_dosis = "";
    if(isset($jenis_dosis)){
        if($jenis_dosis){
            foreach ($jenis_dosis as $key => $value) {
                $str_op_jn_dosis .= "<option value=\"".$value->nm_jenis."\">".$value->nm_jenis."</option>";
            }
        }
    }


    $id_user_kesehatan = "";
    $id_user = "";
    $bb = "";
    $tb = "";
    $q_vaksin = "";
    $d_vaksin = "{}";
    $q_kebutuhan_khusus = "";
    $q_butawarna = "";
    $q_expektor_kb = "";
    $kb_jenis = "";
    $q_stunting = "";
    $stnt_lingkar_kpl = "";
    $q_ibu_hamil = "";
    $hml_rutin_pemeriksaan = "";
    $hml_usia_kehamilan = "";
    $hml_tmp_periksa = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    if(isset($list_data)){
        if($list_data){
            $id_user_kesehatan = $list_data["id_user_kesehatan"];
            $id_user = $list_data["id_user"];
            $bb = $list_data["bb"];
            $tb = $list_data["tb"];
            $q_vaksin = $list_data["q_vaksin"];
            $d_vaksin = $list_data["d_vaksin"];
            
            $q_kebutuhan_khusus = $list_data["q_kebutuhan_khusus"];
            $q_butawarna = $list_data["q_butawarna"];
            $q_expektor_kb = $list_data["q_expektor_kb"];
            $kb_jenis = $list_data["kb_jenis"];
            $q_stunting = $list_data["q_stunting"];
            $stnt_lingkar_kpl = $list_data["stnt_lingkar_kpl"];
            $q_ibu_hamil = $list_data["q_ibu_hamil"];
            $hml_rutin_pemeriksaan = $list_data["hml_rutin_pemeriksaan"];
            $hml_usia_kehamilan = $list_data["hml_usia_kehamilan"];
            $hml_tmp_periksa = $list_data["hml_tmp_periksa"];
            $r_crt_by = $list_data["r_crt_by"];
            $r_crt_time = $list_data["r_crt_time"];
            $r_up_by = $list_data["r_up_by"];
            $r_up_time = $list_data["r_up_time"];
        }
    }

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                    <hr>
                </div>
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Berat Badan / Tinggi Badan :</label>
                                <input type="number" class="form-control col-md-3 alert-rounded" id="bb" name="bb" >
                                <label for="message-text" class="control-label col-md-1">kg</label>

                                <label for="message-text" class="control-label col-md-1">/</label>
                                <input type="number" class="form-control col-md-3 alert-rounded" id="tb" name="tb" >
                                <label for="message-text" class="control-label col-md-1">cm</label>
                               
                                <br><p id="msg_tb" style="color: red;"></p>
                            </div>

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Apakah Anda Berkebutuhan Khusus</label>
                                <input class="radio" type="radio" name="q_kebutuhan_khusus" id="q_kebutuhan_khusus1" value="iya">
                                <label for="q_kebutuhan_khusus1" class="control-label">Ya</label>
                                <input class="radio" type="radio" name="q_kebutuhan_khusus" id="q_kebutuhan_khusus2" value="tidak">
                                <label for="q_kebutuhan_khusus2" class="control-label">Tidak</label>
                                <br><p id="msg_q_kebutuhan_khusus" style="color: red;"></p>
                            </div>

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Apakah Anda Buta Warna</label>
                                
                                <input class="radio" type="radio" name="q_butawarna" id="q_butawarna1" value="iya">
                                <label for="q_butawarna1" class="control-label">Ya</label>

                                <input class="radio" type="radio" name="q_butawarna" id="q_butawarna2" value="tidak">
                                <label for="q_butawarna2" class="control-label">Tidak</label>
                                
                                <br><p id="msg_q_butawarna" style="color: red;"></p>
                            </div>

                           

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Sudah Vaksin Covid-19:</label>
                                <input class="radio" type="radio" name="q_vaksin" id="q_vaksin_1" value="iya" onclick="show_detail_vaksin()">
                                <label for="q_vaksin_1" class="control-label">Ya</label>
                                <input class="radio" type="radio" name="q_vaksin" id="q_vaksin_2" value="tidak" onclick="show_detail_vaksin()">
                                <label for="q_vaksin_2" class="control-label">Tidak</label>
                                <p id="msg_q_vaksin" style="color: red;"></p>
                            </div>

                            <div class="row" id="path_detail_vaksin" style="display:none">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th width="25%">Dosis</th>
                                                    <th width="25%">Jenis Vaksin</th>
                                                    <th width="25%">Tanggal Vaksin</th>
                                                    <th width="25" class="text-nowrap"></th>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <!-- <input type="number" class="form-control" id="d_vaksin" name="d_vaksin" required=""> -->

                                                        <select class="form-control" id="dosis_vaksin" name="dosis_vaksin" required="">
                                                            <option value="" selected>Pilih</option>
                                                            <?=$str_op_jn_dosis?>
                                                        </select>
                                                        <p id="msg_d_vaksin" style="color: red;"></p>
                                                    </td>
                                                    <td>
                                                        <select class="form-control" id="vaksin_jenis" name="vaksin_jenis" required>
                                                            <!-- <option value="-" selected>Pilih</option> -->
                                                            <?=$str_op_jn_vaksin?>
                                                            <!-- <option value="Sinovac">1. Sinovac</option>
                                                            <option value="Astra Zeneca">2. Astra Zeneca</option>
                                                            <option value="Moderna">3. Moderna</option>
                                                            <option value="Sinopharma">4. Sinopharma</option>
                                                            <option value="Pfizer">5. Pfizer</option>
                                                            <option value="Novavax">6. Novavax</option> -->
                                                        </select>
                                                        <p id="msg_vaksin_jenis" style="color: red;"></p>
                                                    </td>
                                                    
                                                    <td>
                                                        <input type="date" class="form-control" id="tanggal_vaksin" name="tanggal_vaksin" >
                                                        <p id="msg tanggal_vaksin" style="color: red;"></p>
                                                    </td>
                                                    <td align="center">
                                                        <button type="button" id="add_data_vaksin" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                                    </td>
                                                </tr>
                                            </thead>
                                            <tbody border="1" id="out_list_data" style="border-collapse: collapse;">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card">
                <div class="card-body">
                            <div class="card-header">
                                <h4 class="card-title m-b-0">Kesehatan Ibu</h4>
                                <h7 class="card-title m-b-0" style="color: #007bff;">*Hanya diisi bila Anda perempuan</h7>
                            </div><br>  

                             <div class="form-group row ">
                                <label for="message-text" class="control-label col-md-3">Akseptor KB</label>
                                <input class="radio" type="radio" name="q_expektor_kb" id="q_expektor_kb_1" value="iya" onclick="show_detail_jn_kb()">
                                <label for="q_expektor_kb_1" class="control-label">Ya</label>
                                <input class="radio" type="radio" name="q_expektor_kb" id="q_expektor_kb_2" value="tidak" onclick="show_detail_jn_kb()">
                                <label for="q_expektor_kb_2" class="control-label">Tidak</label>
                                <br><p id="msg_q_expektor_kb" style="color: red;"></p>
                            </div>

                            <div class="form-group row" id="path_jenis_kb" style="display:none">
                                <div class="col-md-12">
                                    <div class="table-responsive">
                                        <table class="table">
                                            <thead>
                                                <tr>
                                                    <td>
                                                        <label for="message-text" class="control-label">Jenis KB :</label>
                                                    </td>
                                                    <td>
                                                        <div class="col-md-3">
                                                            <select class="custom-select form-control input-sm m-b-10" id="kb_jenis" name="kb_jenis" required>
                                                            <option value="-" selected>Pilih</option>
                                                            <?=$str_op_jn_kb?>
                                                            
                                                        </select>
                                                        <br><p id="msg_kb_jenis" style="color: red;"></p>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </thead>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Apakah anda hamil</label>
                                <input class="radio" type="radio" name="q_ibu_hamil" id="q_ibu_hamil1" value="iya" onclick="show_ibu_hamil()">
                                <label for="q_ibu_hamil1" class="control-label">Ya</label>
                                <input class="radio" type="radio" name="q_ibu_hamil" id="q_ibu_hamil2" value="tidak" onclick="show_ibu_hamil()">
                                <label for="q_ibu_hamil2" class="control-label">Tidak</label>
                                <p id="msg_q_ibu_hamil" style="color: red;"></p>
                            </div>

                            <div class="form-group row" id="path_pemeriksaan" style="display:none">
                                <label for="message-text" class="control-label col-md-3">Apakah anda rutin memeriksakan kehamilan</label>
                                <input class="radio" type="radio" name="hml_rutin_pemeriksaan" id="hml_rutin_pemeriksaan1" value="iya">
                                <label for="hml_rutin_pemeriksaan1" class="control-label">Ya</label>

                                <input class="radio" type="radio" name="hml_rutin_pemeriksaan" id="hml_rutin_pemeriksaan2" value="tidak">
                                <label for="hml_rutin_pemeriksaan2" class="control-label">Tidak</label>
                                <p id="msg_hml_rutin_pemeriksaan" style="color: red;"></p>
                            </div>

                            <div id="path_usia_kehamilan" style="display:none">
                                <div class="form-group row" >
                                    <label for="message-text" class="control-label col-md-3">Usia Kehamilan</label>
                                    <input type="number" class="form-control col-md-1 alert-rounded" id="hml_usia_kehamilan" name="hml_usia_kehamilan" >
                                    <label for="message-text" class="control-label col-md-1">Minggu</label>
                                    <p id="msg_hml_usia_kehamilan" style="color: red;"></p>
                                </div>

                                <div class="form-group row">
                                    <label for="message-text" class="control-label col-md-3">Tempat Periksa Kehamilan</label>
                                    <input type="text" class="form-control col-md-3 alert-rounded" id="hml_tmp_periksa" name="hml_tmp_periksa">
                                    <p id="msg_hml_tmp_periksa" style="color: red;"></p>
                                </div>
                            </div>

                            <div class="card-header">
                                <h4 class="card-title m-b-0">Kesehatan Balita</h4>
                                <h7 class="card-title m-b-0" style="color: #007bff;">*Hanya diisi khusus untuk anak berusia 0-5 Tahun</h7>
                            </div>

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Apakah Stunting</label>
                                <input class="radio" type="radio" name="q_stunting" id="q_stunting_1" value="iya">
                                <label for="q_stunting_1" class="control-label">Ya</label>
                                <input class="radio" type="radio" name="q_stunting" id="q_stunting_2" value="tidak">
                                <label for="q_stunting_2" class="control-label">Tidak</label>
                                <p id="msg_q_stunting" style="color: red;"></p>
                            </div>

                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-3">Lingkar Kepala</label>
                                <input type="number" class="form-control col-md-1 alert-rounded" id="stnt_lingkar_kpl" name="stnt_lingkar_kpl">
                                <label for="message-text" class="control-label col-md-1">cm</label>
                                <p id="msg_stnt_lingkar_kpl" style="color: red;"></p>
                            </div>
                            
                        </div>
                    </div>
                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                    <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;">Loading...</button>
                </div>
                <?php 
                    } 
                ?>
        </div>
    </div>
    <!-- End Row -->
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var list_data = {};

    // var list_data = {};
    try {
        list_data = JSON.parse('<?=$d_vaksin?>')
    }catch(err) {
        console.log("err json format");
    }

    $(document).ready(function(){
        $("#bb").val("<?=isset($list_data['bb']) ? $list_data['bb'] : '0'?>");
        $("#tb").val("<?=isset($list_data['tb']) ? $list_data['tb'] : '0'?>");

        $("input[name='q_kebutuhan_khusus'][value='<?=isset($list_data['q_kebutuhan_khusus']) ? $list_data['q_kebutuhan_khusus'] : 'iya'?>']").prop("checked",true);

        $("input[name='q_butawarna'][value='<?=isset($list_data['q_butawarna']) ? $list_data['q_butawarna'] : 'iya'?>']").prop("checked",true);

        
        $("input[name='q_expektor_kb'][value='<?=isset($list_data['q_expektor_kb']) ? $list_data['q_expektor_kb'] : 'iya'?>']").prop("checked",true);
        show_detail_jn_kb();
        $("#kb_jenis").val("<?= $kb_jenis ? $kb_jenis : "-" ?>");

        $("input[name='q_vaksin'][value='<?=isset($list_data['q_vaksin']) ? $list_data['q_vaksin'] : 'iya'?>']").prop("checked",true);
        show_detail_vaksin()


        $("input[name='q_ibu_hamil'][value='<?=isset($list_data['q_ibu_hamil']) ? $list_data['q_ibu_hamil'] : 'iya'?>']").prop("checked",true);
        show_ibu_hamil()
        $("input[name='hml_rutin_pemeriksaan'][value='<?=isset($list_data['hml_rutin_pemeriksaan']) ? $list_data['hml_rutin_pemeriksaan'] : 'iya'?>']").prop("checked",true);

        $("#hml_usia_kehamilan").val("<?=$hml_usia_kehamilan?>");
        $("#hml_tmp_periksa").val("<?=$hml_tmp_periksa?>");


        $("input[name='q_stunting'][value='<?=isset($list_data['q_stunting']) ? $list_data['q_stunting'] : 'iya'?>']").prop("checked",true);
        $("#stnt_lingkar_kpl").val("<?=$stnt_lingkar_kpl?>");


        render_list_data();
        
    });

    //=========================================================================//
    //-----------------------------------send_data--------------------------//
    //=========================================================================//
        $("#update").click(function() {
            console.log(JSON.stringify(list_data));
            var data_main = new FormData();          
            data_main.append('bb', $("#bb").val());
            data_main.append('tb', $("#tb").val());

            // console.log();
            // data vaksin
            var str_q_vaksin = "";
            var str_d_vakasin = "{}";
            if($('input[name="q_vaksin"]:checked').val()){
                // console.log("ok");
                str_q_vaksin = $('input[name="q_vaksin"]:checked').val();
                console.log(str_q_vaksin);
                if(str_q_vaksin == "iya"){
                    str_d_vakasin = JSON.stringify(list_data);
                }
            }
            data_main.append('q_vaksin', str_q_vaksin);
            data_main.append('d_vaksin', str_d_vakasin);
            
            // data kebutuhan khusus dan buta warna
            data_main.append('q_kebutuhan_khusus', $('input[name="q_kebutuhan_khusus"]:checked').val() ? $('input[name="q_kebutuhan_khusus"]:checked').val() : "");
            data_main.append('q_butawarna', $('input[name="q_butawarna"]:checked').val() ? $('input[name="q_butawarna"]:checked').val() : "");

            // data expektor kb
            var str_q_expektor_kb = "";
            var str_kb_jenis = "";
            if($('input[name="q_expektor_kb"]:checked').val()){
                // console.log("ok");
                str_q_expektor_kb = $('input[name="q_expektor_kb"]:checked').val();
                str_kb_jenis = $("#kb_jenis").val();
            }
            data_main.append('q_expektor_kb', str_q_expektor_kb);
            data_main.append('kb_jenis', str_kb_jenis);
            
            // data stunting
            var str_q_stunting = "";
            var str_stnt_lingkar_kpl = "0";
            if($('input[name="q_stunting"]:checked').val()){
                // console.log("ok");
                str_q_stunting = $('input[name="q_stunting"]:checked').val();
                // if(str_q_stunting == "iya"){
                    // str_stnt_lingkar_kpl = $("#stnt_lingkar_kpl").val();
                // }
                
            }
            str_stnt_lingkar_kpl = $("#stnt_lingkar_kpl").val();

            data_main.append('q_stunting', str_q_expektor_kb);
            data_main.append('stnt_lingkar_kpl', str_stnt_lingkar_kpl ? str_stnt_lingkar_kpl : "0");
            
            // data kehamilan
            var str_q_ibu_hamil = "tidak";
            var str_hml_usia_kehamilan = "0";
            var str_hml_tmp_periksa = "-";
            var str_hml_rutin_pemeriksaan = "tidak";

            if($('input[name="q_ibu_hamil"]:checked').val()){
                // console.log("ok");
                if($('input[name="q_ibu_hamil"]:checked').val() == "iya"){
                    if($('input[name="hml_rutin_pemeriksaan"]:checked').val()){
                        str_hml_rutin_pemeriksaan = $('input[name="hml_rutin_pemeriksaan"]:checked').val();
                    }
                    str_q_ibu_hamil = $('input[name="q_ibu_hamil"]:checked').val();
                    str_hml_usia_kehamilan = $("#hml_usia_kehamilan").val();
                    str_hml_tmp_periksa = $("#hml_tmp_periksa").val();
                }
                str_q_ibu_hamil = $('input[name="q_ibu_hamil"]:checked').val();
            }

            data_main.append('q_ibu_hamil', str_q_ibu_hamil);
            data_main.append('hml_rutin_pemeriksaan', str_hml_rutin_pemeriksaan);
            data_main.append('hml_usia_kehamilan', str_hml_usia_kehamilan ? str_hml_usia_kehamilan : "0");
            data_main.append('hml_tmp_periksa', str_hml_tmp_periksa ? str_hml_tmp_periksa : "-");
            
            $.ajax({
                url: "<?=$main_controller?>act_controller",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                beforeSend: () => {
                    $('#update').hide();
                    $('#update').attr('disabled', true);
                    $('#btn-loading').show();
                },
                complete: () => {
                    $('#update').show();
                    $('#update').attr('disabled', false);
                    $('#btn-loading').hide();
                },
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/kesehatan");?>");
            } else {
                $("#msg_bb").html(detail_msg.bb);
                $("#msg_tb").html(detail_msg.tb);
                $("#msg_q_expektor_kb").html(detail_msg.q_expektor_kb);
                $("#msg_kb_jenis").html(detail_msg.kb_jenis);
                $("#msg_q_stunting").html(detail_msg.q_stunting);
                $("#msg_q_vaksin").html(detail_msg.q_vaksin);
                $("#msg_vaksin_jenis").html(detail_msg.vaksin_jenis);
                $("#msg_ms_tanggal_vaksin").html(detail_msg.ms_tanggal_vaksin);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

    //=========================================================================//
    //-----------------------------------send_data--------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------show-hide--------------------------//
    //=========================================================================//

        function show_detail_jn_kb(){
            var q_kb = $('input[name="q_expektor_kb"]:checked').val();
            if(q_kb == "iya"){
                $("#path_jenis_kb").show(20);
            }else{
                $("#path_jenis_kb").hide(20);
            }
        }

        function show_detail_vaksin(){
            var q_kb = $('input[name="q_vaksin"]:checked').val();
            if(q_kb == "iya"){
                $("#path_detail_vaksin").show(20);
            }else{
                $("#path_detail_vaksin").hide(20);
            }
        }

        function show_ibu_hamil(){
            var q_kb = $('input[name="q_ibu_hamil"]:checked').val();
            if(q_kb == "iya"){
                $("#path_pemeriksaan").show(20);
                $("#path_usia_kehamilan").show(20);
            }else{
                $("#path_pemeriksaan").hide(20);
                $("#path_usia_kehamilan").hide(20);
            }
        }

    //=========================================================================//
    //-----------------------------------show-hide--------------------------//
    //=========================================================================//
        

    //=========================================================================//
    //-----------------------------------add_detail_data--------------------------//
    //=========================================================================//
        function add_list_data(){
            var tmp = {
                "dosis_vaksin":$("#dosis_vaksin").val(),
                "vaksin_jenis":$("#vaksin_jenis").val(),
                "tanggal_vaksin":$("#tanggal_vaksin").val()
            };

            if($("#dosis_vaksin").val() != "" &&
                $("#vaksin_jenis").val() != "" &&
                $("#tanggal_vaksin").val() != ""){
                    var key = $("#dosis_vaksin").val().replaceAll(" ", "");
                    if(!(key in list_data)){
                        list_data[$("#dosis_vaksin").val().replaceAll(" ", "")] = tmp;
                        render_list_data();
                    }else{
                        alert("Data dosis 1 sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_data(){
            var str_list_data = "";
            if(list_data){
                for (const i in list_data) {
                    str_list_data += "<tr>"+
                                        "<td>"+list_data[i].dosis_vaksin+"</td>"+
                                        "<td>"+list_data[i].vaksin_jenis+"</td>"+
                                        "<td>"+list_data[i].tanggal_vaksin+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                }
            }

            $("#out_list_data").html(str_list_data);
        }

        $("#add_data_vaksin").click(function(){
            add_list_data();

            // console.log(list_data);
        });


        function delete_list(key){
            // console.log(list_data[key]);
            delete list_data[key];

            render_list_data();
        }

    //=========================================================================//
    //-----------------------------------add_detail_data--------------------------//
    //=========================================================================//

</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->
