<?php
    $url_main = base_url()."user/ketenagakerjaan";

    $main_controller = base_url()."user/ketenagakerjaanuser/";


    $str_op_sts_pekerjaan = "";
    if(isset($sts_pekerjaan)){
        if($sts_pekerjaan){
            foreach ($sts_pekerjaan as $key => $value) {
                $str_op_sts_pekerjaan .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_pekerjaan = "";
    if(isset($jn_pekerjaan)){
        if($jn_pekerjaan){
            foreach ($jn_pekerjaan as $key => $value) {
                $str_op_jn_pekerjaan .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_bidang_pekerjaan = "";
    if(isset($bidang_pekerjaan)){
        if($bidang_pekerjaan){
            foreach ($bidang_pekerjaan as $key => $value) {
                $str_op_bidang_pekerjaan .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }


    $id_user_kerja = "";
    $id_user = "";
    $sts_kerja = "";
    $sts_kerja_ket = "";
    $jenis_kerja = "";
    $tempat_kerja = "";
    $bidang_pekerjaan = "";
    $penghasilan_perbulan = "";
    $q_terima_pensiun = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    if(isset($list_data)){
        if($list_data){
            $id_user_kerja = $list_data["id_user_kerja"];
            $id_user = $list_data["id_user"];
            $sts_kerja = $list_data["sts_kerja"];
            $sts_kerja_ket = $list_data["sts_kerja_ket"];
            $jenis_kerja = $list_data["jenis_kerja"];
            $tempat_kerja = $list_data["tempat_kerja"];
            $bidang_pekerjaan = $list_data["bidang_pekerjaan"];
            $penghasilan_perbulan = $list_data["penghasilan_perbulan"];
            $q_terima_pensiun = $list_data["q_terima_pensiun"];
            $r_crt_by = $list_data["r_crt_by"];
            $r_crt_time = $list_data["r_crt_time"];
            $r_up_by = $list_data["r_up_by"];
            $r_up_time = $list_data["r_up_time"];
        }
    }
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <p class="note"><label for="inp_catatan">Catatan :</label><br><br> 
            <b>1.</b> Berikan tanda strip(-) jika tidak ada <br>
            <b>2.</b> Lama bekerja /bulan <br>
            </p>
        </div>
    </div>
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show">
                <div class="col-md-6">
                    <div class="form-group row">
                        <label for="message-text" class="control-label col-md-4">Status Kerja :</label>
                        <div class="col-md-8">
                            <select type="text" class="form-control" id="sts_kerja" name="sts_kerja">
                                <option value="" disabled > Pilih</option>
                                <?=$str_op_sts_pekerjaan?>
                            </select>
                            <p id="msg_sts_kerja" style="color: red;"></p></div>       
                        </div>
                    
                    <div class="show-hide" id="path_detail_pekerjaan">
                        <div class="form-group row" >
                            <label for="message-text" class="control-label col-md-4">Jenis Pekerjaan :</label>
                            <div class="col-md-8">
                                <select type="text" class="form-control" id="jenis_kerja" name="jenis_kerja"> 
                                    <option value="-" disabled > Pilih Jenis Pekerjaan </option>
                                    <?=$str_op_jn_pekerjaan?>
                                </select>
                                <p id="msg_jenis_kerja" style="color: red;"></p>
                            </div>   
                        </div>
                        <div class="form-group row" >
                            <label for="message-text" class="control-label col-md-4">Bidang Pekerjaan :</label>
                            <div class="col-md-8">
                                <select class="form-control" id="bidang_pekerjaan" name="bidang_pekerjaan">
                                    <option value="" disabled>Pilih </option>

                                    <?=$str_op_bidang_pekerjaan?>
                                </select>
                                <!-- <input type="text"  maxlength="16" > -->
                                <p id="msg_bidang_pekerjaan" style="color: red;"></p>
                            </div>                              
                        </div>
                        <div class="form-group row" >
                            <label for="message-text" class="control-label col-md-4">Alamat Tempat Bekerja :</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="tempat_kerja" name="tempat_kerja" >
                                <p id="msg_tempat_kerja" style="color: red;"></p>
                            </div>         
                        </div>
                        <div class="form-group row" >
                            <label for="message-text" class="control-label col-md-4">Penghasilan Per Bulan :</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="penghasilan_perbulan" name="penghasilan_perbulan" >
                                <p id="msg_penghasilan_perbulan" style="color: red;"></p>
                            </div>         
                        </div>
                                             
                    </div>

                    <div class="form-group row">
                        <label for="message-text" class="control-label col-md-4">Menerima Pensiun ? :</label><br>
                        <div class="col-md-8">
                            <input class="radio" type="radio" name="q_terima_pensiun" id="q_terima_pensiun_1" value="iya">
                            <label for="q_terima_pensiun_1">Iya</label>
                            <input class="radio" type="radio" name="q_terima_pensiun" id="q_terima_pensiun_2" value="tidak">
                            <label for="q_terima_pensiun_2">Tidak</label>
                            <p id="msg_q_terima_pensiun" style="color: red;"></p>
                        </div>
                        
                    </div>    
                    
                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                    <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;"> Loading...</button>
                </div>

                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
    
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        set_param();
        ch_sts_pekerjaan();
    })

    function set_param(){
        $("#sts_kerja").val("<?=$sts_kerja?>");
        $("#jenis_kerja").val("<?=$jenis_kerja?>");
        $("#tempat_kerja").val("<?=$tempat_kerja?>");
        $("#bidang_pekerjaan").val("<?=$bidang_pekerjaan?>");
        $("#penghasilan_perbulan").val("<?=$penghasilan_perbulan?>");
        
        $("input[name='q_terima_pensiun'][value='<?=isset($list_data['q_terima_pensiun']) ? $list_data['q_terima_pensiun'] : 'iya'?>']").prop("checked",true);
    }


    function ch_sts_pekerjaan(){
        var sts_kerja = $("#sts_kerja").val();
        if(sts_kerja == "bekerja"){
            $('.show-hide').fadeIn();
        }else{
            $('.show-hide').fadeOut();
            $('#jenis_kerja').val('-');
            $('#bidang_pekerjaan').val('-');
            $('#tempat_kerja').val('-');
            $('#penghasilan_perbulan').val('0');
            // $('#keterangan').fadeIn();
            $("input:radio[name=q_terima_pensiun]").filter('[value=iya]').prop("checked",true);
        }
    }

    $("#sts_kerja").change(function(){
        ch_sts_pekerjaan()
    })

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
    $("#update").click(function() {
            var data_main = new FormData();
            data_main.append('sts_kerja', $("#sts_kerja").val());
            data_main.append('jenis_kerja', $("#jenis_kerja").val());
            data_main.append('bidang_pekerjaan', $("#bidang_pekerjaan").val());
            data_main.append('tempat_kerja', $("#tempat_kerja").val());
            data_main.append('penghasilan_perbulan', $("#penghasilan_perbulan").val());

            data_main.append('q_terima_pensiun', $('input[name="q_terima_pensiun"]:checked').val() ? $('input[name="q_terima_pensiun"]:checked').val() : "tidak");

            $.ajax({
                url: "<?=$main_controller?>act_controller",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                beforeSend: () => {
                    $('#update').hide();
                    $('#update').attr('disabled', true);
                    $('#btn-loading').show();
                },
                complete: () => {
                    $('#update').show();
                    $('#update').attr('disabled', false);
                    $('#btn-loading').hide();
                },
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json   = JSON.parse(res);
            var main_msg    = data_json.msg_main;
            var detail_msg  = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/ketenagakerjaan");?>");
            } else {
                $("#msg_sts_kerja").html(detail_msg.sts_kerja);
                $("#msg_jenis_kerja").html(detail_msg.jenis_kerja);
                $("#msg_tempat_kerja").html(detail_msg.tempat_kerja);
                $("#msg_q_menerima_pensiun").html(detail_msg.q_menerima_pensiun);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->
