<?php
    $url_main = base_url()."user/keuangan";

    $main_controller = base_url()."user/Keuanganuser/";

    $id_user_keuangan = "";
    $id_user = "";
    $q_harta_tidak_bergerak = "";
    $htb_detail = "";
    $q_harta_bergerak = "";
    $hb_detail = "";
    $q_deposito = "";
    $q_piutang = "";
    $q_hutang = "";
    $ht_nominal = "";
    $t_kekayaan = "";
    $q_penghasilan_per_th = "";
    $q_penghasilan_profesi = "";
    $q_penghasilan_usaha = "";
    $q_penghasilan_hibah = "";
    $t_penghasilan = "";
    $t_pengeluaran_rutin = "";
    $t_pengeluaran_lainnya = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";


    if(isset($list_data)){
        if($list_data){
          
          $q_harta_tidak_bergerak = $list_data["q_harta_tidak_bergerak"];
          $htb_detail = $list_data["htb_detail"];
          $q_harta_bergerak = $list_data["q_harta_bergerak"];
          $hb_detail = $list_data["hb_detail"];
          $q_deposito = $list_data["q_deposito"];
          $q_piutang = $list_data["q_piutang"];
          $q_hutang = $list_data["q_hutang"];
          $ht_nominal = $list_data["ht_nominal"];
          $t_kekayaan = $list_data["t_kekayaan"];
          $q_penghasilan_per_th = $list_data["q_penghasilan_per_th"];
          $q_penghasilan_profesi = $list_data["q_penghasilan_profesi"];
          $q_penghasilan_usaha = $list_data["q_penghasilan_usaha"];
          $q_penghasilan_hibah = $list_data["q_penghasilan_hibah"];
          $t_penghasilan = $list_data["t_penghasilan"];
          $t_pengeluaran_rutin = $list_data["t_pengeluaran_rutin"];
          $t_pengeluaran_lainnya = $list_data["t_pengeluaran_lainnya"];
          
        }
    }
    
?>
<style>
    .hide {
  display: none;
}
.form-group {
    margin-bottom: -10px;
}
</style>


<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
  <!-- Row -->
  <div class="row">
    <div class="col-md-12">
      <div class="card card-default">
        <div class="card-header">
          <div class="card-actions">
            <a class="" data-action="collapse">
              <i class="ti-minus"></i>
            </a>
            <a class="btn-minimize" data-action="expand">
              <i class="mdi mdi-arrow-expand"></i>
            </a>
          </div>
          <h4 class="card-title m-b-0">1. <?=$title?> </h4>
        </div>
        <div class="card-body collapse show">
          <div class="form-group" style="padding-bottom: 0px">
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Harta Tidak Bergerak (Tanah dan Bangunan)</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_harta_tidak_bergerak" value="iya" id="ya1" onclick="show_htb_detail()">
                <label for="ya1">Iya</label>
                <input class="q_anggota" type="radio" name="q_harta_tidak_bergerak" value="tidak" id="tidak1" onclick="show_htb_detail()">
                <label for="tidak1">Tidak</label>
                <p id="msg_q_harta_tidak_bergerak" style="color: red;"></p>
              </div>
              <div id="path_htb_detail" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th width="10%">No</th>
                        <th width="30%">Jenis Harta</th>
                        <th width="30%">Harga Perolehan/Nominal</th>
                        <th width="20%">Aksi</th>
                      </tr>
                      <tr>
                        <td>
                            #
                        </td>
                        <td>
                          <input type="text" class="form-control" id="jn_htb_detail" name="jn_htb_detail" required="">
                          <p id="msg_jn_htb_detail" style="color: red;"></p>
                        </td>
                        <td>
                          <input type="number" class="form-control" id="n_htb_detail" name="n_htb_detail" required="">
                          <p id="msg_n_htb_detail" style="color: red;"></p>
                        </td>
                        <td>
                          <button type="button" id="add_htb_detail" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                        </td>
                      </tr>
                    </thead>
                    <tbody id="out_list_htb_detail"></tbody>
                    <tfoot>
                      <tr>
                        <th colspan="2" align="center">Total Harta</th>
                        <th><b id="n_t_htb_detail">Rp. 0</b></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <div class="form-group">
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Harta Bergerak (Alat Transportasi, Peternakan, Logam Mulia,dll)</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_harta_bergerak" value="iya" id="ya_harta_bergerak" onclick="show_hb_detail()">
                <label for="ya_harta_bergerak">Iya</label>
                <input class="q_anggota" type="radio" name="q_harta_bergerak" value="tidak" id="tidak_harta_bergerak" onclick="show_hb_detail()">
                <label for="tidak_harta_bergerak">Tidak</label>
                <p id="msg_q_anggota_dasawisma" style="color: red;"></p>
              </div>
              <div id="path_hb_detail" class="ya box col-md-12" style="padding-top: 1px; display: none">
                <div class="table-responsive">
                  <table class="table">
                    <thead>
                      <tr>
                        <th width="10%">No</th>
                        <th width="30%">Jenis Harta</th>
                        <th width="30%">Harga Perolehan/Nominal</th>
                        <th width="20%">Aksi</th>
                      </tr>
                      <tr>
                        <td>
                          #
                        </td>
                        <td>
                          <input type="text" class="form-control" id="jn_hb_detail" name="jn_hb_detail" required="">
                          <p id="msg_jn_hb_detail" style="color: red;"></p>
                        </td>
                        <td>
                          <input type="number" class="form-control" id="n_hb_detail" name="n_hb_detail" required="">
                          <p id="msg_n_hb_detail" style="color: red;"></p>
                        </td>
                        <td>
                          <button type="button" id="add_hb_detail" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                        </td>
                      </tr>
                    </thead>
                    <tbody id="out_list_hb_detail"></tbody>
                    <tfoot>
                      <tr>
                        <th colspan="2" align="center">Total Harta</th>
                        <th><b id="n_t_hb_detail">Rp. 0</b></th>
                      </tr>
                    </tfoot>
                  </table>
                </div>
              </div>
            </div>
          </div>
          
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4">Uang Tunai, Deposito, Giro, Tabungan dan kas Lainnya:</label>
            <div class="col-md-8">
              <input class="q_anggota" type="radio" name="q_deposito" value="iya" id="ya_uang_tunai">
              <label for="ya_uang_tunai">Iya</label>
              <input class="q_anggota" type="radio" name="q_deposito" value="tidak" id="tidak_uang_tunai">
              <label for="tidak_uang_tunai">Tidak</label>
              <p id="msg_q_deposito" style="color: red;"></p>
            </div>
          </div>
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4">Piutang (Barang, Uang):</label>
            <div class="col-md-8">
              <input class="q_anggota" type="radio" name="q_piutang" value="iya" id="ya_piutang">
              <label for="ya_piutang">Iya</label>
              <input class="q_anggota" type="radio" name="q_piutang" value="tidak" id="tidak_piutang">
              <label for="tidak_piutang">Tidak</label>
              <p id="msg_q_piutang" style="color: red;"></p>
            </div>
          </div>
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4">Apakah anda memiliki hutang ? (Barang, Uang):</label>
            <div class="col-md-8">
              <input class="q_anggota" type="radio" name="q_hutang" value="iya" id="ya_q_hutang" onclick="show_t_hutang()">
              <label for="ya_q_hutang">Iya</label>
              <input class="q_anggota" type="radio" name="q_hutang" value="tidak" id="tidak_q_hutang" onclick="show_t_hutang()">
              <label for="tidak_q_hutang">Tidak</label>
              <p id="msg_q_hutang" style="color: red;"></p>
            </div>
          </div>
          <div class="form-group row" id="path_ht_nominal">
            <label for="message-text" class="control-label col-md-4">Hutang</label>
            <div class="col-md-4">
              <input type="number" class="form-control" id="ht_nominal" name="ht_nominal" value="0" placeholder="Rp.">
              <p id="msg_ht_nominal" style="color: red;"></p>
            </div>
          </div>
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4"><b>Total Harta Kekayaan</b></label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="t_kekayaan" name="t_kekayaan" placeholder="Rp.">
              <p id="msg_t_kekayaan" style="color: red;"></p>
            </div>
          </div>

         

          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4"><b>Total Harta :</b></label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="t_kekayaan_all" name="t_kekayaan_all" placeholder="Rp." disabled>
              <p id="msg_t_kekayaan_all" style="color: red;"></p>
            </div>
            <div class="col-md-4">(Total Harta - Hutang</div>
          </div>
          </table>

          <br>
          <h4>2. Penghasilan</h4>
          <br>
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Penghasilan dari Profesi/Keahlian (Per Tahun):</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_penghasilan_per_th" value="iya" id="ya_q_penghasilan_per_th">
                <label for="ya_q_penghasilan_per_th">Iya</label>
                <input class="q_anggota" type="radio" name="q_penghasilan_per_th" value="tidak" id="tidak_q_penghasilan_per_th">
                <label for="tidak_q_penghasilan_per_th">Tidak</label>
                <p id="msg_q_penghasilan_per_th" style="color: red;"></p>
              </div>
            </div>
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Penghasilan dari Profesi/Keahlian (Per Tahun):</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_penghasilan_profesi" value="iya" id="ya_penghasilan_profesi">
                <label for="ya_penghasilan_profesi">Iya</label>
                <input class="q_anggota" type="radio" name="q_penghasilan_profesi" value="tidak" id="tidak_penghasilan_profesi">
                <label for="tidak_penghasilan_profesi">Tidak</label>
                <p id="msg_q_penghasilan_profesi" style="color: red;"></p>
              </div>
            </div>
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Penghasilan dari Usaha Lainnya (Per tahun):</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_penghasilan_usaha" value="iya" id="ya_penghasilan_usaha">
                <label for="ya_penghasilan_usaha">Iya</label>
                <input class="q_anggota" type="radio" name="q_penghasilan_usaha" value="tidak" id="tidak_penghasilan_usaha">
                <label for="tidak_penghasilan_usaha">Tidak</label>
                <p id="msg_q_penghasilan_usaha" style="color: red;"></p>
              </div>
            </div>
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4">Penghasilan dari Hibah Lainnya</label>
              <div class="col-md-8">
                <input class="q_anggota" type="radio" name="q_penghasilan_hibah" value="iya" id="ya_penghasilan_hibah">
                <label for="ya_penghasilan_hibah">Iya</label>
                <input class="q_anggota" type="radio" name="q_penghasilan_hibah" value="tidak" id="tidak_penghasilan_hibah">
                <label for="tidak_penghasilan_hibah">Tidak</label>
                <p id="msg_q_penghasilan_hibah" style="color: red;"></p>
              </div>
            </div>
            <div class="form-group row">
              <label for="message-text" class="control-label col-md-4"><b>Total Penghasilan :</b></label>
              <div class="col-md-4">
                <input type="number" class="form-control" id="t_penghasilan" name="t_penghasilan" placeholder="Rp.">
                <p id="msg_t_penghasilan" style="color: red;"></p>
              </div>
            </div>
          </div>
          <h4> 3. Pengeluaran (Per Tahun)</h4>
          <br>
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4">Pengeluaran Rutin :</label>
            <div class="col-md-4">
              <input type="number" class="form-control" id="t_pengeluaran_rutin" name="t_pengeluaran_rutin" placeholder="Rp.">
              <p id="msg_t_pengeluaran_rutin" style="color: red;"></p>
            </div>
          </div>

          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4">Pengeluaran Lainnya :</label>
            <div class="col-md-4">
              <input type="number" class="form-control" id="t_pengeluaran_lainnya" name="t_pengeluaran_lainnya" placeholder="Rp.">
             
              <p id="msg_t_pengeluaran_lainnya" style="color: red;"></p>
            </div>
          </div>
          <div class="form-group row">
            <label for="message-text" class="control-label col-md-4"><b>Total Pengeluaran :</b></label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="t_pengeluaran" name="t_pengeluaran" placeholder="Rp." disabled>
              </td>
              <p id="msg_t_pengeluaran" style="color: red;"></p>
            </div>
          </div>
           <div class="form-group row">
            <label for="message-text" class="control-label col-md-4"><b>Pengehasilan Bersih :</b></label>
            <div class="col-md-4">
              <input type="text" class="form-control" id="t_penghasilan_bersih" name="t_penghasilan_bersih" placeholder="Rp." disabled>
              <p id="msg_t_penghasilan_bersih" style="color: red;"></p>
            </div>
            <div class="col-md-4">(Total Penghasilan - Total Pengeluaran)</div>
          </div>

        </div>
        <?php
                    if(!$resume){
                ?>
        <div class="card-footer text-right">
          <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
          <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;"> Loading...</button>
        </div>
        <?php 
                    } 
                ?>
      </div>
     
    </div>
  </div>
</div>
<!-- End Row -->
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script src="<?php print_r(base_url()."assets/js/currency.min.js");?>"></script>

<script type="text/javascript">
    var n_t_htb_detail = 0;
    var n_t_hb_detail = 0;

    var list_hb_detail = {};
    try {
      list_hb_detail = JSON.parse('<?=$hb_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    var list_htb_detail = {};
    try {
      list_htb_detail = JSON.parse('<?=$htb_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    $(document).ready(function(){
        set_param()
    })

    function convert_cur(param){
      return "Rp. "+currency(param, { separator: '.', decimal: ',', pattern: '#'}).format();
    }

    function set_param(){
      $("input[name='q_harta_tidak_bergerak'][value='<?=$q_harta_tidak_bergerak ? $q_harta_tidak_bergerak : 'iya'?>']").prop("checked",true);

      $("input[name='q_harta_bergerak'][value='<?=$q_harta_bergerak ? $q_harta_bergerak : 'iya'?>']").prop("checked",true);
      
      $("input[name='q_deposito'][value='<?=$q_deposito ? $q_deposito : 'iya'?>']").prop("checked",true);
      $("input[name='q_piutang'][value='<?=$q_piutang ? $q_piutang : 'iya'?>']").prop("checked",true);
      
      $("input[name='q_hutang'][value='<?=$q_hutang ? $q_hutang : 'iya'?>']").prop("checked",true);
      $("#ht_nominal").val("<?=$ht_nominal?$ht_nominal:"0"?>")
      $("#t_kekayaan").val("<?=$t_kekayaan?$t_kekayaan:"0"?>")
      
      $("input[name='q_penghasilan_per_th'][value='<?=$q_penghasilan_per_th ? $q_penghasilan_per_th : 'iya'?>']").prop("checked",true);
      $("input[name='q_penghasilan_profesi'][value='<?=$q_penghasilan_profesi ? $q_penghasilan_profesi : 'iya'?>']").prop("checked",true);
      $("input[name='q_penghasilan_usaha'][value='<?=$q_penghasilan_usaha ? $q_penghasilan_usaha : 'iya'?>']").prop("checked",true);
      $("input[name='q_penghasilan_hibah'][value='<?=$q_penghasilan_hibah ? $q_penghasilan_hibah : 'iya'?>']").prop("checked",true);

      $("#t_penghasilan").val("<?=$t_penghasilan?$t_penghasilan:"0"?>")
      $("#t_pengeluaran_rutin").val("<?=$t_pengeluaran_rutin?$t_pengeluaran_rutin:"0"?>")
      $("#t_pengeluaran_lainnya").val("<?=$t_pengeluaran_lainnya?$t_pengeluaran_lainnya:"0"?>")

      show_hb_detail()
      show_htb_detail()
      show_t_hutang()

      render_list_hb_detail();
      render_list_htb_detail();

      count_kekayaan()
      count_penghasilan()
    }
       

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#update").click(function() {
          var data_main = new FormData();

          data_main.append('q_harta_tidak_bergerak', $('input[name="q_harta_tidak_bergerak"]:checked').val() ? $('input[name="q_harta_tidak_bergerak"]:checked').val() : "");
          data_main.append('htb_detail', JSON.stringify(list_htb_detail));
          
          data_main.append('q_harta_bergerak', $('input[name="q_harta_bergerak"]:checked').val() ? $('input[name="q_harta_bergerak"]:checked').val() : "");
          data_main.append('hb_detail', JSON.stringify(list_hb_detail));
          
          data_main.append('q_deposito', $('input[name="q_deposito"]:checked').val() ? $('input[name="q_deposito"]:checked').val() : "");
          data_main.append('q_piutang', $('input[name="q_piutang"]:checked').val() ? $('input[name="q_piutang"]:checked').val() : "");
          
          data_main.append('q_hutang', $('input[name="q_hutang"]:checked').val() ? $('input[name="q_hutang"]:checked').val() : "");
          
          var ht_nominal = "0"
          if($('input[name="q_hutang"]:checked').val() == "iya"){
            ht_nominal = $("#ht_nominal").val();
          }
          data_main.append('ht_nominal', ht_nominal?ht_nominal:"0");

          data_main.append('t_kekayaan', $("#t_kekayaan").val()?$("#t_kekayaan").val():"0");
          
          data_main.append('q_penghasilan_per_th', $('input[name="q_penghasilan_per_th"]:checked').val() ? $('input[name="q_penghasilan_per_th"]:checked').val() : "tidak");
          data_main.append('q_penghasilan_profesi', $('input[name="q_penghasilan_profesi"]:checked').val() ? $('input[name="q_penghasilan_profesi"]:checked').val() : "tidak");
          data_main.append('q_penghasilan_usaha', $('input[name="q_penghasilan_usaha"]:checked').val() ? $('input[name="q_penghasilan_usaha"]:checked').val() : "tidak");
          data_main.append('q_penghasilan_hibah', $('input[name="q_penghasilan_hibah"]:checked').val() ? $('input[name="q_penghasilan_hibah"]:checked').val() : "tidak");
          
          data_main.append('t_penghasilan', $("#t_penghasilan").val()?$("#t_penghasilan").val():"0");
          data_main.append('t_pengeluaran_rutin', $("#t_pengeluaran_rutin").val()?$("#t_pengeluaran_rutin").val():"0");
          data_main.append('t_pengeluaran_lainnya', $("#t_pengeluaran_lainnya").val()?$("#t_pengeluaran_lainnya").val():"0");
          

          $.ajax({
              url: "<?=$main_controller?>act_controller",
              dataType: 'html', // what to expect back from the PHP script, if anything
              cache: false,
              contentType: false,
              processData: false,
              data: data_main,
              type: 'post',
              beforeSend: () => {
                  $('#update').hide();
                  $('#update').attr('disabled', true);
                  $('#btn-loading').show();
              },
              complete: () => {
                  $('#update').show();
                  $('#update').attr('disabled', false);
                  $('#btn-loading').hide();
              },
              success: function(res) {
                  response_update(res);
                  console.log(res);
              }
          });
        });

        function response_update(res) {
          var data_json = JSON.parse(res);
          var main_msg = data_json.msg_main;
          var detail_msg = data_json.msg_detail;
          if (main_msg.status) {
              create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/keuangan");?>");
          } else {

            
              $("#msg_q_harta_tidak_bergerak").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_htb_detail").html(detail_msg.q_pendapatan_perbulan);

              $("#msg_q_harta_bergerak").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_hb_detail").html(detail_msg.q_pendapatan_perbulan);
              
              $("#msg_q_deposito").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_q_piutang").html(detail_msg.q_pendapatan_perbulan);
              
              $("#msg_q_hutang").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_ht_nominal").html(detail_msg.q_pendapatan_perbulan);
              
              $("#msg_t_kekayaan").html(detail_msg.q_pendapatan_perbulan);
              
              $("#msg_q_penghasilan_per_th").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_q_penghasilan_profesi").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_q_penghasilan_usaha").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_q_penghasilan_hibah").html(detail_msg.q_pendapatan_perbulan);
              
              $("#msg_t_penghasilan").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_t_pengeluaran_rutin").html(detail_msg.q_pendapatan_perbulan);
              $("#msg_t_pengeluaran_lainnya").html(detail_msg.q_pendapatan_perbulan);
              


              create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
          }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------show_hide-----------------------------//
    //=========================================================================//
        function show_htb_detail(){
          var q_harta_tidak_bergerak = $('input[name="q_harta_tidak_bergerak"]:checked').val()
          if(q_harta_tidak_bergerak == "iya"){
            $("#path_htb_detail").fadeIn()
          }else{
            $("#path_htb_detail").fadeOut()
          }
        }

        function show_hb_detail(){
          var q_harta_bergerak = $('input[name="q_harta_bergerak"]:checked').val()
          if(q_harta_bergerak == "iya"){
            $("#path_hb_detail").fadeIn()
          }else{
            $("#path_hb_detail").fadeOut()
          }
        }

        function show_t_hutang(){
          var q_hutang = $('input[name="q_hutang"]:checked').val()
          if(q_hutang == "iya"){
            $("#path_ht_nominal").fadeIn()
          }else{
            $("#path_ht_nominal").fadeOut()
          }
        }
    //=========================================================================//
    //-----------------------------------show_hide-----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_htb_detail---------------------------//
    //=========================================================================//
        $("#add_htb_detail").click(function(){
          add_htb_detail();
        });

        function add_htb_detail(){
            var tmp = {
                "jn_htb_detail":$("#jn_htb_detail").val(),
                "n_htb_detail":$("#n_htb_detail").val()
            };

            if($("#jn_htb_detail").val() != "" &&
                $("#n_htb_detail").val() != ""){
                    var key = $("#jn_htb_detail").val().replaceAll(" ", "");
                    if(!(key in list_htb_detail)){
                        list_htb_detail[$("#jn_htb_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_htb_detail();
                    }else{
                        alert("Data "+$("#jn_htb_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_htb_detail(){
            var str_list_htb_detail = "";
            n_t_htb_detail = 0;
            if(list_htb_detail){
                var no = 1;
                for (const i in list_htb_detail) {
                    str_list_htb_detail += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_htb_detail[i].jn_htb_detail+"</td>"+
                                        "<td>"+convert_cur(list_htb_detail[i].n_htb_detail)+"</td>"+
                                        "<td align=\"center\">"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_htb_detail('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    n_t_htb_detail += parseInt(list_htb_detail[i].n_htb_detail)
                    no++;
                }
            }

            $("#n_t_htb_detail").html(convert_cur(n_t_htb_detail));
            $("#out_list_htb_detail").html(str_list_htb_detail);
            count_kekayaan()
        }

        $("#add_data_vaksin").click(function(){
            add_htb_detail();

            // console.log(list_htb_detail);
        });


        function delete_list_htb_detail(key){
            // console.log(list_htb_detail[key]);
            delete list_htb_detail[key];

            render_list_htb_detail();
        }
    //=========================================================================//
    //-----------------------------------add_htb_detail---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_hb_detail---------------------------//
    //=========================================================================//
        $("#add_hb_detail").click(function(){
          add_hb_detail();
        });

        function add_hb_detail(){
            var tmp = {
                "jn_hb_detail":$("#jn_hb_detail").val(),
                "n_hb_detail":$("#n_hb_detail").val()
            };

            if($("#jn_hb_detail").val() != "" &&
                $("#n_hb_detail").val() != ""){
                    var key = $("#jn_hb_detail").val().replaceAll(" ", "");
                    if(!(key in list_hb_detail)){
                        list_hb_detail[$("#jn_hb_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_hb_detail();
                    }else{
                        alert("Data "+$("#jn_hb_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_hb_detail(){
            var str_list_hb_detail = "";
            n_t_hb_detail = 0;
            if(list_hb_detail){
                var no = 1;
                for (const i in list_hb_detail) {
                    str_list_hb_detail += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_hb_detail[i].jn_hb_detail+"</td>"+
                                        "<td>"+convert_cur(list_hb_detail[i].n_hb_detail)+"</td>"+
                                        "<td align=\"center\">"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_hb_detail('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    n_t_hb_detail += parseInt(list_hb_detail[i].n_hb_detail)
                    no++;
                }
            }
            
            $("#n_t_hb_detail").html(convert_cur(n_t_hb_detail));
            $("#out_list_hb_detail").html(str_list_hb_detail);
            count_kekayaan()
        }

        $("#add_data_vaksin").click(function(){
            add_hb_detail();

            // console.log(list_hb_detail);
        });


        function delete_list_hb_detail(key){
            // console.log(list_hb_detail[key]);
            delete list_hb_detail[key];

            render_list_hb_detail();
        }
    //=========================================================================//
    //-----------------------------------add_hb_detail---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------ht_nominal----------------------------//
    //=========================================================================//
        $("#ht_nominal").keyup(function(){
          count_kekayaan();
        });
    //=========================================================================//
    //-----------------------------------ht_nominal----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------t_kekayaan----------------------------//
    //=========================================================================//
        $("#t_kekayaan").keyup(function(){
          count_kekayaan();
        });
    //=========================================================================//
    //-----------------------------------t_kekayaan----------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------count_kekayaan------------------------//
    //=========================================================================//
        function count_kekayaan(){
          var ht_nominal = 0;
          if($("#ht_nominal").val()){
            ht_nominal = parseInt($("#ht_nominal").val())
          }

          var t_kekayaan = 0;
          if($("#t_kekayaan").val()){
            t_kekayaan = parseInt($("#t_kekayaan").val())
          }

          var t_kekayaan_all = n_t_htb_detail + n_t_hb_detail + t_kekayaan - ht_nominal;
          $("#t_kekayaan_all").val(convert_cur(t_kekayaan_all));
        }
    //=========================================================================//
    //-----------------------------------count_kekayaan------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------t_penghasilan-------------------------//
    //=========================================================================//
        $("#t_penghasilan").keyup(function(){
          count_penghasilan();
        });
    //=========================================================================//
    //-----------------------------------t_penghasilan-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------t_pengeluaran_rutin-------------------//
    //=========================================================================//
        $("#t_pengeluaran_rutin").keyup(function(){
          count_penghasilan();
        });
    //=========================================================================//
    //-----------------------------------t_pengeluaran_rutin-------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------t_pengeluaran_lainnya-----------------//
    //=========================================================================//
        $("#t_pengeluaran_lainnya").keyup(function(){
          count_penghasilan();
        });
    //=========================================================================//
    //-----------------------------------t_pengeluaran_lainnya-----------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------count_penghasilan---------------------//
    //=========================================================================//
        function count_penghasilan(){
          var t_penghasilan = 0;
          if($("#t_penghasilan").val()){
            t_penghasilan = parseInt($("#t_penghasilan").val())
          }

          var t_pengeluaran_rutin = 0;
          if($("#t_pengeluaran_rutin").val()){
            t_pengeluaran_rutin = parseInt($("#t_pengeluaran_rutin").val())
          }

          var t_pengeluaran_lainnya = 0;
          if($("#t_pengeluaran_lainnya").val()){
            t_pengeluaran_lainnya = parseInt($("#t_pengeluaran_lainnya").val())
          }

          var t_pengeluaran = (t_pengeluaran_rutin + t_pengeluaran_lainnya);
          $("#t_pengeluaran").val(convert_cur(t_pengeluaran));

          var t_penghasilan_bersih = t_penghasilan - (t_pengeluaran_rutin + t_pengeluaran_lainnya);
          $("#t_penghasilan_bersih").val(convert_cur(t_penghasilan_bersih));
        }
    //=========================================================================//
    //-----------------------------------count_penghasilan---------------------//
    //=========================================================================//


</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->
