<?php
    $main_controller = base_url()."user/komunikasi/";

    $path_img = base_url()."assets/prw/img/icon/";

    // var_dump($list_data);
    
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-md-12">
            <p class="note"><label for="inp_catatan">Catatan :</label><br><br> 
            <b>1.</b> Berikan tanda strip(-) jika tidak ada <br>
            <b>2.</b> Lama bekerja /bulan <br>
            </p>
        </div>
    </div> -->
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Akses Internet </label><br>
                        <input class="radio" type="radio" name="q_akses_int" id="ya" value="ya">
                        <label for="ya">Ya</label>
                        <input class="radio" type="radio" name="q_akses_int" id="tidak" value="tidak">
                        <label for="tidak">Tidak</label>
                        <p id="msg_q_akses_int" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Internet Jenis </label><br>
                        <input class="radio" type="radio" name="int_jenis" id="internetmandiri" value="internet mandiri">
                        <label for="internetmandiri">Internet mandiri</label>
                        <input class="radio" type="radio" name="int_jenis" id="internetpemerintah" value="internet yang disediakan pemerintah">
                        <label for="internetpemerintah">Internet disediakan Pemerintah</label>
                        <input class="radio" type="radio" name="int_jenis" id="internetwarga" value="internet hasil usaha warga sekitar">
                        <label for="internetwarga">Internet hasil usaha warga sekitar</label>
                        <p id="msg_int_jenis" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Jumlah Perangkat :</label>
                        <input type="text" class="form-control" id="jml_perangkat" name="jml_perangkat" >
                        <p id="msg_jml_perangkat" style="color: red;"></p>
                    </div>
                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                </div>

                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("input[name='q_akses_int'][value='<?=isset($list_data['q_akses_int']) ? $list_data['q_akses_int'] : ''?>']").prop("checked",true);
        $("input[name='int_jenis'][value='<?=isset($list_data['int_jenis']) ? $list_data['int_jenis'] : ''?>']").prop("checked",true);
        $("#jml_perangkat").val("<?=isset($list_data['jml_perangkat']) ? $list_data['jml_perangkat'] : ''?>")
    })
       
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
    $("#update").click(function() {
            var data_main = new FormData();
            data_main.append('q_akses_int', $('input[name="q_akses_int"]:checked').val() ? $('input[name="q_akses_int"]:checked').val() : "" );
            data_main.append('int_jenis', $('input[name="int_jenis"]:checked').val() ? $('input[name="int_jenis"]:checked').val() : "" );
            data_main.append('jml_perangkat', $("#jml_perangkat").val());

            $.ajax({
                url: "<?php echo base_url()."user/komunikasiuser/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/komunikasi");?>");
            } else {
                $("#msg_q_akses_int").html(detail_msg.q_akses_int);
                $("#msg_int_jenis").html(detail_msg.int_jenis);
                $("#msg_jml_perangkat").html(detail_msg.jml_perangkat);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->