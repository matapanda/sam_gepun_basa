<?php
    $main_url_user = base_url()."user/lingkungan";
    $main_controller = base_url()."user/tmptuser/";

    $str_op_sts_rumah = "";
    if(isset($sts_rumah)){
        if($sts_rumah){
            foreach ($sts_rumah as $key => $value) {
                $str_op_sts_rumah .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_kondisi_rumah = "";
    if(isset($kondisi_rumah)){
        if($kondisi_rumah){
            foreach ($kondisi_rumah as $key => $value) {
                $str_op_kondisi_rumah .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_lantai = "";
    if(isset($jn_lantai)){
        if($jn_lantai){
            foreach ($jn_lantai as $key => $value) {
                $str_op_jn_lantai .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_kriteria_rumah = "";
    if(isset($kriteria_rumah)){
        if($kriteria_rumah){
            foreach ($kriteria_rumah as $key => $value) {
                $str_op_kriteria_rumah .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_listrik = "";
    if(isset($jn_listrik)){
        if($jn_listrik){
            foreach ($jn_listrik as $key => $value) {
                $str_op_jn_listrik .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jn_sanitasi = "";
    if(isset($jn_sanitasi)){
        if($jn_sanitasi){
            foreach ($jn_sanitasi as $key => $value) {
                $str_op_jn_sanitasi .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_sumber_air = "";
    if(isset($sumber_air)){
        if($sumber_air){
            foreach ($sumber_air as $key => $value) {
                $str_op_sumber_air .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $q_punya_rumah = "";
    $py_rmh_sts = "";
    $py_rmh_kondisi = "";
    $py_rmh_jn_lantai = "";
    $py_rmh_kriteria_rmh = "";
    $q_ada_listrik = "";
    $lstr_jenis = "";
    $q_ada_sanitasi = "";
    $sn_jenis = "";
    $q_saluran_limbah = "";
    $q_air_bersih = "";
    $ab_sumber_air = "";
    $q_pembuangan_sampah = "";

    if(isset($list_data)){
        if($list_data){
          
            $q_punya_rumah = $list_data["q_punya_rumah"];
            $py_rmh_sts = $list_data["py_rmh_sts"];
            $py_rmh_kondisi = $list_data["py_rmh_kondisi"];
            $py_rmh_jn_lantai = $list_data["py_rmh_jn_lantai"];
            $py_rmh_kriteria_rmh = $list_data["py_rmh_kriteria_rmh"];
            $q_ada_listrik = $list_data["q_ada_listrik"];
            $lstr_jenis = $list_data["lstr_jenis"];
            $q_ada_sanitasi = $list_data["q_ada_sanitasi"];
            $sn_jenis = $list_data["sn_jenis"];
            $q_saluran_limbah = $list_data["q_saluran_limbah"];
            $q_air_bersih = $list_data["q_air_bersih"];
            $ab_sumber_air = $list_data["ab_sumber_air"];
            $q_pembuangan_sampah = $list_data["q_pembuangan_sampah"];
        }
    }
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-md-12">
            <p class="note"><label for="inp_catatan">Catatan :</label><br><br> 
            <b>1.</b> Berikan tanda strip(-) jika tidak ada <br>
            <b>2.</b> Lama bekerja /bulan <br>
            </p>
        </div>
    </div> -->
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>

                <div class="card-body collapse show">
                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Punya Tempat Tinggal</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_punya_rumah" value="iya" id="q_punya_rumah1">
                                    <label for="q_punya_rumah1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_punya_rumah" value="tidak" id="q_punya_rumah2">
                                    <label for="q_punya_rumah2">Tidak</label>
                                    <p id="msg_q_punya_rumah" style="color: red;"></p>
                                </div>
                        </div>
                    </div>

                <div class="form-group row">
                    <label for="message-text" class="control-label col-md-4">Status Rumah</label>
                        <div class="col-md-4">
                            <select class="custom-select form-control input-sm m-b-10" id="py_rmh_sts" name="py_rmh_sts">
                                <option value="" selected>Pilih</option>
                                <?=$str_op_sts_rumah?>
                            </select>
                            <p id="msg_py_rmh_sts" style="color: red;"></p>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Kondisi Bangunan</label>
                            <div class="col-md-4">
                                <select class="custom-select form-control input-sm m-b-10" id="py_rmh_kondisi" name="py_rmh_kondisi">
                                    <option value="" selected>Pilih</option>
                                    <?=$str_op_kondisi_rumah?>
                                </select>
                                <p id="msg_py_rmh_kondisi" style="color: red;"></p>
                            </div>
                        </div>
                </div>

                <div class="form-group row">
                    <label for="message-text" class="control-label col-md-4">Apakah Jenis Lantai Terluas Rumah?</label>
                        <div class="col-md-4">
                            <select class="custom-select form-control input-sm m-b-10" id="py_rmh_jn_lantai" name="py_rmh_jn_lantai">
                                <option value="" selected>Pilih</option>
                                <?=$str_op_jn_lantai?>
                            </select>
                            <p id="msg_py_rmh_jn_lantai" style="color: red;"></p>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Kriteria Rumah</label>
                            <div class="col-md-4">
                                <select class="custom-select form-control input-sm m-b-10" id="py_rmh_kriteria_rmh" name="py_rmh_kriteria_rmh">
                                    <option value="" selected>Pilih</option>
                                    <?=$str_op_kriteria_rumah?>
                                </select>
                                <p id="msg_py_rmh_kriteria_rmh" style="color: red;"></p>
                            </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Tempat Tinggal Menggunakan Listrik</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_ada_listrik" value="iya" id="q_ada_listrik1">
                                    <label for="q_ada_listrik1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_ada_listrik" value="tidak" id="q_ada_listrik2">
                                    <label for="q_ada_listrik2">Tidak</label>
                                    <p id="msg_q_ada_listrik" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group row">
                    <label for="message-text" class="control-label col-md-4">Jenis Listrik</label>
                        <div class="col-md-4">
                            <select class="custom-select form-control input-sm m-b-10" id="lstr_jenis" name="lstr_jenis">
                                <option value="" selected>Pilih</option>
                                <?=$str_op_jn_listrik?>
                            </select>
                            <p id="msg_lstr_jenis" style="color: red;"></p>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Ada Sanitasi(MCK)</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_ada_sanitasi" value="iya" id="q_ada_sanitasi1">
                                    <label for="q_ada_sanitasi1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_ada_sanitasi" value="tidak" id="q_ada_sanitasi2">
                                    <label for="q_ada_sanitasi2">Tidak</label>
                                    <p id="msg_q_ada_sanitasi" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Jenis Sanitasi</label>
                            <div class="col-md-4">
                                <select class="custom-select form-control input-sm m-b-10" id="sn_jenis" name="sn_jenis">
                                    <option value="" selected>Pilih</option>
                                    <?=$str_op_jn_sanitasi?>
                                </select>
                                <p id="msg_sn_jenis" style="color: red;"></p>
                            </div>
                        </div>
                </div>

             

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Ketersediaan Air Bersih</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_air_bersih" value="iya" id="q_air_bersih1">
                                    <label for="q_air_bersih1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_air_bersih" value="tidak" id="q_air_bersih2">
                                    <label for="q_air_bersih2">Tidak</label>
                                    <p id="msg_q_air_bersih" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Sumber Air Keluarga</label>
                            <div class="col-md-4">
                                <select class="custom-select form-control input-sm m-b-10" id="ab_sumber_air" name="ab_sumber_air">
                                    <option value="" selected>Pilih</option>
                                    <?=$str_op_sumber_air?>
                                </select>
                                <p id="msg_ab_sumber_air" style="color: red;"></p>
                            </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>
  <div class="card">
                <div class="card-body">
                     <h4>Persampahan dan Limbah</h4>
                      <h6 class="card-subtitle">Perangkat Daerah Terkait:  Dinas Lingkungan Hidup</h6>
                    <hr>
                   <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Memiliki Saluran Pembuangan Air Limbah</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_saluran_limbah" value="iya" id="q_saluran_limbah1">
                                    <label for="q_saluran_limbah1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_saluran_limbah" value="tidak" id="q_saluran_limbah2">
                                    <label for="q_saluran_limbah2">Tidak</label>
                                    <p id="msg_q_saluran_limbah" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Memiliki Tempat Pembuangan Sampah</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_pembuangan_sampah" value="iya" id="q_pembuangan_sampah1">
                                    <label for="q_pembuangan_sampah1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_pembuangan_sampah" value="tidak" id="q_pembuangan_sampah2">
                                    <label for="q_pembuangan_sampah2">Tidak</label>
                                    <p id="msg_q_pembuangan_sampah" style="color: red;"></p>
                                </div>
                        </div>
                </div>

              </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                </div>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
        $(document).ready(function(){
            set_param();
        })
       

        function set_param(){
            $("input[name='q_punya_rumah'][value='<?=isset($q_punya_rumah) ? $q_punya_rumah : 'iya'?>']").prop("checked",true);
            
            $("#py_rmh_sts").val("<?=$py_rmh_sts?$py_rmh_sts:""?>");
            $("#py_rmh_kondisi").val("<?=$py_rmh_kondisi?$py_rmh_kondisi:""?>");
            $("#py_rmh_jn_lantai").val("<?=$py_rmh_jn_lantai?$py_rmh_jn_lantai:""?>");
            $("#py_rmh_kriteria_rmh").val("<?=$py_rmh_kriteria_rmh?$py_rmh_kriteria_rmh:""?>");

            
            $("input[name='q_ada_listrik'][value='<?=isset($q_ada_listrik) ? $q_ada_listrik : 'iya'?>']").prop("checked",true);
            $("#lstr_jenis").val("<?=$lstr_jenis?$lstr_jenis:""?>");
            
            $("input[name='q_ada_sanitasi'][value='<?=isset($q_ada_sanitasi) ? $q_ada_sanitasi : 'iya'?>']").prop("checked",true);
            $("#sn_jenis").val("<?=$sn_jenis?$sn_jenis:""?>");
            
            $("input[name='q_saluran_limbah'][value='<?=isset($q_saluran_limbah) ? $q_saluran_limbah : 'iya'?>']").prop("checked",true);
            
            $("input[name='q_air_bersih'][value='<?=isset($q_air_bersih) ? $q_air_bersih : 'iya'?>']").prop("checked",true);
            $("#ab_sumber_air").val("<?=$ab_sumber_air?$ab_sumber_air:""?>");
            
            $("input[name='q_pembuangan_sampah'][value='<?=isset($q_pembuangan_sampah) ? $q_pembuangan_sampah : 'iya'?>']").prop("checked",true);
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#update").click(function() {
            var data_main = new FormData();

            data_main.append('q_punya_rumah', $('input[name="q_punya_rumah"]:checked').val() ? $('input[name="q_punya_rumah"]:checked').val() : "");
            data_main.append('py_rmh_sts', $("#py_rmh_sts").val());
            data_main.append('py_rmh_kondisi', $("#py_rmh_kondisi").val());
            data_main.append('py_rmh_jn_lantai',$("#py_rmh_jn_lantai").val());
            data_main.append('py_rmh_kriteria_rmh',$("#py_rmh_kriteria_rmh").val());

            data_main.append('q_ada_listrik', $('input[name="q_ada_listrik"]:checked').val() ? $('input[name="q_ada_listrik"]:checked').val() : "");
            data_main.append('lstr_jenis', $("#lstr_jenis").val());
            data_main.append('q_ada_sanitasi', $('input[name="q_ada_sanitasi"]:checked').val() ? $('input[name="q_ada_sanitasi"]:checked').val() : "");
            data_main.append('sn_jenis', $("#sn_jenis").val());
            data_main.append('q_saluran_limbah', $('input[name="q_saluran_limbah"]:checked').val() ? $('input[name="q_saluran_limbah"]:checked').val() : "");
            data_main.append('q_air_bersih', $('input[name="q_air_bersih"]:checked').val() ? $('input[name="q_air_bersih"]:checked').val() : "");
            data_main.append('ab_sumber_air', $("#ab_sumber_air").val());

            data_main.append('q_pembuangan_sampah', $('input[name="q_pembuangan_sampah"]:checked').val() ? $('input[name="q_pembuangan_sampah"]:checked').val() : "");

            $.ajax({
                url: "<?=$main_controller?>act_controller",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/lingkungan");?>");
            } else {

                $("#msg_q_punya_rumah").html(detail_msg.q_punya_rumah);
                $("#msg_py_rmh_sts").html(detail_msg.py_rmh_sts);
                $("#msg_py_rmh_kondisi").html(detail_msg.py_rmh_kondisi);
                $("#msg_py_rmh_jn_lantai").html(detail_msg.py_rmh_jn_lantai);
                $("#msg_py_rmh_kriteria_rmh").html(detail_msg.py_rmh_kriteria_rmh);
                $("#msg_q_ada_listrik").html(detail_msg.q_ada_listrik);
                $("#msg_lstr_jenis").html(detail_msg.lstr_jenis);
                $("#msg_q_ada_sanitasi").html(detail_msg.q_ada_sanitasi);
                $("#msg_sn_jenis").html(detail_msg.sn_jenis);
                $("#msg_q_saluran_limbah").html(detail_msg.q_saluran_limbah);
                $("#msg_q_air_bersih").html(detail_msg.q_air_bersih);
                $("#msg_ab_sumber_air").html(detail_msg.ab_sumber_air);
                $("#msg_q_pembuangan_sampah").html(detail_msg.q_pembuangan_sampah);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->