<?php
    $main_url_user = base_url()."user/kepemudaan";
    $main_controller = base_url()."user/oruser/";

    $str_op_jenis_sertifikat = "";
    if(isset($jenis_sertifikat)){
        if($jenis_sertifikat){
            foreach ($jenis_sertifikat as $key => $value) {
                $str_op_jenis_sertifikat .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $or_hobby = "";
    $q_keg_pengamalan_pancasila = "";
    $pc_detail = "";
    $q_kerjabakti = "";
    $q_rukun_kematian = "";
    $q_keg_keagamaan = "";
    $q_jimpitan = "";
    $q_arisan = "";
    $q_gotong_royong = "";
    $q_ikut_organisasi = "";
    $or_detail = "";
    $q_ikut_pelatihan = "";
    $pel_detail = "";

    if(isset($list_data)){
        if($list_data){
          
          $or_hobby = $list_data["or_hobby"];
          $q_keg_pengamalan_pancasila = $list_data["q_keg_pengamalan_pancasila"];
          $pc_detail = $list_data["pc_detail"];
          $q_kerjabakti = $list_data["q_kerjabakti"];
          $q_rukun_kematian = $list_data["q_rukun_kematian"];
          $q_keg_keagamaan = $list_data["q_keg_keagamaan"];
          $q_jimpitan = $list_data["q_jimpitan"];
          $q_arisan = $list_data["q_arisan"];
          $q_gotong_royong = $list_data["q_gotong_royong"];
          $q_ikut_organisasi = $list_data["q_ikut_organisasi"];
          $or_detail = $list_data["or_detail"];
          $q_ikut_pelatihan = $list_data["q_ikut_pelatihan"];
          $pel_detail = $list_data["pel_detail"];
        }
    }

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                     <div class="form-group row">
                        <label for="message-text" class="control-label col-md-4">Hobi</label>
                        <div class="col-md-4">
                          <input type="text" class="form-control" id="or_hobby" name="or_hobby" placeholder="">
                          <p id="msg_or_hobby" style="color: red;"></p>
                        </div>
                    </div>

                <div class="form-group" style="padding-bottom: 0px">
                    <div class="form-group row">
                      <label for="message-text" class="control-label col-md-4">Kegiatan Penghayatan dan Pengamalan Pancasila</label>
                      <div class="col-md-8">
                        <input class="q_anggota" type="radio" name="q_keg_pengamalan_pancasila" value="iya" id="q_keg_pengamalan_pancasila1" onclick="show_keg_pancasila()">
                        <label for="q_keg_pengamalan_pancasila1">Iya</label>
                        <input class="q_anggota" type="radio" name="q_keg_pengamalan_pancasila" value="tidak" id="q_keg_pengamalan_pancasila2" onclick="show_keg_pancasila()">
                        <label for="q_keg_pengamalan_pancasila2">Tidak</label>
                        <p id="msg_q_keg_pengamalan_pancasila" style="color: red;"></p>
                      </div>

                      <div id="path_keg_pengamalan_pancasila" class="ya box col-md-7" style="padding-top: 1px; display: none ">
                        <table class="table" width="100%">
                          <thead>
                            <tr>
                              <th width="5%">No</th>
                              <th width="*">Nama Kegiatan</th>
                              <th width="15%">Aksi</th>
                            </tr>
                            <tr>
                              <td>
                                #
                              </td>
                              <td>
                                <input type="text" class="form-control" id="nm_keg_pancasila" name="nm_keg_pancasila" required="">
                                <p id="msg_nm_keg_pancasila" style="color: red;"></p>
                              </td>
                              <td>
                                <button type="button" id="add_keg_pancasila" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                              </td>
                            </tr>
                          </thead>
                          <tbody id="out_list_keg_pancasila"></tbody>
                        </table>
                      </div>
                    </div>
                  </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Kerja Bakti</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_kerjabakti" value="iya" id="q_kerjabakti1">
                                    <label for="q_kerjabakti1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_kerjabakti" value="tidak" id="q_kerjabakti2">
                                    <label for="q_kerjabakti2">Tidak</label>
                                    <p id="msg_q_kerjabakti" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Rukun Kematian</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_rukun_kematian" value="iya" id="q_rukun_kematian1">
                                    <label for="q_rukun_kematian1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_rukun_kematian" value="tidak" id="q_rukun_kematian2">
                                    <label for="q_rukun_kematian2">Tidak</label>
                                    <p id="msg_q_rukun_kematian" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Kegiatan Keagamaan</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_keg_keagamaan" value="iya" id="q_keg_keagamaan1">
                                    <label for="q_keg_keagamaan1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_keg_keagamaan" value="tidak" id="q_keg_keagamaan2">
                                    <label for="q_keg_keagamaan2">Tidak</label>
                                    <p id="msg_q_keg_keagamaan" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Jimpitan</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_jimpitan" value="iya" id="q_jimpitan1">
                                    <label for="q_jimpitan1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_jimpitan" value="tidak" id="q_jimpitan2">
                                    <label for="q_jimpitan2">Tidak</label>
                                    <p id="msg_q_jimpitan" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Arisan</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_arisan" value="iya" id="q_arisan1">
                                    <label for="q_arisan1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_arisan" value="tidak" id="q_arisan2">
                                    <label for="q_arisan2">Tidak</label>
                                    <p id="msg_q_arisan" style="color: red;"></p>
                                </div>
                        </div>
                </div>

                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Gotong Royong</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_gotong_royong" value="iya" id="q_gotong_royong1">
                                    <label for="q_gotong_royong1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_gotong_royong" value="tidak" id="q_gotong_royong2">
                                    <label for="q_gotong_royong2">Tidak</label>
                                    <p id="msg_q_gotong_royong" style="color: red;"></p>
                                </div>
                        </div>
                </div>
</div>
</div>
<div class="card">
                <div class="card-body">
                <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                          <label for="message-text" class="control-label col-md-4">Apakah Anda pernah mengikuti Organisasi?</label>
                          <div class="col-md-8">
                            <input class="q_anggota" type="radio" name="q_ikut_organisasi" value="iya" id="q_ikut_organisasi1" onclick="show_ikut_organisasi()">
                            <label for="q_ikut_organisasi1">Iya</label>
                            <input class="q_anggota" type="radio" name="q_ikut_organisasi" value="tidak" id="q_ikut_organisasi2" onclick="show_ikut_organisasi()">
                            <label for="q_ikut_organisasi2">Tidak</label>
                            <p id="msg_q_ikut_organisasi" style="color: red;"></p>
                          </div>
                          <div id="path_ikut_organisasi" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th width="5%">No</th>
                                    <th width="25%">Nama Organisasi</th>
                                    <th width="30%">Jenis Organisasi</th>
                                    <th width="25%">Jabatan</th>
                                    <th width="25%">Tahun</th>
                                    <th width="15%">Aksi</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      #
                                      <!-- <input type="number" class="form-control" id="no_organisasi_detail" name="organisasi_detail" required="">
                                      <p id="msg_organisasi_detail" style="color: red;"></p> -->
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="nm_organisasi_detail" name="nm_organisasi_detail" required="">
                                      <p id="msg_nm_organisasi_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control" id="jns_organisasi_detail" name="jns_organisasi_detail" required="">
                                      <p id="msg_jns_organisasi_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="jbtn_organisasi_detail" name="jbtn_organisasi_detail" required="">
                                      <p id="msg_jbtn_organisasi_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" id="thn_organisasi_detail" name="thn_organisasi_detail" required="">
                                      <p id="msg_thn_organisasi_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <button type="button" id="add_ikut_organisasi" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                    </td>
                                  </tr>
                                </thead>
                                <tbody id="out_list_ikut_organisasi"></tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                          <label for="message-text" class="control-label col-md-4">Apakah Anda pernah mengikuti Pelatihan?</label>
                          <div class="col-md-8">
                            <input class="q_anggota" type="radio" name="q_ikut_pelatihan" value="iya" id="q_ikut_pelatihan1" onclick="show_ikut_pelatihan()">
                            <label for="q_ikut_pelatihan1">Iya</label>
                            <input class="q_anggota" type="radio" name="q_ikut_pelatihan" value="tidak" id="q_ikut_pelatihan2" onclick="show_ikut_pelatihan()">
                            <label for="q_ikut_pelatihan2">Tidak</label>
                            <p id="msg_q_ikut_pelatihan" style="color: red;"></p>
                          </div>
                          <div id="path_ikut_pelatihan" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th width="5%">No</th>
                                    <th width="25%">Nama Pelatihan</th>
                                    <th width="30%">Penyelenggara</th>
                                    <th width="25%">Apakah Bersertifikat?</th>
                                    <th width="25%">Tahun</th>
                                    <th width="15%">Aksi</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      #
                                    </td>
                                    <td>
                                        <input type="text" class="form-control" id="nm_pelatihan_detail" name="nm_pelatihan_detail" required="">
                                      <p id="msg_nm_pelatihan_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <input type="text" class="form-control" id="penyelenggara_pelatihan_detail" name="penyelenggara_pelatihan_detail" required="">
                                      <p id="msg_penyelenggara_pelatihan_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                        <select class="custom-select form-control input-sm m-b-10" id="jn_str" name="jn_str">
                                            <option value="" selected>Pilih</option>
                                            <?=$str_op_jenis_sertifikat?>
                                        </select>
                                      <p id="msg_jn_str" style="color: red;"></p>
                                    </td>
                                    <td>
                                        <input type="number" class="form-control" id="thn_pelatihan_detail" name="thn_pelatihan_detail" required="">
                                      <p id="msg_thn_pelatihan_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <button type="button" id="add_ikut_pelatihan" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                    </td>
                                  </tr>
                                </thead>
                                <tbody id="out_list_ikut_pelatihan"></tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>


                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="button" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan Data</button>
                </div>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var list_keg_pancasila = {};
    try {
      list_keg_pancasila = JSON.parse('<?=$pc_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    var list_or_detail = {};
    try {
      list_or_detail = JSON.parse('<?=$or_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    var list_pel_detail = {};
    try {
      list_pel_detail = JSON.parse('<?=$pel_detail?>')
    }catch(err) {
        console.log("err json format");
    }
    
    $(document).ready(function(){
        set_param();
        // q_or_change();
    });

    //=========================================================================//
    //-----------------------------------custom--------------------------------//
    //=========================================================================//
      function set_param(){
        $("#or_hobby").val("<?=$or_hobby?>");

        $("input[name='q_keg_pengamalan_pancasila'][value='<?=isset($q_keg_pengamalan_pancasila) ? $q_keg_pengamalan_pancasila : 'iya'?>']").prop("checked",true);
        $("input[name='q_kerjabakti'][value='<?=isset($q_kerjabakti) ? $q_kerjabakti : 'iya'?>']").prop("checked",true);
        $("input[name='q_rukun_kematian'][value='<?=isset($q_rukun_kematian) ? $q_rukun_kematian : 'iya'?>']").prop("checked",true);
        $("input[name='q_keg_keagamaan'][value='<?=isset($q_keg_keagamaan) ? $q_keg_keagamaan : 'iya'?>']").prop("checked",true);
        $("input[name='q_jimpitan'][value='<?=isset($q_jimpitan) ? $q_jimpitan : 'iya'?>']").prop("checked",true);
        $("input[name='q_arisan'][value='<?=isset($q_arisan) ? $q_arisan : 'iya'?>']").prop("checked",true);
        $("input[name='q_gotong_royong'][value='<?=isset($q_gotong_royong) ? $q_gotong_royong : 'iya'?>']").prop("checked",true);

        $("input[name='q_ikut_organisasi'][value='<?=isset($q_ikut_organisasi) ? $q_ikut_organisasi : 'iya'?>']").prop("checked",true);
        $("input[name='q_ikut_pelatihan'][value='<?=isset($q_ikut_pelatihan) ? $q_ikut_pelatihan : 'iya'?>']").prop("checked",true);


        show_ikut_organisasi()
        show_ikut_pelatihan()
        show_keg_pancasila()

        render_list_keg_pancasila();
        render_list_or_detail();
        render_list_pel_detail();
      }
    //=========================================================================//
    //-----------------------------------custom--------------------------------//
    //=========================================================================//


    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_data").click(function() {
            var data_main = new FormData();
          
            data_main.append('or_hobby'    , $("#or_hobby").val());
            
            data_main.append('q_keg_pengamalan_pancasila'    , $('input[name="q_keg_pengamalan_pancasila"]:checked').val());
            data_main.append('pc_detail'    , JSON.stringify(list_keg_pancasila));
            
            data_main.append('q_kerjabakti'    , $('input[name="q_kerjabakti"]:checked').val());
            data_main.append('q_rukun_kematian'    , $('input[name="q_rukun_kematian"]:checked').val());
            data_main.append('q_keg_keagamaan'    , $('input[name="q_keg_keagamaan"]:checked').val());
            data_main.append('q_jimpitan'    , $('input[name="q_jimpitan"]:checked').val());
            data_main.append('q_arisan'    , $('input[name="q_arisan"]:checked').val());
            data_main.append('q_gotong_royong'    , $('input[name="q_gotong_royong"]:checked').val());
            
            data_main.append('q_ikut_organisasi'    , $('input[name="q_ikut_organisasi"]:checked').val());
            data_main.append('or_detail'    , JSON.stringify(list_or_detail));
            
            data_main.append('q_ikut_pelatihan'    , $('input[name="q_ikut_pelatihan"]:checked').val());
            data_main.append('pel_detail'    , JSON.stringify(list_pel_detail));
            
            $.ajax({
                url: "<?=$main_controller?>act_controller",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                    
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                $("msg_or_hobby").html(detail_msg.or_hobby);
                $("msg_q_keg_pengamalan_pancasila").html(detail_msg.q_keg_pengamalan_pancasila);
                $("msg_pc_detail").html(detail_msg.pc_detail);
                $("msg_q_kerjabakti").html(detail_msg.q_kerjabakti);
                $("msg_q_rukun_kematian").html(detail_msg.q_rukun_kematian);
                $("msg_q_keg_keagamaan").html(detail_msg.q_keg_keagamaan);
                $("msg_q_jimpitan").html(detail_msg.q_jimpitan);
                $("msg_q_arisan").html(detail_msg.q_arisan);
                $("msg_q_gotong_royong").html(detail_msg.q_gotong_royong);
                $("msg_q_ikut_organisasi").html(detail_msg.q_ikut_organisasi);
                $("msg_or_detail").html(detail_msg.or_detail);
                $("msg_q_ikut_pelatihan").html(detail_msg.q_ikut_pelatihan);
                $("msg_pel_detail").html(detail_msg.pel_detail);

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
               
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
          $("or_hobby").val("");
          $("q_keg_pengamalan_pancasila").val("");
          $("pc_detail").val("");
          $("q_kerjabakti").val("");
          $("q_rukun_kematian").val("");
          $("q_keg_keagamaan").val("");
          $("q_jimpitan").val("");
          $("q_arisan").val("");
          $("q_gotong_royong").val("");
          $("q_ikut_organisasi").val("");
          $("or_detail").val("");
          $("q_ikut_pelatihan").val("");
          $("pel_detail").val("");
          
          $("msg_or_hobby").html("");
          $("msg_q_keg_pengamalan_pancasila").html("");
          $("msg_pc_detail").html("");
          $("msg_q_kerjabakti").html("");
          $("msg_q_rukun_kematian").html("");
          $("msg_q_keg_keagamaan").html("");
          $("msg_q_jimpitan").html("");
          $("msg_q_arisan").html("");
          $("msg_q_gotong_royong").html("");
          $("msg_q_ikut_organisasi").html("");
          $("msg_or_detail").html("");
          $("msg_q_ikut_pelatihan").html("");
          $("msg_pel_detail").html("");
        }
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------hide_and_show-------------------------//
    //=========================================================================//

        function show_keg_pancasila(){
          var q_keg_pengamalan_pancasila = $('input[name="q_keg_pengamalan_pancasila"]:checked').val()
          if(q_keg_pengamalan_pancasila == "iya"){
            $("#path_keg_pengamalan_pancasila").fadeIn()
          }else{
            $("#path_keg_pengamalan_pancasila").fadeOut()
          }
        }

        function show_ikut_organisasi(){
          var q_ikut_organisasi = $('input[name="q_ikut_organisasi"]:checked').val()
          if(q_ikut_organisasi == "iya"){
            $("#path_ikut_organisasi").fadeIn()
          }else{
            $("#path_ikut_organisasi").fadeOut()
          }
        }

        function show_ikut_pelatihan(){
          var q_ikut_pelatihan = $('input[name="q_ikut_pelatihan"]:checked').val()
          if(q_ikut_pelatihan == "iya"){
            $("#path_ikut_pelatihan").fadeIn()
          }else{
            $("#path_ikut_pelatihan").fadeOut()
          }
        }
    //=========================================================================//
    //-----------------------------------hide_and_show-------------------------//
    //=========================================================================//

    //=========================================================================//
    //----------------------add_keg_pengamalan_pancasila-----------------------//
    //=========================================================================//
        $("#add_keg_pancasila").click(function(){
          add_keg_pancasila();
        });

        function add_keg_pancasila(){
            var tmp = {
                "nm_keg_pancasila":$("#nm_keg_pancasila").val()
            };

            if($("#nm_keg_pancasila").val() != ""){
                    var key = $("#nm_keg_pancasila").val().replaceAll(" ", "");
                    if(!(key in list_keg_pancasila)){
                        list_keg_pancasila[$("#nm_keg_pancasila").val().replaceAll(" ", "")] = tmp;
                        render_list_keg_pancasila();
                    }else{
                        alert("Data "+$("#nm_keg_pancasila").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_keg_pancasila(){
            var str_list_keg_pancasila = "";
            if(list_keg_pancasila){
                var no = 1;
                for (const i in list_keg_pancasila) {
                    str_list_keg_pancasila += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_keg_pancasila[i].nm_keg_pancasila+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_keg_pancasila('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    no++;
                }
            }

            $("#out_list_keg_pancasila").html(str_list_keg_pancasila);
        }

        $("#add_data_vaksin").click(function(){
            add_keg_pancasila();

            // console.log(list_keg_pancasila);
        });


        function delete_list_keg_pancasila(key){
            // console.log(list_keg_pancasila[key]);
            delete list_keg_pancasila[key];

            render_list_keg_pancasila();
        }
    //=========================================================================//
    //----------------------add_keg_pengamalan_pancasila-----------------------//
    //=========================================================================//

    //=========================================================================//   
    //----------------------add_ikut_organisasi--------------------------------//
    //=========================================================================//
        $("#add_ikut_organisasi").click(function(){
          add_ikut_organisasi();
        });

        function add_ikut_organisasi(){
            var tmp = {
                "nm_organisasi_detail":$("#nm_organisasi_detail").val(),
                "jns_organisasi_detail":$("#jns_organisasi_detail").val(),
                "jbtn_organisasi_detail":$("#jbtn_organisasi_detail").val(),
                "thn_organisasi_detail":$("#thn_organisasi_detail").val(),
            };

            if($("#nm_organisasi_detail").val() != ""){
                    var key = $("#nm_organisasi_detail").val().replaceAll(" ", "");
                    if(!(key in list_or_detail)){
                        list_or_detail[$("#nm_organisasi_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_or_detail();
                    }else{
                        alert("Data "+$("#nm_organisasi_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_or_detail(){
            var str_list_or_detail = "";
            if(list_or_detail){
                var no = 1;
                for (const i in list_or_detail) {
                    str_list_or_detail += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_or_detail[i].nm_organisasi_detail+"</td>"+
                                        "<td>"+list_or_detail[i].jns_organisasi_detail+"</td>"+
                                        "<td>"+list_or_detail[i].jbtn_organisasi_detail+"</td>"+
                                        "<td>"+list_or_detail[i].thn_organisasi_detail+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_or_detail('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    no++;
                }
            }

            $("#out_list_ikut_organisasi").html(str_list_or_detail);
        }


        function delete_list_or_detail(key){
            // console.log(list_or_detail[key]);
            delete list_or_detail[key];

            render_list_or_detail();
        }
    //=========================================================================//
    //----------------------add_ikut_organisasi--------------------------------//
    //=========================================================================//

    //=========================================================================//   
    //----------------------add_ikut_pelatihan--------------------------------//
    //=========================================================================//
        $("#add_ikut_pelatihan").click(function(){
          add_ikut_pelatihan();
        });

        function add_ikut_pelatihan(){
            var tmp = {
                "nm_pelatihan_detail":$("#nm_pelatihan_detail").val(),
                "penyelenggara_pelatihan_detail":$("#penyelenggara_pelatihan_detail").val(),
                "jn_str":$("#jn_str").val(),
                "thn_pelatihan_detail":$("#thn_pelatihan_detail").val(),
            };

            if($("#nm_pelatihan_detail").val() != ""){
                    var key = $("#nm_pelatihan_detail").val().replaceAll(" ", "");
                    if(!(key in list_pel_detail)){
                        list_pel_detail[$("#nm_pelatihan_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_pel_detail();
                    }else{
                        alert("Data "+$("#nm_pelatihan_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_pel_detail(){
            var str_list_pel_detail = "";
            if(list_pel_detail){
                var no = 1;
                for (const i in list_pel_detail) {
                    str_list_pel_detail += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_pel_detail[i].nm_pelatihan_detail+"</td>"+
                                        "<td>"+list_pel_detail[i].penyelenggara_pelatihan_detail+"</td>"+
                                        "<td>"+list_pel_detail[i].jn_str+"</td>"+
                                        "<td>"+list_pel_detail[i].thn_pelatihan_detail+"</td>"+
                                        "<td align=\"center\">"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_pel_detail('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    no++;
                }
            }

            $("#out_list_ikut_pelatihan").html(str_list_pel_detail);
        }


        function delete_list_pel_detail(key){
            // console.log(list_pel_detail[key]);
            delete list_pel_detail[key];

            render_list_pel_detail();
        }
    //=========================================================================//
    //----------------------add_ikut_pelatihan--------------------------------//
    //=========================================================================//
</script>
