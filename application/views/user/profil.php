<?php
    $main_controller = base_url()."admin/adminmain/";

    $path_img = base_url()."assets/prw/img/icon/";

    date_default_timezone_set('Asia/Jakarta');
    // var_dump($list_data);
    $str_json_kel_kec = '[]';
    if($data_kel_kec){
        $str_json_kel_kec = json_encode($data_kel_kec);
    }
?>

<head>
    <!-- Date picker plugins css -->
    <link href="<?php print_r(base_url());?>assets/template/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css" />
    <style>
        /* input[type="text"] {
            border-radius: 15px;
        }
        select[type="text"] {
            border-radius: 15px;
        } */
    </style>
</head>
<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- Row -->
    <!-- <div class="alert alert-warning" style="font-size: larger; border: 1px solid blue;">
        <strong><i class="fa fa-info-circle"></i> <b>Keterangan</b> :</strong> 
        silahkan <b class="text-primary">Login</b> kembali apabila telah mengubah <b class="text-primary">Nomor NIK</b> dan <b class="text-primary">Nomor KK</b>!
    </div> -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header" style="border-bottom: 1px solid silver;">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <!-- <h4 class="card-title m-b-0"><?=$title?></h4> -->
                    <h4 class="card-title m-b-0"><strong><i class="fa fa-address-card-o"></i> Data Personal</strong></h4><br>
                     <h6 class="card-subtitle">Perangkat Daerah Terkait:  Dinas Kependudukan dan Pencatatan Sipil</h6>
                </div>
                <!-- <hr> -->
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Nama Lengkap</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ds_nama_dasawisma" name="ds_nama_dasawisma" >
                                    <p id="msg_ds_nama_dasawisma" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">NIK/KIA</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="nik_user" name="nik_user" maxlength="16">
                                    <p id="msg_nik_user" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Nomor KK</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="kk_user" name="kk_user" maxlength="16">
                                    <p id="msg_kk_user" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Jenis Kelamin</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="ms_jk" id="ms_jk1" value="l">
                                    <label for="ms_jk1" style="font-size: 13px; margin-right: 0.5em;">Laki-laki</label>
                                    <input class="radio" type="radio" name="ms_jk" id="ms_jk2" value="p">
                                    <label for="ms_jk2" style="font-size: 13px; margin-right: 0.5em;">Perempuan</label>
                                    <p id="msg_ms_jk" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Tanggal Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ms_tgl_lhr" name="ms_tgl_lhr" onkeydown="return false" style="cursor: pointer;" placeholder="tangga/bulan/tahun">
                                    <p id="msg_ms_tgl_lhr" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Tempat Lahir</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ms_tmp_lhr" name="ms_tmp_lhr" >
                                    <p id="msg_ms_tmp_lhr" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Status Kawin</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan1" value="0">
                                    <label for="ms_sts_pernikahan1" style="font-size: 13px; margin-right: 0.5em;">Belum</label>
                                    <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan2" value="1">
                                    <label for="ms_sts_pernikahan2" style="font-size: 13px; margin-right: 0.5em;">Kawin</label>
                                    <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan3" value="2">
                                    <label for="ms_sts_pernikahan3" style="font-size: 13px; margin-right: 0.5em;">Cerai (Hidup)</label>
                                    <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan4" value="3">
                                    <label for="ms_sts_pernikahan4" style="font-size: 13px; margin-right: 0.5em;">Cerai (Mati)</label>
                                    <p id="msg_ms_sts_pernikahan" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0" id="div-no-akta-nikah">
                                <label for="message-text" class="control-label col-md-4">No. Akta Nikah</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="srt_nikah_no" name="srt_nikah_no" >
                                    <p id="msg_srt_nikah_no" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0" id="div-no-akta-cerai">
                                <label for="message-text" class="control-label col-md-4">No. Akta Cerai</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="srt_akta_cerai" name="srt_akta_cerai" >
                                    <p id="msg_srt_akta_cerai" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0" id="div-no-akta-cerai-mati">
                                <label for="message-text" class="control-label col-md-4">No. Kematian Pasangan</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="srt_kematian_pasangan" name="srt_kematian_pasangan" >
                                    <p id="msg_srt_kematian_pasangan" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Akta Kelahiran</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="q_akte_user" id="q_akte_user1" value="ya">
                                    <label for="q_akte_user1" style="font-size: 13px; margin-right: 0.5em;">Punya Akta</label>
                                    <input class="radio" type="radio" name="q_akte_user" id="q_akte_user2" value="tidak">
                                    <label for="q_akte_user2" style="font-size: 13px; margin-right: 0.5em;">Tidak Punya</label>
                                    <p id="msg_q_akte_user" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0" id="div-no-akte-kelahiran">
                                <label for="message-text" class="control-label col-md-4">No. Akta Kelahiran</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="akte_no_user" name="akte_no_user" >
                                    <p id="msg_akte_no_user" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0" id="div-no-akte-keterangan">
                                <label for="message-text" class="control-label col-md-4">Keterangan</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="akte_alasan_user" name="akte_alasan_user" >
                                    <p id="msg_akte_alasan_user" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Nomor Paspor</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="wn_no_paspor" name="wn_no_paspor" >
                                    <p id="msg_wn_no_paspor" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kewarganegaraan</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="ms_wn" id="ms_wn1" value="wni">
                                    <label for="ms_wn1" style="font-size: 13px; margin-right: 0.5em;">WNI</label>
                                    <input class="radio" type="radio" name="ms_wn" id="ms_wn2" value="wna">
                                    <label for="ms_wn2" style="font-size: 13px; margin-right: 0.5em;">WNA</label>
                                    <p id="msg_ms_wn" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Suku Bangsa</label>
                                <div class="col-md-8">
                                    <!-- <input type="text" class="form-control" id="ms_suku" name="ms_suku" > -->
                                    <select name="ms_suku" id="ms_suku" class="form-control">
                                        <option value="">Pilih Suku Bangsa</option>
                                        <option value="jawa">Jawa</option>
                                        <option value="sunda">Sunda</option>
                                        <option value="batak">Batak</option>
                                        <option value="bugis">Bugis</option>
                                        <option value="betawi">Betawi</option>
                                        <option value="lainnya">Lainnya</option>
                                    </select>
                                    <p id="msg_ms_suku" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Agama</label>
                                <div class="col-md-8">
                                    <!-- <input type="text" class="form-control" id="ms_agama" name="ms_agama" > -->
                                    <select name="ms_agama" id="ms_agama" class="form-control">
                                        <option value="">Pilih Agama</option>
                                        <option value="islam">Islam</option>
                                        <option value="kristen">Kristen</option>
                                        <option value="katolik">Katolik</option>
                                        <option value="hindu">Hindu</option>
                                        <option value="buddha">Buddha</option>
                                        <option value="konghucu">Konghucu</option>
                                        <option value="penghayat kepercayaan">Penghayat Kepercayaan</option>
                                    </select>
                                    <p id="msg_ms_agama" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Golongan Darah</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="ms_gol_darah" id="ms_gol_darah1" value="a">
                                    <label for="ms_gol_darah1" style="font-size: 13px; margin-right: 0.8em;">A</label>
                                    <input class="radio" type="radio" name="ms_gol_darah" id="ms_gol_darah2" value="b">
                                    <label for="ms_gol_darah2" style="font-size: 13px; margin-right: 0.8em;">B</label>
                                    <input class="radio" type="radio" name="ms_gol_darah" id="ms_gol_darah3" value="o">
                                    <label for="ms_gol_darah3" style="font-size: 13px; margin-right: 0.8em;">O</label>
                                    <input class="radio" type="radio" name="ms_gol_darah" id="ms_gol_darah4" value="ab">
                                    <label for="ms_gol_darah4" style="font-size: 13px; margin-right: 0.8em;">AB</label>
                                    <p id="msg_ms_gol_darah" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- <div class="lawas">
                        <div class="form-group">
                            <label for="message-text" class="control-label">Tempat Lahir :</label>
                            <input type="text" class="form-control" id="ms_tmp_lhr" name="ms_tmp_lhr" >
                            <p id="msg_ms_tmp_lhr" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Tgl Lahir :</label>
                            <input type="date" class="form-control" id="ms_tgl_lhr" name="ms_tgl_lhr" >
                            <p id="msg_ms_tgl_lhr" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Warga Negara :</label>
                            <input type="text" class="form-control" id="ms_wn" name="ms_wn" >
                            <p id="msg_ms_wn" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kecamatan :</label>
                            <select type="text" class="form-control" id="ktp_kec_user_data" name="ktp_kec_user_data">
                            
                            </select>
                            <p id="msg_ktp_kec_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kelurahan :</label>
                            <select type="text" class="form-control" id="ktp_kel_user_data" name="ktp_kel_user_data" required>
                                
                            </select>
                            <p id="msg_ktp_kel_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Pendidikan Terakhir :</label>
                            <input type="text" class="form-control" id="ms_pd_terakhir" name="ms_pd_terakhir" >
                            <p id="msg_ms_pd_terakhir" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Status Perkawinan :</label><br>
                            <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan1" value="0">
                            <label for="ms_sts_pernikahan1">Belum Kawin</label>
                            <input class="radio" type="radio" name="ms_sts_pernikahan" id="ms_sts_pernikahan2" value="1">
                            <label for="ms_sts_pernikahan2">Kawin</label>
                            <p id="msg_ms_sts_pernikahan" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Nama Dasawisma :</label>
                            <input type="text" class="form-control" id="ds_nama_dasawisma" name="ds_nama_dasawisma" >
                            <p id="msg_ds_nama_dasawisma" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Agama :</label>
                            <input type="text" class="form-control" id="ms_agama" name="ms_agama" >
                            <p id="msg_ms_agama" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Suku :</label>
                            <input type="text" class="form-control" id="ms_suku" name="ms_suku" >
                            <p id="msg_ms_suku" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Alamat KTP :</label>
                            <input type="text" class="form-control" id="ktp_alamat_user_data" name="ktp_alamat_user_data" >
                            <p id="msg_ktp_alamat_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">RT :</label>
                            <input type="number" class="form-control" id="ktp_rt_user_data" name="ktp_rt_user_data" >
                            <p id="msg_ktp_rt_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">RW :</label>
                            <input type="number" class="form-control" id="ktp_rw_user_data" name="ktp_rw_user_data" >
                            <p id="msg_ktp_rw_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kota :</label>
                            <input type="text" class="form-control" id="ktp_kota_user_data" name="ktp_kota_user_data" >
                            <p id="msg_ktp_kota_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Akte Kelahiran :</label><br>
                            <input class="radio" type="radio" name="akte_user" id="akte_user1" value="ya">
                            <label for="akte_user1">Ya</label>
                            <input class="radio" type="radio" name="akte_user" id="akte_user2" value="tidak">
                            <label for="akte_user2">Tidak</label>
                            <p id="msg_akte_user" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Alasan Akte :</label>
                            <input type="text" class="form-control" id="akte_alasan_user" name="akte_alasan_user" >
                            <p id="msg_akte_alasan_user" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">No Akte :</label>
                            <input type="text" class="form-control" id="akte_no_user" name="akte_no_user" >
                            <p id="msg_akte_no_user" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kecamatan Alamat Terkini :</label>
                            <input type="text" class="form-control" id="d_kec_user_data" name="d_kec_user_data" >
                            <p id="msg_d_kec_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kelurahan Alamat Terkini :</label>
                            <input type="text" class="form-control" id="d_kel_user_data" name="d_kel_user_data" >
                            <p id="msg_d_kel_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">RT Alamat Terkini :</label>
                            <input type="number" class="form-control" id="d_rt_user_data" name="d_rt_user_data" >
                            <p id="msg_d_rt_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">RW Alamat Terkini :</label>
                            <input type="number" class="form-control" id="d_rw_user_data" name="d_rw_user_data" >
                            <p id="msg_d_rw_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Kota Alamat Terkini :</label>
                            <input type="text" class="form-control" id="d_kota_user_data" name="d_kota_user_data" >
                            <p id="msg_d_kota_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Alamat WN :</label>
                            <input type="text" class="form-control" id="wn_almt_user_data" name="wn_almt_user_data" >
                            <p id="msg_wn_almt_user_data" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">No Surat Nikah :</label>
                            <input type="text" class="form-control" id="srt_nikah_no" name="srt_nikah_no" >
                            <p id="msg_srt_nikah_no" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Status Tempat Tinggal :</label><br>
                            <input class="radio" type="radio" name="q_sts_tinggal" id="q_sts_tinggal1" value="ya">
                            <label for="q_sts_tinggal1">Ya</label>
                            <input class="radio" type="radio" name="q_sts_tinggal" id="q_sts_tinggal2" value="tidak">
                            <label for="q_sts_tinggal2">Tidak</label>
                            <p id="msg_q_sts_tinggal" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Lama Tinggal :</label>
                            <input type="number" class="form-control" id="q_lama_tinggal" name="q_lama_tinggal" >
                            <p id="msg_q_lama_tinggal" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Ingin Tinggal :</label><br>
                            <input class="radio" type="radio" name="q_ingin_tinggal" id="q_ingin_tinggal1" value="ya">
                            <label for="q_ingin_tinggal1">Ya</label>
                            <input class="radio" type="radio" name="q_ingin_tinggal" id="q_ingin_tinggal2" value="tidak">
                            <label for="q_ingin_tinggal2">Tidak</label>
                            <p id="msg_q_ingin_tinggal" style="color: red;"></p>
                        </div>
                        <div class="form-group">
                            <label for="message-text" class="control-label">Alamat Terkini :</label>
                            <input type="text" class="form-control" id="d_alamat_user_data" name="d_alamat_user_data" >
                            <p id="msg_d_alamat_user_data" style="color: red;"></p>
                        </div>
                    </div> -->
                </div>
            </div>

            <div class="card card-default" style="margin-top: -20px;">
                <div class="card-header" style="border-bottom: 1px solid silver;">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><strong><i class="fa fa-map-marker"></i> Alamat</strong></h4>
                </div>
                <!-- <hr> -->
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Alamat Sesuai KTP</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ktp_alamat_user_data" name="ktp_alamat_user_data" >
                                    <p id="msg_ktp_alamat_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Provinsi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ktp_provinsi" name="ktp_provinsi" >
                                    <p id="msg_ktp_provinsi" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kota</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="ktp_kota_user_data" name="ktp_kota_user_data" >
                                    <p id="msg_ktp_kota_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kecamatan</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control" id="ktp_kec_user_data" name="ktp_kec_user_data">
                        
                                    </select>
                                    <p id="msg_ktp_kec_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kelurahan</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control" id="ktp_kel_user_data" name="ktp_kel_user_data" required>
                                        
                                    </select>
                                    <p id="msg_ktp_kel_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">RT / RW</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="ktp_rt_user_data" name="ktp_rt_user_data" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                                        <label class="px-2 pt-2"> / </label>
                                        <input type="text" class="form-control" id="ktp_rw_user_data" name="ktp_rw_user_data" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                                    </div>
                                    <p id="msg_ktp_rt_user_data" style="color: red;"></p>
                                    <p id="msg_ktp_rw_user_data" style="color: red;"></p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Domisili Saat ini</label>
                                <div class="col-md-8">
                                    <input class="radio" type="radio" name="domisili" id="domisili1" value="ya">
                                    <label for="domisili1" style="font-size: 13px; margin-right: 0.5em;">Sesuai KTP</label>
                                    <input class="radio" type="radio" name="domisili" id="domisili2" value="tidak">
                                    <label for="domisili2" style="font-size: 13px; margin-right: 0.5em;">Tidak Sesuai KTP</label>
                                    <p id="msg_domisili" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Alamat Domisili</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="d_alamat_user_data" name="d_alamat_user_data" >
                                    <p id="msg_d_alamat_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Provinsi</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="d_provinsi" name="d_provinsi" >
                                    <p id="msg_d_provinsi" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kota</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="d_kota_user_data" name="d_kota_user_data" >
                                    <p id="msg_d_kota_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kecamatan</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control" id="d_kec_user_data" name="d_kec_user_data">
                        
                                    </select>
                                    <p id="msg_d_kec_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">Kelurahan</label>
                                <div class="col-md-8">
                                    <select type="text" class="form-control" id="d_kel_user_data" name="d_kel_user_data">
                                        
                                    </select>
                                    <p id="msg_d_kel_user_data" style="color: red;"></p>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <label for="message-text" class="control-label col-md-4">RT / RW</label>
                                <div class="col-md-8">
                                    <div class="input-group">
                                        <input type="text" class="form-control" id="d_rt_user_data" name="d_rt_user_data" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                                        <label class="px-2 pt-2"> / </label>
                                        <input type="text" class="form-control" id="d_rw_user_data" name="d_rw_user_data" maxlength="3" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*?)\..*/g, '$1');" >
                                    </div>
                                    <p id="msg_d_rt_user_data" style="color: red;"></p>
                                    <p id="msg_d_rw_user_data" style="color: red;"></p>
                                </div>
                            </div>
        
                            <input type="hidden" class="form-control" id="crt_time" name="crt_time" value="<?=date('Y-m-d H:i:s')?>" >

                        </div>
                    </div>
                </div>
            </div>   
            
            <?php
                    if(!$resume){
                ?>
            <div class="text-right">
                <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info" style="font-size: 14px;"><i class="fa fa-bookmark-o"></i> Simpan</button>
                <button type="button" id="btn-loading" class="btn waves-effect waves-light btn-rounded btn-info" style="font-size: 14px; display: none;"><i class="fa fa-spin fa-spinner"></i> Menyimpan</button>
            </div>
            <?php 
                    } 
                ?>
        </div>
    </div>
    <!-- End Row -->
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<!-- Date Picker Plugin JavaScript -->
<script src="<?php print_r(base_url()); ?>assets/template/assets/plugins/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script type="text/javascript">

    const simple = (req) => {
        if (req) {
            return req
        }else{
            return ""
        }
    }

    $(document).ready(function(){
        // DATE PICKER
        $("#ms_tgl_lhr").datepicker({ 
            format:'dd-mm-yyyy', 
            autoclose: true, 
            todayHighlight: false,
        });
        
        // DATA PERSONAL
        $("#ds_nama_dasawisma").val("<?=isset($list_data['ds_nama_dasawisma']) ? $list_data['ds_nama_dasawisma'] : ''?>")
        $("#nik_user").val("<?=isset($nik_kk['nik_user']) ? $nik_kk['nik_user'] : '' ?>");
        $("#kk_user").val("<?= isset($nik_kk['kk_user']) ? $nik_kk['kk_user'] : '' ?>");
        $("input[name='ms_jk'][value='<?=isset($list_data['ms_jk']) ? $list_data['ms_jk'] : ''?>']").prop("checked",true);
        $("#ms_tgl_lhr").val("<?=isset($list_data['ms_tgl_lhr']) ? $list_data['ms_tgl_lhr'] : ''?>")        
        $("#ms_tmp_lhr").val("<?=isset($list_data['ms_tmp_lhr']) ? $list_data['ms_tmp_lhr'] : ''?>")
        $("input[name='ms_sts_pernikahan'][value='<?=isset($list_data['ms_sts_pernikahan']) ? $list_data['ms_sts_pernikahan'] : ''?>']").prop("checked",true);
        $("#srt_nikah_no").val("<?=isset($list_data['srt_nikah_no']) ? $list_data['srt_nikah_no'] : ''?>")
        $("#srt_akta_cerai").val("<?=isset($list_data['srt_akta_cerai']) ? $list_data['srt_akta_cerai'] : ''?>")
        $("#srt_kematian_pasangan").val("<?=isset($list_data['srt_kematian_pasangan']) ? $list_data['srt_kematian_pasangan'] : ''?>")
        $("input[name='q_akte_user'][value='<?=isset($list_data['q_akte_user']) ? $list_data['q_akte_user'] : ''?>']").prop("checked",true);
        $("#akte_no_user").val("<?=isset($list_data['akte_no_user']) ? $list_data['akte_no_user'] : ''?>")
        $("#akte_alasan_user").val("<?=isset($list_data['akte_alasan_user']) ? $list_data['akte_alasan_user'] : ''?>")
        $("#wn_no_paspor").val("<?=isset($list_data['wn_no_paspor']) ? $list_data['wn_no_paspor'] : ''?>")
        $("input[name='ms_wn'][value='<?=isset($list_data['ms_wn']) ? $list_data['ms_wn'] : ''?>']").prop("checked",true);
        $("#ms_suku").val("<?=isset($list_data['ms_suku']) ? $list_data['ms_suku'] : ''?>")
        $("#ms_agama").val("<?=isset($list_data['ms_agama']) ? $list_data['ms_agama'] : ''?>")
        $("input[name='ms_gol_darah'][value='<?=isset($list_data['ms_gol_darah']) ? $list_data['ms_gol_darah'] : ''?>']").prop("checked",true);
        
        // ALAMAT KTP
        $("#ktp_alamat_user_data").val("<?=isset($list_data['ktp_alamat_user_data']) ? $list_data['ktp_alamat_user_data'] : ''?>")
        $("#ktp_provinsi").val("<?=isset($list_data['ktp_provinsi']) ? $list_data['ktp_provinsi'] : ''?>");
        $("#ktp_kota_user_data").val("<?=isset($list_data['ktp_kota_user_data']) ? $list_data['ktp_kota_user_data'] : ''?>")
        $("#ktp_kec_user_data").val("<?=isset($list_data['ktp_kec_user_data']) ? $list_data['ktp_kec_user_data'] : ''?>").change();
        $("#ktp_kel_user_data").val("<?=isset($list_data['ktp_kel_user_data']) ? $list_data['ktp_kel_user_data'] : ''?>").change();
        $("#ktp_rt_user_data").val("<?=isset($list_data['ktp_rt_user_data']) ? $list_data['ktp_rt_user_data'] : ''?>")
        $("#ktp_rw_user_data").val("<?=isset($list_data['ktp_rw_user_data']) ? $list_data['ktp_rw_user_data'] : ''?>")
        
        // ALAMAT DOMISILI
        $("#d_alamat_user_data").val("<?=isset($list_data['d_alamat_user_data']) ? $list_data['d_alamat_user_data'] : ''?>")
        $("#d_provinsi").val("<?=isset($list_data['d_provinsi']) ? $list_data['d_provinsi'] : ''?>");
        $("#d_kota_user_data").val("<?=isset($list_data['d_kota_user_data']) ? $list_data['d_kota_user_data'] : ''?>")
        $("#d_kec_user_data").val("<?=isset($list_data['d_kec_user_data']) ? $list_data['d_kec_user_data'] : ''?>")
        $("#d_kel_user_data").val("<?=isset($list_data['d_kel_user_data']) ? $list_data['d_kel_user_data'] : ''?>")
        $("#d_rt_user_data").val("<?=isset($list_data['d_rt_user_data']) ? $list_data['d_rt_user_data'] : ''?>")
        $("#d_rw_user_data").val("<?=isset($list_data['d_rw_user_data']) ? $list_data['d_rw_user_data'] : ''?>")

        
        var tgl_lahir_raw    = $("#ms_tgl_lhr").val();
        var tgl_lahir_split  = tgl_lahir_raw.split("-");
        var tgl_lahir_result = tgl_lahir_split[2]+"-"+tgl_lahir_split[1]+"-"+tgl_lahir_split[0];
        $("#ms_tgl_lhr").datepicker("setDate", tgl_lahir_result);
        console.log(tgl_lahir_result);
        
        // $("input[name='q_sts_tinggal'][value='<?=isset($list_data['q_sts_tinggal']) ? $list_data['q_sts_tinggal'] : ''?>']").prop("checked",true);
        // $("input[name='q_ingin_tinggal'][value='<?=isset($list_data['q_ingin_tinggal']) ? $list_data['q_ingin_tinggal'] : ''?>']").prop("checked",true);
        // $("#wn_almt_user_data").val("<?=isset($list_data['wn_almt_user_data']) ? $list_data['wn_almt_user_data'] : ''?>")
        // $("#akte_alasan_user").val("<?=isset($list_data['akte_alasan_user']) ? $list_data['akte_alasan_user'] : ''?>")
        // $("#q_lama_tinggal").val("<?=isset($list_data['q_lama_tinggal']) ? $list_data['q_lama_tinggal'] : ''?>")
        // $("#ms_pd_terakhir").val("<?=isset($list_data['ms_pd_terakhir']) ? $list_data['ms_pd_terakhir'] : ''?>")

        ins_set_kec();
        ins_set_kel();
        ins_set_kel2();
        
        // Status Menikah
            var status_nikah = $("input[name='ms_sts_pernikahan']:checked").val();
            if (!$("input[name='ms_sts_pernikahan']").is("checked")) {
                $('#div-no-akta-cerai').hide();
                $('#div-no-akta-nikah').hide();
                $('#div-no-akta-cerai-mati').hide();
            } 
            
            if (status_nikah == '0') { // belum nikah
                $('#div-no-akta-nikah').hide();
                $('#div-no-akta-cerai').hide();
                $('#div-no-akta-cerai-mati').hide();
            } else if (status_nikah == '1') { // nikah
                $('#div-no-akta-nikah').show();
                $('#div-no-akta-cerai').hide();            
                $('#div-no-akta-cerai-mati').hide();            
            } else if (status_nikah == '2') { // cerai hidup           
                $('#div-no-akta-nikah').hide();
                $('#div-no-akta-cerai').show();
                $('#div-no-akta-cerai-mati').hide();
            } else if (status_nikah == '3') { // cerai mati         
                $('#div-no-akta-nikah').hide();
                $('#div-no-akta-cerai').hide();
                $('#div-no-akta-cerai-mati').show();                
            }
        // End

        // Akta Kelahiran
            var akta_lahir = $("input[name='q_akte_user']:checked").val();
            if (!$("input[name='q_akte_user']").is("checked")) {
                $('#div-no-akte-kelahiran').hide();
                $('#div-no-akte-keterangan').hide();
            }
            
            if (akta_lahir == 'ya') {
                $('#div-no-akte-kelahiran').show();
                $('#div-no-akte-keterangan').hide();
            } else {
                $('#div-no-akte-kelahiran').hide();
                $('#div-no-akte-keterangan').show();
            }
        // End

        // Domisili
            var alamat_ktp = $("#ktp_alamat_user_data").val();
            var alamat_dom = $("#d_alamat_user_data").val();
            if (alamat_ktp) {
                if (alamat_ktp == alamat_dom) {
                    $("input[name='domisili'][value='ya']").prop("checked",true);
                } else{
                    $("input[name='domisili'][value='tidak']").prop("checked",true);
                }
            }
        // End
    })

    // SHOW HIDE DIV NO AKTA KELAHIRAN
    $("input[name='q_akte_user']").click(function() {
        var value = $(this).attr("value");
        if (value == 'ya') {
            $('#akte_no_user').val('');
            $('#div-no-akte-kelahiran').fadeIn();
            $('#div-no-akte-keterangan').fadeOut();
            $('#akte_alasan_user').val('-');
        } else {
            $('#akte_alasan_user').val('');
            $('#div-no-akte-keterangan').fadeIn();
            $('#div-no-akte-kelahiran').fadeOut();
            $('#akte_no_user').val('-');
        }
    });

    // SHOW HIDE DIV STATUS KAWIN
    $("input[name='ms_sts_pernikahan']").click(function() {
        var value = $(this).attr("value");
        if (value == '1') { // nikah
            $('#srt_nikah_no').val('');
            $('#div-no-akta-cerai').fadeOut();
            $('#div-no-akta-cerai-mati').fadeOut();
            $('#div-no-akta-nikah').fadeIn();
            $('#srt_akta_cerai').val('-');
            $('#srt_kematian_pasangan').val('-');
        } else if (value == '2') { // cerai hidup
            $('#srt_akta_cerai').val('');
            $('#div-no-akta-nikah').fadeOut();
            $('#div-no-akta-cerai-mati').fadeOut();
            $('#div-no-akta-cerai').fadeIn();            
            $('#srt_nikah_no').val('-');
            $('#srt_kematian_pasangan').val('-');
        } else if (value == '3') { // cerai mati
            $('#srt_kematian_pasangan').val('');            
            $('#div-no-akta-nikah').fadeOut();
            $('#div-no-akta-cerai').fadeOut();
            $('#div-no-akta-cerai-mati').fadeIn();
            $('#srt_nikah_no').val('-');
            $('#srt_akta_cerai').val('-');
        } else if (value == '0') { // belum menikah
            $('#div-no-akta-nikah').fadeOut();
            $('#div-no-akta-cerai').fadeOut();
            $('#div-no-akta-cerai-mati').fadeOut();
            $('#srt_nikah_no').val('-');
            $('#srt_akta_cerai').val('-');
            $('#srt_kematian_pasangan').val('-');
        }
    });

    // DOMISILI
    $("input[name='domisili']").click(function(){
        var value = $(this).attr("value");
        var alamatKTP    = $("#ktp_alamat_user_data").val();
        var provinsiKTP  = $("#ktp_provinsi").val();
        var kotaKTP      = $("#ktp_kota_user_data").val();
        var kecamatanKTP = $("#ktp_kec_user_data").val();
        var kelurahanKTP = $("#ktp_kel_user_data").val();
        var rtKTP        = $("#ktp_rt_user_data").val();
        var rwKTP        = $("#ktp_rw_user_data").val();
        
        if (value == "ya") {
            $('#d_alamat_user_data').val(alamatKTP);
            $('#d_provinsi').val(provinsiKTP);
            $('#d_kota_user_data').val(kotaKTP);
            $('#d_kec_user_data').val(kecamatanKTP).change();
            $('#d_kel_user_data').val(kelurahanKTP).change();
            $('#d_rt_user_data').val(rtKTP);
            $('#d_rw_user_data').val(rwKTP);
        } else {
            $("#d_alamat_user_data").val("<?=isset($list_data['d_alamat_user_data']) ? $list_data['d_alamat_user_data'] : ''?>");
            $("#d_provinsi").val("<?=isset($list_data['d_provinsi']) ? $list_data['d_provinsi'] : ''?>");
            $("#d_kota_user_data").val("<?=isset($list_data['d_kota_user_data']) ? $list_data['d_kota_user_data'] : ''?>");
            $("#d_kec_user_data").val("<?=isset($list_data['d_kec_user_data']) ? $list_data['d_kec_user_data'] : ''?>").change();
            $("#d_kel_user_data").val("<?=isset($list_data['d_kel_user_data']) ? $list_data['d_kel_user_data'] : ''?>").change();
            $("#d_rt_user_data").val("<?=isset($list_data['d_rt_user_data']) ? $list_data['d_rt_user_data'] : ''?>");
            $("#d_rw_user_data").val("<?=isset($list_data['d_rw_user_data']) ? $list_data['d_rw_user_data'] : ''?>");
        }
    });

    var data_kel_kec = JSON.parse('<?=$str_json_kel_kec?>');

    function ins_set_kec(){
        var str_kec = "<option value='' selected>Pilih Kecamatan</option>";
        
        for (const key in data_kel_kec) {
            var main = data_kel_kec[key].main;
            var item = data_kel_kec[key].item;

            str_kec += '<option value="'+main.id_distrik_pem+'">'+main.nama_distrik_pem+'</option>';
        }

        $("#ktp_kec_user_data").html(str_kec);
        $("#d_kec_user_data").html(str_kec);
        $("#ktp_kec_user_data").val("<?=isset($list_data['ktp_kec_user_data']) ? $list_data['ktp_kec_user_data'] : ''?>")
        $("#d_kec_user_data").val("<?=isset($list_data['d_kec_user_data']) ? $list_data['d_kec_user_data'] : ''?>")
    }

    $('#ktp_kec_user_data').change(function(){
        ins_set_kel();
        $("#ktp_kel_user_data").val("")
    })

    $('#d_kec_user_data').change(function(){
        ins_set_kel2();
        $("#d_kel_user_data").val("")
    })

    // KTP
    function ins_set_kel(){
        var val_kec = $("#ktp_kec_user_data").val();
        var str_kel = "<option value='' selected>Pilih Kelurahan</option>";
        if (simple(val_kec)) {
            var list_kec = data_kel_kec[val_kec].item;
            for (const key in list_kec) {
                // var main = data_kel_kec[key].main;
                var item = list_kec[key];

                str_kel += '<option value="'+item.id_distrik_pem+'">'+item.nama_distrik_pem+'</option>';
            }
        }

        $("#ktp_kel_user_data").html(str_kel);
        $("#ktp_kel_user_data").val("<?=isset($list_data['ktp_kel_user_data']) ? $list_data['ktp_kel_user_data'] : ''?>")
    }

    // DOMISILI
    function ins_set_kel2(){
        var val_kec = $("#d_kec_user_data").val();
        var str_kel = "<option value='' selected>Pilih Kelurahan</option>";
        if (simple(val_kec)) {
            var list_kec = data_kel_kec[val_kec].item;
            for (const key in list_kec) {
                // var main = data_kel_kec[key].main;
                var item = list_kec[key];

                str_kel += '<option value="'+item.id_distrik_pem+'">'+item.nama_distrik_pem+'</option>';
            }
        }

        $("#d_kel_user_data").html(str_kel);
        $("#d_kel_user_data").val("<?=isset($list_data['d_kel_user_data']) ? $list_data['d_kel_user_data'] : ''?>")
    }    

    
       

    //=========================================================================//
    //-----------------------------------get_admin_update----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
    $("#update").click(function() {
        var data_main = new FormData();
        // Data Personal
        data_main.append('ds_nama_dasawisma'   , $("#ds_nama_dasawisma").val()); // nama lengkap
        data_main.append('nik_user'   , $("#nik_user").val()); // NIK
        data_main.append('kk_user'   , $("#kk_user").val()); // KK
        data_main.append('ms_jk', simple($("input[name='ms_jk']:checked").val())); // jenis kelamin
        data_main.append('ms_tgl_lhr'        , $("#ms_tgl_lhr").val()); // tanggal lahir
        data_main.append('ms_tmp_lhr', $("#ms_tmp_lhr").val()); // tempat lahir
        data_main.append('ms_sts_pernikahan'   , simple($("input[name='ms_sts_pernikahan']:checked").val())); // status pernikahan
        data_main.append('srt_nikah_no'   , $("#srt_nikah_no").val()); // nomor akta nikah
        data_main.append('srt_akta_cerai'   , $("#srt_akta_cerai").val()); // nomor akta cerai
        data_main.append('srt_kematian_pasangan'   , $("#srt_kematian_pasangan").val()); // nomor akta cerai mati
        data_main.append('q_akte_user'   , simple($("input[name='q_akte_user']:checked").val()));
        data_main.append('akte_no_user'   , $("#akte_no_user").val()); // nomor akta lahir
        data_main.append('akte_alasan_user'   , $("#akte_alasan_user").val()); // alasan tidak ada akta lahir
        data_main.append('wn_no_paspor'   , $("#wn_no_paspor").val() ? $("#wn_no_paspor").val(): "0"); // nomor paspor
        data_main.append('ms_wn'   , simple($("input[name='ms_wn']:checked").val())); // kewarganegaraan
        data_main.append('ms_suku'   , $("#ms_suku").val()); // suku
        data_main.append('ms_agama'   , $("#ms_agama").val()); // agam
        data_main.append('ms_gol_darah'   , simple($("input[name='ms_gol_darah']:checked").val())); // golongan darah
        
        // Alamat
        data_main.append('ktp_alamat_user_data'   , $("#ktp_alamat_user_data").val()); // alamat KTP
        data_main.append('ktp_provinsi'   , $("#ktp_provinsi").val()); // provinsi KTP
        data_main.append('ktp_kota_user_data'   , $("#ktp_kota_user_data").val()); // kota KTP
        data_main.append('ktp_kec_user_data'     , $("#ktp_kec_user_data").val()); // lecamatan KTP
        data_main.append('ktp_kel_user_data'   , $("#ktp_kel_user_data").val()); // kelurahan KTP
        data_main.append('ktp_rt_user_data'   , $("#ktp_rt_user_data").val()); // rt KTP
        data_main.append('ktp_rw_user_data'   , $("#ktp_rw_user_data").val()); // rw KTP
        
        data_main.append('d_alamat_user_data'   , $("#d_alamat_user_data").val()); // alamat Domisili
        data_main.append('d_provinsi'   , $("#d_provinsi").val()); // provinsi Domisili
        data_main.append('d_kota_user_data'   , $("#d_kota_user_data").val()); // kota Domisili
        data_main.append('d_kec_user_data'   , $("#d_kec_user_data").val()); // kecamatan Domisili
        data_main.append('d_kel_user_data'   , $("#d_kel_user_data").val()); // kelurahan Domisili
        data_main.append('d_rt_user_data'   , $("#d_rt_user_data").val()); // rt Domisili
        data_main.append('d_rw_user_data'   , $("#d_rw_user_data").val()); // rw Domisili
        
        data_main.append('crt_time'   , $("#crt_time").val());
        
        // data_main.append('ms_pd_terakhir'   , $("#ms_pd_terakhir").val()); 
        // data_main.append('akte_alasan_user'   , $("#akte_alasan_user").val());
        // data_main.append('wn_almt_user_data'   , $("#wn_almt_user_data").val());
        // data_main.append('q_sts_tinggal'   , simple($("input[name='q_sts_tinggal']:checked").val()));
        // data_main.append('q_lama_tinggal'   , $("#q_lama_tinggal").val());
        // data_main.append('q_ingin_tinggal'   , simple($("input[name='q_ingin_tinggal']:checked").val()));
            
            
        $.ajax({
            url: "<?php echo base_url()."user/profiluser/update";?>",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            beforeSend: () => {
                $('#update').hide();
                $('#btn-loading').show();
            },
            complete: () => {
                $('#btn-loading').hide();
                $('#update').show();
            },
            success: function(res) {
                response_update(res);
                console.log(res);
            }
        });
    });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/profil");?>");
            } else {
                $("#msg_ds_nama_dasawisma").html(detail_msg.ds_nama_dasawisma);
                $("#msg_nik_user").html(detail_msg.nik_user);
                $("#msg_kk_user").html(detail_msg.kk_user);
                $("#msg_ms_jk").html(detail_msg.ms_jk);
                $("#msg_ms_tgl_lhr").html(detail_msg.ms_tgl_lhr);
                $("#msg_ms_tmp_lhr").html(detail_msg.ms_tmp_lhr);
                $("#msg_ms_sts_pernikahan").html(detail_msg.ms_sts_pernikahan);
                $("#msg_srt_nikah_no").html(detail_msg.srt_nikah_no);
                $("#msg_srt_akta_cerai").html(detail_msg.srt_akta_cerai);
                $("#msg_srt_kematian_pasangan").html(detail_msg.srt_kematian_pasangan);
                $("#msg_q_akte_user").html(detail_msg.q_akte_user);
                $("#msg_akte_no_user").html(detail_msg.akte_no_user);
                $("#msg_akte_alasan_user").html(detail_msg.akte_alasan_user);
                $("#msg_wn_no_paspor").html(detail_msg.wn_no_paspor);
                $("#msg_ms_wn").html(detail_msg.ms_wn);
                $("#msg_ms_suku").html(detail_msg.ms_suku);
                $("#msg_ms_agama").html(detail_msg.ms_agama);
                $("#msg_ms_gol_darah").html(detail_msg.ms_gol_darah);

                $("#msg_ktp_alamat_user_data").html(detail_msg.ktp_alamat_user_data);
                $("#msg_ktp_provinsi").html(detail_msg.ktp_provinsi);
                $("#msg_ktp_kota_user_data").html(detail_msg.ktp_kota_user_data);
                $("#msg_ktp_kec_user_data").html(detail_msg.ktp_kec_user_data);
                $("#msg_ktp_kel_user_data").html(detail_msg.ktp_kel_user_data);
                $("#msg_ktp_rt_user_data").html(detail_msg.ktp_rt_user_data);
                $("#msg_ktp_rw_user_data").html(detail_msg.ktp_rw_user_data);

                $("#msg_d_alamat_user_data").html(detail_msg.d_alamat_user_data);
                $("#msg_d_provinsi").html(detail_msg.d_provinsi);
                $("#msg_d_kota_user_data").html(detail_msg.d_kota_user_data);
                $("#msg_d_kec_user_data").html(detail_msg.d_kec_user_data);
                $("#msg_d_kel_user_data").html(detail_msg.d_kel_user_data);
                $("#msg_d_rt_user_data").html(detail_msg.d_rt_user_data);
                $("#msg_d_rw_user_data").html(detail_msg.d_rw_user_data);
                
                // $("#msg_ms_pd_terakhir").html(detail_msg.ms_pd_terakhir);
                // $("#msg_akte_alasan_user").html(detail_msg.akte_alasan_user);
                // $("#msg_wn_almt_user_data").html(detail_msg.wn_almt_user_data);
                // $("#msg_q_sts_tinggal").html(detail_msg.q_sts_tinggal);
                // $("#msg_q_lama_tinggal").html(detail_msg.q_lama_tinggal);
                // $("#msg_q_ingin_tinggal").html(detail_msg.q_ingin_tinggal);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

       

</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->
