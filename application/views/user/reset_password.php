
<!doctype html>

<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1,maximum-scale=1, user-scalable=no">

<title>Reset Password</title>
<link rel="shortcut icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">
<link rel="icon" href="<?= base_url(); ?>/assets/template_front/images/favicon.ico">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/fontawesome-all.min.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/slick.css">
<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>/assets/template_front/css/style.css">
<link href="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.css" rel="stylesheet" type="text/css">
<link href="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/skins/all.css" rel="stylesheet">
<script src="https://www.google.com/recaptcha/api.js" async defer></script>
<style type="text/css">

   * {
  padding: 0;
  margin: 0;
}

@keyframes load {
  0% {
    left: 0;
    width: 0px;
  }
  50% {
    left: 50%;
    width: 150px;
  }
  100% {
    left: 100%;
    width: 0px;
  }
}



.loader {
  position: relative;
  width: 100%;
  min-height: 8px;
}

.loader::before {
  position: absolute;
  content: "";
  top: 0;
  left: 0;
  width: 100%;
  min-height: 8px;
  background-color: #0099B0;
}

.loader::after {
  position: absolute;
  content: "";
  top: 0;
  left: 0;
  width: 30px;
  min-height: 8px;
  background-color: orange;
  animation: load 1.5s linear infinite;
}
.loader{
    display: none;
}
</style>
</head>

<body class="fullpage">
<div id="form-section" class="container-fluid signup">
    <a class="website-logo" href="<?= base_url(); ?>landing_page/Landingpage">
        <img class="logo" src="<?= base_url(); ?>/assets/template_front/images/logo.png" style="width: 70%" alt="Hostino">
    </a>
    <div class="menu-holder">
        <ul class="main-links">
            <li><a class="normal-link" href="<?= base_url(); ?>user/login">Sudah Memiliki Akun?</a></li>
            <li><a class="sign-button" href="<?= base_url(); ?>user/login">Login <i class="hno hno-arrow-right"></i></a></li>
        </ul>
    </div>
    <div class="row">
        <div class="form-holder">
            <div class="signin-signup-form">
                <div class="form-items">         
                     <div class="card-body" >
                    <!-- <form class="form-horizontal form-material" id="loginform" action="index.html"> -->
                        <h1 class="box-title m-b- text-center "><b>Lupa Password</b></h1>
                        <h3 class="box-title m-b-20 text-center"><b>SAM GEPUN BASA</b></h3>
                        <br>
                         <?= form_open('password_reset'); ?>
                            <div class="" style="background-image:url(<?php print_r(base_url());?>assets/template/assets/images/background/login-register.jpg);">

                                <div class="login-box card">
                                          <div class="loader " style="background-color: white;"></div>
                                    <div class="card-body">
                                        <!-- <form class="form-horizontal form-material" id="loginform" action="index.html"> -->
                                                 
                                        <div class="form-group m-t-40">
                                            <div class="col-xs-12 form-text">
                                                <input class="form-control" type="email" name="email_user" id="email_user" required="" placeholder="Masukkan Email">
                                                <p id="msg_nama_user" style="color: red;"></p>
                                            </div>
                                        </div>           
                                        <div class="form-group text-center m-t-20">
                                            <div class="col-xs-12">
                                                <button class="btn btn-info btn-lg btn-block text-uppercase waves-effect waves-light" type="submit" name="submit" id="click_load">Submit</button>

                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?=form_close()?>
                        
                    </div>
                    <br><br>
          
                </div>

            </div>
        </div>
        <div class="info-slider-holder">
            <div class="info-holder">
                <div class="img-text-slider">
                    <div>
                        <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img4.png" alt="">
                        <p>Smart City Kota Malang Gerakan Menghimpun Data <br> Berbasis Dasa Wisma</p>
                    </div>
                    <div>
                        <img src="<?= base_url(); ?>/assets/template_front/images/main-slide-img5.png" alt="">
                        <p>Smart City Kota Malang Gerakan Menghimpun Data<br> Berbasis Dasa Wisma</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="<?= base_url(); ?>/assets/template_front/js/jquery.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/bootstrap.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/slick.min.js"></script>
<script src="<?= base_url(); ?>/assets/template_front/js/main.js"></script>

<script src="<?php print_r(base_url());?>assets/template/assets/plugins/sweetalert/sweetalert.min.js"></script>
    <!-- icheck -->
<script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.min.js"></script>
<script src="<?php print_r(base_url());?>assets/template/assets/plugins/icheck/icheck.init.js"></script>
<script type="text/javascript">
jQuery(document).ready(function($){
    $('.loader').hide();
    $('#click_load').click(function(){
        $('.loader').fadeIn();
    });
});
        $("#sbm").click(function(evt) {
            var response = grecaptcha.getResponse();
            if(response.length == 0) 
            { 
                //reCaptcha not verified
                alert("Centang captcha terlebih dahulu!"); 
                evt.preventDefault();
                return false
            }
            if ($("#nik_user").val().length < 16 || $("#kk_user").val().length < 16) {
                    swal({
                        title: "NIK / KK",
                        text: "Kurang dari 16 Digit",
                        type: "warning",
                        showCancelButton: false,
                        confirmButtonColor: "#ffb22b",
                        confirmButtonText: "OK",
                        closeOnConfirm: false
                    }, function() {
                        swal.close();
                        evt.preventDefault();
                        return false
                        // window.location.href = window.location.href;
                    });
            }else{
                var data_main = new FormData();
                data_main.append('nama_user'     , $("#nama_user").val());
                data_main.append('nik_user'     , $("#nik_user").val());
                data_main.append('kk_user'     , $("#kk_user").val());
                data_main.append('tlp_user'     , $("#tlp_user").val());
                data_main.append('email_user'     , $("#email_user").val());
                data_main.append('username_user'     , $("#username_user").val());
                data_main.append('password_user'     , $("#password_user").val());
                data_main.append('repassword'     , $("#repassword").val());
                data_main.append('captcha'     , response);
                
                $.ajax({
                    url: "<?php echo base_url()."user/register/insert_register";?>",
                    dataType: 'html', // what to expect back from the PHP script, if anything
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: data_main,
                    type: 'post',
                    success: function(res) {
                        response_register(res);

                        console.log(res);
                    }
                });
            }
        });

        function response_register(res) {
            ! function($) {
                "use strict";
                var SweetAlert = function() {};
                SweetAlert.prototype.init = function() {

                    var data_json = JSON.parse(res);
                    var main_msg = data_json.msg_main;
                    var detail_msg = data_json.msg_detail;
                    if (main_msg.status) {
                        swal({
                            title: "Register Success",
                            text: main_msg.msg,
                            type: "success",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "Lanjutkan",
                            closeOnConfirm: false
                        }, function() {
                            // swal.close();
                            window.location.href = "<?=base_url()."user/login"?>";
                        });
                    } else {
                        swal({
                            title: "Register Gagal",
                            text: main_msg.msg,
                            type: "warning",
                            showCancelButton: false,
                            confirmButtonColor: "#ffb22b",
                            confirmButtonText: "OK",
                            closeOnConfirm: false
                        }, function() {
                            swal.close();
                            // window.location.href = window.location.href;
                        });

                        $("#msg_email_sent").html(detail_msg.email_sent);
                       
                    }
                    
                },
                //init
                $.SweetAlert = new SweetAlert, $.SweetAlert.Constructor = SweetAlert
            }(window.jQuery),

            function($) {
                "use strict";
                $.SweetAlert.init()
            }(window.jQuery);
            
        }
    </script>
</body>
</html>
