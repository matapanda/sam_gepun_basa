<?php
    $url_main = base_url()."user/resume";

    $url_home = base_url()."user/beranda";

    $main_controller = base_url()."user/resumemain/";

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <!-- <h4 class="card-title m-b-0"><?=$title?></h4> -->
                </div>
                <div class="card-body collapse show">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="vtabs ">
                                <ul class="nav nav-tabs tabs-vertical" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" data-toggle="tab" href="#profil" role="tab" aria-selected="true" id="ls_ck_profile">
                                            <span><i class="mdi mdi-home-variant"></i></span>&nbsp;&nbsp;Data Kependudukan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#ketenagakerjaan" role="tab" aria-selected="true" id="ls_user_kerja">
                                            <span><i class="mdi mdi-account-edit"></i></span>&nbsp;&nbsp;Data Ketenagakerjaan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_kesehatan" role="tab" aria-selected="true" id="ls_user_kesehatan">
                                            <span><i class="mdi mdi-ambulance"></i></span>&nbsp;&nbsp;Data Kesehatan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_keuangan" role="tab" aria-selected="true" id="ls_user_keuangan">
                                            <span><i class="mdi mdi-cash-usd"></i></span>&nbsp;&nbsp;Data Keuangan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_or" role="tab" aria-selected="true" id="ls_user_or">
                                            <span><i class="mdi mdi-school"></i></span>&nbsp;&nbsp;Data Kegiatan dan Organisasi
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_pendidikan" role="tab" aria-selected="true" id="ls_user_pendidikan">
                                            <span><i class="mdi mdi-school"></i></span>&nbsp;&nbsp;Data Pendidikan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_sosial" role="tab" aria-selected="true" id="ls_user_sosial">
                                            <span><i class="mdi mdi-cash"></i></span>&nbsp;&nbsp;Data Sosial Ekonomi
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_tmpt" role="tab" aria-selected="true" id="ls_user_tmpt">
                                            <span><i class="mdi mdi-car"></i></span>&nbsp;&nbsp;Data Lingkungan
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="nav-link" data-toggle="tab" href="#user_transport" role="tab" aria-selected="true" id="ls_user_transport">
                                            <span><i class="mdi mdi-hops"></i></span>&nbsp;&nbsp;Data Transportasi dan Komunikasi
                                        </a>
                                    </li>

                                    
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content" style="width: 100%;">
                                    <div class="tab-pane active" id="profil" role="tabpanel">

                                        <?php
                                            include "resume_profile.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="ketenagakerjaan" role="tabpanel">

                                        <?php
                                            include "resume_user_kerja.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_kesehatan" role="tabpanel">

                                        <?php
                                            include "resume_user_kesehatan.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_keuangan" role="tabpanel">

                                        <?php
                                            include "resume_user_keuangan.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_or" role="tabpanel">

                                        <?php
                                            include "resume_user_or.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_pendidikan" role="tabpanel">

                                        <?php
                                            include "resume_user_pendidikan.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_sosial" role="tabpanel">

                                        <?php
                                            include "resume_user_sosial.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_tmpt" role="tabpanel">

                                        <?php
                                            include "resume_user_tmpt.php";
                                        ?>
                                    </div>
                                    <div class="tab-pane" id="user_transport" role="tabpanel">

                                        <?php
                                            include "resume_user_transport.php";
                                        ?>
                                    </div>
                                </div>
                            </div>        
                        </div>
                    </div>
                </div>

                <div class="card-footer text-right">
                    <?php
                    if(!$resume){
                        if(!array_search("", $inject_list)){
                            print_r('<button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Submit Data</button>
                            <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;"> Loading...</button>');
                        }
                    }
                    ?>
                    
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>


<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->

<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script>
    
    function klik_ck_profile(){
        var ck_profile = $("#ck_profile").is(":checked");
        if (ck_profile) {
            $("#ls_ck_profile").css("color", "#009933");
        } else {
            $("#ls_ck_profile").removeAttr("style", true);
        }
    }

    function klik_ck_user_kerja(){
        var ck_user_kerja = $("#ck_user_kerja").is(":checked");
        if (ck_user_kerja) {
            $("#ls_user_kerja").css("color", "#009933");
        } else {
            $("#ls_user_kerja").removeAttr("style", true);
        }
    }

    function klik_ck_user_kesehatan(){
        var ck_user_kesehatan = $("#ck_user_kesehatan").is(":checked");
        if (ck_user_kesehatan) {
            $("#ls_user_kesehatan").css("color", "#009933");
        } else {
            $("#ls_user_kesehatan").removeAttr("style", true);
        }
    }

    function klik_ck_user_keuangan(){
        var ck_user_keuangan = $("#ck_user_keuangan").is(":checked");
        if (ck_user_keuangan) {
            $("#ls_user_keuangan").css("color", "#009933");
        } else {
            $("#ls_user_keuangan").removeAttr("style", true);
        }
    }

    function klik_ck_user_or(){
        var ck_user_or = $("#ck_user_or").is(":checked");
        if (ck_user_or) {
            $("#ls_user_or").css("color", "#009933");
        } else {
            $("#ls_user_or").removeAttr("style", true);
        }
    }

    function klik_ck_user_pendidikan(){
        var ck_user_pendidikan = $("#ck_user_pendidikan").is(":checked");
        if (ck_user_pendidikan) {
            $("#ls_user_pendidikan").css("color", "#009933");
        } else {
            $("#ls_user_pendidikan").removeAttr("style", true);
        }
    }

    function klik_ck_user_sosial(){
        var ck_user_sosial = $("#ck_user_sosial").is(":checked");
        if (ck_user_sosial) {
            $("#ls_user_sosial").css("color", "#009933");
        } else {
            $("#ls_user_sosial").removeAttr("style", true);
        }
    }

    function klik_ck_user_tmpt(){
        var ck_user_tmpt = $("#ck_user_tmpt").is(":checked");
        if (ck_user_tmpt) {
            $("#ls_user_tmpt").css("color", "#009933");
        } else {
            $("#ls_user_tmpt").removeAttr("style", true);
        }
    }

    function klik_ck_user_transport(){
        var ck_user_transport = $("#ck_user_transport").is(":checked");
        if (ck_user_transport) {
            $("#ls_user_transport").css("color", "#009933");
        } else {
            $("#ls_user_transport").removeAttr("style", true);
        }
    }

    //=========================================================================//
    //-----------------------------------add_detail_data-----------------------//
    //=========================================================================//
    
    $("#update").click(function() {
        
        var data_main = new FormData();          
        data_main.append('ck_profile', $("#ck_profile").is(":checked") ? $("#ck_profile").val() : "");
        data_main.append('ck_user_kerja', $("#ck_user_kerja").is(":checked") ? $("#ck_user_kerja").val() : "");
        data_main.append('ck_user_kesehatan', $("#ck_user_kesehatan").is(":checked") ? $("#ck_user_kesehatan").val() : "");
        data_main.append('ck_user_keuangan', $("#ck_user_keuangan").is(":checked") ? $("#ck_user_keuangan").val() : "");
        data_main.append('ck_user_or', $("#ck_user_or").is(":checked") ? $("#ck_user_or").val() : "");
        data_main.append('ck_user_pendidikan', $("#ck_user_pendidikan").is(":checked") ? $("#ck_user_pendidikan").val() : "");
        data_main.append('ck_user_sosial', $("#ck_user_sosial").is(":checked") ? $("#ck_user_sosial").val() : "");

        data_main.append('ck_user_tmpt', $("#ck_user_tmpt").is(":checked") ? $("#ck_user_tmpt").val() : "");
        data_main.append('ck_user_transport', $("#ck_user_transport").is(":checked") ? $("#ck_user_transport").val() : "");
        
        
        $.ajax({
            url: "<?=$main_controller?>insert",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            beforeSend: () => {
                $('#update').hide();
                $('#update').attr('disabled', true);
                $('#btn-loading').show();
            },
            complete: () => {
                $('#update').show();
                $('#update').attr('disabled', false);
                $('#btn-loading').hide();
            },
            success: function(res) {
                response_update(res);
                console.log(res);
            }
        });
    });

    function response_update(res) {
        var data_json = JSON.parse(res);
        var main_msg = data_json.msg_main;
        var detail_msg = data_json.msg_detail;
        if (main_msg.status) {
            create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$url_home?>");
        } else {
            if(detail_msg.ck_profile){
                $("#ls_ck_profile").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_kerja){
                $("#ls_user_kerja").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_kesehatan){
                $("#ls_user_kesehatan").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_keuangan){
                $("#ls_user_keuangan").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_or){
                $("#ls_user_or").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_pendidikan){
                $("#ls_user_pendidikan").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_sosial){
                $("#ls_user_sosial").css("color", "#b32d00");
            }
            if(detail_msg.ck_user_tmpt){
                $("#ls_user_tmpt").css("color", "#b32d00");
            }

            if(detail_msg.ck_user_transport){
                $("#ls_user_transport").css("color", "#b32d00");
            }
            

            create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
        }
    }
</script>