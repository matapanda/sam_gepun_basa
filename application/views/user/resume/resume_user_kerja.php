<?php
    $id_user_kerja = "";
    $id_user = "";
    $sts_kerja = "";
    $sts_kerja_ket = "";
    $jenis_kerja = "";
    $tempat_kerja = "";
    $bidang_pekerjaan = "";
    $penghasilan_perbulan = "";
    $q_terima_pensiun = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    if(isset($user_kerja)){
        if($user_kerja){
            $id_user_kerja = $user_kerja["id_user_kerja"];
            $id_user = $user_kerja["id_user"];
            $sts_kerja = $user_kerja["sts_kerja"];
            $sts_kerja_ket = $user_kerja["sts_kerja_ket"];
            $jenis_kerja = $user_kerja["jenis_kerja"];
            $tempat_kerja = $user_kerja["tempat_kerja"];
            $bidang_pekerjaan = $user_kerja["bidang_pekerjaan"];
            $penghasilan_perbulan = $user_kerja["penghasilan_perbulan"];
            $q_terima_pensiun = $user_kerja["q_terima_pensiun"];
            $r_crt_by = $user_kerja["r_crt_by"];
            $r_crt_time = $user_kerja["r_crt_time"];
            $r_up_by = $user_kerja["r_up_by"];
            $r_up_time = $user_kerja["r_up_time"];

        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Status Kerja :</label>
                <div class="col-md-8">
                    <?= strtoupper($sts_kerja)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jenis Pekerjaan :</label>
                <div class="col-md-8">
                    <?= strtoupper($jenis_kerja)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Bidang Pekerjaan :</label>
                <div class="col-md-8">
                <?= strtoupper($bidang_pekerjaan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Penghasilan Per Bulan :</label>
                <div class="col-md-8">
                    <?= strtoupper($penghasilan_perbulan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Menerima Pensiun ? :</label>
                <div class="col-md-8">
                    <?= strtoupper($q_terima_pensiun)?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_kerja" name="check_data[]" value="ck_user_kerja" class="filled-in" onclick="klik_ck_user_kerja()">
                    <label for="ck_user_kerja">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    