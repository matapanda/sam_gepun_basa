<?php
    $id_user_kesehatan = "";
    $id_user = "";
    $bb = "";
    $tb = "";
    $q_vaksin = "";
    $d_vaksin = "";
    $q_kebutuhan_khusus = "";
    $q_butawarna = "";
    $q_expektor_kb = "";
    $kb_jenis = "";
    $q_stunting = "";
    $stnt_lingkar_kpl = "";
    $q_ibu_hamil = "";
    $hml_rutin_pemeriksaan = "";
    $hml_usia_kehamilan = "";
    $hml_tmp_periksa = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";
    

    if(isset($user_kesehatan)){
        if($user_kesehatan){
            $id_user_kesehatan = $user_kesehatan["id_user_kesehatan"];
            $id_user = $user_kesehatan["id_user"];
            $bb = $user_kesehatan["bb"];
            $tb = $user_kesehatan["tb"];
            $q_vaksin = $user_kesehatan["q_vaksin"];
            $d_vaksin = $user_kesehatan["d_vaksin"];
            $q_kebutuhan_khusus = $user_kesehatan["q_kebutuhan_khusus"];
            $q_butawarna = $user_kesehatan["q_butawarna"];
            $q_expektor_kb = $user_kesehatan["q_expektor_kb"];
            $kb_jenis = $user_kesehatan["kb_jenis"];
            $q_stunting = $user_kesehatan["q_stunting"];
            $stnt_lingkar_kpl = $user_kesehatan["stnt_lingkar_kpl"];
            $q_ibu_hamil = $user_kesehatan["q_ibu_hamil"];
            $hml_rutin_pemeriksaan = $user_kesehatan["hml_rutin_pemeriksaan"];
            $hml_usia_kehamilan = $user_kesehatan["hml_usia_kehamilan"];
            $hml_tmp_periksa = $user_kesehatan["hml_tmp_periksa"];
            $r_crt_by = $user_kesehatan["r_crt_by"];
            $r_crt_time = $user_kesehatan["r_crt_time"];
            $r_up_by = $user_kesehatan["r_up_by"];
            $r_up_time = $user_kesehatan["r_up_time"];
        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Berat Badan / Tinggi Badan :</label>
                <div class="col-md-8">
                    <?= strtoupper($bb)." / ".$tb?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Berkebutuhan Khusus</label>
                <div class="col-md-8">
                    <?= strtoupper($q_kebutuhan_khusus)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Buta Warna</label>
                <div class="col-md-8">
                <?= strtoupper($q_butawarna)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Akseptor KB</label>
                <div class="col-md-8">
                    <?= strtoupper($q_expektor_kb)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Sudah Vaksin Covid-19:</label>
                <div class="col-md-8">
                    <?= strtoupper($q_vaksin)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah anda rutin memeriksakan kehamilan</label>
                <div class="col-md-8">
                    <?= strtoupper($hml_rutin_pemeriksaan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Usia Kehamilan</label>
                <div class="col-md-8">
                    <?= strtoupper($hml_usia_kehamilan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Tempat Periksa Kehamilan</label>
                <div class="col-md-8">
                    <?= strtoupper($hml_tmp_periksa)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Stunting</label>
                <div class="col-md-8">
                    <?= strtoupper($q_stunting)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Lingkar Kepala</label>
                <div class="col-md-8">
                    <?= strtoupper($stnt_lingkar_kpl)?>
                </div>
            </div>
            
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_kesehatan" name="check_data[]" value="ck_user_kesehatan" class="filled-in" onclick="klik_ck_user_kesehatan()">
                    <label for="ck_user_kesehatan">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    