<?php
    $id_user_or = "";
    $id_user = "";
    $or_hobby = "";
    $q_keg_pengamalan_pancasila = "";
    $pc_detail = "";
    $q_kerjabakti = "";
    $q_rukun_kematian = "";
    $q_keg_keagamaan = "";
    $q_jimpitan = "";
    $q_arisan = "";
    $q_gotong_royong = "";
    $q_ikut_organisasi = "";
    $or_detail = "";
    $q_ikut_pelatihan = "";
    $pel_detail = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";

    $str_pc_detail = "";
    $str_or_detail = "";
    $str_pel_detail = "";
     

    if(isset($user_or)){
        if($user_or){
            $id_user_or = $user_or["id_user_or"];
            $id_user = $user_or["id_user"];
            $or_hobby = $user_or["or_hobby"];
            $q_keg_pengamalan_pancasila = $user_or["q_keg_pengamalan_pancasila"];
            $pc_detail = json_decode($user_or["pc_detail"]);
            if($pc_detail){
                $no = 1;
                foreach ($pc_detail as $key => $value) {
                    $str_pc_detail .= "<tr>
                                            <td>".$no."</td>
                                            <td>".$value->nm_keg_pancasila."</td>
                                        </tr>";
                    
                    $no++;
                }
            }

            $q_kerjabakti = $user_or["q_kerjabakti"];
            $q_rukun_kematian = $user_or["q_rukun_kematian"];
            $q_keg_keagamaan = $user_or["q_keg_keagamaan"];
            $q_jimpitan = $user_or["q_jimpitan"];
            $q_arisan = $user_or["q_arisan"];
            $q_gotong_royong = $user_or["q_gotong_royong"];
            $q_ikut_organisasi = $user_or["q_ikut_organisasi"];
            $or_detail = json_decode($user_or["or_detail"]);
            if($or_detail){
                $no = 1;
                foreach ($or_detail as $key => $value) {
                    $str_or_detail .= "<tr>
                                        <td>".$no."</td>
                                        <td>".$value->nm_organisasi_detail."</td>
                                        <td>".$value->jns_organisasi_detail."</td>
                                        <td>".$value->jbtn_organisasi_detail."</td>
                                        <td>".$value->thn_organisasi_detail."</td>
                                    </tr>";

                    $no++;
                }
            }

            $q_ikut_pelatihan = $user_or["q_ikut_pelatihan"];
            $pel_detail = json_decode($user_or["pel_detail"]);
            if($pel_detail){
                $no = 1;
                foreach ($pel_detail as $key => $value) {
                    $str_pel_detail .= "<tr>
                                        <td>".$no."</td>
                                        <td>".$value->nm_pelatihan_detail."</td>
                                        <td>".$value->penyelenggara_pelatihan_detail."</td>
                                        <td>".$value->jn_str."</td>
                                        <td>".$value->thn_pelatihan_detail."</td>
                                    </tr>";

                    $no++;
                }
            }

            $r_crt_by = $user_or["r_crt_by"];
            $r_crt_time = $user_or["r_crt_time"];
            $r_up_by = $user_or["r_up_by"];
            $r_up_time = $user_or["r_up_time"];

        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Hobi</label>
                <div class="col-md-8">
                    <?= strtoupper($or_hobby)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kegiatan Penghayatan dan Pengamalan Pancasila</label>
                <div class="col-md-8">
                    <?= strtoupper($q_keg_pengamalan_pancasila)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Detail Kegiatan</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Nama Kegiatan Pancasila</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_pc_detail?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kerja Bakti</label>
                <div class="col-md-8">
                <?= strtoupper($q_kerjabakti)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Rukun Kematian</label>
                <div class="col-md-8">
                    <?= strtoupper($q_rukun_kematian)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kegiatan Keagamaan</label>
                <div class="col-md-8">
                    <?= strtoupper($q_keg_keagamaan)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jimpitan</label>
                <div class="col-md-8">
                    <?= strtoupper($q_jimpitan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Arisan</label>
                <div class="col-md-8">
                    <?= strtoupper($q_arisan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Gotong Royong</label>
                <div class="col-md-8">
                    <?= strtoupper($q_gotong_royong)?>
                </div>
            </div>

            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda pernah mengikuti Organisasi?</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ikut_organisasi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Detail Organisasi</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Nama Organisasi</td>
                                <td>Jenis Organisasi</td>
                                <td>Jabatan Organisasi</td>
                                <td>Tahun Organisasi</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_or_detail?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda pernah mengikuti Pelatihan?</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ikut_pelatihan)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Detail Pelatihan</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Nama Pelatihan</td>
                                <td>Penyelenggara Pelatihan</td>
                                <td>Sertifikat</td>
                                <td>Tahun Pelatihan</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_pel_detail?>
                        </tbody>
                    </table>
                </div>
            </div>            
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_or" name="check_data[]" value="ck_user_or" class="filled-in" onclick="klik_ck_user_or()">
                    <label for="ck_user_or">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    