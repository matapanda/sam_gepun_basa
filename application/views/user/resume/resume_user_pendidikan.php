<?php
    $id_user_pendidikan = "";
    $id_user = "";
    $d_pendidikan = "";
    $q_buta_huruf = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";
    
    $str_d_pendidikan = [];

    if(isset($user_pendidikan)){
        if($user_pendidikan){
            $id_user_pendidikan = $user_pendidikan["id_user_pendidikan"];
            $id_user = $user_pendidikan["id_user"];
            $d_pendidikan = json_decode($user_pendidikan["d_pendidikan"]);
            $q_buta_huruf = $user_pendidikan["q_buta_huruf"];
            if($d_pendidikan){
                $no = 1;
                foreach ($d_pendidikan as $key => $value) {
                    $str_d_pendidikan = "<tr>
                                            <td>".$no."</td>
                                            <td>".$value->jenjang_pd."</td>
                                            <td>".$value->nm_sklh."</td>
                                            <td>".$value->jurusan_pd."</td>
                                            <td>".$value->th_lulus."</td>
                                        </tr>";

                    $no++;
                }
            }
            
            $r_crt_by = $user_pendidikan["r_crt_by"];
            $r_crt_time = $user_pendidikan["r_crt_time"];
            $r_up_by = $user_pendidikan["r_up_by"];
            $r_up_time = $user_pendidikan["r_up_time"];
        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah anda buta huruf ?</label>
                <div class="col-md-8">
                    <?= strtoupper($q_buta_huruf)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Riwayat Pendidikan</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Jenjang</td>
                                <td>Nama Sekolah</td>
                                <td>Jurusan</td>
                                <td>Tahun lulus</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_d_pendidikan?>
                        </tbody>
                    </table>
                </div>
            </div>         
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_pendidikan" name="check_data[]" value="ck_user_pendidikan" class="filled-in" onclick="klik_ck_user_pendidikan()">
                    <label for="ck_user_pendidikan">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    