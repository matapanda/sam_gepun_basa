<?php
    
    $q_anggota_dasawisma = "";
    $dw_nama = "";
    $q_activitas_pyd = "";
    $pyd_frequensi = "";
    $q_program_bina_balita = "";
    $q_ikut_kel_belajar = "";
    $q_paud = "";
    $q_ikut_kooperasi = "";
    $q_punya_tabungan = "";
    $q_makan_pokok = "";
    $q_punya_usaha = "";
    $usaha_skala = "";
    $usaha_no = "";
    $usaha_bidang = "";

    if(isset($user_sosial)){
        if($user_sosial){
          
            $q_anggota_dasawisma = $user_sosial["q_anggota_dasawisma"];
            $dw_nama = $user_sosial["dw_nama"];
            $q_activitas_pyd = $user_sosial["q_activitas_pyd"];
            $pyd_frequensi = $user_sosial["pyd_frequensi"];
            $q_program_bina_balita = $user_sosial["q_program_bina_balita"];
            $q_ikut_kel_belajar = $user_sosial["q_ikut_kel_belajar"];
            $q_paud = $user_sosial["q_paud"];
            $q_ikut_kooperasi = $user_sosial["q_ikut_kooperasi"];
            $q_punya_tabungan = $user_sosial["q_punya_tabungan"];
            $q_makan_pokok = $user_sosial["q_makan_pokok"];
            $q_punya_usaha = $user_sosial["q_punya_usaha"];
            $usaha_skala = $user_sosial["usaha_skala"];
            $usaha_no = $user_sosial["usaha_no"];
            $usaha_bidang = $user_sosial["usaha_bidang"];
        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Anggota Dasawisma</label>
                <div class="col-md-8">
                    <?= strtoupper($q_anggota_dasawisma)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Nama Dasawisma</label>
                <div class="col-md-8">
                    <?= strtoupper($dw_nama)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Aktif Dalam Posyandu</label>
                <div class="col-md-8">
                <?= strtoupper($q_activitas_pyd)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Frekuensi Ke-Posyandu</label>
                <div class="col-md-8">
                    <?= strtoupper($pyd_frequensi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti Program Bina Keluarga Balita</label>
                <div class="col-md-8">
                    <?= strtoupper($q_program_bina_balita)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti Kelompok Belajar</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ikut_kel_belajar)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti PAUD / Sejenis</label>
                <div class="col-md-8">
                    <?= strtoupper($q_paud)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Ikut dalam Kegiatan Koperasi</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ikut_kooperasi)?>
                </div>
            </div>

            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Anda Memiliki Tabungan</label>
                <div class="col-md-8">
                    <?= strtoupper($q_punya_tabungan)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Makanan Pokok Sehari-hari</label>
                <div class="col-md-8">
                    <?= strtoupper($q_makan_pokok)?>
                </div>
            </div>  
            
            <br>
                <h4>Data Usaha</h4>
            <br>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Punya Usaha</label>
                <div class="col-md-8">
                    <?= strtoupper($q_punya_usaha)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Skala Usaha</label>
                <div class="col-md-8">
                    <?= strtoupper($usaha_skala)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Nomor Induk Berusaha (NIB)</label>
                <div class="col-md-8">
                    <?= strtoupper($usaha_no)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Bidang Usaha</label>
                <div class="col-md-8">
                    <?= strtoupper($usaha_bidang)?>
                </div>
            </div>            
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_sosial" name="check_data[]" value="ck_user_sosial" class="filled-in" onclick="klik_ck_user_sosial()">
                    <label for="ck_user_sosial">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    