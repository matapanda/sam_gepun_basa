<?php
    
    $q_punya_rumah = "";
    $py_rmh_sts = "";
    $py_rmh_kondisi = "";
    $py_rmh_jn_lantai = "";
    $py_rmh_kriteria_rmh = "";
    $q_ada_listrik = "";
    $lstr_jenis = "";
    $q_ada_sanitasi = "";
    $sn_jenis = "";
    $q_saluran_limbah = "";
    $q_air_bersih = "";
    $ab_sumber_air = "";
    $q_pembuangan_sampah = "";

    if(isset($user_tmpt)){
        if($user_tmpt){
          
            $q_punya_rumah = $user_tmpt["q_punya_rumah"];
            $py_rmh_sts = $user_tmpt["py_rmh_sts"];
            $py_rmh_kondisi = $user_tmpt["py_rmh_kondisi"];
            $py_rmh_jn_lantai = $user_tmpt["py_rmh_jn_lantai"];
            $py_rmh_kriteria_rmh = $user_tmpt["py_rmh_kriteria_rmh"];
            $q_ada_listrik = $user_tmpt["q_ada_listrik"];
            $lstr_jenis = $user_tmpt["lstr_jenis"];
            $q_ada_sanitasi = $user_tmpt["q_ada_sanitasi"];
            $sn_jenis = $user_tmpt["sn_jenis"];
            $q_saluran_limbah = $user_tmpt["q_saluran_limbah"];
            $q_air_bersih = $user_tmpt["q_air_bersih"];
            $ab_sumber_air = $user_tmpt["ab_sumber_air"];
            $q_pembuangan_sampah = $user_tmpt["q_pembuangan_sampah"];
        }
    }
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Punya Tempat Tinggal</label>
                <div class="col-md-8">
                    <?= strtoupper($q_punya_rumah)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Status Rumah</label>
                <div class="col-md-8">
                    <?= strtoupper($py_rmh_sts)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kondisi Bangunan</label>
                <div class="col-md-8">
                <?= strtoupper($py_rmh_kondisi)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Jenis Lantai Terluas Rumah</label>
                <div class="col-md-8">
                    <?= strtoupper($py_rmh_jn_lantai)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Kriteria Rumah</label>
                <div class="col-md-8">
                    <?= strtoupper($py_rmh_kriteria_rmh)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Apakah Tempat Tinggal Menggunakan Listrik</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ada_listrik)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jenis Listrik</label>
                <div class="col-md-8">
                    <?= strtoupper($lstr_jenis)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Ada Sanitasi(MCK)</label>
                <div class="col-md-8">
                    <?= strtoupper($q_ada_sanitasi)?>
                </div>
            </div>

            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jenis Sanitasi</label>
                <div class="col-md-8">
                    <?= strtoupper($sn_jenis)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Memiliki Saluran Pembuangan Air Limbah</label>
                <div class="col-md-8">
                    <?= strtoupper($q_saluran_limbah)?>
                </div>
            </div>  
            
            <br>
                <h4>Data Usaha</h4>
            <br>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Ketersediaan Air Bersih</label>
                <div class="col-md-8">
                    <?= strtoupper($q_air_bersih)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Sumber Air Keluarga</label>
                <div class="col-md-8">
                    <?= strtoupper($ab_sumber_air)?>
                </div>
            </div>  
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Memiliki Tempat Pembuangan Sampah</label>
                <div class="col-md-8">
                    <?= strtoupper($q_pembuangan_sampah)?>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_tmpt" name="check_data[]" value="ck_user_tmpt" class="filled-in" onclick="klik_ck_user_tmpt()">
                    <label for="ck_user_tmpt">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    