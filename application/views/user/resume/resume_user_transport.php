<?php
   
   $id_user_transport = "";
   $id_user = "";
   $q_punya_tr = "";
   $tr_detail = "";
   $q_akses_internet = "";
   $jn_internet_akses = "";
   $q_punya_perangkat = "";
   $prng_detail = "";
   $r_crt_by = "";
   $r_crt_time = "";
   $r_up_by = "";
   $r_up_time = "";

   
   $str_tr_detail = "";
   $str_prng_detail = "";

   if(isset($user_transport)){
       if($user_transport){
            $id_user_transport = $user_transport["id_user_transport"];
            $id_user = $user_transport["id_user"];
            
            $q_punya_tr = $user_transport["q_punya_tr"];
            $tr_detail = json_decode($user_transport["tr_detail"]);
            if($tr_detail){
                $no = 1;
                foreach ($tr_detail as $key => $value) {
                    $str_tr_detail = "<tr>
                                            <td>".$no."</td>
                                            <td>".$value->jns_tr_detail."</td>
                                            <td>".$value->jml_tr_detail."</td>
                                            <td>".$value->sts_tr_detail."</td>
                                        </tr>";

                    $no++;
                }
            }

            $q_akses_internet = $user_transport["q_akses_internet"];
            $jn_internet_akses = $user_transport["jn_internet_akses"];

            $q_punya_perangkat = $user_transport["q_punya_perangkat"];
            
            $prng_detail = json_decode($user_transport["prng_detail"]);
            if($prng_detail){
                $no = 1;
                try {
                    foreach ($prng_detail as $key => $value) {
                        $str_prng_detail = "<tr>
                                                <td>".$no."</td>
                                                <td>".$value->jns_prng_detail."</td>
                                                <td>".$value->jml_prng_detail."</td>
                                            </tr>";
    
                        $no++;
                    }
                } catch (\Throwable $th) {
                    //throw $th;
                }
                
            }

            $r_crt_by = $user_transport["r_crt_by"];
            $r_crt_time = $user_transport["r_crt_time"];
            $r_up_by = $user_transport["r_up_by"];
            $r_up_time = $user_transport["r_up_time"];

       }
   }

    
    
?>    
    
    <div class="row">
        <div class="col-md-6">
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Punya Moda Transportasi Pribadi</label>
                <div class="col-md-8">
                    <?= strtoupper($q_punya_tr)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">List Data Trasnportasi</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Jenis</td>
                                <td>Jumlah</td>
                                <td>Kepemilikan</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_tr_detail?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Ada Akses Internet</label>
                <div class="col-md-8">
                <?= strtoupper($q_akses_internet)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Jenis Internet</label>
                <div class="col-md-8">
                    <?= strtoupper($jn_internet_akses)?>
                </div>
            </div>
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">Punya Perangkat Telepon / Komputer</label>
                <div class="col-md-8">
                    <?= strtoupper($q_punya_perangkat)?>
                </div>
            </div>
            
            <div class="form-group row mb-0">
                <label for="message-text" class="control-label col-md-4">List Data Perangkat</label>
                <div class="col-md-8">
                    <table border="1" width="100%" style="border-collapse: collapse;">
                        <thead>
                            <tr>
                                <td>No. </td>
                                <td>Perangkat</td>
                                <td>Jumlah</td>
                            </tr>
                        </thead>
                        <tbody>
                            <?= $str_prng_detail?>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>

        <div class="col-md-12">
            <br><br>
            <div class="form-group row mb-0">
                <div class="demo-checkbox">
                    <input type="checkbox" id="ck_user_transport" name="check_data[]" value="ck_user_transport" class="filled-in" onclick="klik_ck_user_transport()">
                    <label for="ck_user_transport">Centang input ini untuk memastikan bahwa yang data yang anda simpan adalah benar.</label>
                </div>
            </div>
        </div>
    </div>
    