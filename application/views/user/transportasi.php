<?php
    $url_main = base_url()."user/transportasi";

    $main_controller = base_url()."user/Transkomuser/";


    $str_op_jenis_transportasi = "";
    if(isset($jenis_transportasi)){
        if($jenis_transportasi){
            foreach ($jenis_transportasi as $key => $value) {
                $str_op_jenis_transportasi .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_sts_kepemilikan = "";
    if(isset($sts_kepemilikan)){
        if($sts_kepemilikan){
            foreach ($sts_kepemilikan as $key => $value) {
                $str_op_sts_kepemilikan .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jenis_internet = "";
    if(isset($jenis_internet)){
        if($jenis_internet){
            foreach ($jenis_internet as $key => $value) {
                $str_op_jenis_internet .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_jenis_perangkat = "";
    if(isset($jenis_perangkat)){
        if($jenis_perangkat){
            foreach ($jenis_perangkat as $key => $value) {
                $str_op_jenis_perangkat .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    


    $id_user_transport = "";
    $id_user = "";
    $q_punya_tr = "";
    $tr_detail = "";
    $q_akses_internet = "";
    $jn_internet_akses = "";
    $q_punya_perangkat = "";
    $prng_detail = "";
    $r_crt_by = "";
    $r_crt_time = "";
    $r_up_by = "";
    $r_up_time = "";


    if(isset($list_data)){
        if($list_data){
            $id_user_transport = $list_data["id_user_transport"];
            $id_user = $list_data["id_user"];
            
            $q_punya_tr = $list_data["q_punya_tr"];
            $tr_detail = $list_data["tr_detail"];
            
            $q_akses_internet = $list_data["q_akses_internet"];
            $jn_internet_akses = $list_data["jn_internet_akses"];
            
            $prng_detail = $list_data["prng_detail"];

            $r_crt_by = $list_data["r_crt_by"];
            $r_crt_time = $list_data["r_crt_time"];
            $r_up_by = $list_data["r_up_by"];
            $r_up_time = $list_data["r_up_time"];

        }
    }
    
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-md-12">
            <p class="note"><label for="inp_catatan">Catatan :</label><br><br> 
            <b>1.</b> Berikan tanda strip(-) jika tidak ada <br>
            <b>2.</b> Lama bekerja /bulan <br>
            </p>
        </div>
    </div> -->
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0">Transportasi</h4>
                    <hr>
                </div>
                <div class="card-body collapse show">
                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                          <label for="message-text" class="control-label col-md-4">Punya Moda Transportasi Pribadi</label>
                          <div class="col-md-8">
                            <input class="q_anggota" type="radio" name="q_punya_tr" value="iya" id="q_punya_tr1" onclick="show_d_trans()">
                            <label for="q_punya_tr1">Iya</label>
                            <input class="q_anggota" type="radio" name="q_punya_tr" value="tidak" id="q_punya_tr2" onclick="show_d_trans()">
                            <label for="q_punya_tr2">Tidak</label>
                            <p id="msg_q_punya_tr" style="color: red;"></p>
                          </div>
                          <div id="path_transportasi" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th width="10%">No</th>
                                    <th width="25%">Jenis Transportasi</th>
                                    <th width="25%">Jumlah</th>
                                    <th width="25%">Status Kepemilikan</th>
                                    <th width="15%">Aksi</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      #
                                    </td>
                                    <td>
                                        <select class="form-control" id="jns_tr_detail" name="tr_detail">
                                            <option value="" selected>Pilih</option>
                                            <?=$str_op_jenis_transportasi?>
                                            <!-- <option value="mobil">1. Mobil</option>
                                            <option value="sepeda motor">2. Sepeda Motor</option>
                                            <option value="sepeda roda dua">3. Sepeda Roda Dua</option> -->
                                        </select>
                                      <p id="msg_tr_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <input type="number" class="form-control" id="jml_tr_detail" name="tr_detail" required="">
                                      <p id="msg_tr_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                        <select class="form-control" id="sts_tr_detail" name="tr_detail">
                                            <option value="" selected>Pilih</option>
                                            <?=$str_op_sts_kepemilikan?>
                                            <!-- <option value="milik sendiri">1. Milik Sendiri</option>
                                            <option value="pinjaman">2. Pinjaman</option> -->
                                        </select>
                                      <p id="msg_tr_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <button type="button" id="add_list_tr" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                    </td>
                                  </tr>
                                </thead>
                                <tbody id="out_list_tr"></tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>
                  </div>
              </div>

            <div class="card">
                <div class="card-body">
                    
                    <h4>Komunikasi</h4>
                    <hr>
                    <br>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                          <label for="message-text" class="control-label col-md-4">Ada Akses Internet</label>
                          <div class="col-md-8">
                            <input class="q_anggota" type="radio" name="q_akses_internet" value="iya" id="q_akses_internet1" onclick="show_d_int()">
                            <label for="q_akses_internet1">Iya</label>
                            <input class="q_anggota" type="radio" name="q_akses_internet" value="tidak" id="q_akses_internet2" onclick="show_d_int()">
                            <label for="q_akses_internet2">Tidak</label>
                            <p id="msg_q_akses_internet" style="color: red;"></p>
                          </div>
                          <div id="path_internet" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                            <div class="form-group row">
                                <label for="message-text" class="control-label col-md-4">Jenis Internet</label>
                                    <div class="col-md-4">
                                        <select class="custom-select form-control input-sm m-b-10" id="jn_internet_akses" name="jn_internet_akses">
                                            <option value="" selected>Pilih</option>
                                            <!-- <option value="internet mandiri">1. Internet Mandiri</option>
                                            <option value="disediakan pemerintah">2. Disediakan Pemerintah</option>
                                            <option value="Internet Usaha Warga Sekitar">3. Internet Usaha Warga Sekitar</option> -->
                                            <?=$str_op_jenis_internet?>
                                        </select>
                                        <p id="msg_jn_internet_akses" style="color: red;"></p>
                                    </div>
                            </div>
                          </div>
                        </div>
                      </div>

                      <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                          <label for="message-text" class="control-label col-md-4">Punya Perangkat Telepon / Komputer</label>
                          <div class="col-md-8">
                            <input class="q_anggota" type="radio" name="q_punya_perangkat" value="iya" id="q_punya_perangkat1" onclick="show_d_pr()">
                            <label for="q_punya_perangkat1">Iya</label>
                            <input class="q_anggota" type="radio" name="q_punya_perangkat" value="tidak" id="q_punya_perangkat2" onclick="show_d_pr()">
                            <label for="q_punya_perangkat2">Tidak</label>
                            <p id="msg_q_punya_perangkat" style="color: red;"></p>
                          </div>
                          <div id="path_perangkat" class="ya box col-md-12" style="padding-top: 1px; display: none ">
                            <div class="table-responsive">
                              <table class="table">
                                <thead>
                                  <tr>
                                    <th width="10%">No</th>
                                    <th width="25%">Jenis Perangkat</th>
                                    <th width="25%">Jumlah</th>
                                    <th width="15%">Aksi</th>
                                  </tr>
                                  <tr>
                                    <td>
                                      #
                                    </td>
                                    <td>
                                        <select class="form-control" id="jns_prng_detail" name="prng_detail">
                                            <option value="" selected>Pilih</option>
                                            <!-- <option value="telepon rumah">1. Telepon Rumah</option>
                                            <option value="handphone / ponsel">2. Handphone / Ponsel</option>
                                            <option value="komputer">3. Komputer</option> -->
                                            <?=$str_op_jenis_perangkat?>

                                        </select>
                                      <p id="msg_prng_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <input type="number" class="form-control" id="jml_prng_detail" name="prng_detail" required="">
                                      <p id="msg_prng_detail" style="color: red;"></p>
                                    </td>
                                    <td>
                                      <button type="button" id="add_list_pr" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                    </td>
                                  </tr>
                                </thead>
                                <tbody id="out_list_pr"></tbody>
                              </table>
                            </div>
                          </div>
                        </div>
                      </div>


                    </div>
                  </div>
                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                    <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;"> Loading...</button>
                </div>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">

    var list_tr = {};
    try {
      list_tr = JSON.parse('<?=$tr_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    var list_pr = {};
    try {
      list_pr = JSON.parse('<?=$prng_detail?>')
    }catch(err) {
        console.log("err json format");
    }

    $(document).ready(function(){
        set_param();


    })

    function set_param(){
        $("input[name='q_punya_tr'][value='<?=isset($q_punya_t) ? $q_punya_t : 'iya'?>']").prop("checked",true);
        $("input[name='q_akses_internet'][value='<?=isset($q_akses_internet) ? $q_akses_internet : 'iya'?>']").prop("checked",true);
        $("input[name='q_punya_perangkat'][value='<?=isset($q_akses_internet) ? $q_akses_internet : 'iya'?>']").prop("checked",true);

        $("#jn_internet_akses").val("<?=$jn_internet_akses?$jn_internet_akses:""?>")

        show_d_trans()
        show_d_int()
        show_d_pr()

        render_list_tr();
        render_list_pr();
    }
       
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
      $("#update").click(function() {
        var data_main = new FormData();
        data_main.append('q_punya_tr', $('input[name="q_punya_tr"]:checked').val() ? $('input[name="q_punya_tr"]:checked').val() : "");
        data_main.append('q_akses_internet', $('input[name="q_akses_internet"]:checked').val() ? $('input[name="q_akses_internet"]:checked').val() : "");
        data_main.append('q_punya_perangkat', $('input[name="q_punya_perangkat"]:checked').val() ? $('input[name="q_punya_perangkat"]:checked').val() : "");

        data_main.append('tr_detail', JSON.stringify(list_tr));
        data_main.append('jn_internet_akses', $("#jn_internet_akses").val());
        data_main.append('prng_detail', JSON.stringify(list_pr));
        
        $.ajax({
            url: "<?=$main_controller?>act_controller",
            dataType: 'html', // what to expect back from the PHP script, if anything
            cache: false,
            contentType: false,
            processData: false,
            data: data_main,
            type: 'post',
            beforeSend: () => {
                $('#update').hide();
                $('#update').attr('disabled', true);
                $('#btn-loading').show();
            },
            complete: () => {
                $('#update').show();
                $('#update').attr('disabled', false);
                $('#btn-loading').hide();
            },
            success: function(res) {
                response_update(res);
                console.log(res);
            }
        });
      });

      function response_update(res) {
          var data_json = JSON.parse(res);
          var main_msg = data_json.msg_main;
          var detail_msg = data_json.msg_detail;
          if (main_msg.status) {
              create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/transportasi");?>");
          } else {
              $("#msg_q_punya_tr").html(detail_msg.q_punya_tr);
              $("#msg_tr_jn").html(detail_msg.tr_jn);
              $("#msg_tr_jml").html(detail_msg.tr_jml);

              create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
          }
      }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------hide_and_show-------------------------//
    //=========================================================================//

        function show_d_trans(){
          var q_punya_tr = $('input[name="q_punya_tr"]:checked').val()
          if(q_punya_tr == "iya"){
            $("#path_transportasi").fadeIn()
          }else{
            $("#path_transportasi").fadeOut()
          }
        }

        function show_d_int(){
          var q_akses_internet = $('input[name="q_akses_internet"]:checked').val()
          if(q_akses_internet == "iya"){
            $("#path_internet").fadeIn()
          }else{
            $("#path_internet").fadeOut()
          }
        }

        function show_d_pr(){
          var q_punya_perangkat = $('input[name="q_punya_perangkat"]:checked').val()
          if(q_punya_perangkat == "iya"){
            $("#path_perangkat").fadeIn()
          }else{
            $("#path_internet").fadeOut()
          }
        }
    //=========================================================================//
    //-----------------------------------hide_and_show-------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_d_trans---------------------------//
    //=========================================================================//
        $("#add_list_tr").click(function(){
          add_list_tr();
        });

        function add_list_tr(){
            var tmp = {
                "jns_tr_detail":$("#jns_tr_detail").val(),
                "jml_tr_detail":$("#jml_tr_detail").val(),
                "sts_tr_detail":$("#sts_tr_detail").val()
            };

            if($("#jns_tr_detail").val() != "" &&
                $("#jml_tr_detail").val() != "" &&
                $("#sts_tr_detail").val() != ""){
                    var key = $("#jns_tr_detail").val().replaceAll(" ", "");
                    if(!(key in list_tr)){
                        list_tr[$("#jns_tr_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_tr();
                    }else{
                        alert("Data "+$("#jns_tr_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_tr(){
            var str_list_tr = "";
            if(list_tr){
                var no = 1;
                for (const i in list_tr) {
                    str_list_tr += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_tr[i].jns_tr_detail+"</td>"+
                                        "<td>"+list_tr[i].jml_tr_detail+"</td>"+
                                        "<td>"+list_tr[i].sts_tr_detail+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    no++;
                }
            }

            $("#out_list_tr").html(str_list_tr);
        }

        $("#add_data_vaksin").click(function(){
            add_list_tr();

            // console.log(list_tr);
        });


        function delete_list(key){
            // console.log(list_tr[key]);
            delete list_tr[key];

            render_list_tr();
        }
    //=========================================================================//
    //-----------------------------------add_d_trans---------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------add_d_perangkat-----------------------//
    //=========================================================================//
      $("#add_list_pr").click(function(){
          add_list_pr();
        });

        function add_list_pr(){
            var tmp = {
                "jns_prng_detail":$("#jns_prng_detail").val(),
                "jml_prng_detail":$("#jml_prng_detail").val()
            };

            if($("#jns_prng_detail").val() != "" &&
                $("#jml_prng_detail").val() != ""){
                    var key = $("#jns_prng_detail").val().replaceAll(" ", "");
                    if(!(key in list_pr)){
                        list_pr[$("#jns_prng_detail").val().replaceAll(" ", "")] = tmp;
                        render_list_pr();
                    }else{
                        alert("Data "+$("#jns_prng_detail").val()+" sudah dimasukkan !!");
                    }                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_pr(){
          // console.log(list_pr);
            var str_list_pr = "";
            if(list_pr){
                var no = 1;
                for (const i in list_pr) {
                    str_list_pr += "<tr>"+
                                        "<td align='center'><b>"+no+"</b></td>"+
                                        "<td>"+list_pr[i].jns_prng_detail+"</td>"+
                                        "<td>"+list_pr[i].jml_prng_detail+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list_pr('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                    no++;
                }
            }

            $("#out_list_pr").html(str_list_pr);
        }

        function delete_list_pr(key){
            console.log(list_pr[key]);
            delete list_pr[key];

            render_list_pr();
        }
    //=========================================================================//
    //-----------------------------------add_d_perangkat-----------------------//
    //=========================================================================//


</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->