<?php
    $main_controller = base_url()."user/umkm/";

    $path_img = base_url()."assets/prw/img/icon/";

    // var_dump($list_data);
    
?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->

<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
    <!-- <div class="row">
        <div class="col-md-12">
            <p class="note"><label for="inp_catatan">Catatan :</label><br><br> 
            <b>1.</b> Berikan tanda strip(-) jika tidak ada <br>
            <b>2.</b> Lama bekerja /bulan <br>
            </p>
        </div>
    </div> -->
    <!-- Row -->
    <div class="row">
        <div class="col-md-12">
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show">
                    <div class="form-group">
                        <label for="message-text" class="control-label">Punya UMKM ?</label><br>
                        <input class="radio" type="radio" name="q_memiliki_umkm" id="ya" value="ya">
                        <label for="ya">Iya</label>
                        <input class="radio" type="radio" name="q_memiliki_umkm" id="tidak" value="tidak">
                        <label for="tidak">Tidak</label>
                        <p id="msg_q_memiliki_umkm" style="color: red;"></p>
                    </div>
                    <div class="form-group">
                        <label for="message-text" class="control-label">Keterangan UMKM :</label>
                        <input type="text" class="form-control" id="j_data_umkm" name="j_data_umkm" >
                        <p id="msg_j_data_umkm" style="color: red;"></p>
                    </div>
                </div>

                <div class="card-footer text-right">
                <button type="submit" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan</button>
                    <button class="btn waves-effect waves-light btn-rounded btn-info" id="btn-loading" style="display: none;"> Loading...</button>
                </div>
            </div>
        </div>
    </div>
    <!-- End Row -->
</div>
<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $("input[name='q_memiliki_umkm'][value='<?=isset($list_data['q_memiliki_umkm']) ? $list_data['q_memiliki_umkm'] : ''?>']").prop("checked",true);
        $("#j_data_umkm").val("<?=isset($list_data['j_data_umkm']) ? $list_data['j_data_umkm'] : ''?>")
    })
       
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
    $("#update").click(function() {
            var data_main = new FormData();
            data_main.append('q_memiliki_umkm', $('input[name="q_memiliki_umkm"]:checked').val() ? $('input[name="q_memiliki_umkm"]:checked').val() : "");
            data_main.append('j_data_umkm', $("#j_data_umkm").val());

            $.ajax({
                url: "<?php echo base_url()."user/umkmuser/update";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                beforeSend: () => {
                    $('#update').hide();
                    $('#update').attr('disabled', true);
                    $('#btn-loading').show();
                },
                complete: () => {
                    $('#update').show();
                    $('#update').attr('disabled', false);
                    $('#btn-loading').hide();
                },
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/umkm");?>");
            } else {
                $("#msg_q_memiliki_umkm").html(detail_msg.q_memiliki_umkm);
                $("#msg_j_data_umkm").html(detail_msg.j_data_umkm);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
</script>

<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->