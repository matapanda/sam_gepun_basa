<?php
    $main_url_user = base_url()."user/pendidikan";
    $main_controller = base_url()."user/pendidikanuser/";

    $path_img = base_url()."assets/prw/img/icon/";


    $id_user_pendidikan = "";
    $id_user = "";
    $d_pendidikan = "[]";
    $q_buta_huruf = "";

    if(isset($list_data)){
        if($list_data){
            $id_user_pendidikan = $list_data["id_user_pendidikan"];
            $id_user = $list_data["id_user"];
            $d_pendidikan = $list_data["d_pendidikan"];
            $q_buta_huruf = $list_data["q_buta_huruf"];
        }
    }

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->
<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                </div>
                <div class="card-body collapse show" style="">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="message-text" class="control-label">Apakah anda buta huruf ?</label>

                                <select class="form-control" id="q_buta_huruf" name="q_buta_huruf">
                                    <option value="ya">IYA</option>
                                    <option value="tidak">TIDAK</option>
                                </select>
                                <p id="_msg_q_buta_huruf" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="table-responsive">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th width="25%">Jenjang Pendidikan</th>
                                            <th width="30%">Nama Sekolah</th>
                                            <th width="20%">Jurusan</th>
                                            <th width="15%">Tahun Lulus</th>
                                            <th width="10" class="text-nowrap">#</th>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select class="form-control" id="jenjang_pd" name="jenjang_pd">
                                                    <option value="sd">SD</option>
                                                    <option value="smp">SMP</option>
                                                    <option value="sma">SMA/SMK</option>
                                                    <option value="d1">D1</option>
                                                    <option value="d2">D2</option>
                                                    <option value="d3">D3</option>
                                                    <option value="sarjana">S1/D4</option>
                                                    <option value="magister">S2</option>
                                                    <option value="doctor">S3</option>
                                                    <option value="profesor">PROFESOR</option>
                                                </select>
                                                <p id="msg_jenjang_pd" style="color: red;"></p>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="nm_sklh" name="nm_sklh" required="">
                                                <p id="msg_nm_sklh" style="color: red;"></p>
                                            </td>
                                            <td>
                                                <input type="text" class="form-control" id="jurusan_pd" name="jurusan_pd" required="">
                                                <p id="msg_jurusan_pd" style="color: red;"></p>
                                            </td>
                                            <td>
                                                <input type="number" class="form-control" id="th_lulus" name="th_lulus" required="">
                                                <p id="msg_th_lulus" style="color: red;"></p>
                                            </td>
                                            <td align="center">
                                                <button type="button" id="add_data_single" class="btn waves-effect waves-light btn-sm btn-info">Tambah Data</button>
                                            </td>
                                        </tr>
                                    </thead>
                                    <tbody id="out_list_data">
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="button" id="add_data" class="btn waves-effect waves-light btn-rounded btn-info">Simpan Data</button>
                </div>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    var list_data = JSON.parse('<?=$d_pendidikan?>');

    $(document).ready(function(){
        set_first_param();
    });

    //=========================================================================//
    //-----------------------------------set_first_param-----------------------//
    //=========================================================================//
        function set_first_param(){
            $("#q_buta_huruf").val("<?=$q_buta_huruf?>");
            render_list_data();
        }
    //=========================================================================//
    //-----------------------------------set_first_param-----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------manage_list_data----------------------//
    //=========================================================================//

        function add_list_data(){
            var tmp = {
                "jenjang_pd":$("#jenjang_pd").val(),
                "nm_sklh":$("#nm_sklh").val(),
                "jurusan_pd":$("#jurusan_pd").val(),
                "th_lulus":$("#th_lulus").val()
            };

            if($("#jenjang_pd").val() != "" &&
                $("#nm_sklh").val() != "" &&
                $("#jurusan_pd").val() != "" &&
                $("#th_lulus").val() != ""){
                    list_data.push(tmp);
                    render_list_data();
                
            }else{
                alert("Periksa kembali input sadara. Masih terdapat input yang kosong !!");
            }
        }

        function render_list_data(){
            var str_list_data = "";
            if(list_data){
                for (const i in list_data) {
                    str_list_data += "<tr>"+
                                        "<td>"+list_data[i].jenjang_pd+"</td>"+
                                        "<td>"+list_data[i].nm_sklh+"</td>"+
                                        "<td>"+list_data[i].jurusan_pd+"</td>"+
                                        "<td>"+list_data[i].th_lulus+"</td>"+
                                        "<td align=\"center\">"+
                                            // "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Ubah\" onclick=\"edit_list('0')\"> <i class=\"fa fa-pencil text-inverse m-r-10\"></i> </a>"+
                                            "<a href=\"javascript:void(0)\" data-toggle=\"tooltip\" data-original-title=\"Hapus\" onclick=\"delete_list('"+i+"')\"> <i class=\"fa fa-close text-danger\"></i> </a>"+
                                        "</td>"+
                                    "</tr>";
                }
            }

            $("#out_list_data").html(str_list_data);
        }

        $("#add_data_single").click(function(){
            add_list_data();
        });

        function delete_list(i){
            alert("Data berhasil "+list_data[i].jenjang_pd+" dihpaus!!");
            list_data.splice(i, 1);
            render_list_data();
        }
    //=========================================================================//
    //-----------------------------------manage_list_data----------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
        $("#add_data").click(function() {
            var data_main = new FormData();
            data_main.append('id_user_pendidikan'  , '<?=$id_user_pendidikan?>');
            data_main.append('q_buta_huruf'    , $("#q_buta_huruf").val());
            data_main.append('d_pendidikan'    , JSON.stringify(list_data));

            $.ajax({
                url: "<?php echo $main_controller."insert_data";?>",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_insert(res);
                    console.log(res);
                    
                }
            });
        });

        function response_insert(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                $('#insert_admin').modal('toggle');
                clear_form_insert();

                $("#msg_q_buta_huruf").html(detail_msg.q_buta_huruf);
                $("#msg_q_buta_huruf").html(detail_msg.q_buta_huruf);
                $("#msg_d_pendidikan").html(detail_msg.d_pendidikan);

                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?=$main_url_user?>");
            } else {
               
                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }

        function clear_form_insert(){
            $("#q_buta_huruf").val();
            $("#q_buta_huruf").val();
            $("#d_pendidikan").val();
            
            $("#msg_q_buta_huruf").html();
            $("#msg_q_buta_huruf").html();
            $("#msg_d_pendidikan").html();
            
        }

        
    //=========================================================================//
    //-----------------------------------insert_admin--------------------------//
    //=========================================================================//
</script>
