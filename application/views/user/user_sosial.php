<?php
    $main_url_user = base_url()."user/lingkungan";
    $main_controller = base_url()."user/sosialuser/";

    $str_op_makanan_pokok = "";
    if(isset($makanan_pokok)){
        if($makanan_pokok){
            foreach ($makanan_pokok as $key => $value) {
                $str_op_makanan_pokok .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_skala_usaha = "";
    if(isset($skala_usaha)){
        if($skala_usaha){
            foreach ($skala_usaha as $key => $value) {
                $str_op_skala_usaha .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $str_op_bidang_usaha = "";
    if(isset($bidang_usaha)){
        if($bidang_usaha){
            foreach ($bidang_usaha as $key => $value) {
                $str_op_bidang_usaha .= "<option value=\"".$value->kode_jenis."\">".($key+1).". ".$value->nm_jenis."</option>";
            }
        }
    }

    $q_anggota_dasawisma = "";
    $dw_nama = "";
    $q_activitas_pyd = "";
    $pyd_frequensi = "";
    $q_program_bina_balita = "";
    $q_ikut_kel_belajar = "";
    $q_paud = "";
    $q_ikut_kooperasi = "";
    $q_punya_tabungan = "";
    $q_makan_pokok = "";
    $q_punya_usaha = "";
    $usaha_skala = "";
    $usaha_no = "";
    $usaha_bidang = "";

    if(isset($list_data)){
        if($list_data){
          
            $q_anggota_dasawisma = $list_data["q_anggota_dasawisma"];
            $dw_nama = $list_data["dw_nama"];
            $q_activitas_pyd = $list_data["q_activitas_pyd"];
            $pyd_frequensi = $list_data["pyd_frequensi"];
            $q_program_bina_balita = $list_data["q_program_bina_balita"];
            $q_ikut_kel_belajar = $list_data["q_ikut_kel_belajar"];
            $q_paud = $list_data["q_paud"];
            $q_ikut_kooperasi = $list_data["q_ikut_kooperasi"];
            $q_punya_tabungan = $list_data["q_punya_tabungan"];
            $q_makan_pokok = $list_data["q_makan_pokok"];
            $q_punya_usaha = $list_data["q_punya_usaha"];
            $usaha_skala = $list_data["usaha_skala"];
            $usaha_no = $list_data["usaha_no"];
            $usaha_bidang = $list_data["usaha_bidang"];
        }
    }

?>

<!-- ============================================================== -->
<!-- Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->
<div class="row page-titles">
    <div class="col-md-6 align-self-center">
        <h3 class="text-themecolor"><?=$title?></h3>
    </div>
   
</div>
<!-- ============================================================== -->
<!-- End Bread crumb and right sidebar toggle -->
<!-- ============================================================== -->


<!-- ============================================================== -->
<!-- Container fluid  -->
<!-- ============================================================== -->

<div class="container-fluid">
<div class="row">
        <div class="col-lg-12 col-md-12">
            <!-- Column -->
            <div class="card card-default">
                <div class="card-header">
                    <div class="card-actions">
                        <a class="" data-action="collapse"><i class="ti-minus"></i></a>
                        <a class="btn-minimize" data-action="expand"><i class="mdi mdi-arrow-expand"></i></a>
                    </div>
                    <h4 class="card-title m-b-0"><?=$title?></h4>
                    <hr>
                </div>
                <div class="card-body collapse show">
                    
                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Anggota Dasawisma :</label>
                            <div class="col-md-8">
                                <input class="q_anggota" type="radio" name="q_anggota_dasawisma" value="iya" id="q_anggota_dasawisma1">
                                <label for="q_anggota_dasawisma1">Iya</label>
                                <input class="q_anggota" type="radio" name="q_anggota_dasawisma" value="tidak" id="q_anggota_dasawisma2">
                                <label for="q_anggota_dasawisma2">Tidak</label>
                                <p id="msg_q_anggota_dasawisma" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Nama Dasawisma :</label>
                            <div class="col-md-4">
                                <input type="text" class="form-control" id="dw_nama" name="dw_nama" placeholder="">
                                <p id="msg_dw_nama" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Aktif Dalam Posyandu :</label>
                            <div class="col-md-8">
                                <input class="q_anggota" type="radio" name="q_activitas_pyd" value="iya" id="q_activitas_pyd1">
                                <label for="q_activitas_pyd1">Iya</label>
                                <input class="q_anggota" type="radio" name="q_activitas_pyd" value="tidak" id="q_activitas_pyd2">
                                <label for="q_activitas_pyd2">Tidak</label>
                                <p id="msg_q_activitas_pyd" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Frekuensi Ke-Posyandu :</label>
                            <div class="col-md-4">
                                <input type="number" class="form-control" id="pyd_frequensi" name="pyd_frequensi" placeholder="">
                                <p id="msg_pyd_frequensi" style="color: red;"></p>
                            </div>
                            <!-- <div class="col-md-4"> -->
                                <label for="message-text" class="control-label col-md-4">kali per Tahun</label>
                            <!-- </div> -->
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti Program Bina Keluarga Balita :</label>
                            <div class="col-md-8">
                                <input class="q_anggota" type="radio" name="q_program_bina_balita" value="iya" id="q_program_bina_balita1">
                                <label for="q_program_bina_balita1">Iya</label>
                                <input class="q_anggota" type="radio" name="q_program_bina_balita" value="tidak" id="q_program_bina_balita2">
                                <label for="q_program_bina_balita2">Tidak</label>
                                <p id="msg_q_program_bina_balita" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti Kelompok Belajar :</label>
                            <div class="col-md-8">
                                <input class="q_anggota" type="radio" name="q_ikut_kel_belajar" value="iya" id="q_ikut_kel_belajar1">
                                <label for="q_ikut_kel_belajar1">Iya</label>
                                <input class="q_anggota" type="radio" name="q_ikut_kel_belajar" value="tidak" id="q_ikut_kel_belajar2">
                                <label for="q_ikut_kel_belajar2">Tidak</label>
                                <p id="msg_q_ikut_kel_belajar" style="color: red;"></p>
                            </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Mengikuti PAUD / Sejenis</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_paud" value="iya" id="q_paud1">
                                    <label for="q_paud1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_paud" value="tidak" id="q_paud2">
                                    <label for="q_paud2">Tidak</label>
                                    <p id="msg_q_paud" style="color: red;"></p>
                                </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Ikut dalam Kegiatan Koperasi</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_ikut_kooperasi" value="iya" id="q_ikut_kooperasi1">
                                    <label for="q_ikut_kooperasi1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_ikut_kooperasi" value="tidak" id="q_ikut_kooperasi2">
                                    <label for="q_ikut_kooperasi2">Tidak</label>
                                    <p id="msg_q_ikut_kooperasi" style="color: red;"></p>
                                </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Anda Memiliki Tabungan</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_punya_tabungan" value="iya" id="q_punya_tabungan1">
                                    <label for="q_punya_tabungan1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_punya_tabungan" value="tidak" id="q_punya_tabungan2">
                                    <label for="q_punya_tabungan2">Tidak</label>
                                    <p id="msg_q_punya_tabungan" style="color: red;"></p>
                                </div>
                        </div>
                    </div>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Makanan Pokok Sehari-hari</label>
                                <div class="col-md-4">
                                    <select class="custom-select form-control input-sm m-b-10" id="q_makan_pokok" name="q_makan_pokok">
                                        <option value="" selected>Pilih</option>
                                        <?=$str_op_makanan_pokok?>
                                    </select>
                                    <p id="msg_q_makan_pokok" style="color: red;"></p>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
  <div class="card">
                <div class="card-body">
                    
                      <h4>Data Usaha</h4>
                      <hr>
                    <br>

                    <div class="form-group" style="padding-bottom: 0px">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Apakah Punya Usaha</label>
                                <div class="col-md-8">
                                    <input class="q_anggota" type="radio" name="q_punya_usaha" value="iya" id="q_punya_usaha1" onclick="show_q_punya_usaha()">
                                    <label for="q_punya_usaha1">Iya</label>
                                    <input class="q_anggota" type="radio" name="q_punya_usaha" value="tidak" id="q_punya_usaha2" onclick="show_q_punya_usaha()">
                                    <label for="q_punya_usaha2">Tidak</label>
                                    <p id="msg_q_punya_usaha" style="color: red;"></p>
                                </div>
                        </div>
                    </div>

                    <div id="path_q_punya_usaha">
                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Skala Usaha</label>
                                <div class="col-md-4">
                                    <select class="custom-select form-control input-sm m-b-10" id="usaha_skala" name="usaha_skala">
                                        <option value="" selected>Pilih</option>
                                        <?=$str_op_skala_usaha?>
                                    </select>
                                    <p id="msg_usaha_skala" style="color: red;"></p>
                                </div>
                        </div>

                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Nomor Induk Berusaha (NIB)</label>
                            <div class="col-md-4">
                            <input type="number" class="form-control" id="usaha_no" name="usaha_no" placeholder="">
                            <p id="msg_usaha_no" style="color: red;"></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="message-text" class="control-label col-md-4">Bidang Usaha</label>
                                <div class="col-md-4">
                                    <select class="custom-select form-control input-sm m-b-10" id="usaha_bidang" name="usaha_bidang">
                                        <option value="" selected>Pilih</option>
                                        <?=$str_op_bidang_usaha?>
                                    </select>
                                    <p id="msg_usaha_bidang" style="color: red;"></p>
                                </div>
                        </div>
                    </div>
                </div>


                <?php
                    if(!$resume){
                ?>
                <div class="card-footer text-right">
                    <button type="button" id="update" class="btn waves-effect waves-light btn-rounded btn-info">Simpan Data</button>
                </div>
                <?php 
                    } 
                ?>
            </div>
        </div>
    </div>
</div>
<!-- ============================================================== -->
<!-- --------------------------End Container fluid----------------  -->
<!-- ============================================================== -->


<script src="<?php print_r(base_url()."assets/js/custom/main_custom.js");?>"></script>
<script type="text/javascript">
    $(document).ready(function(){
        set_param();       
    })

    function set_param(){
        $("input[name='q_anggota_dasawisma'][value='<?=$q_anggota_dasawisma ? $q_anggota_dasawisma : 'iya'?>']").prop("checked",true);
        $("#dw_nama").val("<?=$dw_nama ? $dw_nama : ""?>");

        $("input[name='q_activitas_pyd'][value='<?=$q_activitas_pyd ? $q_activitas_pyd : 'iya'?>']").prop("checked",true);
        $("#pyd_frequensi").val("<?=$pyd_frequensi ? $pyd_frequensi : ""?>");

        $("input[name='q_program_bina_balita'][value='<?=$q_program_bina_balita ? $q_program_bina_balita : 'iya'?>']").prop("checked",true);
        $("input[name='q_ikut_kel_belajar'][value='<?=$q_ikut_kel_belajar ? $q_ikut_kel_belajar : 'iya'?>']").prop("checked",true);
        $("input[name='q_paud'][value='<?=$q_paud ? $q_paud : 'iya'?>']").prop("checked",true);
        $("input[name='q_ikut_kooperasi'][value='<?=$q_ikut_kooperasi ? $q_ikut_kooperasi : 'iya'?>']").prop("checked",true);
        $("input[name='q_punya_tabungan'][value='<?=$q_punya_tabungan ? $q_punya_tabungan : 'iya'?>']").prop("checked",true);
        $("#q_makan_pokok").val("<?=$q_makan_pokok ? $q_makan_pokok : ""?>");

        $("input[name='q_punya_usaha'][value='<?=$q_punya_usaha ? $q_punya_usaha : 'iya'?>']").prop("checked",true);
        $("#usaha_skala").val("<?=$usaha_skala ? $usaha_skala : ""?>");
        $("#usaha_no").val("<?=$usaha_no ? $usaha_no : "0"?>");
        $("#usaha_bidang").val("<?=$usaha_bidang ? $usaha_bidang : ""?>");

        show_q_punya_usaha()
    }
    

    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//
        $("#update").click(function() {
            var data_main = new FormData();
            data_main.append('q_anggota_dasawisma', $('input[name="q_anggota_dasawisma"]:checked').val() ? $('input[name="q_anggota_dasawisma"]:checked').val() : "tidak");
            data_main.append("dw_nama", $("#dw_nama").val());

            data_main.append('q_activitas_pyd', $('input[name="q_activitas_pyd"]:checked').val() ? $('input[name="q_activitas_pyd"]:checked').val() : "tidak");
            data_main.append("pyd_frequensi", $("#pyd_frequensi").val());

            data_main.append('q_program_bina_balita', $('input[name="q_program_bina_balita"]:checked').val() ? $('input[name="q_program_bina_balita"]:checked').val() : "tidak");
            data_main.append('q_ikut_kel_belajar', $('input[name="q_ikut_kel_belajar"]:checked').val() ? $('input[name="q_ikut_kel_belajar"]:checked').val() : "tidak");

            data_main.append('q_paud', $('input[name="q_paud"]:checked').val() ? $('input[name="q_paud"]:checked').val() : "tidak");
            data_main.append('q_ikut_kooperasi', $('input[name="q_ikut_kooperasi"]:checked').val() ? $('input[name="q_ikut_kooperasi"]:checked').val() : "tidak");            
            data_main.append('q_punya_tabungan', $('input[name="q_punya_tabungan"]:checked').val() ? $('input[name="q_punya_tabungan"]:checked').val() : "tidak");
            data_main.append("q_makan_pokok", $("#q_makan_pokok").val());

            data_main.append('q_punya_usaha', $('input[name="q_punya_usaha"]:checked').val() ? $('input[name="q_punya_usaha"]:checked').val() : "tidak");
            data_main.append("usaha_skala", $("#usaha_skala").val());
            data_main.append("usaha_no", $("#usaha_no").val());
            data_main.append("usaha_bidang", $("#usaha_bidang").val());
            
            
            $.ajax({
                url: "<?=$main_controller?>act_controller",
                dataType: 'html', // what to expect back from the PHP script, if anything
                cache: false,
                contentType: false,
                processData: false,
                data: data_main,
                type: 'post',
                success: function(res) {
                    response_update(res);
                    console.log(res);
                }
            });
        });

        function response_update(res) {
            var data_json = JSON.parse(res);
            var main_msg = data_json.msg_main;
            var detail_msg = data_json.msg_detail;
            if (main_msg.status) {
                create_sweet_alert("Proses Berhasil", main_msg.msg, "success", "<?php print_r(base_url()."user/sosial");?>");
            } else {
                
                $("#msg_q_anggota_dasawisma").html(detail_msg.q_anggota_dasawisma);
                $("#msg_dw_nama").html(detail_msg.dw_nama);
                $("#msg_q_activitas_pyd").html(detail_msg.q_activitas_pyd);
                $("#msg_pyd_frequensi").html(detail_msg.pyd_frequensi);
                $("#msg_q_program_bina_balita").html(detail_msg.q_program_bina_balita);
                $("#msg_q_ikut_kel_belajar").html(detail_msg.q_ikut_kel_belajar);
                $("#msg_q_paud").html(detail_msg.q_paud);
                $("#msg_q_ikut_kooperasi").html(detail_msg.q_ikut_kooperasi);
                $("#msg_q_punya_tabungan").html(detail_msg.q_punya_tabungan);
                $("#msg_q_makan_pokok").html(detail_msg.q_makan_pokok);
                $("#msg_q_punya_usaha").html(detail_msg.q_punya_usaha);
                $("#msg_usaha_skala").html(detail_msg.usaha_skala);
                $("#msg_usaha_no").html(detail_msg.usaha_no);
                $("#msg_usaha_bidang").html(detail_msg.usaha_bidang);

                create_sweet_alert("Proses Gagal", main_msg.msg, "error", "");
            }
        }
    //=========================================================================//
    //-----------------------------------update_admin--------------------------//
    //=========================================================================//

    //=========================================================================//
    //-----------------------------------show_hide-----------------------------//
    //=========================================================================//
        function show_q_punya_usaha(){
          var q_punya_usaha = $('input[name="q_punya_usaha"]:checked').val()
          if(q_punya_usaha == "iya"){
            $("#path_q_punya_usaha").fadeIn()
          }else{
            $("#path_q_punya_usaha").fadeOut()
          }
        }
    //=========================================================================//
    //-----------------------------------show_hide-----------------------------//
    //=========================================================================//

</script>
