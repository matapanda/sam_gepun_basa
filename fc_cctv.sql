-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 07 Bulan Mei 2021 pada 08.43
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fc_cctv`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(12) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('AD2019110001', '0', 'suryahanggara@gmail.com', 'surya', '21232f297a57a5a743894a0e4a801fc3', '1', 'surya', '20190001', '0'),
('AD2021040001', '1', 'toni@gmail.com', 'toni', '21232f297a57a5a743894a0e4a801fc3', '1', 'toni', 'a', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `index_number`
--

CREATE TABLE `index_number` (
  `id` varchar(32) NOT NULL,
  `id_index` varchar(100) NOT NULL,
  `kode` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `index_number`
--

INSERT INTO `index_number` (`id`, `id_index`, `kode`) VALUES
('master_cctv_main', '5', 'CCM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_cctv_jn`
--

CREATE TABLE `master_cctv_jn` (
  `id_jn_mcctv` int(11) NOT NULL,
  `nama_jn_mcctv` varchar(255) DEFAULT NULL,
  `ket_jn_mcctv` text DEFAULT NULL,
  `icon_jn_mcctv` varchar(64) DEFAULT NULL,
  `is_delete` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_cctv_jn`
--

INSERT INTO `master_cctv_jn` (`id_jn_mcctv`, `nama_jn_mcctv`, `ket_jn_mcctv`, `icon_jn_mcctv`, `is_delete`) VALUES
(14, 'test aja lah sss sasdas fgdgdfg', 'malang setalan sebelah kanan sss ddsdvvvsd sdvdsvdsv', '2121212105050707055930.jpg', '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_cctv_main`
--

CREATE TABLE `master_cctv_main` (
  `id_mcctv_main` varchar(32) NOT NULL,
  `id_jn_mcctv` varchar(32) DEFAULT NULL,
  `nama_mcctv_main` varchar(255) DEFAULT NULL,
  `point_mcctv_main` varchar(64) DEFAULT NULL,
  `alamat_mcctv_main` text DEFAULT NULL,
  `ket_mcctv_main` text DEFAULT NULL,
  `actv_mcctv_main` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `master_cctv_main`
--

INSERT INTO `master_cctv_main` (`id_mcctv_main`, `id_jn_mcctv`, `nama_mcctv_main`, `point_mcctv_main`, `alamat_mcctv_main`, `ket_mcctv_main`, `actv_mcctv_main`) VALUES
('CCM202105070000000005', '14', 'sadsad', '[\"21321\",\"213213\"]', 'asdsadas', 'dsads sad sad asd', '0');

--
-- Trigger `master_cctv_main`
--
DELIMITER $$
CREATE TRIGGER `ins_bf_main_cctv` BEFORE INSERT ON `master_cctv_main` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
		DECLARE kode_in VARCHAR(32);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'master_cctv_main';
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next WHERE id = 'master_cctv_main';
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 10, '0'));
    
    SET NEW.id_mcctv_main = fix_key_user;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`) USING BTREE;

--
-- Indeks untuk tabel `index_number`
--
ALTER TABLE `index_number`
  ADD PRIMARY KEY (`id`) USING BTREE;

--
-- Indeks untuk tabel `master_cctv_jn`
--
ALTER TABLE `master_cctv_jn`
  ADD PRIMARY KEY (`id_jn_mcctv`);

--
-- Indeks untuk tabel `master_cctv_main`
--
ALTER TABLE `master_cctv_main`
  ADD PRIMARY KEY (`id_mcctv_main`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `master_cctv_jn`
--
ALTER TABLE `master_cctv_jn`
  MODIFY `id_jn_mcctv` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
