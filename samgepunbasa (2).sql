-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 31 Agu 2021 pada 19.11
-- Versi server: 10.4.18-MariaDB
-- Versi PHP: 7.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `samgepunbasa`
--

DELIMITER $$
--
-- Prosedur
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `get_status_data_user` (`ID_USER` INT)  BEGIN
	SELECT @id_user:= id_user as id_user, nama_user
-- 		(SELECT COUNT(*) FROM user_data WHERE id_user = @id_user) as dt_diri,
-- 		(SELECT COUNT(*) FROM user_kerja WHERE id_user = @id_user) as dt_kerja,
-- 		(SELECT COUNT(*) FROM user_sosial WHERE id_user = @id_user) as dt_sosial,
-- 		(SELECT COUNT(*) FROM user_keuangan WHERE id_user = @id_user) as dt_keuangan,
-- 		(SELECT COUNT(*) FROM user_kesehatan WHERE id_user = @id_user) as dt_kesehatan,
-- 		(SELECT COUNT(*) FROM user_pendidikan WHERE id_user = @id_user) as dt_pendidikan
	FROM user WHERE id_user = CONCAT(ID_USER);
END$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `admin`
--

CREATE TABLE `admin` (
  `id_admin` char(32) NOT NULL,
  `id_tipe_admin` varchar(3) NOT NULL,
  `jn_admin` enum('0','1','2','3') NOT NULL,
  `kd_admin` varchar(32) NOT NULL,
  `id_dinas` varchar(32) NOT NULL,
  `email` text NOT NULL,
  `username` varchar(15) NOT NULL,
  `password` varchar(256) NOT NULL,
  `status_active` enum('0','1','2') NOT NULL,
  `nama_admin` varchar(100) NOT NULL,
  `nip_admin` varchar(64) NOT NULL,
  `is_delete` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `admin`
--

INSERT INTO `admin` (`id_admin`, `id_tipe_admin`, `jn_admin`, `kd_admin`, `id_dinas`, `email`, `username`, `password`, `status_active`, `nama_admin`, `nip_admin`, `is_delete`) VALUES
('ADM202106280000003', '0', '0', 'XX', 'dinas kominfo', 'suryahanggara@gmail.com', 'suryahanggara', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya hanggara', '3573001903940006', '0'),
('ADM202106280000004', '0', '0', '0', 'Malang', 'suryahanggara2@gmail.com', 'suryahanggara2', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08', '1', 'surya', 'a', '0');

--
-- Trigger `admin`
--
DELIMITER $$
CREATE TRIGGER `gan_insert_adm` BEFORE INSERT ON `admin` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'admin';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'admin';
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_admin = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `index_number`
--

CREATE TABLE `index_number` (
  `id` varchar(32) NOT NULL,
  `id_index` varchar(32) NOT NULL,
  `kode` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `index_number`
--

INSERT INTO `index_number` (`id`, `id_index`, `kode`) VALUES
('admin', '4', 'ADM'),
('ms_distrik_pem', '73', 'MDP'),
('ms_jn_pekerjaan', '0', 'JPK'),
('user', '0', 'USR'),
('user_data', '5', 'USD'),
('user_kerja', '4', 'KER'),
('user_kesehatan', '7', 'KES'),
('user_keuangan', '20', 'KEU'),
('user_or', '4', 'OLR'),
('user_pendidikan', '4', 'PDD'),
('user_sosial', '7', 'SOS'),
('user_tmpt', '4', 'TMP'),
('user_transport', '4', 'TRN'),
('user_umkm', '2', 'UKM');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_distrik_pem`
--

CREATE TABLE `ms_distrik_pem` (
  `id_distrik_pem` varchar(32) NOT NULL,
  `distrik_pem_id` varchar(32) DEFAULT NULL,
  `kd_distrik_pem` varchar(32) DEFAULT NULL,
  `nama_distrik_pem` varchar(64) DEFAULT NULL,
  `ket_distrik_pem` text DEFAULT NULL,
  `sts_distrik_pem` enum('0','1') DEFAULT NULL,
  `icon_distrik_pem` varchar(64) DEFAULT NULL,
  `nm_shp` varchar(64) DEFAULT NULL,
  `loc_shp` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ms_distrik_pem`
--

INSERT INTO `ms_distrik_pem` (`id_distrik_pem`, `distrik_pem_id`, `kd_distrik_pem`, `nama_distrik_pem`, `ket_distrik_pem`, `sts_distrik_pem`, `icon_distrik_pem`, `nm_shp`, `loc_shp`) VALUES
('MDP202106030000007', '0', NULL, 'KEC. BLIMBING', 'KEC. BLIMBING', '1', NULL, 'blimbing', NULL),
('MDP202106030000008', '0', NULL, 'KEC. KEDUNGKANDANG', 'KEC. KEDUNGKANDANG', '1', NULL, 'kedung_kandang', NULL),
('MDP202106030000009', '0', NULL, 'KEC. KLOJEN', 'KEC. KLOJEN', '1', NULL, 'klojen', NULL),
('MDP202106030000010', '0', NULL, 'KEC. LOWOKWARU', 'KEC. LOWOKWARU', '1', NULL, 'lowokwaru', NULL),
('MDP202106030000011', '0', NULL, 'KEC. SUKUN', 'KEC. SUKUN', '1', NULL, 'sukun', NULL),
('MDP202106030000017', 'MDP202106030000007', NULL, 'KEL. ARJOSARI', 'KEL. ARJOSARI', '1', NULL, 'KELURAHAN ARJOSARI', NULL),
('MDP202106030000018', 'MDP202106030000007', NULL, 'KEL. BALEARJOSARI', 'KEL. BALEARJOSARI', '1', NULL, 'KELURAHAN BALEARJOSARI', NULL),
('MDP202106030000019', 'MDP202106030000007', NULL, 'KEL. BLIMBING', 'KEL. BLIMBING', '1', NULL, 'KELURAHAN BLIMBING', NULL),
('MDP202106030000020', 'MDP202106030000007', NULL, 'KEL. BUNULREJO', 'KEL. BUNULREJO', '1', NULL, 'KELURAHAN BUNULREJO', NULL),
('MDP202106030000021', 'MDP202106030000007', NULL, 'KEL. JODIPAN', 'KEL. JODIPAN', '1', NULL, 'KELURAHAN JODIPAN', NULL),
('MDP202106030000022', 'MDP202106030000007', NULL, 'KEL. KSATRIAN', 'KEL. KSATRIAN', '1', NULL, 'KELURAHAN KESATRIAN', NULL),
('MDP202106030000023', 'MDP202106030000007', NULL, 'KEL. PANDANWANGI', 'KEL. PANDANWANGI', '1', NULL, 'KELURAHAN PANDANWANGI', NULL),
('MDP202106030000024', 'MDP202106030000007', NULL, 'KEL. POLEHAN', 'KEL. POLEHAN', '1', NULL, 'KELURAHAN POLEHAN', NULL),
('MDP202106030000025', 'MDP202106030000007', NULL, 'KEL. POLOWIJEN', 'KEL. POLOWIJEN', '1', NULL, 'KELURAHAN POLOWIJEN', NULL),
('MDP202106030000026', 'MDP202106030000007', NULL, 'KEL. PURWANTORO', 'KEL. PURWANTORO', '1', NULL, 'KELURAHAN PURWANTORO', NULL),
('MDP202106030000027', 'MDP202106030000007', NULL, 'KEL. PURWODADI', 'KEL. PURWODADI', '1', NULL, 'KELURAHAN PURWODADI', NULL),
('MDP202106030000028', 'MDP202106030000007', NULL, 'KEL. RAMPAL CELAKET', 'KEL. RAMPAL CELAKET', '1', NULL, 'KELURAHAN RAMPALCELAKET', NULL),
('MDP202106030000029', 'MDP202106030000008', NULL, 'KEL. ARJOWINANGUN', 'KEL. ARJOWINANGUN', '1', NULL, 'KELURAHAN ARJOWINANGUN', NULL),
('MDP202106030000030', 'MDP202106030000008', NULL, 'KEL. BUMIAYU', 'KEL. BUMIAYU', '1', NULL, 'KELURAHAN BUMIAYU', NULL),
('MDP202106030000031', 'MDP202106030000008', NULL, 'KEL. BURING', 'KEL. BURING', '1', NULL, 'KELURAHAN BURING', NULL),
('MDP202106030000032', 'MDP202106030000008', NULL, 'KEL. CEMORO KANDANG', 'KEL. CEMORO KANDANG', '1', NULL, 'KELURAHAN CEMOROKANDANG', NULL),
('MDP202106030000033', 'MDP202106030000008', NULL, 'KEL. KEDUNGKANDANG', 'KEL. KEDUNGKANDANG', '1', NULL, 'KELURAHAN KEDUNGKANDANG', NULL),
('MDP202106030000034', 'MDP202106030000008', NULL, 'KEL. KOTA LAMA', 'KEL. KOTA LAMA', '1', NULL, 'KELURAHAN KOTALAMA', NULL),
('MDP202106030000035', 'MDP202106030000008', NULL, 'KEL. LESANPURO', 'KEL. LESANPURO', '1', NULL, 'KELURAHAN LESANPURO', NULL),
('MDP202106030000036', 'MDP202106030000008', NULL, 'KEL. MADYOPURO', 'KEL. MADYOPURO', '1', NULL, 'KELURAHAN MADYOPURO', NULL),
('MDP202106030000037', 'MDP202106030000008', NULL, 'KEL. MERGOSONO', 'KEL. MERGOSONO', '1', NULL, 'KELURAHAN MERGOSONO', NULL),
('MDP202106030000038', 'MDP202106030000008', NULL, 'KEL. SAWOJAJAR', 'KEL. SAWOJAJAR', '1', NULL, 'KELURAHAN SAWOJAJAR', NULL),
('MDP202106030000039', 'MDP202106030000008', NULL, 'KEL. TLOGOWARU', 'KEL. TLOGOWARU', '1', NULL, 'KELURAHAN TELOGOWARU', NULL),
('MDP202106030000040', 'MDP202106030000008', NULL, 'KEL. WONOKOYO', 'KEL. WONOKOYO', '1', NULL, 'KELURAHAN WONOKOYO', NULL),
('MDP202106030000041', 'MDP202106030000009', NULL, 'KEL. BARENG', 'KEL. BARENG', '1', NULL, 'KELURAHAN BARENG', NULL),
('MDP202106030000042', 'MDP202106030000009', NULL, 'KEL. GADING KASRI', 'KEL. GADING KASRI', '1', NULL, 'KELURAHAN GADINGKASRI', NULL),
('MDP202106030000043', 'MDP202106030000009', NULL, 'KEL. KASIN', 'KEL. KASIN', '1', NULL, 'KELURAHAN KASIN', NULL),
('MDP202106030000044', 'MDP202106030000009', NULL, 'KEL. KAUMAN', 'KEL. KAUMAN', '1', NULL, 'KELURAHAN KAUMAN', NULL),
('MDP202106030000045', 'MDP202106030000009', NULL, 'KEL. KIDUL DALEM', 'KEL. KIDUL DALEM', '1', NULL, 'KELURAHAN KIDULDALEM', NULL),
('MDP202106030000046', 'MDP202106030000009', NULL, 'KEL. KLOJEN', 'KEL. KLOJEN', '1', NULL, 'KELURAHAN KLOJEN', NULL),
('MDP202106030000047', 'MDP202106030000009', NULL, 'KEL. ORO-ORO DOWO', 'KEL. ORO-ORO DOWO', '1', NULL, 'KELURAHAN ORO-ORODOWO', NULL),
('MDP202106030000048', 'MDP202106030000009', NULL, 'KEL. PENANGGUNGAN', 'KEL. PENANGGUNGAN', '1', NULL, 'KELURAHAN PENAGGUNGAN', NULL),
('MDP202106030000049', 'MDP202106030000009', NULL, 'KEL. SAMA\'AN', 'KEL. SAMAAN', '1', NULL, 'KELURAHAN SAMAAN', NULL),
('MDP202106030000050', 'MDP202106030000009', NULL, 'KEL. SUKOHARJO', 'KEL. SUKOHARJO', '1', NULL, 'KELURAHAN SUKOHARJO', NULL),
('MDP202106030000051', 'MDP202106030000010', NULL, 'KEL. DINOYO', 'KEL. DINOYO', '1', NULL, 'KELURAHAN DINOYO', NULL),
('MDP202106030000052', 'MDP202106030000010', NULL, 'KEL. JATIMULYO', 'KEL. JATIMULYO', '1', NULL, 'KELURAHAN JATIMULYO', NULL),
('MDP202106030000053', 'MDP202106030000010', NULL, 'KEL. KETAWANG GEDE', 'KEL. KETAWANG GEDE', '1', NULL, 'KELURAHAN KETAWANGGEDE', NULL),
('MDP202106030000054', 'MDP202106030000010', NULL, 'KEL. LOWOKWARU', 'KEL. LOWOKWARU', '1', NULL, 'KELURAHAN LOWOKWARU', NULL),
('MDP202106030000055', 'MDP202106030000010', NULL, 'KEL. MERJOSARI', 'KEL. MERJOSARI', '1', NULL, 'KELURAHAN MERJOSARI', NULL),
('MDP202106030000056', 'MDP202106030000010', NULL, 'KEL. MOJOLANGU', 'KEL. MOJOLANGU', '1', NULL, 'KELURAHAN MOJOLANGU', NULL),
('MDP202106030000057', 'MDP202106030000010', NULL, 'KEL. SUMBERSARI', 'KEL. SUMBERSARI', '1', NULL, 'KELURAHAN SUMBERSARI', NULL),
('MDP202106030000058', 'MDP202106030000010', NULL, 'KEL. TASIKMADU', 'KEL. TASIKMADU', '1', NULL, 'KELURAHAN TASIKMADU', NULL),
('MDP202106030000059', 'MDP202106030000010', NULL, 'KEL. TLOGOMAS', 'KEL. TLOGOMAS', '1', NULL, 'KELURAHAN TLOGOMAS', NULL),
('MDP202106030000060', 'MDP202106030000010', NULL, 'KEL. TULUSREJO', 'KEL. TULUSREJO', '1', NULL, 'KELURAHAN TULUSREJO', NULL),
('MDP202106030000061', 'MDP202106030000010', NULL, 'KEL. TUNGGUL WULUNG', 'KEL. TUNGGUL WULUNG', '1', NULL, 'KELURAHAN TUNGGULWULUNG', NULL),
('MDP202106030000062', 'MDP202106030000010', NULL, 'KEL. TUNJUNG SEKAR', 'KEL. TUNJUNG SEKAR', '1', NULL, 'KELURAHAN TUNJUNGSEKAR', NULL),
('MDP202106030000063', 'MDP202106030000011', NULL, 'KEL. BAKALANKRAJAN', 'KEL. BAKALANKRAJAN', '1', NULL, 'KELURAHAN BAKALANKRAJAN', NULL),
('MDP202106030000064', 'MDP202106030000011', NULL, 'KEL. BANDULAN', 'KEL. BANDULAN', '1', NULL, 'KELURAHAN BANDULAN', NULL),
('MDP202106030000065', 'MDP202106030000011', NULL, 'KEL. BANDUNG REJOSARI', 'KEL. BANDUNG REJOSARI', '1', NULL, 'KELURAHAN BANDUNGREJOSARI', NULL),
('MDP202106030000066', 'MDP202106030000011', NULL, 'KEL. CIPTOMULYO', 'KEL. CIPTOMULYO', '1', NULL, 'KELURAHAN CIPTOMULYO', NULL),
('MDP202106030000067', 'MDP202106030000011', NULL, 'KEL. GADANG', 'KEL. GADANG', '1', NULL, 'KELURAHAN GADANG', NULL),
('MDP202106030000068', 'MDP202106030000011', NULL, 'KEL. KARANG BESUKI', 'KEL. KARANG BESUKI', '1', NULL, 'KELURAHAN KARANGBESUKI', NULL),
('MDP202106030000069', 'MDP202106030000011', NULL, 'KEL. KEBONSARI', 'KEL. KEBONSARI', '1', NULL, 'KELURAHAN KEBONSARI', NULL),
('MDP202106030000070', 'MDP202106030000011', NULL, 'KEL. MULYOREJO', 'KEL. MULYOREJO', '1', NULL, 'KELURAHAN MULYOREJO', NULL),
('MDP202106030000071', 'MDP202106030000011', NULL, 'KEL. PISANG CANDI', 'KEL. PISANG CANDI', '1', NULL, 'KELURAHAN PISANGCANDI', NULL),
('MDP202106030000072', 'MDP202106030000011', NULL, 'KEL. SUKUN', 'KEL. SUKUN', '1', NULL, 'KELURAHAN SUKUN', NULL),
('MDP202106030000073', 'MDP202106030000011', NULL, 'KEL. TANJUNGREJO', 'KEL. TANJUNGREJO', '1', NULL, 'KELURAHAN TANJUNGREJO', NULL);

--
-- Trigger `ms_distrik_pem`
--
DELIMITER $$
CREATE TRIGGER `bf_ins_copy1` BEFORE INSERT ON `ms_distrik_pem` FOR EACH ROW BEGIN
	DECLARE rowcount INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE rowcount_next INT;
    DECLARE fix_key_user VARCHAR(32);
    
    SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number WHERE id = "ms_distrik_pem";
     
 	SEt rowcount_next = rowcount + 1;
 
 	UPDATE index_number SET id_index=rowcount_next 
    WHERE id = "ms_distrik_pem";
    
    SET fix_key_user = concat(kode_in, left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_distrik_pem = fix_key_user;

END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jenis_all`
--

CREATE TABLE `ms_jenis_all` (
  `id_ms_jenis_all` int(11) UNSIGNED NOT NULL,
  `ms_jenis_all_id` int(11) UNSIGNED NOT NULL,
  `pattern` text NOT NULL,
  `tipe` int(11) DEFAULT NULL,
  `kode_jenis` varchar(32) NOT NULL,
  `nm_jenis` varchar(64) NOT NULL,
  `desc` text NOT NULL,
  `sts_act` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `ms_jenis_all`
--

INSERT INTO `ms_jenis_all` (`id_ms_jenis_all`, `ms_jenis_all_id`, `pattern`, `tipe`, `kode_jenis`, `nm_jenis`, `desc`, `sts_act`) VALUES
(0, 0, '[]', 0, 'DEFAULT', 'DEFAULT', 'DEFAULT', '1'),
(1, 0, '[\"0\"]', 1, 'JENIS_KB', 'Jenis Kb', 'Jenis Kb', '1'),
(4, 1, '[\"0\", \"1\"]', 2, 'PIL KB', 'PIL KB', 'PIL KB', '1'),
(5, 1, '[\"0\", \"1\"]', 2, '-', 'Suntik KB', 'Suntik KB', '1'),
(9, 0, '[\"0\"]', 1, 'JN_VAKSIN', 'JN_VAKSIN', 'JN_VAKSIN', '1'),
(10, 9, '[\"0\",\"9\"]', 2, 'SINOVAC', 'SINOVAC', 'SINOVAC', '1'),
(11, 9, '[\"0\",\"9\"]', 2, 'ASTRA_ZENECA', 'ASTRA_ZENECA', 'ASTRA_ZENECA', '1'),
(12, 9, '[\"0\",\"9\"]', 2, 'MODERNA', 'MODERNA', 'MODERNA', '1'),
(13, 0, '[\"0\"]', 1, 'JN_DOSIS', 'JENIS DOSIS', 'JENIS DOSIS', '1'),
(14, 13, '[\"0\",\"13\"]', 2, 'DOSIS1', 'DOSIS 1', 'DOSIS1', '1'),
(15, 13, '[\"0\",\"13\"]', 2, 'DOSIS2', 'DOSIS 2', 'DOSIS 2', '1'),
(16, 13, '[\"0\",\"13\"]', 2, 'DOSIS3', 'DOSIS 3', 'DOSIS1', '1'),
(17, 0, '[\"0\"]', 1, 'JN_PEKERJAAN', 'JN_PEKERJAAN', 'JN_PEKERJAAN', '1'),
(18, 0, '[\"0\"]', 1, 'STS_PEKERJAAN', 'STS_PEKERJAAN', 'STS_PEKERJAAN', '1'),
(19, 18, '[\"0\",\"18\"]', 2, 'bekerja', 'Bekerja', 'bekerja', '1'),
(20, 18, '[\"0\",\"18\"]', 2, 'tidak bekerja', 'Tidak Atau Belum Bekerja', 'Tidak / Belum Bekerja', '1'),
(21, 17, '[\"0\",\"17\"]', 2, 'PNS', 'PNS', 'PNS', '1'),
(22, 17, '[\"0\",\"17\"]', 2, 'SWASTA', 'SWASTA', 'SWASTA', '1'),
(23, 0, '[\"0\"]', 1, 'JN_KENDARAAN', 'JN_KENDARAAN', 'JN_KENDARAAN', '1'),
(24, 0, '[\"0\"]', 1, 'STS_KEPEMILIKAN', 'STS_KEPEMILIKAN', 'STS_KEPEMILIKAN', '1'),
(25, 0, '[\"0\"]', 1, 'JN_INTERNET', 'JN_INTERNET', 'JN_INTERNET', '1'),
(26, 0, '[\"0\"]', 1, 'JN_PERANGKAT', 'JN_PERANGKAT', 'JN_PERANGKAT', '1'),
(27, 23, '[\"0\",\"23\"]', 2, 'Sepeda Roda Dua', 'Sepeda Roda Dua', 'Sepeda Roda Dua', '1'),
(28, 23, '[\"0\",\"23\"]', 2, 'Sepeda Motor', 'Sepeda Motor', 'Sepeda Motor', '1'),
(29, 23, '[\"0\",\"23\"]', 2, 'Mobil', 'Mobil', 'Mobil', '1'),
(30, 24, '[\"0\",\"24\"]', 2, 'Milik Sendiri', 'Milik Sendiri', 'Milik Sendiri', '1'),
(31, 24, '[\"0\",\"24\"]', 2, 'Pinjaman', 'Pinjaman', 'Pinjaman', '1'),
(32, 25, '[\"0\",\"25\"]', 2, 'Internet Mandiri', 'Internet Mandiri', 'Internet Mandiri', '1'),
(33, 25, '[\"0\",\"25\"]', 2, 'Disediakan Pemerintah', 'Disediakan Pemerintah', 'Disediakan Pemerintah', '1'),
(34, 26, '[\"0\",\"26\"]', 2, 'Telephone Seluler', 'Telephone Seluler', 'Telephone Seluler', '1'),
(35, 26, '[\"0\",\"26\"]', 2, 'Handphone', 'Handphone', 'Handphone', '1'),
(36, 26, '[\"0\",\"26\"]', 2, 'Komputer', 'Komputer', 'Komputer', '1');

-- --------------------------------------------------------

--
-- Struktur dari tabel `ms_jn_pekerjaan`
--

CREATE TABLE `ms_jn_pekerjaan` (
  `id_ms_jn_pekerjaan` int(5) UNSIGNED ZEROFILL NOT NULL,
  `nama_ms_jn_pekerjaan` varchar(255) DEFAULT NULL,
  `sts_ms_jn_pekerjaan` enum('0','1') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id_user` int(11) NOT NULL,
  `email_user` text CHARACTER SET utf8 NOT NULL,
  `username_user` varchar(15) CHARACTER SET utf8 NOT NULL,
  `password_user` varchar(256) CHARACTER SET utf8 NOT NULL,
  `status_active_user` enum('0','1','2') NOT NULL,
  `nama_user` varchar(100) CHARACTER SET utf8 NOT NULL,
  `nik_user` varchar(16) CHARACTER SET utf8 NOT NULL,
  `kk_user` varchar(16) CHARACTER SET utf8 NOT NULL,
  `tlp_user` varchar(15) CHARACTER SET utf8 NOT NULL,
  `sts_submit` enum('0','1') DEFAULT NULL,
  `is_del_user` enum('0','1') NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id_user`, `email_user`, `username_user`, `password_user`, `status_active_user`, `nama_user`, `nik_user`, `kk_user`, `tlp_user`, `sts_submit`, `is_del_user`) VALUES
(4, 'suryahanggara@gmail.com', 'suryahanggara', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya hanggara', '3573011903940006', '3573011903940006', '081230695774', NULL, '0'),
(6, 'suryahanggara@gmail.com', 'suryahanggara', '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918', '1', 'surya hanggara', '3573011903940006', '3573011903940006', '081230695774', NULL, '0');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_data`
--

CREATE TABLE `user_data` (
  `id_user_data` varchar(64) CHARACTER SET utf8 NOT NULL,
  `id_user` int(11) NOT NULL COMMENT 'nama dasawisma',
  `ds_nama_dasawisma` text NOT NULL,
  `ms_jk` enum('l','p') DEFAULT NULL COMMENT 'jenis kelamin --> enum isinya (''l'',''p'')',
  `ms_tmp_lhr` varchar(100) NOT NULL COMMENT 'tempat lahir',
  `ms_tgl_lhr` date DEFAULT NULL COMMENT 'tanggal lahir',
  `ms_pd_terakhir` varchar(64) NOT NULL COMMENT 'pendidikan terakhir',
  `ms_sts_pernikahan` enum('0','1','2','3') NOT NULL COMMENT 'status pernikahan ==> 0 = belum kawin, 1 = sudah kawin, 2 =cerai hidup, 3 = cerai mati',
  `ms_wn` varchar(255) NOT NULL COMMENT 'warga negara - langsung asal negara',
  `ms_agama` varchar(32) NOT NULL,
  `ms_suku` varchar(32) NOT NULL,
  `ms_gol_darah` varchar(255) DEFAULT NULL COMMENT 'golongan darah -> isi manual',
  `ktp_alamat_user_data` text NOT NULL,
  `ktp_kec_user_data` varchar(32) NOT NULL COMMENT 'isi id_kecamatan dari table ms_distrik',
  `ktp_kel_user_data` varchar(32) NOT NULL COMMENT 'isi id_kel dari table ms_distrik yg memiliki distrik_id = 0',
  `ktp_rt_user_data` int(3) UNSIGNED ZEROFILL NOT NULL,
  `ktp_rw_user_data` int(3) UNSIGNED ZEROFILL NOT NULL,
  `ktp_kota_user_data` varchar(64) NOT NULL,
  `ktp_provinsi` varchar(255) NOT NULL,
  `q_akte_user` enum('ya','tidak') CHARACTER SET latin1 NOT NULL COMMENT 'pertanyaan apakah user memiliki akte ? isi enum ''ya'',''tidak''',
  `akte_alasan_user` varchar(255) CHARACTER SET latin1 NOT NULL COMMENT 'jika tidak punya akte maka isi alasan user tidak memiliki akte',
  `akte_no_user` varchar(32) CHARACTER SET latin1 NOT NULL COMMENT 'jika punya akte maka isi no akte user',
  `q_d_alamat_sesuai_ktp` enum('ya','tidak') DEFAULT NULL COMMENT 'petanyaan apakah alamt domisili sesuai dengan ktp',
  `d_alamat_user_data` text NOT NULL,
  `d_kec_user_data` varchar(32) NOT NULL,
  `d_kel_user_data` varchar(32) NOT NULL,
  `d_rt_user_data` int(3) UNSIGNED ZEROFILL NOT NULL,
  `d_rw_user_data` int(3) UNSIGNED ZEROFILL NOT NULL,
  `d_kota_user_data` varchar(64) NOT NULL,
  `d_provinsi` varchar(255) NOT NULL,
  `wn_almt_user_data` varchar(255) NOT NULL COMMENT 'jika user merupakan wna maka isi almat user di negara tersebut',
  `wn_no_paspor` varchar(32) DEFAULT NULL COMMENT 'jika user merupakan wna maka isi no pasport',
  `srt_nikah_no` varchar(64) NOT NULL COMMENT 'jika status user menikan isi no surat nikah',
  `srt_akta_cerai` varchar(64) DEFAULT NULL COMMENT 'jika status user cerai hidup isi no akta cerai',
  `srt_kematian_pasangan` varchar(64) DEFAULT NULL COMMENT 'jika status user cerai mati isi no surat kematian pasangan',
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime NOT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_data`
--

INSERT INTO `user_data` (`id_user_data`, `id_user`, `ds_nama_dasawisma`, `ms_jk`, `ms_tmp_lhr`, `ms_tgl_lhr`, `ms_pd_terakhir`, `ms_sts_pernikahan`, `ms_wn`, `ms_agama`, `ms_suku`, `ms_gol_darah`, `ktp_alamat_user_data`, `ktp_kec_user_data`, `ktp_kel_user_data`, `ktp_rt_user_data`, `ktp_rw_user_data`, `ktp_kota_user_data`, `ktp_provinsi`, `q_akte_user`, `akte_alasan_user`, `akte_no_user`, `q_d_alamat_sesuai_ktp`, `d_alamat_user_data`, `d_kec_user_data`, `d_kel_user_data`, `d_rt_user_data`, `d_rw_user_data`, `d_kota_user_data`, `d_provinsi`, `wn_almt_user_data`, `wn_no_paspor`, `srt_nikah_no`, `srt_akta_cerai`, `srt_kematian_pasangan`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('USD.4.202107210000004', 4, '1', NULL, '1', '2021-08-24', '1', '1', '1', '1', '1', NULL, '1', '', '1', 001, 001, '1', '', 'ya', '1', '1', NULL, '1', '', '1', 001, 001, '1', '', '1', NULL, '1', NULL, NULL, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00'),
('USD.6.202108260000005', 6, '1', NULL, '1', '2021-08-24', '1', '1', '1', '1', '1', NULL, '1', '', '1', 001, 001, '1', '', 'ya', '1', '1', NULL, '1', '', '1', 001, 001, '1', '', '1', NULL, '1', NULL, NULL, '1', '0000-00-00 00:00:00', '1', '0000-00-00 00:00:00');

--
-- Trigger `user_data`
--
DELIMITER $$
CREATE TRIGGER `in_bf_data` BEFORE INSERT ON `user_data` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_data';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_data';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_data = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_kerja`
--

CREATE TABLE `user_kerja` (
  `id_user_kerja` varchar(64) CHARACTER SET utf8 NOT NULL,
  `id_user` int(11) NOT NULL,
  `sts_kerja` enum('bekerja','mengurus rumah tangga','sekolah','kuliah','pensiun','tidak bekerja') NOT NULL,
  `sts_kerja_ket` varchar(255) DEFAULT NULL COMMENT 'di kosongkan dulu saja',
  `jenis_kerja` varchar(255) NOT NULL,
  `tempat_kerja` text NOT NULL COMMENT 'alamat kerja',
  `bidang_pekerjaan` varchar(255) DEFAULT NULL,
  `penghasilan_perbulan` varchar(32) NOT NULL,
  `q_terima_pensiun` enum('iya','tidak') DEFAULT NULL,
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_kerja`
--

INSERT INTO `user_kerja` (`id_user_kerja`, `id_user`, `sts_kerja`, `sts_kerja_ket`, `jenis_kerja`, `tempat_kerja`, `bidang_pekerjaan`, `penghasilan_perbulan`, `q_terima_pensiun`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('KER.4.202107210000003', 4, 'tidak bekerja', NULL, 'null', '-', '-', '0', 'iya', '', NULL, '4', '2021-08-31 09:53:43'),
('KER.4.202108260000004', 4, 'tidak bekerja', NULL, 'null', '-', '-', '0', 'iya', '', NULL, '4', '2021-08-31 09:53:43');

--
-- Trigger `user_kerja`
--
DELIMITER $$
CREATE TRIGGER `ins_bf` BEFORE INSERT ON `user_kerja` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_kerja';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_kerja';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_kerja = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_kesehatan`
--

CREATE TABLE `user_kesehatan` (
  `id_user_kesehatan` varchar(64) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `bb` int(11) NOT NULL COMMENT 'berat badan',
  `tb` int(11) DEFAULT NULL COMMENT 'tinggi badan',
  `q_vaksin` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah sudah vaksin',
  `d_vaksin` text DEFAULT NULL COMMENT 'isi dengan json detail vaksin [{"dosis": 1/2/3, "jenis_vaksin": sesuai list, "tgl_vaksin": yyyy-mm-dd}]',
  `q_kebutuhan_khusus` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah berkebutuhan khusus',
  `q_butawarna` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah butawarna',
  `q_expektor_kb` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user inspektor kb ?',
  `kb_jenis` varchar(255) DEFAULT NULL COMMENT 'jenis kb sesuai list',
  `q_stunting` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user stunting',
  `stnt_lingkar_kpl` int(11) DEFAULT NULL COMMENT 'jika stunting diisi ya maka isi lingkar kepala',
  `q_ibu_hamil` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user ibu hamil',
  `hml_rutin_pemeriksaan` enum('iya','tidak') DEFAULT NULL COMMENT 'jika pertanyaan ibu hamil ya maka isi pertanyaan apakah user rutin melakukan pemeriksaan kehamilan',
  `hml_usia_kehamilan` int(11) DEFAULT NULL COMMENT 'jika pertanyaan ibu hamil ya maka isi usia kehamilan',
  `hml_tmp_periksa` varchar(255) DEFAULT NULL COMMENT 'jika pertanyaan ibu hamil ya maka isi tempat pemeriksaan kehamilan',
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_kesehatan`
--

INSERT INTO `user_kesehatan` (`id_user_kesehatan`, `id_user`, `bb`, `tb`, `q_vaksin`, `d_vaksin`, `q_kebutuhan_khusus`, `q_butawarna`, `q_expektor_kb`, `kb_jenis`, `q_stunting`, `stnt_lingkar_kpl`, `q_ibu_hamil`, `hml_rutin_pemeriksaan`, `hml_usia_kehamilan`, `hml_tmp_periksa`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('KES.4.202108310000007', 4, 12, 12, 'iya', '{}', 'iya', 'iya', 'tidak', 'Suntik KB', 'tidak', 15, 'tidak', 'tidak', 0, '-', '4', '2021-08-31 05:59:55', '4', '2021-08-31 06:28:39');

--
-- Trigger `user_kesehatan`
--
DELIMITER $$
CREATE TRIGGER `ins_kes` BEFORE INSERT ON `user_kesehatan` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_kesehatan';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_kesehatan';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_kesehatan = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_keuangan`
--

CREATE TABLE `user_keuangan` (
  `id_user_keuangan` varchar(64) NOT NULL,
  `id_user` int(11) NOT NULL,
  `q_harta_tidak_bergerak` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan harta tidak bergerak',
  `htb_detail` text NOT NULL COMMENT 'jika harta tidak bergerak ya maka isi detail degan json [{"jenis_harta":tanah, "harga_perolehan":10000 }]',
  `q_harta_bergerak` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan kepemilikan harta bergerak user',
  `hb_detail` text DEFAULT NULL COMMENT 'jika harta bergerak ya maka isi dengan json [{"jenis_harta":mobil, "jml":2000}]',
  `q_deposito` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan kepemilikan deposito',
  `q_piutang` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan kepemilikan piutang',
  `q_hutang` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan kepemilikan hutang',
  `ht_nominal` varchar(32) DEFAULT NULL COMMENT 'jika hutang ya maka isi nominal hutang',
  `t_kekayaan` varchar(32) DEFAULT NULL COMMENT 'total kekayaan user',
  `q_penghasilan_per_th` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan penghasilan jabatan pertahun',
  `q_penghasilan_profesi` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan penghasilan keahlian',
  `q_penghasilan_usaha` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan dari usaha lainnnya',
  `q_penghasilan_hibah` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan penghasilan dari hibah',
  `t_penghasilan` varchar(32) DEFAULT NULL COMMENT 'total seluruh penghasilan',
  `t_pengeluaran_rutin` varchar(32) DEFAULT NULL COMMENT 'total pengeluaran rutin',
  `t_pengeluaran_lainnya` varchar(32) DEFAULT NULL COMMENT 'total pengeluaran lain lain',
  `r_crt_by` varchar(32) DEFAULT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) DEFAULT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_keuangan`
--

INSERT INTO `user_keuangan` (`id_user_keuangan`, `id_user`, `q_harta_tidak_bergerak`, `htb_detail`, `q_harta_bergerak`, `hb_detail`, `q_deposito`, `q_piutang`, `q_hutang`, `ht_nominal`, `t_kekayaan`, `q_penghasilan_per_th`, `q_penghasilan_profesi`, `q_penghasilan_usaha`, `q_penghasilan_hibah`, `t_penghasilan`, `t_pengeluaran_rutin`, `t_pengeluaran_lainnya`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('KEU.4.202108270000020', 4, 'tidak', '{\"ada lah sip\"}', 'tidak', '{\"ada lah sip\"}', 'tidak', 'tidak', 'tidak', '20000012', '500000000012', 'tidak', 'tidak', 'tidak', 'tidak', '5000000012', '10000020012', '100000012', '4', '2021-08-27 05:37:47', '4', '2021-08-27 05:38:05');

--
-- Trigger `user_keuangan`
--
DELIMITER $$
CREATE TRIGGER `bf_ins` BEFORE INSERT ON `user_keuangan` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_keuangan';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_keuangan';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_keuangan = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_or`
--

CREATE TABLE `user_or` (
  `id_user_or` varchar(32) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `or_hobby` varchar(255) DEFAULT NULL,
  `q_keg_pengamalan_pancasila` enum('iya','tidak') DEFAULT NULL,
  `q_kerjabakti` enum('iya','tidak') DEFAULT NULL,
  `q_rukun_kematian` enum('iya','tidak') DEFAULT NULL,
  `q_keg_keagamaan` enum('iya','tidak') DEFAULT NULL,
  `q_jimpitan` enum('iya','tidak') DEFAULT NULL,
  `q_arisan` enum('iya','tidak') DEFAULT NULL,
  `q_gotong_royong` enum('iya','tidak') DEFAULT NULL,
  `q_ikut_organisasi` enum('iya','tidak') DEFAULT NULL,
  `q_ikut_pelatihan` enum('iya','tidak') DEFAULT NULL,
  `pel_detail` text DEFAULT NULL,
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_or`
--

INSERT INTO `user_or` (`id_user_or`, `id_user`, `or_hobby`, `q_keg_pengamalan_pancasila`, `q_kerjabakti`, `q_rukun_kematian`, `q_keg_keagamaan`, `q_jimpitan`, `q_arisan`, `q_gotong_royong`, `q_ikut_organisasi`, `q_ikut_pelatihan`, `pel_detail`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('OLR.4.202108270000004', 4, 'main ss', 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', '{\"sip siap mantap\"}', '4', '2021-08-27 06:31:32', '4', '2021-08-27 08:07:22');

--
-- Trigger `user_or`
--
DELIMITER $$
CREATE TRIGGER `bf_or` BEFORE INSERT ON `user_or` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_or';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_or';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_or = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_pendidikan`
--

CREATE TABLE `user_pendidikan` (
  `id_user_pendidikan` varchar(64) NOT NULL,
  `id_user` int(11) NOT NULL,
  `d_pendidikan` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `q_buta_huruf` enum('ya','tidak') NOT NULL,
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_pendidikan`
--

INSERT INTO `user_pendidikan` (`id_user_pendidikan`, `id_user`, `d_pendidikan`, `q_buta_huruf`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('PDD.4.202107210000003', 4, '[{\"jenjang_pd\":\"sd\",\"nm_sklh\":\"SD NU Blimbing Malang\",\"jurusan_pd\":\"-\",\"th_lulus\":\"2006\"},{\"jenjang_pd\":\"smp\",\"nm_sklh\":\"SMP N 1 Malang\",\"jurusan_pd\":\"-\",\"th_lulus\":\"2009\"},{\"jenjang_pd\":\"sma\",\"nm_sklh\":\"SMA N 7 Malang\",\"jurusan_pd\":\"IPS\",\"th_lulus\":\"2012\"},{\"jenjang_pd\":\"d3\",\"nm_sklh\":\"STIMATA\",\"jurusan_pd\":\"MANAJEMEN INFORMATIKA\",\"th_lulus\":\"2015\"},{\"jenjang_pd\":\"sarjana\",\"nm_sklh\":\"STIMATA\",\"jurusan_pd\":\"SISTEM INFORMASI\",\"th_lulus\":\"2017\"}]', 'tidak', '', NULL, '', NULL),
('PDD.4.202108260000004', 4, '[{\"jenjang_pd\":\"sd\",\"nm_sklh\":\"SD NU Blimbing Malang\",\"jurusan_pd\":\"-\",\"th_lulus\":\"2006\"},{\"jenjang_pd\":\"smp\",\"nm_sklh\":\"SMP N 1 Malang\",\"jurusan_pd\":\"-\",\"th_lulus\":\"2009\"},{\"jenjang_pd\":\"sma\",\"nm_sklh\":\"SMA N 7 Malang\",\"jurusan_pd\":\"IPS\",\"th_lulus\":\"2012\"},{\"jenjang_pd\":\"d3\",\"nm_sklh\":\"STIMATA\",\"jurusan_pd\":\"MANAJEMEN INFORMATIKA\",\"th_lulus\":\"2015\"},{\"jenjang_pd\":\"sarjana\",\"nm_sklh\":\"STIMATA\",\"jurusan_pd\":\"SISTEM INFORMASI\",\"th_lulus\":\"2017\"}]', 'tidak', '', NULL, '', NULL);

--
-- Trigger `user_pendidikan`
--
DELIMITER $$
CREATE TRIGGER `in_bf_datas` BEFORE INSERT ON `user_pendidikan` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_pendidikan';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_pendidikan';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_pendidikan = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sosial`
--

CREATE TABLE `user_sosial` (
  `id_user_sosial` varchar(64) NOT NULL,
  `id_user` int(11) NOT NULL,
  `q_anggota_dasawisma` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan apakah user anggota dasawisma',
  `dw_nama` varchar(64) DEFAULT NULL COMMENT 'jika user anggota dasawisma maka input nama dasawisma',
  `q_activitas_pyd` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan pakah user aktif posyandu',
  `pyd_frequensi` int(11) DEFAULT NULL COMMENT 'frekuensi user ke posyandu',
  `q_program_bina_balita` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user mengikuti program bina keluarga balita',
  `q_ikut_kel_belajar` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan pakah user mengikuti kelompok belajar',
  `q_paud` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user mengikuti paud',
  `q_ikut_kooperasi` enum('iya','tidak') DEFAULT NULL COMMENT 'petanyaan apakah user mengikuti kooperasi',
  `q_punya_tabungan` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan user memiliki tabungan',
  `q_makan_pokok` enum('0','1') DEFAULT NULL COMMENT 'pertanyaan makanan pokok (0 =beras dan 1 = non-beras)',
  `q_punya_usaha` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah punya usaha',
  `usaha_skala` varchar(64) DEFAULT NULL COMMENT 'jika punya usaha maka isi skala usaha sesuai jenis',
  `usaha_no` varchar(64) DEFAULT NULL COMMENT 'jika punya usaha maka isi no usaha',
  `usaha_bidang` varchar(64) DEFAULT NULL COMMENT 'jika punya usaha maka isi bidang usaha sesuai list',
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime NOT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_sosial`
--

INSERT INTO `user_sosial` (`id_user_sosial`, `id_user`, `q_anggota_dasawisma`, `dw_nama`, `q_activitas_pyd`, `pyd_frequensi`, `q_program_bina_balita`, `q_ikut_kel_belajar`, `q_paud`, `q_ikut_kooperasi`, `q_punya_tabungan`, `q_makan_pokok`, `q_punya_usaha`, `usaha_skala`, `usaha_no`, `usaha_bidang`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('SOS.4.202108270000007', 4, 'tidak', 'apa dongtidak', 'tidak', 4, 'tidak', 'tidak', 'tidak', 'tidak', 'tidak', '', 'tidak', 'besar tidak', '11', 'sehat tidak', '4', '2021-08-27 08:24:00', '4', '2021-08-27 08:25:09');

--
-- Trigger `user_sosial`
--
DELIMITER $$
CREATE TRIGGER `ins_bf_sos` BEFORE INSERT ON `user_sosial` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_sosial';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_sosial';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_sosial = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_tmpt`
--

CREATE TABLE `user_tmpt` (
  `id_user_tmpt` varchar(64) NOT NULL,
  `id_user` int(11) DEFAULT NULL,
  `q_punya_rumah` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user memiliki rumah',
  `py_rmh_sts` varchar(255) DEFAULT NULL COMMENT 'jika q punya rmh status rumah (sesuai list)',
  `py_rmh_kondisi` varchar(255) DEFAULT NULL COMMENT 'jika q punya rmh kondisi bangunan rumah (sesuai list)',
  `py_rmh_jn_lantai` varchar(255) DEFAULT NULL COMMENT 'jika q punya rmh jenis lantai rumah isi sesuai list',
  `py_rmh_kriteria_rmh` enum('0','1') DEFAULT NULL COMMENT 'kriteria rumah 0 = sehat, 1 = kurang sehat',
  `q_ada_listrik` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan apakah user memasang listrik di rumahnya',
  `lstr_jenis` varchar(255) DEFAULT NULL COMMENT 'jika ada listrik maka isi jenis listrik (isi sesuai dengan list)',
  `q_ada_sanitasi` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan ketersediaan sanitasi di rumah user',
  `sn_jenis` varchar(255) DEFAULT NULL COMMENT 'jika sanitasi ada maka isi jenis sanitasi',
  `q_saluran_limbah` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan ketersediaan saluran limbah user',
  `q_air_bersih` enum('iya','tidak') DEFAULT NULL COMMENT 'pertanyaan ketersediaan air bersih user',
  `ab_sumber_air` varchar(255) DEFAULT NULL COMMENT 'jika air bersih tersedia isi sumber air',
  `q_pembuangan_sampah` enum('iya','tidak') DEFAULT NULL,
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime NOT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_tmpt`
--

INSERT INTO `user_tmpt` (`id_user_tmpt`, `id_user`, `q_punya_rumah`, `py_rmh_sts`, `py_rmh_kondisi`, `py_rmh_jn_lantai`, `py_rmh_kriteria_rmh`, `q_ada_listrik`, `lstr_jenis`, `q_ada_sanitasi`, `sn_jenis`, `q_saluran_limbah`, `q_air_bersih`, `ab_sumber_air`, `q_pembuangan_sampah`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('TMP.4.202108270000004', 4, 'tidak', 'ada tidak', 'ada tidak', 'ada tidak ', '', 'tidak', 'ada tidak', 'tidak', 'ada tidak', 'tidak', 'tidak', 'tidak', 'tidak', '4', '2021-08-27 08:58:35', '4', '2021-08-27 09:00:25');

--
-- Trigger `user_tmpt`
--
DELIMITER $$
CREATE TRIGGER `bf_ins_tmpt` BEFORE INSERT ON `user_tmpt` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_tmpt';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_tmpt';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_tmpt = fix_key_user;
END
$$
DELIMITER ;

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_transport`
--

CREATE TABLE `user_transport` (
  `id_user_transport` varchar(32) NOT NULL,
  `id_user` int(11) NOT NULL,
  `q_punya_tr` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan kepemilikan transportasi user',
  `tr_detail` text NOT NULL COMMENT 'jika memiliki kendaraan isi detail dengan json {"jenis_tr":"sepeda", "jlm":2, "sts_kepemilikan":punya sendiri/pinjam}',
  `q_akses_internet` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan kepemilikian akses internet',
  `jn_internet_akses` int(11) UNSIGNED NOT NULL,
  `q_punya_perangkat` enum('iya','tidak') NOT NULL COMMENT 'pertanyaan kepemilikan peralatan komputer',
  `prng_detail` text NOT NULL COMMENT 'jika memiliki perangkat maka isi detail perangkat dengan json {"jenis_perangkat":komputer, "jml":6} ',
  `r_crt_by` varchar(32) NOT NULL,
  `r_crt_time` datetime DEFAULT NULL,
  `r_up_by` varchar(32) NOT NULL,
  `r_up_time` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_transport`
--

INSERT INTO `user_transport` (`id_user_transport`, `id_user`, `q_punya_tr`, `tr_detail`, `q_akses_internet`, `jn_internet_akses`, `q_punya_perangkat`, `prng_detail`, `r_crt_by`, `r_crt_time`, `r_up_by`, `r_up_time`) VALUES
('TRN.4.202108270000004', 4, 'iya', '{\"siap\"}', 'iya', 0, 'iya', '{\"mantap\"}', '4', '2021-08-27 09:09:53', '4', '2021-08-27 09:10:57');

--
-- Trigger `user_transport`
--
DELIMITER $$
CREATE TRIGGER `bf_tr` BEFORE INSERT ON `user_transport` FOR EACH ROW BEGIN
    DECLARE rowcount INT;
    DECLARE rowcount_next INT;
    DECLARE kode_in VARCHAR(10);
    DECLARE fix_key_user VARCHAR(32);
		
    DECLARE inp_id_user INT;
    
		SET inp_id_user = NEW.id_user;
		
		SELECT id_index, kode
    INTO rowcount, kode_in
    FROM index_number where id = 'user_transport';
     
		SEt rowcount_next = rowcount + 1;
 
		UPDATE index_number SET id_index=rowcount_next WHERE id = 'user_transport';
    
    SET fix_key_user = concat(kode_in, ".", inp_id_user, ".",left(NOW()+0, 8),"",LPAD(rowcount_next, 7, '0'));
    
    SET NEW.id_user_transport = fix_key_user;
END
$$
DELIMITER ;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id_admin`) USING BTREE;

--
-- Indeks untuk tabel `index_number`
--
ALTER TABLE `index_number`
  ADD PRIMARY KEY (`id`) USING BTREE,
  ADD UNIQUE KEY `kd` (`kode`) USING BTREE;

--
-- Indeks untuk tabel `ms_distrik_pem`
--
ALTER TABLE `ms_distrik_pem`
  ADD PRIMARY KEY (`id_distrik_pem`) USING BTREE;

--
-- Indeks untuk tabel `ms_jenis_all`
--
ALTER TABLE `ms_jenis_all`
  ADD PRIMARY KEY (`id_ms_jenis_all`);

--
-- Indeks untuk tabel `ms_jn_pekerjaan`
--
ALTER TABLE `ms_jn_pekerjaan`
  ADD PRIMARY KEY (`id_ms_jn_pekerjaan`) USING BTREE;

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_data`
--
ALTER TABLE `user_data`
  ADD PRIMARY KEY (`id_user_data`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_kerja`
--
ALTER TABLE `user_kerja`
  ADD PRIMARY KEY (`id_user_kerja`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_kesehatan`
--
ALTER TABLE `user_kesehatan`
  ADD PRIMARY KEY (`id_user_kesehatan`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_keuangan`
--
ALTER TABLE `user_keuangan`
  ADD PRIMARY KEY (`id_user_keuangan`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_or`
--
ALTER TABLE `user_or`
  ADD PRIMARY KEY (`id_user_or`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_pendidikan`
--
ALTER TABLE `user_pendidikan`
  ADD PRIMARY KEY (`id_user_pendidikan`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_sosial`
--
ALTER TABLE `user_sosial`
  ADD PRIMARY KEY (`id_user_sosial`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- Indeks untuk tabel `user_tmpt`
--
ALTER TABLE `user_tmpt`
  ADD PRIMARY KEY (`id_user_tmpt`),
  ADD KEY `id_user` (`id_user`);

--
-- Indeks untuk tabel `user_transport`
--
ALTER TABLE `user_transport`
  ADD PRIMARY KEY (`id_user_transport`) USING BTREE,
  ADD KEY `id_user` (`id_user`) USING BTREE;

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `ms_jenis_all`
--
ALTER TABLE `ms_jenis_all`
  MODIFY `id_ms_jenis_all` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT untuk tabel `ms_jn_pekerjaan`
--
ALTER TABLE `ms_jn_pekerjaan`
  MODIFY `id_ms_jn_pekerjaan` int(5) UNSIGNED ZEROFILL NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `user_data`
--
ALTER TABLE `user_data`
  ADD CONSTRAINT `user_data_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_kerja`
--
ALTER TABLE `user_kerja`
  ADD CONSTRAINT `user_kerja_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_kesehatan`
--
ALTER TABLE `user_kesehatan`
  ADD CONSTRAINT `user_kesehatan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_keuangan`
--
ALTER TABLE `user_keuangan`
  ADD CONSTRAINT `user_keuangan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_or`
--
ALTER TABLE `user_or`
  ADD CONSTRAINT `user_or_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_pendidikan`
--
ALTER TABLE `user_pendidikan`
  ADD CONSTRAINT `user_pendidikan_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_sosial`
--
ALTER TABLE `user_sosial`
  ADD CONSTRAINT `user_sosial_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_tmpt`
--
ALTER TABLE `user_tmpt`
  ADD CONSTRAINT `user_tmpt_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;

--
-- Ketidakleluasaan untuk tabel `user_transport`
--
ALTER TABLE `user_transport`
  ADD CONSTRAINT `user_transport_ibfk_1` FOREIGN KEY (`id_user`) REFERENCES `user` (`id_user`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
